<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?> 
<h1 style="text-align: left;">Способы оплаты</h1>

<h1 style="text-align: center;">&laquo;Оплата наличными&raquo; при получении</h1>
 
<div> 
  <br />
 </div>
 
<div> 
  <p style="margin: 0px 0px 18px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Если вы хотите оплатить ваш заказ наличными, выберите способ оплаты «Наличными» при оформлении заказа.В случае если вы решили воспользоваться услугами курьера, постарайтесь приготовить сумму равную стоимости заказа (во избежание непредвиденных ситуаций).Также вы можете оплатить заказ<span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"> наличными частично</span>. Например, вы можете внести часть средств банковской картой, часть &ndash; используя сервис «Яндекс.Деньги» и оставшуюся часть – наличными при получении заказа. Ограничения на количество способов оплаты в одном заказе нет.</p>
 
  <p style="margin: 0px 0px 18px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Внимание:</span><font face="Arial, Helvetica, sans-serif" size="2"> </font>возврат денежных средств за товар, который вы вернули в магазин, возможен только наличными, если вы оформляли возврат в нашем официальном пункте выдачи. При возврате товара в партнерский пункт выдачи, возврат денежных средств производится только безналичным способом на расчетный счет, который вы укажете в акте на возврат.</p>

 
  <h1 style="text-align: center; margin: 0px 0px 18px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Банковская карта VISA или  MasterCard</h1>
 <dl style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"> 
    <p style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">Оплатить заказ банковской картой можно при получении заказа в пункте выдачи или при доставке курьером. Для этого при оформлении заказа необходимо выбрать способ оплаты «Картой Visa или MasterCard при получении».В настоящий момент мы принимаем платежи с любых карт Visa или MasterCard, кроме карт American Express, Diners Club и STB Card.</p>
   <dt style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"></dt></dl> 
  <p style="margin: 0px 0px 18px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"> 
    <br />
   </p>
 

 
  <p></p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>