<?
if (!IS_AJAX)
{
?>
    <? if($APPLICATION->GetCurPage(false)!==SITE_DIR):?>
        </div></div>
    <? endif;?>
</main>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-2 logo-block">
				<?if ($_SERVER['REQUEST_URI'] != '/'){?><a href="/" class="logo-link"><?}else{?><span class="logo-link"><?}?>
					<img src="<?=LOGOTIP_FILE?>" alt="Alexvit" />
				<?if ($_SERVER['REQUEST_URI'] != '/'){?></a><?}else{?></span><?}?>
            </div>
            <div class="col-xs-4 copyright-block">
                <small>
				<?if($_SERVER['REQUEST_URI'] == '/'){?>
					&copy; Copyright. 2014-<?=date('Y')?> г. ООО «АлексВит» — напольные и настенные покрытия в Москве
				<?}else{?>
					&copy; Copyright. 2014-<?=date('Y')?> г. ООО «АлексВит» — <?php include($_SERVER['DOCUMENT_ROOT'].'/copyrights/give_me_copyright.php'); ?>
				<?}?>
				<br>
                    Все права защищены. Копирование материалов<br>
                    с сайта без разрешения правооблодателя<br>
                    запрещено законом.</small>
            </div>
            <div class="col-xs-2 col-xs-offset-1 adress-block">
                <p><?=FOOTER_ADDRESS_TEXT?></p>
            </div>
            <div class="col-xs-2 col-xs-offset-1 contacts-block">
                <a class="phone ya-phone" href="tel:<?=PHONE_1_URL_TEXT?>"><?=PHONE_1_TEXT?></a><br>
                <a class="mail" href="mailto:info@alexvit.ru">info@alexvit.ru</a>
                <p class="created-by">Создание сайта <a href="https://ameton.ru/" target="_blank">Ameton</a></p>
            </div>
         </div>
    </div>
    <a href="#" class="customFormLink footerQuestionLink"
        data-params="YNYYN-NNYNN"
        data-form-code="shapka_zvonok"
        data-title="Введите ваши данные, мы перезвоним"
        data-text="Заполните форму заявки и мы дадим Вам скидку и мы продадим вам по цене конкурентов."
        data-btn-text="Отправить"
        data-service-form-name=""
        data-field-title-phone=""
        data-field-title-name=""
        data-field-placeholder-name="Представьтесь пожалуйста"
        data-field-placeholder-phone="Ваш контактный телефон"
        data-custom-textarea-title=""
        data-custom-textarea-placeholder="Удобное время звонка"
		data-metrika-counter="43821664"
		data-metrika-target-success="RECALL"
		data-metrika-target-try="RECALL"
		data-ga-target-success="otpravit"
		data-ga-target-try="otpravit"
        >
        <span class="footerQuestionLink-icon"></span>
        <span class="footerQuestionLink-inner"></span>
    </a>
</footer>
</div>
<div class="search_window">
    <div class="search_window_table">
        <div class="search_window_table_cell">
            <div class="title">Поиск по сайту <span class="close"></span></div>
            <form method="get" action="/catalog.php">
                <input type="text" class="form-control" value="<?=trim(htmlspecialchars($_GET["q"]));?>" name="q" placeholder="Введите поисковый запрос, например: клей А124" />
                <div class="text-right">
                    <button type="submit" class="btn">Найти <i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--[if lt IE 9]><script data-skip-moving="true" src="<?=SITE_TEMPLATE_PATH?>/js/html5shiv.js"></script><script data-skip-moving="true" src="<?=SITE_TEMPLATE_PATH?>/js/respond.min.js"></script><![endif]-->
<!--[if IE]><script data-skip-moving="true" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script data-skip-moving="true">var IS_GADGET=<?php echo IS_GADGET;?>, IS_PHONE=<?php echo IS_PHONE;?>, IS_TABLET=<?php echo IS_TABLET;?>;</script>
<script data-skip-moving="true" type="text/javascript" src="/minify_bx/?t=<?=$tpl_name?>&amp;g=js&amp;v=8"></script>
<?
if (!IS_GADGET)
{
    ?><link type="text/css" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/pc.css" property='stylesheet' /><?
}
if (IS_TABLET)
{
    ?><link type="text/css" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/tablet.css" property='stylesheet' /><?
}
if (IS_PHONE)
{
    ?><link type="text/css" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/mobile.css?v=1" property='stylesheet' /><?
}
?>
<script>
    var success_request_message="<?=SUCCESS_REQUEST_TITLE_TEXT?>";
    var index_slide_speed=<?=INDEX_SLIDE_SPEED_TEXT?>;
</script>

<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96391888-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter43821664 = new Ya.Metrika({ id:43821664, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/43821664" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

<!--LiveInternet counter--><script type="text/javascript">
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank class='live'><img src='//counter.yadro.ru/hit?t50.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
</script><!--/LiveInternet-->

<!-- Yandex.Metrika counter --> 
<script type="text/javascript" > 
   (function (d, w, c) { 
       (w[c] = w[c] || []).push(function() { 
           try { 
               w.yaCounter45445452 = new Ya.Metrika({ 
                   id:45445452, 
                   clickmap:true, 
                   trackLinks:true, 
                   accurateTrackBounce:true, 
                   webvisor:true 
               }); 
           } catch(e) { } 
       }); 

       var n = d.getElementsByTagName("script")[0], 
           s = d.createElement("script"), 
           f = function () { n.parentNode.insertBefore(s, n); }; 
       s.type = "text/javascript"; 
       s.async = true; 
       s.src = "https://mc.yandex.ru/metrika/watch.js"; 

       if (w.opera == "[object Opera]") { 
           d.addEventListener("DOMContentLoaded", f, false); 
       } else { f(); } 
   })(document, window, "yandex_metrika_callbacks"); 
</script> 
<noscript><div><img src="https://mc.yandex.ru/watch/45445452"; style="position:absolute; left:-9999px;" alt="" /></div></noscript> 
<!-- /Yandex.Metrika counter --> 
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47600086 = new Ya.Metrika({
                    id:47600086,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>

<noscript><div><img src="https://mc.yandex.ru/watch/47600086" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
<?
}
?>