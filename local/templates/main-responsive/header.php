<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!IS_AJAX)
{
global $USER;
$tpl_name=basename(__DIR__);
$isShowH1=true;
$isShowMeta=true;
$curDir=$APPLICATION->GetCurDir();
if ($curDir=='/proekty/')
{
    $isShowH1=false;
}
if ($_SERVER["REAL_FILE_PATH"]=="/catalog.php" || $_SERVER["PHP_SELF"]=="/catalog.php")
{
    $isShowH1=false;
}
if((strpos($_SERVER['REQUEST_URI'],'brand-') || strpos($_SERVER['REQUEST_URI'],'vid-') || strpos($_SERVER['REQUEST_URI'],'kraski-') || strpos($_SERVER['REQUEST_URI'],'type-')) 
&& !strpos($_SERVER['REQUEST_URI'],'kollektsiya-') && !strpos($_SERVER['REQUEST_URI'],'.html')){
	$title = ' - ООО «АлексВит»';
	$isShowMeta=false;
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?if($_SERVER["REQUEST_URI"]=="/brand-armstrong/"){echo '<title>Напольные покрытия Armstrong купить оптом в Москве - «АлексВит»</title><meta name="description" content="Продажа напольных покрытий Armstrong оптом по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="напольные покрытия armstrong"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/brand-barlinek/"){echo '<title>Напольные покрытия Barlinek купить оптом в Москве - «АлексВит»</title><meta name="description" content="Продажа напольных покрытий Barlinek оптом по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="напольные покрытия barlinek"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/brand-grabo/"){echo '<title>Напольные покрытия Grabo оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа настенных покрытий Grabo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="напольные покрытия grabo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/brand-modulyss/"){echo '<title>Напольные покрытия Modulyss купить оптом в Москве - «АлексВит»</title><meta name="description" content="Продажа напольных покрытий Modulyss оптом по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="напольные покрытия modulyss"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/brand-vertigo-trend/"){echo '<title>Напольные покрытия VERTIGO Trend купить оптом в Москве - «АлексВит»</title><meta name="description" content="Продажа напольных покрытий VERTIGO Trend оптом по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="напольные покрытия vertigo trend"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/dizayn-plitka-pvkh/brand-forbo/"){echo '<title>Дизайн плитка ПВХ Forbo оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа дизайн плитки ПВХ Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="дизайн плитка пвх  Forbo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/falshpol/brand-perfaten/"){echo '<title>Фальшполы Perfaten оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа фальшполов Perfaten по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="фальшполы perfaten"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/"){echo '<title>Флокированное покрытие Forbo</title><meta name="description" content="Оптовая продажа флокированного покрытия Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="флокированное покрытие Forbo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/igloprobivnoy-kovrolin/brand-forbo/"){echo '<title>Иглопробивной ковролин Forbo</title><meta name="description" content="Оптовая продажа иглопробивного ковролина Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="иглопробивной ковролин Forbo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/"){$h1="Керамогранит ColiseumGres"; echo '<title>Керамогранит ColiseumGres оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита ColiseumGres по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит coliseumgres"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/"){$h1="Керамогранит Estima"; echo '<title>Керамогранит Estima оптом в Москве оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита Estima по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит estima"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=derevo"){$h1="Керамогранит Estima под дерево"; echo '<title>Керамогранит Estima под дерево оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита под камень Estima по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит estima дерево"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=dizayn"){$h1="Керамогранит Estima серия Дизайн"; echo '<title>Керамогранит Estima серии Дизайн оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита Estima серии Дизайн по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит estima дизайн"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=kamen"){$h1="Керамогранит Estima под камень"; echo '<title>Керамогранит Estima под камень оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита под камень Estima по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит estima камень"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/"){$h1="Керамогранит Kerama Marazzi"; echo '<title>Керамогранит Kerama Marazzi оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита Kerama Marazzi по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит kerama marazzi"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=derevo"){$h1="Керамогранит Kerama Marazzi под дерево"; echo '<title>Керамогранит Kerama Marazzi под дерево оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита под дерево Kerama Marazzi по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит kerama marazzi дерево"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=kamen"){$h1="Керамогранит Kerama Marazzi под камень"; echo '<title>Керамогранит Kerama Marazzi под камень оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита под камень Kerama Marazzi по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит kerama marazzi камень"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-armstrong/"){$h1="Коммерческий линолеум Armstrong"; echo '<title>Коммерческий линолеум Armstrong оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа коммерческого линолеума Armstrong по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="коммерческий линолеум Armstrong"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-armstrong/?vid=gomogennyy"){echo '<title>Гомогенный линолеум Armstrong оптом в Москве - ООО «АлексВит»</title><meta name="description" content="Оптовая продажа гомогенного линолеума Armstrong по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="гомогенный линолеум Armstrong"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/"){$h1="Коммерческий линолеум Forbo"; echo '<title>Коммерческий линолеум Forbo оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа коммерческого линолеума Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="коммерческий линолеум Forbo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-grabo/"){$h1="Коммерческий линолеум Grabo"; echo '<title>Коммерческий линолеум Grabo оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа коммерческого линолеума Grabo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="коммерческий линолеум Grabo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-grabo/?vid=polukummercheskiy"){$h1="Полукоммерческий линолеум Grabo"; echo '<title>Полукоммерческий линолеум Grabo оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа полукоммерческого линолеума Grabo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="полукоммерческий линолеум Grabo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-polyflor/"){echo '<title>Коммерческий линолеум Polyflor оптом в Москве - ООО «АлексВит»</title><meta name="description" content="Оптовая продажа коммерческого линолеума Polyflor по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="коммерческий линолеум Polyflor"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-escom/"){$h1="Ковровая плитка Escom"; echo '<title>Ковровая плитка Escom оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа ковровой плитки Escom по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="ковровая плитка Escom"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-forbo/"){$h1="Ковровая плитка Forbo"; echo '<title>Ковровая плитка Forbo оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа ковровой плитки Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="ковровая плитка Forbo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-modulyss/"){$h1="Ковровая плитка Modulyss"; echo '<title>Ковровая плитка Modulyss оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа ковровой плитки Modulyss по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="ковровая плитка Modulyss"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/"){$h1="Натуральный линолеум Forbo"; echo '<title>Натуральный линолеум Forbo оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа натурального линолеума Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="натуральный линолеум Forbo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=marmoleum-marbled"){$h1="Натуральный линолеум Forbo Marmoleum Marbled"; echo '<title>Натуральный линолеум Forbo Marmoleum Marbled</title><meta name="description" content="Оптовая продажа натурального линолеума Forbo Marmoleum Marbled по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="натуральный линолеум Forbo MMarmoleum Marbled"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/parketnaya-doska/brand-barlinek/"){$h1="Паркетная доска Barlinek"; echo '<title>Паркетная доска Barlinek оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа паркетной доски Barlinek по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="паркетная доска Barlinek"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/parketnaya-doska/brand-grabo/"){$h1="Паркетная доска — Grabo"; echo '<title>Паркетная доска Grabo оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа паркетной доски Grabo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="паркетная доска Grabo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/sportivnye-pokrytiya/brand-forbo/"){$h1="Спортивные покрытия — Forbo"; echo '<title>Спортивные покрытия Forbo</title><meta name="description" content="Оптовая продажа спортивных покрытий Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="спортивное покрытие Forbo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/stsenicheskie-pokrytiya/brand-grabo/"){echo '<title>Сценические покрытия Grabo оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа сценических покрытий Grabo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="сценическое покрытие Grabo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/nastennye-pokrytiya/keramicheskaya-plitka/brand-kerama-marazzi/"){$h1="Керамическая плитка Kerama Marazzi"; echo '<title>Керамическая плитка Kerama Marazzi оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамической плитки Kerama Marazzi по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамическая плитка kerama marazzi"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/nastennye-pokrytiya/keramicheskaya-plitka/brand-sokol/"){$h1="Керамическая плитка Сокол"; echo '<title>Керамическая плитка Сокол оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамической плитки Сокол по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамическая плитка сокол"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/nastennye-pokrytiya/lakokrasochnye-materialy/kraski-lacos/"){echo '<title>Лакокрасочные материалы Lacos оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа лаков и красок Lacos по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="краски lacos"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/"){echo '<title>Модульные потолки Ecophon: подвесные и аккустические оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа модульных потолков Ecophon по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="подвесной потолок ecophon, акустические потолки ecophon"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/?group_collection=advantage"){echo '<title>Модульные потолки Ecophon коллекции Advantage оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа модульных потолков Ecophon коллекции Advantage по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="модульные потолки ecophon advantage"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/?group_collection=focus"){echo '<title>Модульные потолки Ecophon коллекции Focus оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа модульных потолков Ecophon коллекции Focus по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="потолок ecophon focus"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/?group_collection=hygiene"){echo '<title>Модульные потолки Ecophon коллекции Hygiene оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа модульных потолков Ecophon коллекции Hygiene по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="модульные потолки ecophon hygiene"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/potolki/svobodno-visyashchie-elementy-i-baffly/brand-ecophon/"){echo '<title>Баффлы Ecophon оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа баффлов Ecophon по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="баффлы ecophon"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/dizayn-plitka-pvkh/brand-grabo/"){echo '<title>Дизайн плитка ПВХ Grabo оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа дизайн плитки ПВХ Grabo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="Дизайн плитка ПВХ Grabo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/sportivnye-pokrytiya/brand-grabosport/"){echo '<title>Спортивные покрытия GraboSport оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа спортивных покрытий Grabosport по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="спортивные покрытия GraboSport"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/dizayn-plitka-pvkh/brand-vertigo-trend/"){echo '<title>Дизайн плитка ПВХ VERTIGO Trend оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа дизайн плитка ПВХ VERTIGO Trend по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="дизайн плитка ПВХ VERTIGO Trend"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-armstrong/?vid=protivoskolzyashchiy"){echo '<title>Противоскользящий линолеум Armstrong оптом в Москве - ООО «АлексВит»</title><meta name="description" content="Оптовая продажа противоскользящего линолеума Armstrong по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="противоскользящий линолеум Armstrong"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/?vid=beton"){echo '<title>Керамогранит ColiseumGres Бетон оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита ColiseumGres Бетон по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="Керамогранит ColiseumGres Бетон"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/?vid=derevo"){echo '<title>Керамогранит ColiseumGres Дерево оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита ColiseumGres Дерево по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="Керамогранит ColiseumGres Дерево"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/?vid=kamen"){echo '<title>Керамогранит ColiseumGres Камень оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита ColiseumGres Камень по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="Керамогранит ColiseumGres Камень"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/?vid=mramor"){echo '<title>Керамогранит ColiseumGres Мрамор оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита ColiseumGres Мрамор по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="Керамогранит ColiseumGres Мрамор"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-escom/?vid=petlevoy-vors"){echo '<title>Ковровая плитка Escom с петлевым ворсом в Москве оптом - ООО «АлексВит»</title><meta name="description" content="Оптовая продажа ковровой плитки Escom с петлевым ворсом по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="ковровая плитка Escom с петлевым ворсом"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=beton"){echo '<title>Керамогранит Estima Бетон оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита Estima Бетон по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит estima бетон"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=mramor"){echo '<title>Керамогранит Estima под мрамор оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита Estima под мрамор по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит estima под мрамор"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=tekhnika"){echo '<title>Керамогранит Estima Техника оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита Estima серии Техника по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит estima техника"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-grabo/?vid=gomogennyy"){echo '<title>Гомогенный линолеум Grabo оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа гомогенного линолеума Grabo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="гомогенный линолеум Grabo"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=beton"){echo '<title>Керамогранит Kerama Marazzi Бетон оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита Kerama Marazzi Бетон по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит kerama marazzi бетон"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=dizayn"){echo '<title>Керамогранит Kerama Marazzi Дизайн оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита Kerama Marazzi Дизайн по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит kerama marazzi дизайн"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=mramor"){echo '<title>Керамогранит Kerama Marazzi Мрамор оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа керамогранита Kerama Marazzi Мрамор по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="керамогранит kerama marazzi мрамор"/>';
	}elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-polyflor/?vid=gomogennyy"){echo '<title>Гомогенный линолеум Polyflor оптом в Москве - «АлексВит»</title><meta name="description" content="Оптовая продажа гомогенного линолеума Polyflor по доступным ценам. Доставка заказов на объекты по Москве и Московской области."/><meta name="keywords" content="гомогенный линолеум Polyflor"/>';
	
	}elseif(strpos($_SERVER['REQUEST_URI'],'brand-forbo')) {?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<?$APPLICATION->ShowMeta("description")?>
		<?$APPLICATION->ShowMeta("keywords")?>
	<?}else{?>
		<title><?$APPLICATION->ShowTitle(); echo $title;?></title>
		<?if($isShowMeta){?><?$APPLICATION->ShowMeta("description")?>
		<?$APPLICATION->ShowMeta("keywords")?><?}?>
	<?}?>
    <link rel="stylesheet" type="text/css" href="/minify_bx/?t=<?=$tpl_name?>&amp;g=css&amp;v=28"/>
    <link href="//fonts.googleapis.com/css?family=Fira+Sans:300,400,500&amp;subset=cyrillic" rel="stylesheet">
    <?$APPLICATION->ShowCSS()?>
    <?if (IS_GADGET) {?><style>* {cursor:pointer}</style><?}?>
    <?
    //$APPLICATION->ShowHeadStrings();
    //$APPLICATION->ShowHeadScripts();
    $APPLICATION->ShowHead()
    ?>
    
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/favicon/manifest.json">
    <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/favicon/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="Alexvit">
    <meta name="application-name" content="Alexvit">
    <meta name="msapplication-config" content="/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
	
<meta name="yandex-verification" content="10c8c0e8cd0ee008" />
<meta name="google-site-verification" content="5PWskfPm6sS0ZY_ASsOMC91jjPlnEtk5obIFMWD3Dv4" />	
</head>
<body>
    <?$APPLICATION->ShowPanel();?>
    <div class="page-wrapper">
    <header id="header" class="site-header">
        <div class="container">
            <div class="row">
                <div class="col-vmid col-xs-2 site-logo__wrap">
                    <?if ($_SERVER['REQUEST_URI'] != '/'){?><a href="/" class="logo-link"><?}else{?><span class="logo-link"><?}?>
                        <img src="<?=LOGOTIP_FILE?>" alt="ООО «АлексВит» поставка напольных покрытий и потолков" />
                    <?if ($_SERVER['REQUEST_URI'] != '/'){?></a><?}else{?></span><?}?>
                </div><!--
                --><div class="col-vmid col-xs-8">
                    <nav>
                        <?$APPLICATION->IncludeComponent('bitrix:menu', "top", array(
            				"ROOT_MENU_TYPE" => "top",
            				"MENU_CACHE_TYPE" => "A",
            				"MENU_CACHE_TIME" => "36000000",
            				"MENU_CACHE_USE_GROUPS" => "N",
            				"MENU_CACHE_GET_VARS" => array(),
            				"MAX_LEVEL" => "2",
            				"USE_EXT" => "Y",
            				"ALLOW_MULTI_SELECT" => "N"
             			));?>
                   </nav> 
                </div><!--
                --><div class="col-vmid col-xs-2">
                    <?/*<a class="search-bar" href="javascript:void(0);">Поиск</a>*/?>
                    <?/*<span class="h-phone hide"><span class="ya-phone">+7 (499) 653-89-66</span></span>*/?>
                    <a class="search-bar min" href="javascript:void(0);"></a>
                    <span class="h-phone"><span class="ya-phone">+7 (499) 653-89-66</span></span>
                </div>
            </div>
        </div>
    </header>
    <main class="main">
        <!--[if lte IE 9]><div class="container">Вы используете устаревший браузер.<br />Для корректной работы сайта загрузите и установите один из этих браузеров:<br /><br /><a href="https://www.google.com/chrome/" target="_blank" rel="nofollow">Google Chrome</a>, <a href="http://www.opera.com/" target="_blank" rel="nofollow">Opera</a>, <a href="http://www.mozilla.org/ru/firefox/new/" target="_blank" rel="nofollow">Mozilla Firefox</a></div><![endif]-->
        <? if($APPLICATION->GetCurPage(false)!==SITE_DIR):?>
            <div class="container inner-pages">
            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "breadcrumbs",
                Array(),
                false
            );?>
            <?
            if ($isShowH1)
            {
                ?><h1><?$APPLICATION->ShowTitle(false);?></h1><?    
            }
            ?>            
            <div class="content">
        <? endif;?>
        
        <?
}?>