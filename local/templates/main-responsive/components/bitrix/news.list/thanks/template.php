<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (count($arResult["ITEMS"])>0)
{
	foreach($arResult["ITEMS"] as $key=>$arItem)
    {
		$img = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>2560, 'height'=>9999), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        if (!empty($img))
        {
        ?>
			<div class="slide-item">
				<div class="img">
					<img src="<?=$img["src"]?>" alt="<?=$arItem["NAME"]?>"/>
				</div>
				<div class="info">
					<p class="title"><?=$arItem["NAME"]?></p>
					<p class="date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></p>
					<div class="intro"><?=$arItem["PREVIEW_TEXT"]?></div>
					<p class="more"><a href="#">Подробнее...</a></p>
				</div>
			</div><? 
		}	
	}
}
?>