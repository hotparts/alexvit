<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (count($arResult["ITEMS"])>0)
{
    ?>
    <div class="main-categories">
        <div class="container">
            <p class="title">Подбор по типу помещений</p><ul>
                <?
                foreach($arResult["ITEMS"] as $key=>$arItem)
                {
                    $img = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width'=>93, 'height'=>70), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                    ?><li>
                        <a style="background-image: url(<?=$img["src"]?>);" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><span class="cat-name"><?=$arItem["NAME"]?></span></a>
                        <? /* <?if ($_GET['dev'] == 'y'){ ?><div class="main-categories__tool-tip"><?=$arItem["NAME"]?></div><? } ?> */ ?>
                    </li><?      
                }
                ?>
            </ul>
        </div>    
    </div>
    <?
}
?>