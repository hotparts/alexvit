<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (count($arResult["ITEMS"])>0)
{
    ?>
		<?
		foreach($arResult["ITEMS"] as $key=>$arItem)
		{
			$img = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width'=>162, 'height'=>120), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			?>
			<a class="logo-item" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
				<img src="<?=$img["src"]?>" alt="">
			</a>
			<?      
		}
		?>
    <?
}
?>