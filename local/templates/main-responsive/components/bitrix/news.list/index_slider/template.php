<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (count($arResult["ITEMS"])>0)
{
    ?><div class="islider my-slider" id="index_slider"><?
    foreach($arResult["ITEMS"] as $arItem)
    {
        $img = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width'=>2560, 'height'=>9999), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        ?><div class="my-slider-item">
            <div class="my-slider-item__img-cont" style="background-image: url(<?=$img["src"]?>);"></div>
            <div class="container">
                <p class="title">
                    <?=$arItem["NAME"]?>
                    <span>
                        <a href="<?=$arItem["PROPERTIES"]["URL"]["VALUE"]?>"<?if($arItem["PROPERTIES"]["TARGET_BLANK"]["VALUE"]=="да") echo ' target="_blank"';?><?if($arItem["PROPERTIES"]["NOFOLLOW"]["VALUE"]=="да") echo ' rel="nofollow"';?> >Подробнее</a>
                    </span>        
                </p>
            </div>
            
        </div><?      
    }
    ?></div><?
}
?>