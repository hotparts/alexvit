<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-detail">
    <?
    if ($arResult["DETAIL_PICTURE"]["ID"])
    {
        ?>
        <div class="detail_pic">
            <?$img = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"]["ID"], array('width'=>500, 'height'=>9999), BX_RESIZE_IMAGE_PROPRTIONAL, true);?>
            <img src="<?=$img["src"]?>" width="<?=$img["width"]?>" height="<?=$img["height"]?>" alt="<?=$arResult["NAME"]?>" />
        </div>
        <?
    }
    ?>
    <p><span class="date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span></p>
    <?
    $detailText=trim($arResult["~DETAIL_TEXT"]);
    if ($detailText!="")
    {
    	if ($arResult["DETAIL_TEXT_TYPE"]=="html")
    	{
     		echo $detailText;
    	} else
     	{
    		?><p><?=str_replace("\n","<br />",$detailText)?></p><?
      	}
    }
?>
</div>