<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
foreach($arResult["ITEMS"] as $arItem)
{
    $img="";
    if ($arItem["DETAIL_PICTURE"]["ID"])
    {
        $img = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width'=>360, 'height'=>9999), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $img=$img["src"];
    }
    ?>
    <div class="news__item row">
        <div class="col-xs-4 news__item-left-col">
            <?
            if ($img=="")
            {
                ?>
                <a class="img-link img-link-nofoto" href="<?=$arItem["DETAIL_PAGE_URL"]?>"></a>
                <?
            } else
            {
                ?>
                <a class="img-link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <img src="<?=$img?>" alt="<?=$arItem["NAME"]?>">
                </a>
                <?
            }
            ?>
        </div>
        <div class="col-xs-8 text-block news__item-right-col"">
            <div class="name"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
            <span class="date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
            <div class="text">
                <?
                $anons=strip_tags(trim($arItem["~DETAIL_TEXT"]));
                $max_length_anons=600;
                if (strlen($anons)>$max_length_anons)
                {
                    $pos_probel=strpos($anons," ",$max_length_anons);
                    if ($pos_probel>0)
                    {
                        $anons=substr($anons,0,$pos_probel);
                    } else
                    {
                        $anons=substr($anons,0,$max_length_anons);
                    }
                    $anons.="...";
                }
                echo $anons;
                ?>          
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="read-more">читать далее</a>  
            </div>
        </div>
    </div>
    <?
}
echo $arResult["NAV_STRING"];
?>