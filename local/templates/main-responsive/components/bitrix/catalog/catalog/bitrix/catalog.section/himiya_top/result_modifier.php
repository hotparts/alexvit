<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arResult["BRANDS"]=array();
$arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'ACTIVE'=>'Y', 'SECTION_ID'=>$arResult["ID"]);
$db_list = CIBlockSection::GetList(Array("SORT"=>'ASC'), $arFilter, true);
while($ar_result = $db_list->GetNext())
{
    if ($ar_result["PICTURE"])
    {
        $img = CFile::ResizeImageGet($ar_result["PICTURE"], array('width'=>116, 'height'=>80), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        $arResult["BRANDS"][$ar_result["ID"]]=array(
            "NAME" => $ar_result['NAME'],
            "IMG" => $img["src"],
            "URL" => $ar_result["SECTION_PAGE_URL"],
        );
    }
}
?>