<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (count($arResult["ITEMS"])>0)
{
    ?>
    
        <?
        foreach ($arResult["SECTIONS"] as $section)
        {
            if (!empty($section["SUB_SECTIONS"]))
            {
                ?>
                <h2 style="font-size: 28px;margin-bottom:15px"><?=$section["NAME"]?></h2>
                <ul class="row catalog-grid default_catalog_top">
                    <?
                    foreach ($section["SUB_SECTIONS"] as $subsection)
                    {
                        ?><li class="catalog-grid__item col-xs-4 col-vtop">
                            <p class="catalog-grid__item-title"><a href="<?=$subsection["URL"]?>"><?=$subsection["NAME"]?></a></p>
                            <div class="catalog-grid__item-wrap">
                                <span class="catalog-grid__item-tcell">
                                    <a href="<?=$subsection["URL"]?>">
                                        <img src="<?=$subsection["IMG"]?>" alt="">
                                    </a>
                                <span>
                            </span></span></div>
                        </li><?
                    }
                    ?>                        
                </ul>
                <?
            }
        }
        ?>
    
    <?
}
?>