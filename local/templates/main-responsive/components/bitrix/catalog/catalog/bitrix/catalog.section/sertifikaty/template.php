<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$params = Array(
   "max_len" => "100", // обрезает символьный код до 100 символов
   "change_case" => "L", // буквы преобразуются к нижнему регистру
   "replace_space" => "-", // меняем пробелы на нижнее подчеркивание
   "replace_other" => "-", // меняем левые символы на нижнее подчеркивание
   "delete_repeat_replace" => "true", // удаляем повторяющиеся нижние подчеркивания
   "use_google" => "false", // отключаем использование google
); 

if (count($arResult["itemsByBrand"])>0)
{
    ?>
    <ul class="catalog-logos-tree">
        <?
        foreach ($arResult["itemsByBrand"] as $brandName=>$brandItems)
        {
            if ($_GET['brand']=="")
            {
                $hideCurBrand=true;
            } else
            {
                $CODE = CUtil::translit($brandName, "ru" , $params);
                
                if (strcasecmp(trim($CODE),trim($_GET['brand']))==0)
                {
                    $hideCurBrand=false;
                } else
                {
                    $hideCurBrand=true;
                }
            }
            ?>
            <li>
                <a class="catalog-section closed <?if (!$hideCurBrand){?>opened<?}?>" href="javascript:void(0);"><?=$brandName?></a>
                <ul class="catalog-logos-items"<?if ($hideCurBrand){?>style="display:none"<?}else{?>style="display:block"<?}?>>
                    <?
                    foreach ($brandItems as $item)
                    {
                        ?>
                        <li>
                            <a href="<?=$item["FILE"]?>" target="_blank">
                                <span>
                                    <i><?=$item["NAME"]?></i>
                                </span>
                            </a>
                            <?
                            /*<figure class="logo-item" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                <a href="<?=$item["IMG"]?>" data-size="<?=$item["IMG_W"]?>x<?=$item["IMG_H"]?>" itemprop="contentUrl">
                                    <span>
                                        <i><?=$item["NAME"]?></i>
                                    </span>
                                </a>
                                <figcaption itemprop="caption description"><?=$brandName." - ".$item["NAME"]?></figcaption>
                            </figure>
                            <?
                            foreach ($item["DOP_FOTOS"] as $foto)
                            {
                                ?><figure class="logo-item hidden" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                    <a href="<?=$foto["IMG"]?>" data-size="<?=$foto["IMG_W"]?>x<?=$foto["IMG_H"]?>" itemprop="contentUrl">
                                        <span>
                                            <i><?=$item["NAME"]?></i>
                                        </span>
                                    </a>
                                    <figcaption itemprop="caption description"><?=$brandName." - ".$item["NAME"]?></figcaption>
                                </figure><?
                            }*/
                            ?>
                        </li>
                        <?
                    }
                    ?>
                </ul>
            </li>
            <?
        }
        ?>
    </ul>
    <?
} else
{
    
}
?>