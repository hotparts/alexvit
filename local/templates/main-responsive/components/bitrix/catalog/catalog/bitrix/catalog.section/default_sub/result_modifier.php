<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arResult["VID_TOVARA"]=array();
$arFilter = Array('IBLOCK_ID'=>10, 'ACTIVE'=>'Y');
$db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, true);
while($ar_result = $db_list->GetNext())
{
    $arResult["VID_TOVARA"][$ar_result['ID']]["ITEMS"]=array();
    $arResult["VID_TOVARA"][$ar_result['ID']]["NAME"]=$ar_result['NAME'];
}
$vseVidyTovarov=array();
$arSelect = Array("ID", "NAME", "DETAIL_PICTURE", "IBLOCK_SECTION_ID", "CODE");
$arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    if ($arFields["DETAIL_PICTURE"])
    {
        $img = CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array('width'=>118, 'height'=>118), BX_RESIZE_IMAGE_EXACT, true);
        $vseVidyTovarov[$arFields["ID"]]=array(
            "SECTION_ID" => $arFields['IBLOCK_SECTION_ID'],
            "NAME" => $arFields['NAME'],
            "IMG" => $img["src"],
            "URL" => $arResult["PATH"][1]["SECTION_PAGE_URL"]."vid-".$arFields["CODE"]."/",
        );
    }
}

$arResult["BRANDS"]=array();

foreach ($arResult["ITEMS"] as $item)
{
    $curBrand=$item["PROPERTIES"]["BRAND"]["VALUE"];
    if ($curBrand>0 && !isset($arResult["BRANDS"][$curBrand]))
    {
        $res = CIBlockElement::GetByID($curBrand);
        if($ar_res = $res->GetNext())
        {
            if ($ar_res["DETAIL_PICTURE"])
            {
                $img = CFile::ResizeImageGet($ar_res["DETAIL_PICTURE"], array('width'=>116, 'height'=>50), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                $arResult["BRANDS"][$curBrand]=array(
                    "NAME" => $ar_res['NAME'],
                    "IMG" => $img["src"],
                    "URL" => $arResult["PATH"][1]["SECTION_PAGE_URL"]."brand-".$ar_res["CODE"]."/",
                );
            }
        }
    }
    
    foreach ($item["PROPERTIES"]["VID_TOVARA"]["VALUE"] as $curVidTovara)
    {
        if ($curVidTovara>0)
        {
            $arResult["VID_TOVARA"][$vseVidyTovarov[$curVidTovara]["SECTION_ID"]]["ITEMS"][$vseVidyTovarov[$curVidTovara]["NAME"]]=$vseVidyTovarov[$curVidTovara];
        }
    }
}
#$arResult["BRANDS"]=array_merge($arResult["BRANDS"],$arResult["BRANDS"],$arResult["BRANDS"]);
#$arResult["FAKTURA"]=array_merge($arResult["FAKTURA"],$arResult["FAKTURA"],$arResult["FAKTURA"]);
?>