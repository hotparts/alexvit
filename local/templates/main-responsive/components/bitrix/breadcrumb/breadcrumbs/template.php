<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';
$itemSize = count($arResult);

$pagenKey="";
foreach ($arResult as $key=>$chain)
{
    if (stristr($chain["LINK"],"PAGEN"))
    {
        $pagenChain=$chain;
        $pagenKey=$key;
        break;
    }
}
if ($pagenKey!="")
{
    $newResult=array();
    unset($arResult[$pagenKey]);
    foreach ($arResult as $key=>$chain)
    {
        $newResult[]=$chain;
    }
    $newResult[]=$pagenChain;
    $arResult=$newResult;
}
if ($itemSize>1)
{
    $strReturn .= '<div class="bx-breadcrumb">';
    
    for($index = 0; $index < $itemSize; $index++)
    {
    	$title = trim(htmlspecialcharsex($arResult[$index]["TITLE"]));
    
    	$nextRef = ($index < $itemSize && $arResult[$index+1]["LINK"] <> ""? ' itemref="bx_breadcrumb_'.($index+1).'"' : '');
    	$child = ($index > 0? ' itemprop="child"' : '');
    
    	if($arResult[$index]["LINK"] <> "" && $index<$itemSize)
    	{
    		$strReturn .= '
    			<div class="bx-breadcrumb-item" id="bx_breadcrumb_'.$index.'" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"'.$child.$nextRef.'>
    				<a href="'.$arResult[$index]["LINK"].'" itemprop="url"><span itemprop="title">'.$title.'</span></a>';
    	}
    	else
    	{
    		$strReturn .= '
    			<div class="bx-breadcrumb-item">
    				<span>'.$title.'</span>';
    	}
            
        $strReturn .= '</div>';
        if ($index < $itemSize-1)
        {
            $strReturn .= '<i class="fa fa-angle-right"></i>';
        }
    }
    $strReturn .= '</div>';
}
//AddMessage2Log(count($arResult).'$arFields = '.print_r($arResult, true),'');
return $strReturn;
?>
