<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult))
{
    ?>
    <ul class="main-menu"><?
        $previousLevel = 0;
        foreach($arResult as $arItem)
        {
        	if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel)
            {
        		str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
        	}
        
        	if ($arItem["IS_PARENT"])
            {
                ?><li class="have-menu">
                    <a href="<?=$arItem["LINK"]?>" <?if ($arItem["SELECTED"]):?>class="active"<?endif?>><?=$arItem["TEXT"]?><i class="arrow fa fa-caret-right"></i></a>
    				<ul class="submenu"><?
        		    
            } else
            {
                ?><li<?if ($arItem["CONTENT_SUB"]!=""){?> class="have-menu"<?}?>>
                    <?if ($arItem["SELECTED"]){?>
						<span class="active"><?=$arItem["TEXT"]?></span>
                    <?}else{?>
						<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
					<?}?>
					<?
                    if ($arItem["CONTENT_SUB"]!="")
                    {
                        ?><ul class="submenu"><?=$arItem["CONTENT_SUB"]?></ul><?
                    }
                    ?>
                </li><?
            }
            $previousLevel = $arItem["DEPTH_LEVEL"];
        }
        if ($previousLevel > 1)
        {
        	str_repeat("</ul></li>", ($previousLevel-1) );
        }?>
    </ul>
    <?
}?>