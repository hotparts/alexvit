<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach ($arResult as &$item)
{
    if ($item["PARAMS"]["IS_CATALOG"]=="Y")
    {
        CModule::IncludeModule("iblock");
        $item["CONTENT_SUB"]='';
        $arFilter = Array('IBLOCK_ID'=>CATALOG_IBLOCK_ID, 'ACTIVE'=>'Y', 'DEPTH_LEVEL'=>1);
        $db_list = CIBlockSection::GetList(Array("SORT"=>'ASC'), $arFilter, true);
        while($ar_result = $db_list->GetNext())
        {
            if ($_SERVER['REQUEST_URI']==$ar_result["SECTION_PAGE_URL"]){
				$item["CONTENT_SUB"].='<li><span>'.$ar_result["NAME"].'</span></li>';}
			else{
				$item["CONTENT_SUB"].='<li><a href="'.$ar_result["SECTION_PAGE_URL"].'">'.$ar_result["NAME"].'</a></li>';}
        }
    }
}
?>