<?php
$tpl_name=basename(__DIR__);
return array(
    'js' => array(
        '//local/templates/'.$tpl_name.'/js/jquery.js',
        '//local/templates/'.$tpl_name.'/js/jquery.cookie.js',
        '//local/templates/'.$tpl_name.'/js/jquery.magnific-popup.min.js',
        '//local/templates/'.$tpl_name.'/js/photoswipe/photoswipe.js',
        '//local/templates/'.$tpl_name.'/js/photoswipe/photoswipe-ui-default.min.js',
        '//local/templates/'.$tpl_name.'/js/slick_slider/slick.min.js',
        '//local/templates/'.$tpl_name.'/js/my/nano_scroller.js',
        '//local/templates/'.$tpl_name.'/js/customForm/script.js',
        '//local/templates/'.$tpl_name.'/js/my/utils.js',
        '//local/templates/'.$tpl_name.'/js/my/common.js',        
        '//local/templates/'.$tpl_name.'/js/my/catalog.js',
        '//local/templates/'.$tpl_name.'/js/my/catalog_item.js'
    ),
    'css' => array(
        '//local/templates/'.$tpl_name.'/css/bootstrap.css', 
        '//local/templates/'.$tpl_name.'/js/customForm/styles.css',
        '//local/templates/'.$tpl_name.'/css/magnific-popup.css',
        '//local/templates/'.$tpl_name.'/css/font-awesome.min.css',
        '//local/templates/'.$tpl_name.'/js/photoswipe/photoswipe.css',
        '//local/templates/'.$tpl_name.'/js/photoswipe/default-skin/default-skin.css',
        '//local/templates/'.$tpl_name.'/js/slick_slider/slick.css',
        '//local/templates/'.$tpl_name.'/js/slick_slider/slick-theme.css',
        '//local/templates/'.$tpl_name.'/styles-all.css'
    )
);