$(function() {    
    $(document).on('submit', '#ilogin_form', function(){
        var curUrl=$(this).attr('action');
        var curForm=$(this);
        var curSubmitBTN=curForm.find('input[type="submit"]');
        var curSubmitBTNtext=curSubmitBTN.val();
        var newLocation=curForm.find('input[name="new_location"]').val();
        curSubmitBTN.val('Подождите...');
        curSubmitBTN.attr('disabled','disabled');
        $.ajax
        ({
            url: curUrl,
            data: $(this).serialize(),
            type: "POST",   
            cache: false,   
            success: function(html)
            {
                curSubmitBTN.val(curSubmitBTNtext);
                curSubmitBTN.removeAttr('disabled');
                if ($.trim(html)=="ok")
                {
                    if (newLocation!="")
                    {
                        location.href=newLocation;
                    } else
                    {
                        location.reload();
                    }                       
                } else
                {
                    curForm.find('.warning').html(html).fadeIn(300);
                }
            },
        	error:  function(xhr, ajaxOptions, thrownError){
                alert(xhr.status+" "+thrownError);
        	}              
        });
        return false;
    });
    
    $(document).on('submit', '#ireg_form', function(){
        var curUrl=$(this).attr('action');
        var curForm=$(this);
        var curSubmitBTN=curForm.find('input[type="submit"]');
        var curSubmitBTNtext=curSubmitBTN.val();
        curSubmitBTN.val('Подождите...');
        curSubmitBTN.attr('disabled','disabled');
        $.ajax
        ({
            url: curUrl,
            data: $(this).serialize(),
            type: "POST",   
            cache: false,   
            success: function(html)
            {
                curSubmitBTN.val(curSubmitBTNtext);
                curSubmitBTN.removeAttr('disabled');
                if ($.trim(html)=="ok")
                {
                    location.href='/kabinet/';                       
                } else
                {
                    curForm.find('.warning').html(html).fadeIn(300);
                }
            },
        	error:  function(xhr, ajaxOptions, thrownError){
                alert(xhr.status+" "+thrownError);
        	}              
        });
        return false;
    });
    
    $(document).on('submit', '#iforgot_form', function(){
        var curUrl=$(this).attr('action');
        var curForm=$(this);
        var curSubmitBTN=curForm.find('input[type="submit"]');
        var curSubmitBTNtext=curSubmitBTN.val();
        curSubmitBTN.val('Подождите...');
        curSubmitBTN.attr('disabled','disabled');
        $.ajax
        ({
            url: curUrl,
            data: $(this).serialize(),
            type: "POST",   
            cache: false,   
            success: function(html)
            {
                curSubmitBTN.val(curSubmitBTNtext);
                curSubmitBTN.removeAttr('disabled');
                curForm.find('.result_form').html(html).fadeIn(300);
            },
        	error:  function(xhr, ajaxOptions, thrownError){
                alert(xhr.status+" "+thrownError);
        	}              
        });
        return false;
    });
    
    $(document).on('click', 'a.ajax_a', function(){
        var curURL=$(this).data('src');
        if (curURL!=undefined && curURL!="")
        {
            var timeoutLength=0;
            if ($('.white-popup').length>0)
            {
                $.magnificPopup.close();
                timeoutLength=350;
            }
            window.setTimeout(function () {
                $.magnificPopup.open({
                    tClose: 'Закрыть (Esc)',
                    tLoading: 'Загрузка...',
                    items: {
                        src: '/ajax/'+curURL,
                    },
                    type: 'ajax',
                    preloader: true,
        	        modal: false,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in',
                    closeOnBgClick:false,
                    callbacks: {
                        ajaxContentAdded: function() {
                            if (IS_GADGET)
                            {
                                document.location.hash='popup';
                                observerCloseMagnificPopup();
                            }
                            $('.white-popup input[name="REGISTER[PERSONAL_PHONE]"]').mask("+9 (999) 999-9999");
                        },
                        open: function() {
                            if (IS_GADGET)
                            {
                                $.magnificPopup.instance.close = function () {
                                    removeHashForPopups();   
                                    $.magnificPopup.proto.close.call(this);
                                };
                            } 
                            
                        },
                    }
                });
            }, timeoutLength);
            return false;
        }
    });
});