$(function() {
    
   
    $(document).on('click','.addToCart',function(){
        if (!$(this).hasClass('disabled') && !$(this).hasClass('wait'))
        {
            var offerID=$(this).data('id');
            var curBTN=$(this);
            var curBTNtext=curBTN.html();
            curBTN.addClass('wait');
            
            var curData=new Object();
            curData["offerID"]=offerID;
            
            $.ajax
            ({
                url: "/ajax/cart_add.php",
                data: curData,
                type: "POST",   
                dataType: 'json',
                cache: false,   
                success: function(res)
                {
                    if (res.success=="Y")
                    {
                        curBTN.html(curBTNtext).removeClass('wait');    
                        $('#cart__num').html(res.cnt);

                    } else
                    {
                        curBTN.removeClass('wait').html(curBTNtext);
                        alert('Произошла ошибка. Попробуйте позднее.');
                    }
                },
            	error:  function(xhr, ajaxOptions, thrownError){
            		curBTN.removeClass('wait').html(curBTNtext);
                    alert('Произошла ошибка. Попробуйте позднее.');
            	}              
            });
        }
    });
    
    $(document).on('click','.addToCompare',function(){
        var curID=""+$(this).data('id')+"";
        var curCompareList=[];
        if ($.cookie("compareList"))
        {
            var curCompareList=$.cookie("compareList").split(',');
            if ($.inArray(curID,curCompareList)!=-1)
            {
                curCompareList = $.grep(curCompareList, function(value) {
                  return value != curID;
                });
            } else
            {
                curCompareList.push(curID);
            }
        } else
        {
            curCompareList.push(curID);
        }
        $('#compare__num').html(curCompareList.length);
        $.cookie("compareList", curCompareList.join(','), { expires: 3000000, path: "/"});
    });
    
    $(document).on('click','.removeFromCompare',function(){
        var curID=""+$(this).data('id')+"";
        var curCompareList=[];
        if ($.cookie("compareList"))
        {
            var curCompareList=$.cookie("compareList").split(',');
            curCompareList = $.grep(curCompareList, function(value) {
              return value != curID;
            });
        }
        $('#compare__num').html(curCompareList.length);
        $.cookie("compareList", curCompareList.join(','), { expires: 3000000, path: "/"});
        $('.compare_list td[data-id="'+curID+'"]').remove();
        $('.compare_list .tr_prop').each(function(){
           var isNeedRemoveThis=true;
           $(this).children().each(function(){
                if (!$(this).hasClass('first_td') && $(this).html()!="")
                {
                    isNeedRemoveThis=false;     
                }
           });
           if (isNeedRemoveThis)
           {
                $(this).remove();   
           } 
        });
    });
    
    
});