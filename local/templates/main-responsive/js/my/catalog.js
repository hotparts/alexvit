$(function() {
    
    $('.collection_group_link').click(function(){
       var curGroup=$(this).data('id');
       $('.isCollectionGroup').hide();
       console.log(curGroup)
       $('.isCollectionGroup[data-group="'+curGroup+'"]').show(); 
    });
    
    //БЫСТРЫЙ ПРОСМОТР    
    $(document).on('click','.catalog-logos-items a.ajax',function(){
        var curID=$(this).data('id');
        var curProd=$(this).data('prod');
        curHref='/ajax/himiya_detail.php?id='+curID+'&prod='+curProd;
        
        $.magnificPopup.open({
            tClose: 'Закрыть',
            tLoading: 'Загрузка...',
            items: {
                src: curHref,
            },
            type: 'ajax',
            preloader: true,
	        modal: false,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in',
            callbacks: {
                ajaxContentAdded: function() {
                    if (IS_GADGET)
                    {
                        document.location.hash='popup';
                        observerCloseMagnificPopup();
                    } 
                    $(window).trigger('resize');
                }
            } 
        });
        return false;
    });
 
});

//подгрузка следующей страницы каталога по кнопке "Показать ещё"
$(document).ready(function() {
    $(document).on('click','.quick_next_page a',function(){
        if ($('.modern-page-next').length>0)
        {
            var curBtn=$(this);
            var ajaxURL=$('.modern-page-next').attr('href');
            var prevText=curBtn.html();
            curBtn.html('Загрузка...');

            $.ajax
            ({
                url: ajaxURL,
                type: "POST",   
                cache: false,   
                success: function(html)
                {
                    $('.quick_next_page, .modern-page-navigation').remove();
                    $('#catalog_row').html($.trim($('#catalog_row').html())+$.trim(html));
                },
            	error:  function(xhr, ajaxOptions, thrownError){
            	    curBtn.html(prevText);
                    //alert(xhr.status+" "+thrownError);
            	}              
            });    
        }
    });
});