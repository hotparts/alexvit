var item;

$(function() {

    
    $('.btn_more_colors_detail').click(function(){
        $('.collections .img-wrap.ajax').fadeIn(300);
        $(this).hide();
    });
    
    //БЫСТРЫЙ ПРОСМОТР    
    $(document).on('click','.collections .img-wrap.ajax',function(){
        var curSRC=$(this).find('img').data('src');
        var curTitle=$(this).find('img').attr('title');
        var curName=$('h1').text();
        var curImgWidth=$(this).find('img').data('width');
        item = $(this);
        var curIndex = $(this).index();

        
        if ($(item).next().length > 0 && !$(item).next().hasClass('wrap-btn-more')) 
        { 
            var arrowRightClick = "$(item).next().click()";
        } else 
        { 
            var arrowRightClick = "$(item).closest('.section-tile').children().first().click()"; 
        }
        
        
        if ($(item).prev().length > 0)
        { 
            var arrowLeftClick = '$(item).prev().click()';
        } else 
        { 
            var arrowLeftClick =  "$(item).closest('.section-tile').children(':not(.wrap-btn-more)').last().click()";
        }

        
        $.magnificPopup.open({
            tClose: 'Закрыть',
            tLoading: 'Загрузка...',
            items: {
                src: '<div class="white-popup white-popup-block zoom-anim-dialog unsel" style="width:'+curImgWidth+'px">\
                    <div class="texture__wrap"><div class="texture__title">'+curName+' '+curTitle+'</div>\
                        <img src="'+curSRC+'" alt=""><div class="texture__footer">\
                        <a class="customFormLink product-item__btn-order" href="javascript:void(0)"\
                            data-params="YNYYN-NNYNN"\
                            data-form-code="collection_zakaz"\
                            data-title="Заказать"\
                            data-text="'+curName+' '+curTitle+'"\
                            data-btn-text="Отправить"\
                            data-service-form-name=""\
                            data-field-title-phone=""\
                            data-field-title-name=""\
                            data-field-placeholder-name="Представьтесь пожалуйста"\
                            data-field-placeholder-phone="Ваш контактный телефон"\
                            data-custom-textarea-title=""\
                            data-custom-textarea-placeholder="Комментарии"\
                        >Заказать</a>\
                    </div></div><i class="arrow-left" onclick="'+arrowLeftClick+'"></i><i class="arrow-right" onclick="'+arrowRightClick+'"></i></div>',
            },
            type: 'inline',
            preloader: true,
	        modal: false,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in',
            closeOnBgClick: false,
            callbacks: {
                ajaxContentAdded: function() {
                    if (IS_GADGET)
                    {
                        document.location.hash='popup';
                        observerCloseMagnificPopup();
                    } 
                    $(window).trigger('resize');
                }
            } 
        });
        return false;
    });

});