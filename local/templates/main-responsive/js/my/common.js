if (IS_GADGET)
{
    var isFadeSliders=false;
} else
{
    var isFadeSliders=true;
}
var isInitFirstProjectSlider=false;
$(function() {
	$('.form_shapka_zvonok').submit(function(){
		yaCounter45445452.reachGoal('RECALL');
	});
	
// 	$(window).scroll(function () {
// 		if ($(this).scrollTop() > 100) {
// 			$('.site-header .search-bar').addClass('min');
// 			$('.site-header .search-bar').text('');
// 			$('.site-header .h-phone').removeClass('hide');
// 		} else {
// 			$('.site-header .search-bar').removeClass('min');
// 			$('.site-header .search-bar').text('Поиск');
// 			$('.site-header .h-phone').addClass('hide');
// 		}
// 	});
	
    $('.search-bar').click(function(){
        $('.search_window').addClass('active');
    });
    $('.search_window .close').click(function(){
        $('.search_window').removeClass('active');
    });
    
    $('.read-more-section').click(function(){
        $('html,body').animate({ scrollTop: $(".catalog-descript").offset().top}, 500 );
    });
    
    
    initPhotoSwipeFromDOM('.sert_img',true);
    
    /*проекты*/
    $('.project_item .project-slider').on('init', function(event, slick, currentSlide, nextSlide){
        initPhotoSwipeFromDOM('.project-slider .slick-track',true);
    });
    $('.projects-list a').click(function(){
        var curID=$(this).data('id');
        location.hash=curID;
        $('.projects-list a').removeClass('active');
        $(this).addClass('active');
        $('.project_item').removeClass('active');
        $('.project_item[data-id="'+curID+'"]').addClass('active');
        
        if (!$('.project_item[data-id="'+curID+'"] .project-slider').hasClass('slick-initialized'))
        {
            $('.project_item[data-id="'+curID+'"] .project-slider').slick({ 
                arrows: true,
        		dots: false,
        		infinite: false,
        		speed: 300,
        		slidesToShow: 4,
        		slidesToScroll: 4,
        		autoplay: false,
                touchThreshold: 100,
                autoplaySpeed: 5000,
                adaptiveHeight:true,
        		nextArrow: '<button type="button" class="slick-next slider__next"><i class="icon-next-big"></i></button>',
        		prevArrow: '<button type="button" class="slick-prev slider__prev"><i class="icon-prev-big"></i></button>',
        		draggable: false
        	});
            
        }
    });
    $('.projects .main-photo img').click(function(){
       $(this).parents('.project_item').find('.project-slider figure').first().click(); 
    });
    if (location.pathname=="/proekty/")
    {
        if (location.hash!="")
        {
            var curProjectID=location.hash.substr(1);
        } else
        {
            var curProjectID=$('.projects-list a.active').data('id');
        }
        $('.projects-list a[data-id="'+curProjectID+'"]').click();
    }
    
    /*проекты --- конец*/
    
    if ($('.main_slider #index_slider').length)
    {
        $('.main_slider #index_slider').slick({ 
            fade: false,
            arrows: true,
    		dots: true,
    		infinite: true,
    		speed: 300,
    		slidesToShow: 1,
    		slidesToScroll: 1,
            touchThreshold: 100,
    		autoplay: true,
            autoplaySpeed: index_slide_speed,
    		nextArrow: '<button type="button" class="slick-next slider__next"><i class="icon-next-big"></i></button>',
    		prevArrow: '<button type="button" class="slick-prev slider__prev"><i class="icon-prev-big"></i></button>',
    		draggable: false
    	});
    }
	
	if ($('.product-slider').length)
    {
        $('.product-slider').slick({ 
            fade: false,
            arrows: true,
    		dots: false,
    		infinite: true,
    		speed: 300,
    		slidesToShow: 1,
    		slidesToScroll: 1,
            touchThreshold: 100,
			autoplay: true,
            autoplaySpeed: index_slide_speed,
    		nextArrow: '<button type="button" class="slick-next slider__next"><i class="icon-next-big"></i></button>',
    		prevArrow: '<button type="button" class="slick-prev slider__prev"><i class="icon-prev-big"></i></button>',
    		draggable: false
    	});
    }
    
    if ($('.logo-slider').length)
    {
        $('.logo-slider').slick({ 
            fade: false,
            arrows: true,
    		dots: false,
    		infinite: true,
    		speed: 300,
    		slidesToShow: 5,
    		slidesToScroll: 5,
            touchThreshold: 100,
    		autoplay: false,
            autoplaySpeed: 5000,
    		draggable: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
    	});
    }
    
    if ($('.project-slider-main').length)
    {
        $(document).on('click','.project-slider-main .slick-dots .arr_left',function(){
            $('.project-slider-main .slider__prev').click();
        });
        $(document).on('click','.project-slider-main .slick-dots .arr_right',function(){
            $('.project-slider-main .slider__next').click();
        });
        $('.project-slider-main').on('init', function(event, slick, currentSlide, nextSlide){
            $('.project-slider-main .slick-dots li').wrapAll('<span>');
            $('.project-slider-main .slick-dots').prepend('<span class="arrow arr_left"></span>');
            $('.project-slider-main .slick-dots').append('<span class="arrow arr_right"></span>');
        });
        $('.project-slider-main').slick({ 
            arrows: true,
    		dots: true,
    		infinite: true,
    		speed: 300,
    		slidesToShow: 4,
    		slidesToScroll: 4,
    		autoplay: false,
            autoplaySpeed: 5000,
            touchThreshold: 100,
            adaptiveHeight:true,
    		nextArrow: '<button type="button" class="slick-next slider__next"><i class="icon-next-big"></i></button>',
    		prevArrow: '<button type="button" class="slick-prev slider__prev"><i class="icon-prev-big"></i></button>',
    		draggable: false
    	});
    }
    
    
    /*$('.main-slider').slick({ 
        fade: isFadeSliders,
        arrows: false,
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: false,
        autoplaySpeed: 5000,
        adaptiveHeight:true,
		nextArrow: '<button type="button" class="slick-next slider__next"><i class="icon-next-big"></i></button>',
		prevArrow: '<button type="button" class="slick-prev slider__prev"><i class="icon-prev-big"></i></button>',
		draggable: false,
		responsive: [
			{
				breakpoint: 991,
				settings: {
					dots: true,
					arrows: false
				}
			}
		]
	});*/
    
    $('.prv-product__list').slick({ 
        vertical: true,
        arrows: true,
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: false,
        autoplaySpeed: 5000,
        adaptiveHeight:true,
		nextArrow: '<button type="button" class="slick-next slider__next"><i class="icon-next-big"></i></button>',
		prevArrow: '<button type="button" class="slick-prev slider__prev"><i class="icon-prev-big"></i></button>',
		draggable: false
	});    
    
});

$(document).ready(function () {
    
	$('.catalog-section-list .menu-arrow').on('click', function(event) {
        event.preventDefault(); 
        event.stopPropagation();
        $(this).toggleClass('opening').closest('li').find('ul').toggleClass('active');
    });

	$(".catalog-section").click(function(){
		$(this).toggleClass("opened");
		sectionParent = $(this).next('ul');
		$(sectionParent).stop(true,true).slideToggle(600);
	});
    
});

$(window).on('load resize',function(){
    var curWinH=$(window).height();
    $('main').css('min-height','0px');
    var curH=$('header').outerHeight(true)+$('main').outerHeight(true)+$('footer').outerHeight(true);
    if (curH<curWinH)
    {
        var delta=curWinH-curH;
        var mainH=$('main').outerHeight(true)+delta;
        $('main').css('min-height',mainH+'px');
    }
});
