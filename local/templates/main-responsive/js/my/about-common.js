$(function() {
	$('.slider.sert .carousel').carouFredSel({
		items: 'height',
		responsive: false,
		height: 209,
		width: '100%',
		prev: '.slider.sert .btn-prev',
		next: '.slider.sert .btn-next',
		padding: [0, 40],
		scroll: {
			items: 1,
			fx: 'scroll',
			//easing: 'elastic',
		},
		auto: {
			timeoutDuration: 5000,
			duration: 2000
		},
		pagination: {
			container: '#pager',
			duration: 300
		}
	});
	
	$('.slider.thanks .carousel').carouFredSel({
		items: 'height',
		responsive: false,
		//height: 209,
		width: '100%',
		prev: '.slider.thanks .btn-prev',
		next: '.slider.thanks .btn-next',
		scroll: {
			items: 1,
			fx: 'scroll',
			//easing: 'elastic',
		},
		auto: {
			timeoutDuration: 5000,
			duration: 2000
		},
		pagination: {
			container: '#pager',
			duration: 300
		}
	});
});