function refreshSlickSliders()
{
    $('.slick-slider').each(function(){
        $(this).slick('setPosition');
    });
}

function removeHashForPopups () { 
    document.location.hash="_";
}
function formatPrice(num){
    var dec=10;
    var numSTR=num.toString().replace('.',',');
    var s = 0;
    var str = '';
    for( var i=numSTR.length-1; i>=0; i-- ) 
    {
        s++;
        str = numSTR.charAt(i) + str;
        if(numSTR.charAt(i)==',') s=0;
        if( s > 0 && !(s % 3) ) str  = " " + str;
    }
    return str; 
}
function makeTablesResponsive(onlyBody,parent)
{
    if (parent==undefined) parent='';
    $(parent+' table.big-data').each(function(){
        var curHeaders=new Array();
        
        $(this).children('thead').find('th').each(function(){
            curHeaders.push($(this).text());
        });
        
        $(this).children('tbody').children('tr').children('td').each(function(){
            if (!$(this).hasClass('wo-title'))
            {
                $(this).wrapInner('<div class="text"/>');
                $(this).prepend('<div class="title">'+curHeaders[$(this).index()]+'</div>');
            } else
            {
                $(this).wrapInner('<div class="text"/>');
            }
        });
    });
}
function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts= url.split('?');   
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(parameter)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i= pars.length; i-- > 0;) {    
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
                pars.splice(i, 1);
            }
        }

        url= urlparts[0]+'?'+pars.join('&');
        return url;
    } else {
        return url;
    }
}

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}

function alertObj(obj) { 
    var str = ""; 
    for(k in obj) { 
        str += k+": "+ obj[k]+"\r\n"; 
    } 
    alert(str); 
} 