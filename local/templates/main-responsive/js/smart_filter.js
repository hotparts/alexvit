var modefTop;
if (typeof smartFilterAjax != 'undefined')
{
function JCSmartFilter(ajaxURL, viewMode)
{
	this.ajaxURL = ajaxURL;
	this.form = null;
	this.timer = null;
	this.cacheKey = '';
	this.cache = [];
	this.viewMode = viewMode;
}

JCSmartFilter.prototype.keyup = function(input)
{
	if(!!this.timer)
	{
		clearTimeout(this.timer);
	}
	this.timer = setTimeout(BX.delegate(function(){
		this.reload(input);
	}, this), 150);
};

JCSmartFilter.prototype.click = function(checkbox)
{
	if(!!this.timer)
	{
		clearTimeout(this.timer);
	}

	this.timer = setTimeout(BX.delegate(function(){
		this.reload(checkbox);
	}, this), 150);
};

JCSmartFilter.prototype.reload = function(input)
{
	if (this.cacheKey !== '')
	{
		//Postprone backend query
		if(!!this.timer)
		{
			clearTimeout(this.timer);
		}
		this.timer = setTimeout(BX.delegate(function(){
			this.reload(input);
		}, this), 150);
		return;
	}
	this.cacheKey = '|';

	this.position = BX.pos(input, true);
	this.form = BX.findParent(input, {'tag':'form'});
    $('#overlay_filter').show();
    $('.filter_vertical .bx_filter_popup_result').hide();
    $('.panel-smart-filter.filter_vertical').addClass('blured');
	if (this.form)
	{
		var values = [];
		values[0] = {name: 'ajax', value: 'y'};
		this.gatherInputsValues(values, BX.findChildren(this.form, {'tag': new RegExp('^(input|select)$', 'i')}, true));

		for (var i = 0; i < values.length; i++)
			this.cacheKey += values[i].name + ':' + values[i].value + '|';

		if (this.cache[this.cacheKey])
		{
			this.curFilterinput = input;
			this.postHandler(this.cache[this.cacheKey], true);
		}
		else
		{
			this.curFilterinput = input;
			BX.ajax.loadJSON(
				this.ajaxURL,
				this.values2post(values),
				BX.delegate(this.postHandler, this)
			);
		}
	}
};

JCSmartFilter.prototype.updateItem = function (PID, arItem)
{
	if (arItem.PROPERTY_TYPE === 'N' || arItem.PRICE)
	{
		var trackBar = window['trackBar' + PID];
		if (!trackBar && arItem.ENCODED_ID)
			trackBar = window['trackBar' + arItem.ENCODED_ID];

		if (trackBar && arItem.VALUES)
		{
			if (arItem.VALUES.MIN && arItem.VALUES.MIN.FILTERED_VALUE)
			{
				trackBar.setMinFilteredValue(arItem.VALUES.MIN.FILTERED_VALUE);
			}

			if (arItem.VALUES.MAX && arItem.VALUES.MAX.FILTERED_VALUE)
			{
				trackBar.setMaxFilteredValue(arItem.VALUES.MAX.FILTERED_VALUE);
			}
		}
	}
	else if (arItem.VALUES)
	{
		for (var i in arItem.VALUES)
		{
			if (arItem.VALUES.hasOwnProperty(i))
			{
				var value = arItem.VALUES[i];
				var control = BX(value.CONTROL_ID);

				if (!!control)
				{
					var label = document.querySelector('[data-role="label_'+value.CONTROL_ID+'"]');
					if (value.DISABLED)
					{
						if (label)
                        {
                            $('[data-role="label_'+value.CONTROL_ID+'"]').parent().find('.checkbox').addClass('disabled');
                            $('[data-role="label_'+value.CONTROL_ID+'"]').parent().find('.overlay').addClass('disabled');
                            $('[data-role="label_'+value.CONTROL_ID+'"]').parent().addClass('disabled');
							BX.addClass(label, 'disabled');
                            /*if ($('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').find('.bx_filter_input_checkbox.disabled').length==$('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').find('.bx_filter_input_checkbox').length
                            &&
                            $('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').find('.bx_filter_input_checkbox').length>0)
                            {
                                $('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').parents('.bx_filter_parameters_box').find('.page-header').hide();
                                $('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').prev().hide();
                            } else
                            {
                                $('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').parents('.bx_filter_parameters_box').find('.page-header').show();
                                $('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').prev().show();
                            }*/
                        }
						else
                        {
							BX.addClass(control.parentNode, 'disabled');
                        }
					}
					else
					{
						if (label)
                        {
                            $('[data-role="label_'+value.CONTROL_ID+'"]').parent().find('.checkbox').removeClass('disabled');
                            $('[data-role="label_'+value.CONTROL_ID+'"]').parent().find('.overlay').removeClass('disabled');
                            $('[data-role="label_'+value.CONTROL_ID+'"]').parent().removeClass('disabled');
							BX.removeClass(label, 'disabled');
                            /*if ($('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').find('.bx_filter_input_checkbox.disabled').length==$('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').find('.bx_filter_input_checkbox').length
                            &&
                            $('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').find('.bx_filter_input_checkbox').length>0)
                            {
                                $('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').parents('.bx_filter_parameters_box').find('.page-header').hide();
                                $('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').prev().hide();
                            } else
                            {
                                $('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').parents('.bx_filter_parameters_box').find('.page-header').show();
                                $('[data-role="label_'+value.CONTROL_ID+'"]').parents('.filter_props_container').prev().show();
                            }*/
                        }    
						else
                        {
							BX.removeClass(control.parentNode, 'disabled');
                        }
					}
                    
                    if ($('.colors_container label.disabled').length==$('.colors_container label').length)
                    {
                        $('.colors_container').find('.page-header').hide();
                    } else
                    {
                        $('.colors_container').find('.page-header').show();
                    }

					if (value.hasOwnProperty('ELEMENT_COUNT'))
					{
						label = document.querySelector('[data-role="count_'+value.CONTROL_ID+'"]');
						if (label)
							label.innerHTML = value.ELEMENT_COUNT;
					}
				}
			}
		}
	}
};

JCSmartFilter.prototype.postHandler = function (result, fromCache)
{
	var hrefFILTER, url, curProp;
	var modef = BX('modef');
	var modef_num = BX('modef_num');
    $('#overlay_filter').hide();
    $('.panel-smart-filter.filter_vertical').removeClass('blured');
	if (!!result && !!result.ITEMS)
	{
		for(var PID in result.ITEMS)
		{
			if (result.ITEMS.hasOwnProperty(PID))
			{
				this.updateItem(PID, result.ITEMS[PID]);
			}
		}

		if (!!modef && !!modef_num)
		{
			modef_num.innerHTML = result.ELEMENT_COUNT;
			hrefFILTER = BX.findChildren(modef, {tag: 'A'}, true);

			if (result.FILTER_URL && hrefFILTER)
			{
				hrefFILTER[0].href = BX.util.htmlspecialcharsback(result.FILTER_URL);
                hrefFILTER[0].href=removeURLParameter(hrefFILTER[0].href,'PAGEN_1');
                hrefFILTER[0].href=removeURLParameter(hrefFILTER[0].href,'its_catalog');
                if ($('#go_filter').length>0)
                {
                    if ($('#is_s_page').length>0)
                    {
                        var newFilterURL=hrefFILTER[0].href.split('/filter/');
                        hrefFILTER[0].href=$('#is_s_page').val()+"filter/"+newFilterURL[1];
                    }
                    $('#go_filter').attr('href',hrefFILTER[0].href);
                }
			}

			if (result.FILTER_AJAX_URL && result.COMPONENT_CONTAINER_ID)
			{
				BX.bind(hrefFILTER[0], 'click', function(e)
				{
					url = BX.util.htmlspecialcharsback(result.FILTER_AJAX_URL);
                    
					BX.ajax.insertToNode(url, result.COMPONENT_CONTAINER_ID);
                    
					return BX.PreventDefault(e);
				});
			}

			if (result.INSTANT_RELOAD && result.COMPONENT_CONTAINER_ID)
			{
				url = BX.util.htmlspecialcharsback(result.FILTER_AJAX_URL);
				//BX.ajax.insertToNode(url, result.COMPONENT_CONTAINER_ID);
                BX.ajax.post(
                 url,
                 "",
                 function(res) {
                    $('#'+result.COMPONENT_CONTAINER_ID).html(res);
                    if ($('.shops_catalog').length>0)
                    {
                        //initShopsCarousel(); 
                        console.log(123123) 
                        initPhotoSwipeFromDOM('.shop_item .slick-track',false);
                        $('#mobile_panel_closer').trigger(eventClick1);
                    }
                 }
                );
                
			}
			else
			{
				if (modef.style.display === 'none')
				{
					modef.style.display = 'inline-block';
				}
				curProp = BX.findChild(BX.findParent(this.curFilterinput, {'class':'bx_filter_parameters_box'}), {'class':'bx_filter_container_modef'}, true, false);
				
                if (!$('.panel-smart-filter').hasClass('filter_horizontal'))
                {
                    curProp.appendChild(modef);
                }
                if (result.ELEMENT_COUNT==0)
                {
                    $('#modef a').hide();
                    $('#modef .nothing').show();
                   
                } else
                {
                    $('#modef a').show();
                    $('#modef .nothing').hide();
                }
			}
		}
        //$('#go_filter span').css('display','block');
        //$('#go_filter span').html('Найдено: '+result.ELEMENT_COUNT);
        
        if ($('#modef').parent().parent().hasClass('colors_container'))
        {
            modefTop=35;
        }
        modefTop=modefTop-55;
        $('.panel-smart-filter .bx_filter_popup_result.right').css('top',modefTop+'px');
        if (result.ELEMENT_COUNT==0)
        {
            $('#go_filter').addClass('disabled');
        } else
        {
            $('#go_filter').removeClass('disabled');
        }
	}

	if (!fromCache && this.cacheKey !== '')
	{
		this.cache[this.cacheKey] = result;
	}
	this.cacheKey = '';
};

JCSmartFilter.prototype.gatherInputsValues = function (values, elements)
{
	if(elements)
	{
		for(var i = 0; i < elements.length; i++)
		{
			var el = elements[i];
			if (el.disabled || !el.type)
				continue;

			switch(el.type.toLowerCase())
			{
				case 'text':
				case 'textarea':
				case 'password':
				case 'hidden':
				case 'select-one':
					if(el.value.length)
						values[values.length] = {name : el.name, value : el.value.replace(/ /g,"")};
					break;
				case 'radio':
				case 'checkbox':
					if(el.checked)
						values[values.length] = {name : el.name, value : el.value};
					break;
				case 'select-multiple':
					for (var j = 0; j < el.options.length; j++)
					{
						if (el.options[j].selected)
							values[values.length] = {name : el.name, value : el.options[j].value};
					}
					break;
				default:
					break;
			}
		}
	}
};

JCSmartFilter.prototype.values2post = function (values)
{
	var post = [];
	var current = post;
	var i = 0;

	while(i < values.length)
	{
		var p = values[i].name.indexOf('[');
		if(p == -1)
		{
			current[values[i].name] = values[i].value;
			current = post;
			i++;
		}
		else
		{
			var name = values[i].name.substring(0, p);
			var rest = values[i].name.substring(p+1);
			if(!current[name])
				current[name] = [];

			var pp = rest.indexOf(']');
			if(pp == -1)
			{
				//Error - not balanced brackets
				current = post;
				i++;
			}
			else if(pp == 0)
			{
				//No index specified - so take the next integer
				current = current[name];
				values[i].name = '' + current.length;
			}
			else
			{
				//Now index name becomes and name and we go deeper into the array
				current = current[name];
				values[i].name = rest.substring(0, pp) + rest.substring(pp+1);
			}
		}
	}
	return post;
};

JCSmartFilter.prototype.hideFilterProps = function(element)
{
	var easing;
	var obj = element.parentNode;
	var filterBlock = BX.findChild(obj, {className:"bx_filter_block"}, true, false);

	if(BX.hasClass(obj, "active"))
	{
		easing = new BX.easing({
			duration : 300,
			start : { opacity: 1,  height: filterBlock.offsetHeight },
			finish : { opacity: 0, height:0 },
			transition : BX.easing.transitions.quart,
			step : function(state){
				filterBlock.style.opacity = state.opacity;
				filterBlock.style.height = state.height + "px";
			},
			complete : function() {
				filterBlock.setAttribute("style", "");
				BX.removeClass(obj, "active");
			}
		});
		easing.animate();
	}
	else
	{
		filterBlock.style.display = "block";
		filterBlock.style.opacity = 0;
		filterBlock.style.height = "auto";

		var obj_children_height = filterBlock.offsetHeight;
		filterBlock.style.height = 0;

		easing = new BX.easing({
			duration : 300,
			start : { opacity: 0,  height: 0 },
			finish : { opacity: 1, height: obj_children_height },
			transition : BX.easing.transitions.quart,
			step : function(state){
				filterBlock.style.opacity = state.opacity;
				filterBlock.style.height = state.height + "px";
			},
			complete : function() {
			}
		});
		easing.animate();
		BX.addClass(obj, "active");
	}
};

JCSmartFilter.prototype.showDropDownPopup = function(element, popupId)
{
	var contentNode = element.querySelector('[data-role="dropdownContent"]');
	BX.PopupWindowManager.create("smartFilterDropDown"+popupId, element, {
		autoHide: true,
		offsetLeft: 0,
		offsetTop: 3,
		overlay : false,
		draggable: {restrict:true},
		closeByEsc: true,
		content: contentNode
	}).show();
};

JCSmartFilter.prototype.selectDropDownItem = function(element, controlId)
{
	var labelFor=element.htmlFor
    if (!$('label[for="'+labelFor+'"]').hasClass('disabled'))
    {
        this.keyup(BX(controlId));
    	var wrapContainer = BX.findParent(BX(controlId), {className:"bx_filter_select_container"}, false);
    
    	var currentOption = wrapContainer.querySelector('[data-role="currentOption"]');
    	currentOption.innerHTML = element.innerHTML;
    	
        $('.bx_filter_select_popup').hide();
    }
};

var smartFilter = new JCSmartFilter(smartFilterAjax, 'vertical');

$(document).ready(function() {
    BX.namespace("BX.Iblock.SmartFilter");
    BX.Iblock.SmartFilter = (function()
    {
    	var SmartFilter = function(arParams)
    	{
    		if (typeof arParams === 'object')
    		{
    			this.isTouch = false;
    			this.init();
    		}
    	};
    	return SmartFilter;
    })();


    $(document).on('click','.panel-smart-filter .bx_filter_input_checkbox .overlay',function(event){
        if (!$(this).hasClass('disabled'))
        {
            $(this).next().click();
            modefTop=$(this).parents('.bx_filter_input_checkbox').position().top;
            console.log(modefTop);
        }
    });
    
    $(document).on('click','.checkbox',function(){
        if (!$(this).hasClass('disabled'))
        {
            $(this).prev().click();
            modefTop=$(this).parents('.bx_filter_input_checkbox').position().top;
            console.log(modefTop);
        } 
    });
    
    $(document).on('click','.panel-smart-filter .bx_filter_input_checkbox label',function(){
        if (!$(this).hasClass('disabled'))
        {
            $(this).prev().click();
            modefTop=$(this).parents('.bx_filter_input_checkbox').position().top;
            console.log(modefTop);
        } 
    });
    
   
    $(document).on('click','.bx_filter_select_text,.bx_filter_select_arrow',function(e){
        e.stopPropagation();
        var curPopUp=$(this).parent().find('.bx_filter_select_popup');
        if (curPopUp.is(':visible'))
        {
            $('.bx_filter_select_popup').hide();
        } else
        {
            $('.bx_filter_select_popup').hide();
            curPopUp.show();
        }
    });
    
    $('body').click(function(event){
        if ($(event.target).parents('.filter_vertical').length==0)
        {
            $('.filter_vertical .bx_filter_popup_result').hide();
        }
        if(event.target.className.indexOf('bx_filter_select_text')===-1) 
        {
            $('.bx_filter_select_popup').hide();
        }
    });
    
    $('form.smartfilter').submit(function(){
        if ($('#modef').is(':visible'))
        {
            location.href=$('#modef a').attr('href');
        }
        return false; 
    });
    
    $(document).on('keyup','#brand_quick_search input',function(){
        var searchVal=$.trim($(this).val().toLowerCase());
        $('.brands_container .prop_text').each(function(){
            var curVal=$.trim($(this).html().toLowerCase());
            console.log(curVal.indexOf(searchVal));
            if (curVal.indexOf(searchVal)!=-1)
            {
                if (!$(this).parents('.bx_filter_input_checkbox').hasClass('disabled'))
                {
                    $(this).parents('.bx_filter_input_checkbox').show();
                }
            } else
            {
                $(this).parents('.bx_filter_input_checkbox').hide();
            }
        })
    });
    
    $('.bx_sm_slider').each(function(){
        var curID=$(this).attr('id');
        var slider = document.getElementById(curID);
        var lowerVal=parseInt($(this).data('min'));
        var upperVal=parseInt($(this).data('max'));
        var valueInputMin = $(this).parents('.bx_filter_parameters_box_container').find('.min-price').first(),
        	valueInputMax = $(this).parents('.bx_filter_parameters_box_container').find('.max-price').first();
            
        var valueInputMinVal=valueInputMin.val();
        var valueInputMaxVal=valueInputMax.val();
        if (valueInputMinVal=="") valueInputMinVal=$(this).data('min');
        if (valueInputMaxVal=="") valueInputMaxVal=$(this).data('max');
        
        valueInputMinVal=parseInt(valueInputMinVal);
        valueInputMaxVal=parseInt(valueInputMaxVal);
        
        noUiSlider.create(slider, {
        	start: [ valueInputMinVal, valueInputMaxVal ], // Handle start position
        	step: 100, // Slider moves in increments of '10'

        	connect: true, // Display a colored bar between the handles
        	range: { // Slider can select '0' to '100'
        		'min': lowerVal,
        		'max': upperVal
        	}
        });

        
        
        // When the slider value changes, update the input and span
        slider.noUiSlider.on('update', function( values, handle ) {
        	if ( handle ) {
        	    if (values[handle]!=upperVal)
                {
     		         valueInputMax.val(formatPrice(Math.round(values[handle])));
                     
                } else
                {
                    valueInputMax.val('');
                }
        	} else {
        	    if (values[handle]!=lowerVal)
                {
                    valueInputMin.val(formatPrice(Math.round(values[handle])));
                } else
                {
                    valueInputMin.val('');
                }
        	}
            modefTop=$('input.min-price').position().top+55;
        });
        
        slider.noUiSlider.on('change', function( values, handle ) {
        	if ( handle ) {
        	    if (values[handle]!=upperVal)
                {
     		         valueInputMax.val(formatPrice(Math.round(values[handle])));
                } else
                {
                    valueInputMax.val('');
                }
                valueInputMax.trigger('keyup');
        	} else {
        	    if (values[handle]!=lowerVal)
                {
                    valueInputMin.val(formatPrice(Math.round(values[handle])));
                } else
                {
                    valueInputMin.val('');
                }
                valueInputMin.trigger('keyup');
             }
             modefTop=$('input.min-price').position().top+55;
        });
        
        // When the input changes, set the slider value
        valueInputMax.on('change', function(){
        	slider.noUiSlider.set([null, this.value]);
            modefTop=$('input.min-price').position().top+55;
        });
        valueInputMin.on('change', function(){
        	slider.noUiSlider.set([this.value,null]);
            modefTop=$('input.min-price').position().top+55;
        });
    });
});
}