/*
    jQuery Masked Input Plugin
    Copyright (c) 2007 - 2015 Josh Bush (digitalbush.com)
    Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
    Version: 1.4.1
*/
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof exports?require("jquery"):jQuery)}(function(a){var b,c=navigator.userAgent,d=/iphone/i.test(c),e=/chrome/i.test(c),f=/android/i.test(c);a.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},autoclear:!0,dataName:"rawMaskFn",placeholder:"_"},a.fn.extend({caret:function(a,b){var c;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof a?(b="number"==typeof b?b:a,this.each(function(){this.setSelectionRange?this.setSelectionRange(a,b):this.createTextRange&&(c=this.createTextRange(),c.collapse(!0),c.moveEnd("character",b),c.moveStart("character",a),c.select())})):(this[0].setSelectionRange?(a=this[0].selectionStart,b=this[0].selectionEnd):document.selection&&document.selection.createRange&&(c=document.selection.createRange(),a=0-c.duplicate().moveStart("character",-1e5),b=a+c.text.length),{begin:a,end:b})},unmask:function(){return this.trigger("unmask")},mask:function(c,g){var h,i,j,k,l,m,n,o;if(!c&&this.length>0){h=a(this[0]);var p=h.data(a.mask.dataName);return p?p():void 0}return g=a.extend({autoclear:a.mask.autoclear,placeholder:a.mask.placeholder,completed:null},g),i=a.mask.definitions,j=[],k=n=c.length,l=null,a.each(c.split(""),function(a,b){"?"==b?(n--,k=a):i[b]?(j.push(new RegExp(i[b])),null===l&&(l=j.length-1),k>a&&(m=j.length-1)):j.push(null)}),this.trigger("unmask").each(function(){function h(){if(g.completed){for(var a=l;m>=a;a++)if(j[a]&&C[a]===p(a))return;g.completed.call(B)}}function p(a){return g.placeholder.charAt(a<g.placeholder.length?a:0)}function q(a){for(;++a<n&&!j[a];);return a}function r(a){for(;--a>=0&&!j[a];);return a}function s(a,b){var c,d;if(!(0>a)){for(c=a,d=q(b);n>c;c++)if(j[c]){if(!(n>d&&j[c].test(C[d])))break;C[c]=C[d],C[d]=p(d),d=q(d)}z(),B.caret(Math.max(l,a))}}function t(a){var b,c,d,e;for(b=a,c=p(a);n>b;b++)if(j[b]){if(d=q(b),e=C[b],C[b]=c,!(n>d&&j[d].test(e)))break;c=e}}function u(){var a=B.val(),b=B.caret();if(o&&o.length&&o.length>a.length){for(A(!0);b.begin>0&&!j[b.begin-1];)b.begin--;if(0===b.begin)for(;b.begin<l&&!j[b.begin];)b.begin++;B.caret(b.begin,b.begin)}else{for(A(!0);b.begin<n&&!j[b.begin];)b.begin++;B.caret(b.begin,b.begin)}h()}function v(){A(),B.val()!=E&&B.change()}function w(a){if(!B.prop("readonly")){var b,c,e,f=a.which||a.keyCode;o=B.val(),8===f||46===f||d&&127===f?(b=B.caret(),c=b.begin,e=b.end,e-c===0&&(c=46!==f?r(c):e=q(c-1),e=46===f?q(e):e),y(c,e),s(c,e-1),a.preventDefault()):13===f?v.call(this,a):27===f&&(B.val(E),B.caret(0,A()),a.preventDefault())}}function x(b){if(!B.prop("readonly")){var c,d,e,g=b.which||b.keyCode,i=B.caret();if(!(b.ctrlKey||b.altKey||b.metaKey||32>g)&&g&&13!==g){if(i.end-i.begin!==0&&(y(i.begin,i.end),s(i.begin,i.end-1)),c=q(i.begin-1),n>c&&(d=String.fromCharCode(g),j[c].test(d))){if(t(c),C[c]=d,z(),e=q(c),f){var k=function(){a.proxy(a.fn.caret,B,e)()};setTimeout(k,0)}else B.caret(e);i.begin<=m&&h()}b.preventDefault()}}}function y(a,b){var c;for(c=a;b>c&&n>c;c++)j[c]&&(C[c]=p(c))}function z(){B.val(C.join(""))}function A(a){var b,c,d,e=B.val(),f=-1;for(b=0,d=0;n>b;b++)if(j[b]){for(C[b]=p(b);d++<e.length;)if(c=e.charAt(d-1),j[b].test(c)){C[b]=c,f=b;break}if(d>e.length){y(b+1,n);break}}else C[b]===e.charAt(d)&&d++,k>b&&(f=b);return a?z():k>f+1?g.autoclear||C.join("")===D?(B.val()&&B.val(""),y(0,n)):z():(z(),B.val(B.val().substring(0,f+1))),k?b:l}var B=a(this),C=a.map(c.split(""),function(a,b){return"?"!=a?i[a]?p(b):a:void 0}),D=C.join(""),E=B.val();B.data(a.mask.dataName,function(){return a.map(C,function(a,b){return j[b]&&a!=p(b)?a:null}).join("")}),B.one("unmask",function(){B.off(".mask").removeData(a.mask.dataName)}).on("focus.mask",function(){if(!B.prop("readonly")){clearTimeout(b);var a;E=B.val(),a=A(),b=setTimeout(function(){B.get(0)===document.activeElement&&(z(),a==c.replace("?","").length?B.caret(0,a):B.caret(a))},10)}}).on("blur.mask",v).on("keydown.mask",w).on("keypress.mask",x).on("input.mask paste.mask",function(){B.prop("readonly")||setTimeout(function(){var a=A(!0);B.caret(a),h()},0)}),e&&f&&B.off("input.mask").on("input.mask",u),A()})}})});
function showAjaxError()
{
    var ajaxErrorMessage="При загрузке данных произошла ошибка! Возможно, отсутствует соединение с Internet, попробуйте, пожалуйста, позже";
    alert(ajaxErrorMessage);
}

function foto_HandleChanges(el)
{
    var cntFiles=el.parents('.customFormContent').find('.file')[0].files.length; 
    if (cntFiles>0)
    {
        var file_name=el.val();
        reWin=/.*\\(.*)/;
        var fileTitle=file_name.replace(reWin,"$1");
        reUnix=/.*\/(.*)/;
        fileTitle=fileTitle.replace(reUnix,"$1");
        el.parents('.customFormContent').find('.file_placeholder').text(fileTitle);
    } else
    {
        el.parents('.customFormContent').find('.file_placeholder').text(fileTitle);    
    }
    
    
};

$(document).ready(function() {
    $(document).on('click','.file_placeholder',function(){
       $(this).parents('.customFormContent').find('.file').click(); 
    });
});

$(window).load(function() {
    //Вешаем на контейнер инициализацию формы
    $('.customFormContainer').each(function(){
        customFormInit($(this), $(this));        
    });
    //Вешаем на ссылку инициализацию формы
    $(document).on('click','.customFormLink',function(){
        customFormInit('body', $(this));
        return false;
    });
    
    //Очищаем числовые поля от лишних символов
    $(document).on('keyup blur','.customFormDigitalField',function(){
        this.value = this.value.replace (/[\D]+/, '');
    });
    
    $(document).on('focus','.customFormFieldInputText',function(){
        $(this).removeClass('customErrorField');
        $('.customFormErrors').fadeOut(300);
    });
    
    //Вешаемся на отправку формы 
    $(document).on('submit','.customForm form',function(){
        var curForm=$(this);
        var curSubmitBTN=curForm.find('input[type="submit"]');
        var btnText=curSubmitBTN.val();
        var curFormErrorContainer=curForm.find('.customFormErrors');
        if (curForm.parents('.customFormContainer').length>0)
        {
            var needRefreshForm=true;
        } else
        {
            var needRefreshForm=false;
        }
        curFormErrorContainer.hide();
        
        /*Данные счётчика метрики*/
        var metrikaCounter=curForm.find('input.metrikaCounter').val();
        var metrikaTargetTry=curForm.find('input.metrikaTargetTry').val();
        var metrikaTargetSuccess=curForm.find('input.metrikaTargetSuccess').val();
        if (metrikaCounter && metrikaTargetTry && metrikaTargetSuccess)
        {
            window['yaCounter'+metrikaCounter].reachGoal(metrikaTargetTry);
        }
        /*Данные счётчика метрики --- конец*/
        
        /*Данные счётчика GA*/
        var gaTargetTry=curForm.find('input.gaTargetTry').val();
        var gaTargetSuccess=curForm.find('input.gaTargetSuccess').val();
        if (gaTargetTry && gaTargetSuccess)
        {
            ga('send', 'event', 'button', 'click', gaTargetTry);
        }
        /*Данные счётчика GA --- конец*/
        
        var redirectURL=curForm.find('input.redirectURL').val();
        
        var curName=curForm.find('input[name="name"]').first();
        var curEmail=curForm.find('input[name="email"]').first();
        var curPhone=curForm.find('input[name="phone"]').first();
        var curTextarea=curForm.find('textarea[name="textarea"]').first();
        var curCustomField1=curForm.find('input[name="customField1"]').first();
        var curCustomField2=curForm.find('input[name="customField2"]').first();

        var curFormCode=curForm.find('input[name="form_code"]').first().val();

        var formIsEmpty=false;
        var curNameVal=$.trim(curName.val());
        var curEmailVal=$.trim(curEmail.val());
        var curPhoneVal=$.trim(curPhone.val());
        var curTextareaVal=$.trim(curTextarea.val());
        var curCustomField1Val=$.trim(curCustomField1.val());
        var curCustomField2Val=$.trim(curCustomField2.val());
        //var curFormName=curForm.find('.customFormName').html();
        //var curFormText=curForm.find('.customFormText').html();
        
        var initParams=curForm.data('init-params');
        
        //var curFormTitle=curForm.find('input[name="form_title"]').val(curFormName);
        startWaitForm(curSubmitBTN);
        
        //проверяем имя
        if (curName.length!=0)
        {
            if (curName.hasClass('customFormFieldRequired'))
            {
                if (curNameVal.length<3)
                {
                    curName.addClass('customErrorField');
                    curForm.find('.customFormErrorsNameEmpty').show();
                } else
                {
                    curName.removeClass('customErrorField');
                    curForm.find('.customFormErrorsNameEmpty').hide();
                }
            }   
        }
        
        //проверяем e-mail
        if (curEmail.length!=0)
        {
            if (curEmail.hasClass('customFormFieldRequired'))
            {
                curForm.find('.customFormErrorsMailError').hide();
                curForm.find('.customFormErrorsMailEmpty').hide();
                curForm.find('.customFormErrorsMailDomainError').hide();
                if (curEmailVal.length>0)
                {
                    var email_pattern=/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,14}$/i;
                    if (email_pattern.test(curEmailVal))
                    {
                        curForm.find('.customFormErrorsMailError').hide();
                        $.ajax
                        ({
                            url: "/ajax/checkDomain.php",
                            data: {mail:curEmailVal},
                            type: "POST", 
                            dataType: 'json', 
                            async: false, 
                            cache: false,   
                            success: function(result)
                            {
                                if (result.success=="1")
                                {
                                    curEmail.removeClass('customErrorField');
                                    curForm.find('.customFormErrorsMailDomainError').hide();
                                    
                                    
                                } else
                                {
                                    curEmail.addClass('customErrorField');
                                    curForm.find('.customFormErrorsMailDomainError').show();
                                    
                                }
                            },
                        	error:  function(xhr, ajaxOptions, thrownError){
                        	    var ajaxStatus="Status: "+xhr.status+" Error: "+thrownError+"\n"+ajaxOptions; 
                        	}
                        });
                    } else
                    {
                        curEmail.addClass('customErrorField');
                        curForm.find('.customFormErrorsMailError').show();
                        
                    }
                } else
                {
                    curEmail.addClass('customErrorField');
                    curForm.find('.customFormErrorsMailEmpty').show();
                    
                }
            }   
        }
        
        //проверяем телефон
        if (curPhone.hasClass('customFormFieldRequired'))
        {
            curForm.find('.customFormErrorsPhoneEmpty').hide();
            if (curPhoneVal.length!=17)
            {    
                curPhone.addClass('customErrorField');
                curForm.find('.customFormErrorsPhoneEmpty').show();
            } else
            {
                curPhone.removeClass('customErrorField');
                curForm.find('.customFormErrorsPhoneEmpty').hide();
            }
        }
        
        //проверяем текстовое поле
        if (curTextarea.hasClass('customFormFieldRequired'))
        {
           if (curTextareaVal.length>0)
           {
               curTextarea.removeClass('customErrorField');
               curForm.find('.customFormErrorsTextareaError').hide();
           } else
           {
               curTextarea.addClass('customErrorField');
               curForm.find('.customFormErrorsTextareaError').show();
           }
        }

        //проверяем кастомное поле 1
        if (curCustomField1.hasClass('customFormFieldRequired'))
        {
           if (curCustomField1Val.length>0)
           {
               curCustomField1.removeClass('customErrorField');
               curForm.find('.customFormErrorsCustomField1Error').hide();
           } else
           {
               curCustomField1.addClass('customErrorField');
               curForm.find('.customFormErrorsCustomField1Error').show();
           }
        }

        //проверяем кастомное поле 2
        if (curCustomField2.hasClass('customFormFieldRequired'))
        {
           if (curCustomField2Val.length>0)
           {
               curCustomField2.removeClass('customErrorField');
               curForm.find('.customFormErrorsCustomField2Error').hide();
           } else
           {
               curCustomField2.addClass('customErrorField');
               curForm.find('.customFormErrorsCustomField2Error').show();
           }
        }
        
        //Если ошибок заполнения полей нет, пытаемся отправить форму
        if (curForm.find('.customErrorField').length==0)
        {
            var form = curForm[0]; // You need to use standart javascript object here
            var formData = new FormData(form);
            
            if (curForm.find('.file').length>0 && curForm.find('.file')[0].files.length>0)
            {
                formData.append('file', curForm.find('.file')[0].files[0]);
                formData.append('file1', curForm.find('.file')[0].files[1]);
                formData.append('file2', curForm.find('.file')[0].files[2]);
                formData.append('file3', curForm.find('.file')[0].files[3]);
                formData.append('file4', curForm.find('.file')[0].files[4]);
                formData.append('file5', curForm.find('.file')[0].files[5]);
                formData.append('file6', curForm.find('.file')[0].files[6]);
                formData.append('file7', curForm.find('.file')[0].files[7]);
                formData.append('file8', curForm.find('.file')[0].files[8]);
                formData.append('file9', curForm.find('.file')[0].files[9]);
            }
            
            $.ajax
            ({
                url: "/ajax/sendForm.php",
                data: formData,
                contentType: false,
                processData: false,
                type: "POST",
                dataType: 'json',  
                cache: false,   
                success: function(res)
                {
                    if (res.success=="1")
                    {
                        if (metrikaCounter && metrikaTargetSuccess)
                        {
                            window['yaCounter'+metrikaCounter].reachGoal(metrikaTargetSuccess);
                        }
                        if (gaTargetTry && gaTargetSuccess)
                        {
                            ga('send', 'event', 'button', 'click', gaTargetSuccess);
                        }
                        if (redirectURL)
                        {
                            location.href=redirectURL;
                        } else
                        {
                            var curSuccessText=curForm.find('.successText').text();
                            if (curSuccessText=='undefined' || curSuccessText=="")
                            {
                                curSuccessText=success_request_message;
                            }
                            $.magnificPopup.open({
                                items: {
                                    src: $('<div class="white-popup white-popup-block zoom-anim-dialog request_popup"><div class="popup_header customFormName">Спасибо!</div><div class="request_success">'+curSuccessText+'</div></div>'),
                                    type: 'inline'
                                },
                        		preloader: false,
                        		modal: false,
                                removalDelay: 300,
                                closeOnBgClick: false,
                                mainClass: 'my-mfp-slide-bottom',
                                callbacks: {
                                    open: function() {
                                        if (IS_GADGET)
                                        {
                                            document.location.hash='popup';
                                            observerCloseMagnificPopup();
                                            $.magnificPopup.instance.close = function () {
                                                removeHashForPopups();    
                                                $.magnificPopup.proto.close.call(this);
                                            };
                                        }
                                    }
                                }                            
                            });
                            if (needRefreshForm)
                            {
                        	    customFormInit(curForm.parents('.customFormContainer'), curForm.parents('.customFormContainer'));
                            }      
                            stopWaitForm(curSubmitBTN,btnText);              
                        }
                    } else
                    {
                        stopWaitForm(curSubmitBTN,btnText);
                        alert(res.error);
                    }
                },
            	error:  function(xhr, ajaxOptions, thrownError){
            	    var ajaxStatus="Status: "+xhr.status+" Error: "+thrownError+"\n"+ajaxOptions;
            	    stopWaitForm(curSubmitBTN,btnText);
                    showAjaxError();
            	}              
            });                
        } else
        {
            stopWaitForm(curSubmitBTN,btnText,curFormErrorContainer,curForm,true);  
        }
        return false;
    });    
});

//Функция, которая стартует перед отправкой формы и показывает процесс ожидания работы формы
function startWaitForm(formSubmitBtnObj)
{
    formSubmitBtnObj.val('Подождите...');
    formSubmitBtnObj.attr('disabled','disabled');
}
//Функция, которая стартует после отправки формы, завершает ожидание
function stopWaitForm(formSubmitBtnObj,btnText,curFormErrorContainer,curForm,needShowErrors)
{
    window.setTimeout(function () {
        formSubmitBtnObj.removeAttr('disabled');
        formSubmitBtnObj.val(btnText);
        
        if (needShowErrors)
        {
            curFormErrorContainer.fadeIn(300);
            var curFormErrorContainerHeight=curFormErrorContainer.outerHeight();
            var curFormErrorContainerBottom=curFormErrorContainerHeight-curForm.find('.customFormFields').position().top;
            if (curFormErrorContainerBottom>0)
            {
                curFormErrorContainer.css('top','-'+curFormErrorContainerBottom+'px');
            } else
            {
                curFormErrorContainer.css('top',Math.abs(curFormErrorContainerBottom)+'px');
            }
        }
    }, 10);        
}
/*Инициализация формы*/
function customFormInit(parentContainer, objInitiator)
{
    var title=objInitiator.data('title');
    var text=objInitiator.data('text');
    var text_bottom_before_btn=objInitiator.data('text-bottom-before-btn');
    var text_bottom_after_btn=objInitiator.data('text-bottom-after-btn');
    
    var initParams=objInitiator.data('params');

    var initParamsIndexes=''+objInitiator.data('params-indexes')+'';
    if (initParamsIndexes=='undefined') initParamsIndexes='13245';

    var col_class=objInitiator.data('col-class');
    if (col_class==undefined) col_class='col-xs-12';
    
    var btn_class=objInitiator.data('btn-class');
    if (btn_class==undefined) btn_class='';
    
    var successText=objInitiator.data('success-text');
    var metrikaCounter=objInitiator.data('metrika-counter');
    var metrikaTargetTry=objInitiator.data('metrika-target-try');
    var metrikaTargetSuccess=objInitiator.data('metrika-target-success');
    
    var gaTargetTry=objInitiator.data('ga-target-try');
    var gaTargetSuccess=objInitiator.data('ga-target-success');
    
    var redirectURL=objInitiator.data('redirect-url');
    var bntText=objInitiator.data('btn-text');
    var formCode=objInitiator.data('form-code');
    var serviceFormName=objInitiator.data('service-form-name');
    
    var fieldTitleName=objInitiator.data('field-title-name');
    var fieldTitleEmail=objInitiator.data('field-title-email');
    var fieldTitlePhone=objInitiator.data('field-title-phone');
    var fieldTitleFile=objInitiator.data('field-title-file');
    var customTextareaTitle=objInitiator.data('custom-textarea-title');
    var fieldPlaceholderName=objInitiator.data('field-placeholder-name');
    var fieldPlaceholderEmail=objInitiator.data('field-placeholder-email');
    var fieldPlaceholderPhone=objInitiator.data('field-placeholder-phone');
    var fieldPlaceholderFile=objInitiator.data('field-placeholder-file');
    var customTextareaPlaceholder=objInitiator.data('custom-textarea-placeholder');
    
    var showName=initParams[0];
    var showMail=initParams[1];
    var showPhone=initParams[2];
    var showTextarea=initParams[3];
    var showFile=initParams[4];
    var needName=initParams[6];
    var needMail=initParams[7];
    var needPhone=initParams[8];
    var needTextarea=initParams[9];
    var needFile=initParams[10];
    var showNameIndex=initParamsIndexes[0];
    var showMailIndex=initParamsIndexes[1];
    var showPhoneIndex=initParamsIndexes[2];
    var showTextareaIndex=initParamsIndexes[3];
    var showFileIndex=initParamsIndexes[4];

    var customField1Title=objInitiator.data('custom-field1-title');
    var customField1Placeholder=objInitiator.data('custom-field1-placeholder');
    var customField1Req=objInitiator.data('custom-field1-req');
    var customField1Index=objInitiator.data('custom-field1-index');

    var customField2Title=objInitiator.data('custom-field2-title');
    var customField2Placeholder=objInitiator.data('custom-field2-placeholder');
    var customField2Req=objInitiator.data('custom-field2-req');
    var customField2Index=objInitiator.data('custom-field2-index');

    var classRequired="";
    var star="";
    var formHTML="";

    if (customTextareaPlaceholder==undefined)
    {
        customTextareaPlaceholder="";
    }
    
    if (parentContainer=="body")
    {
        formHTML='<div id="customFormPopupContainer">';
    }
    
    formHTML+='<div class="customForm"><div class="customFormContent">';
    
    if (parentContainer=="body")
    {
        formHTML+='<div id="customFormCloseBtn"></div>';
    }
	
    if(formCode=='shapka_zvonok') ya="yaCounter45445452.reachGoal('RECALL'); return true;";
    formHTML+='<form method="post" data-init-params="'+initParams+'" class="form_'+formCode+'" onsubmit="'+ya+'"><div class="successText"><span>'+successText+'</span></div>';
    if (title!=undefined)
    {
        formHTML+='<div class="popup_header">'+title+'</div>';
        formHTML+='<input type="hidden" name="form_title" value="'+title+'" />';
    }
    if (text!=undefined)
    {
        formHTML+='<div class="customFormText">'+text+'</div>';
        formHTML+='<input type="hidden" name="form_text" value="'+text+'" />';
    }    
        
    formHTML+='<div class="customFormErrors"><div class="customFormErrorsNameEmpty">Введите "Имя"</div>'+
              '<div class="customFormErrorsPhoneEmpty">Введите номер телефона</div><div class="customFormErrorsMailEmpty">Введите E-mail</div><div class="customFormErrorsMailError">Неправильный формат e-mail</div>'+
              '<div class="customFormErrorsMailDomainError">Неправильно указан e-mail адрес</div>'+
              '<div class="customFormErrorsPhoneError">Неправильный формат номера телефона</div>';
    if (customField1Title!=undefined && customField1Placeholder!=undefined && customField1Req!=undefined && customField1Index!=undefined  )
    {
        if (customField1Title!="")
        {
            formHTML+='<div class="customFormErrorsCustomField1Error">Вы не заполнили поле "'+customField1Title+'"</div>';
        } else
        {
            formHTML+='<div class="customFormErrorsCustomField2Error">Вы не заполнили поле "'+customField1Placeholder+'"</div>';
        }
    }
    if (customField2Title!=undefined && customField2Placeholder!=undefined && customField2Req!=undefined && customField2Index!=undefined  )
    {
        if (customField2Title!="")
        {
            formHTML+='<div class="customFormErrorsTextareaError">Вы не заполнили поле "'+customField2Title+'"</div>';
        } else
        {
            formHTML+='<div class="customFormErrorsTextareaError">Вы не заполнили поле "'+customField2Placeholder+'"</div>';
        }
    }
    if (needTextarea=="Y")
    {
        if (customTextareaTitle!="")
        {
            formHTML+='<div class="customFormErrorsTextareaError">Вы не заполнили поле "'+customTextareaTitle+'"</div>';
        } else
        {
            formHTML+='<div class="customFormErrorsTextareaError">Вы не заполнили поле "'+customTextareaPlaceholder+'"</div>';
        }
    }
    formHTML+='</div><div class="customFormFields row">';

    var formCols=[];

    if (showName=="Y")
    {
        var formHTMLcol='<div class="col-vtop '+col_class+'">';

        classRequired="";
        star="";
        if (needName=="Y")
        {
            classRequired=" customFormFieldRequired";     
            star=" *";       
        }
        //Если существует параметр кастомного тайтла поля "Имя"
        if (fieldTitleName!=undefined)
        {
            //Если параметр кастомного тайтла поля "Имя" НЕ пустой
            if (fieldTitleName!="")
            {
                formHTMLcol+='<div class="customFormFieldTitle">'+fieldTitleName+star+'</div>';
            } else
            {
                //проверяем, указан ли кастомный placeholder
                //Если параметр кастомного тайтла поля "Имя" ПУСТОЙ и существует кастомный placeholder
                if (fieldPlaceholderName!=undefined)
                {
                    //Если кастомный placeholder ПУСТОЙ
                    //то выводим стандартный тайтл!
                    if (fieldPlaceholderName=="")
                    {
                        formHTMLcol+='<div class="customFormFieldTitle">Имя'+star+'</div>';
                    }
                }
            }
        } else
        {
            //Если НЕ существует параметр кастомного тайтла поля "Имя"
            formHTMLcol+='<div class="customFormFieldTitle">Имя'+star+'</div>';
        }
        
        //Если существует кастомный placeholder поля "Имя"
        if (fieldPlaceholderName!=undefined)
        {
            //Если он НЕ пустой
            if (fieldPlaceholderName!="")
            {
                //проверяем, существует ли кастомный тайтл
                if (fieldTitleName!=undefined)
                {
                    //если кастомный тайтл пустой, выводим кастомный placeholder с признаком обязательности заполнения
                    if (fieldTitleName=="")
                    {
                        formHTMLcol+='<div class="customFormFieldBody"><input type="text" name="name" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="'+fieldPlaceholderName+star+'" /></div>';
                    } else
                    {
                        //если кастомный тайтл НЕ пустой
                        //то просто подменяем placeholder
                        formHTMLcol+='<div class="customFormFieldBody"><input type="text" name="name" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="'+fieldPlaceholderName+'" /></div>';
                    }
                } else
                {
                    //если кастомный тайтл НЕ существует
                    //то просто подменяем placeholder
                    formHTMLcol+='<div class="customFormFieldBody"><input type="text" name="name" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="'+fieldPlaceholderName+'" /></div>';
                }
            } else
            {
                formHTMLcol+='<div class="customFormFieldBody"><input type="text" name="name" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="" /></div>';
            }
        } else
        {
            if (fieldTitleName!=undefined)
            {
                //если кастомный тайтл пустой, выводим стандартный placeholder с признаком обязательности заполнения
                if (fieldTitleName=="")
                {
                    formHTMLcol+='<div class="customFormFieldBody"><input type="text" name="name" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="Иван Иванович'+star+'" /></div>';
                } else
                {
                    //если кастомный тайтл НЕ пустой
                    //то просто выводим стандартный placeholder
                    formHTMLcol+='<div class="customFormFieldBody"><input type="text" name="name" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="Иван Иванович" /></div>';
                }
            } else
            {
                formHTMLcol+='<div class="customFormFieldBody"><input type="text" name="name" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="Иван Иванович" /></div>';
            }
        }  
        formHTMLcol+='</div>';
        formCols[showNameIndex]=formHTMLcol;
    } else
    {
        formCols[showNameIndex]='';
    }
    if (showPhone=="Y")
    {
        formHTMLcol='<div class="col-vtop '+col_class+'">';
        
        classRequired="";
        star="";
        if (needPhone=="Y")
        {
            classRequired=" customFormFieldRequired";   
            star=" *";         
        }        
        //Если существует параметр кастомного тайтла поля "Телефон"
        if (fieldTitlePhone!=undefined)
        {
            //Если параметр кастомного тайтла поля "Телефон" НЕ пустой
            if (fieldTitlePhone!="")
            {
                formHTMLcol+='<div class="customFormFieldTitle">'+fieldTitlePhone+star+'</div>';
            } else
            {
                //проверяем, указан ли кастомный placeholder
                //Если параметр кастомного тайтла поля "Телефон" ПУСТОЙ и существует кастомный placeholder
                if (fieldPlaceholderPhone!=undefined)
                {
                    //Если кастомный placeholder ПУСТОЙ
                    //то выводим стандартный тайтл!
                    if (fieldPlaceholderPhone=="")
                    {
                        formHTMLcol+='<div class="customFormFieldTitle">Ваш номер телефона'+star+'</div>';
                    }
                }
            }
        } else
        {
            //Если НЕ существует параметр кастомного тайтла поля "Телефон"
            formHTMLcol+='<div class="customFormFieldTitle">Ваш номер телефона'+star+'</div>';
        }
        
        //Если существует кастомный placeholder поля "Телефон"
        if (fieldPlaceholderPhone!=undefined)
        {
            //Если он НЕ пустой
            if (fieldPlaceholderPhone!="")
            {
                //проверяем, существует ли кастомный тайтл
                if (fieldTitlePhone!=undefined)
                {
                    //если кастомный тайтл пустой, выводим кастомный placeholder с признаком обязательности заполнения
                    if (fieldTitlePhone=="")
                    {
                        formHTMLcol+='<div class="customFormFieldBody"><input type="tel" name="phone" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="'+fieldPlaceholderPhone+star+'" /></div>';
                    } else
                    {
                        //если кастомный тайтл НЕ пустой
                        //то просто подменяем placeholder
                        formHTMLcol+='<div class="customFormFieldBody"><input type="tel" name="phone" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="'+fieldPlaceholderPhone+'" /></div>';
                    }
                } else
                {
                    //если кастомный тайтл НЕ существует
                    //то просто подменяем placeholder
                    formHTMLcol+='<div class="customFormFieldBody"><input type="tel" name="phone" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="'+fieldPlaceholderPhone+'" /></div>';
                }
            } else
            {
                formHTMLcol+='<div class="customFormFieldBody"><input type="tel" name="phone" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="" /></div>';
            }
        } else
        {
            if (fieldTitlePhone!=undefined)
            {
                //если кастомный тайтл пустой, выводим стандартный placeholder с признаком обязательности заполнения
                if (fieldTitlePhone=="")
                {
                    formHTMLcol+='<div class="customFormFieldBody"><input type="tel" name="phone" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="Ваш номер телефона'+star+'" /></div>';
                } else
                {
                    //если кастомный тайтл НЕ пустой
                    //то просто выводим стандартный placeholder
                    formHTMLcol+='<div class="customFormFieldBody"><input type="tel" name="phone" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="Ваш номер телефона" /></div>';
                }
            } else
            {
                formHTMLcol+='<div class="customFormFieldBody"><input type="tel" name="phone" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="Ваш номер телефона" /></div>';
            }
        }       
        formHTMLcol+='</div>';
        formCols[showPhoneIndex]=formHTMLcol;
    } else
    {
        formCols[showPhoneIndex]='';
    }
    if (showMail=="Y")
    {
        formHTMLcol='<div class="col-vtop '+col_class+'">';
        
        classRequired="";
        star="";
        if (needMail=="Y")
        {
            classRequired=" customFormFieldRequired";   
            star=" *";         
        }        
        //Если существует параметр кастомного тайтла поля "E-mail"
        if (fieldTitleEmail!=undefined)
        {
            //Если параметр кастомного тайтла поля "E-mail" НЕ пустой
            if (fieldTitleEmail!="")
            {
                formHTMLcol+='<div class="customFormFieldTitle">'+fieldTitleEmail+star+'</div>';
            } else
            {
                //проверяем, указан ли кастомный placeholder
                //Если параметр кастомного тайтла поля "E-mail" ПУСТОЙ и существует кастомный placeholder
                if (fieldPlaceholderEmail!=undefined)
                {
                    //Если кастомный placeholder ПУСТОЙ
                    //то выводим стандартный тайтл!
                    if (fieldPlaceholderEmail=="")
                    {
                        formHTMLcol+='<div class="customFormFieldTitle">Введите Ваш E-MAIL'+star+'</div>';
                    }
                }
            }
        } else
        {
            //Если НЕ существует параметр кастомного тайтла поля "E-mail"
            formHTMLcol+='<div class="customFormFieldTitle">Введите Ваш E-MAIL'+star+'</div>';
        }
        
        //Если существует кастомный placeholder поля "E-mail"
        if (fieldPlaceholderEmail!=undefined)
        {
            //Если он НЕ пустой
            if (fieldPlaceholderEmail!="")
            {
                //проверяем, существует ли кастомный тайтл
                if (fieldTitleEmail!=undefined)
                {
                    //если кастомный тайтл пустой, выводим кастомный placeholder с признаком обязательности заполнения
                    if (fieldTitleEmail=="")
                    {
                        formHTMLcol+='<div class="customFormFieldBody"><input type="email" name="email" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="'+fieldPlaceholderEmail+star+'" /></div>';
                    } else
                    {
                        //если кастомный тайтл НЕ пустой
                        //то просто подменяем placeholder
                        formHTMLcol+='<div class="customFormFieldBody"><input type="email" name="email" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="'+fieldPlaceholderEmail+'" /></div>';
                    }
                } else
                {
                    //если кастомный тайтл НЕ существует
                    //то просто подменяем placeholder
                    formHTMLcol+='<div class="customFormFieldBody"><input type="email" name="email" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="'+fieldPlaceholderEmail+'" /></div>';
                }
            } else
            {
                formHTMLcol+='<div class="customFormFieldBody"><input type="email" name="email" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="" /></div>';
            }
        } else
        {
            if (fieldTitleEmail!=undefined)
            {
                //если кастомный тайтл пустой, выводим стандартный placeholder с признаком обязательности заполнения
                if (fieldTitleEmail=="")
                {
                    formHTMLcol+='<div class="customFormFieldBody"><input type="email" name="email" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="ivan.ivanovich@mail.ru'+star+'" /></div>';
                } else
                {
                    //если кастомный тайтл НЕ пустой
                    //то просто выводим стандартный placeholder
                    formHTMLcol+='<div class="customFormFieldBody"><input type="email" name="email" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="ivan.ivanovich@mail.ru" /></div>';
                }
            } else
            {
                formHTMLcol+='<div class="customFormFieldBody"><input type="email" name="email" class="form-control customFormFieldInputText'+classRequired+'" maxlength="50" placeholder="ivan.ivanovich@mail.ru" /></div>';
            }
        }        
        formHTMLcol+='</div>';
        formCols[showMailIndex]=formHTMLcol;
    } else
    {
        formCols[showMailIndex]='';
    }
    if (showFile=="Y")
    {
        formHTMLcol='<div class="col-vtop '+col_class+'">';
        
        classRequired="";
        star="";
        if (needFile=="Y")
        {
            classRequired=" customFormFieldRequired";   
            star=" *";         
        }        
        //Если существует параметр кастомного тайтла поля "Файл"
        if (fieldTitleFile!=undefined)
        {
            //Если параметр кастомного тайтла поля "Файл" НЕ пустой
            if (fieldTitleFile!="")
            {
                formHTMLcol+='<div class="customFormFieldTitle">'+fieldTitleFile+star+'</div>';
            } else
            {
                //проверяем, указан ли кастомный placeholder
                //Если параметр кастомного тайтла поля "Файл" ПУСТОЙ и существует кастомный placeholder
                if (fieldPlaceholderFile!=undefined)
                {
                    //Если кастомный placeholder ПУСТОЙ
                    //то выводим стандартный тайтл!
                    if (fieldPlaceholderFile=="")
                    {
                        formHTMLcol+='<div class="customFormFieldTitle">Выберите файл'+star+'</div>';
                    }
                }
            }
        } else
        {
            //Если НЕ существует параметр кастомного тайтла поля "Файл"
            formHTMLcol+='<div class="customFormFieldTitle">Выберите файл'+star+'</div>';
        }
        
        //Если существует кастомный placeholder поля "Файл"
        if (fieldPlaceholderFile!=undefined)
        {
            //Если он НЕ пустой
            if (fieldPlaceholderFile!="")
            {
                //проверяем, существует ли кастомный тайтл
                if (fieldTitleFile!=undefined)
                {
                    //если кастомный тайтл пустой, выводим кастомный placeholder с признаком обязательности заполнения
                    if (fieldTitleFile=="")
                    {
                        formHTMLcol+='<div class="customFormFieldBody"><div class="file_placeholder form-control">'+fieldPlaceholderFile+star+'</div></div>';
                    } else
                    {
                        //если кастомный тайтл НЕ пустой
                        //то просто подменяем placeholder
                        formHTMLcol+='<div class="customFormFieldBody"><div class="file_placeholder form-control">'+fieldPlaceholderFile+'</div></div>';
                    }
                } else
                {
                    //если кастомный тайтл НЕ существует
                    //то просто подменяем placeholder
                    formHTMLcol+='<div class="customFormFieldBody"><div class="file_placeholder form-control">'+fieldPlaceholderFile+'</div></div>';
                }
            } else
            {
                formHTMLcol+='<div class="customFormFieldBody"><div class="file_placeholder form-control">Выберите файл</div></div>';
            }
        } else
        {
            if (fieldTitleFile!=undefined)
            {
                //если кастомный тайтл пустой, выводим стандартный placeholder с признаком обязательности заполнения
                if (fieldTitleFile=="")
                {
                    formHTMLcol+='<div class="customFormFieldBody"><div class="file_placeholder form-control">Выберите файл'+star+'</div></div>';
                } else
                {
                    //если кастомный тайтл НЕ пустой
                    //то просто выводим стандартный placeholder
                    formHTMLcol+='<div class="customFormFieldBody"><div class="file_placeholder form-control">Выберите файл</div></div>';
                }
            } else
            {
                formHTMLcol+='<div class="customFormFieldBody"><div class="file_placeholder form-control">Выберите файл</div></div>';
            }
        }     
        if (fieldPlaceholderFile==undefined || fieldPlaceholderFile=='')
        {
            fieldPlaceholderFile='Выберите файл';
        }
        fieldPlaceholderFile+=star;
        formHTMLcol+='<input data-title="'+fieldPlaceholderFile+'" style="display:none" type="file" class="file hidden" onchange="foto_HandleChanges($(this))" name="file">';
        
        formHTMLcol+='</div>';
        formCols[showFileIndex]=formHTMLcol;
    } else
    {
        formCols[showFileIndex]='';
    }
    
    if (showTextarea=="Y")
    {
        formHTMLcol='<div class="col-vtop '+col_class+'">';
        
        classRequired="";
        star="";
        if (needTextarea=="Y")
        {
            classRequired=" customFormFieldRequired";
            star=" *";
        }  
        if (customTextareaTitle!="")
        {
            formHTMLcol+='<div class="customFormFieldTitle">'+customTextareaTitle+star+'</div>';
            star='';
            formHTMLcol+='<input type="hidden" name="textarea_title" value="'+customTextareaTitle+'" />';
        } else
        if (customTextareaPlaceholder!="")
        {
            formHTMLcol+='<input type="hidden" name="textarea_title" value="'+customTextareaPlaceholder+'" />';
        }
        formHTMLcol+='<div class="customFormFieldBody"><textarea name="textarea" class="form-control textareaField customFormFieldInputText'+classRequired+'" maxlength="1000" placeholder="'+customTextareaPlaceholder+star+'"></textarea></div>';
        formHTMLcol+='</div>';
        formCols[showTextareaIndex]=formHTMLcol;
    } else
    {
        formCols[showTextareaIndex]='';
    }

    if (customField1Title!=undefined && customField1Placeholder!=undefined && customField1Req!=undefined && customField1Index!=undefined  )
    {
        formHTMLcol='<div class="col-vtop '+col_class+'">';

        classRequired="";
        star="";
        if (customField1Req=="Y")
        {
            classRequired=" customFormFieldRequired";
            star=" *";
        }
        if (customField1Title!="")
        {
            formHTMLcol+='<div class="customFormFieldTitle">'+customField1Title+star+'</div>';
            formHTMLcol+='<input type="hidden" name="customField1Name" value="'+customField1Title+'" />';
            star='';
        } else
        if (customField1Placeholder!="")
        {
            formHTMLcol+='<input type="hidden" name="customField1Name" value="'+customField1Placeholder+'" />';
        }

        formHTMLcol+='<div class="customFormFieldBody"><input type="text" name="customField1" class="form-control customFormFieldInputText'+classRequired+'" maxlength="255" placeholder="'+customField1Placeholder+star+'" /></div>';
        formHTMLcol+='</div>';
        formCols[customField1Index]=formHTMLcol;
    }

    if (customField2Title!=undefined && customField2Placeholder!=undefined && customField2Req!=undefined && customField2Index!=undefined  )
    {
        formHTMLcol='<div class="col-vtop '+col_class+'">';

        classRequired="";
        star="";
        if (customField2Req=="Y")
        {
            classRequired=" customFormFieldRequired";
            star=" *";
        }
        if (customField2Title!="")
        {
            formHTMLcol+='<div class="customFormFieldTitle">'+customField2Title+star+'</div>';
            formHTMLcol+='<input type="hidden" name="customField2Name" value="'+customField2Title+'" />';
            star='';
        } else
        if (customField2Placeholder!="")
        {
            formHTMLcol+='<input type="hidden" name="customField2Name" value="'+customField2Placeholder+'" />';
        }
        formHTMLcol+='<div class="customFormFieldBody"><input type="text" name="customField2" class="form-control customFormFieldInputText'+classRequired+'" maxlength="255" placeholder="'+customField2Placeholder+star+'" /></div>';
        formHTMLcol+='</div>';
        formCols[customField2Index]=formHTMLcol;
    }
    
    if (text_bottom_before_btn!=undefined && text_bottom_before_btn!="")
    {
        for (var i=1;i<formCols.length;i++)
        {
            formHTML+=formCols[i];
        }

        formHTML+='</div>';   
        formHTML+='<div class="customFormTextBottomBeforeBtn">'+text_bottom_before_btn+'</div>';
        formHTML+='<input type="hidden" name="form_text_bottom_before_btn" value="'+text_bottom_before_btn+'" />'; 
        if (bntText)
        {
            formHTML+='<div class="form-btn-block"><input class="customFormSubmitBtn btn '+btn_class+'" type="submit" value="'+bntText+'" /></div>';    
        } else
        {
            formHTML+='<div class="form-btn-block"><input class="customFormSubmitBtn btn '+btn_class+'" type="submit" value=" Отправить " /></div>';
        }
    } else
    {
        formHTMLcol='<div class="col-vtop '+col_class+'">';
        if (bntText)
        {
            formHTMLcol+='<div class="form-btn-block"><input class="customFormSubmitBtn btn '+btn_class+'" type="submit" value="'+bntText+'" /></div>';
        } else
        {
            formHTMLcol+='<div class="form-btn-block"><input class="customFormSubmitBtn btn '+btn_class+'" type="submit" value=" Отправить " /></div>';
        }
        formHTMLcol+='</div>';

        for (var i=1;i<formCols.length;i++)
        {
            formHTML+=formCols[i];
        }

        formHTML+=formHTMLcol+'</div>';
    }
    
    
    
    formHTML+='<input type="hidden" name="site" value="'+location.href+'" /><input type="hidden" name="referrer" value="'+document.referrer+'" /><input type="hidden" name="form_code" value="'+formCode+'" />';
    if (metrikaCounter && metrikaTargetTry && metrikaTargetSuccess)
    {
        formHTML+='<input type="hidden" class="metrikaCounter" value="'+metrikaCounter+'" /><input type="hidden" class="metrikaTargetTry" value="'+metrikaTargetTry+'" /><input type="hidden" class="metrikaTargetSuccess" value="'+metrikaTargetSuccess+'" />';
    }
    if (gaTargetTry && gaTargetSuccess)
    {
        formHTML+='<input type="hidden" class="gaTargetTry" value="'+gaTargetTry+'" /><input type="hidden" class="gaTargetSuccess" value="'+gaTargetSuccess+'" />';
    }
    if (redirectURL)
    {
        formHTML+='<input type="hidden" class="redirectURL" value="'+redirectURL+'" />';
    } 
    if (serviceFormName)
    {
        formHTML+='<input type="hidden" name="serviceFormName" value="'+serviceFormName+'" />';
    }
    formHTML+='<textarea name="hiddenAnyText" class="hiddenAnyText"></textarea>';
    if (text_bottom_after_btn!=undefined)
    {
        formHTML+='<div class="customFormTextBottomAfterBtn">'+text_bottom_after_btn+'</div>';
        formHTML+='<input type="hidden" name="form_text_bottom_after_btn" value="'+text_bottom_after_btn+'" />';
    } 
    formHTML+='</form></div></div>';
    if (parentContainer=="body")
    {
        formHTML+='</div>';
        var timeoutLength=0;
        if ($('.white-popup').length>0)
        {
            $.magnificPopup.close();
            timeoutLength=350;
        }
        window.setTimeout(function () {
            $.magnificPopup.open({
                tClose: 'Закрыть (Esc)',
                tLoading: 'Загрузка...',
                items: {
                    src: '<div class="white-popup white-popup-block zoom-anim-dialog request_popup">'+formHTML+'</div>',
                },
                type: 'inline',
                preloader: true,
    	        modal: false,
                removalDelay: 300,
                closeOnBgClick:false,
                mainClass: 'my-mfp-zoom-in',
                midClick: true,
                callbacks: {
                    open: function() {
                        $("#customFormPopupContainer input[name='phone']").mask("+9 (999) 999-9999");
                        if (IS_GADGET)
                        {
                            document.location.hash='popup';
                            observerCloseMagnificPopup();
                            $.magnificPopup.instance.close = function () {
                                removeHashForPopups();
                                $.magnificPopup.proto.close.call(this);
                            };
                        } 
                        if (typeof customFormInitCallback == 'function') { customFormInitCallback($("#customFormPopupContainer"),formCode); }
                    }
                }
            });
        }, timeoutLength);
    } else
    {
        parentContainer.html(formHTML);
        parentContainer.find("input[name='phone']").mask("+9 (999) 999-9999");
        if (typeof customFormInitCallback == 'function') { customFormInitCallback(parentContainer,formCode); }
    }        
}
/*Инициализация формы --- конец*/

/*
Использовать данную функцию для дополнительных действий с формами после их инициализации,
code - это код формы (например, data-form-code="55") 
function customFormInitCallback(obj,code) {
    switch (code) {
        case 55:
            obj.find('.customFormFields').append('<div class="customFormFieldBody"><input type="text" id="form_mk_time" class="form-control customFormFieldInputText" placeholder="Удобное время звонка"></div>');
            break;
    }
}
*/