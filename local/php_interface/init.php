<?
if(
    stristr($_SERVER['REQUEST_URI'],"/index.php") && 
    !isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
    !isset($_SERVER["HOME"]) && 
    strpos($_SERVER['REQUEST_URI'],"/local/")!==0 &&
    strpos($_SERVER['REQUEST_URI'],"/cron/")!==0 &&
    strpos($_SERVER['REQUEST_URI'],"/korzina/")!==0
) 
{
    if ($_SERVER["QUERY_STRING"]!="")
    {
        LocalRedirect($APPLICATION->GetCurDir()."?".$_SERVER["QUERY_STRING"]);
    } else    
    {
        LocalRedirect($APPLICATION->GetCurDir());
    }
    exit();
}

CModule::IncludeModule("iblock");

define("SETTINGS_IBLOCK_ID",1);
define("REQUESTS_IBLOCK_ID",2);
define("CATALOG_IBLOCK_ID",3);
define("BRAND_IBLOCK_ID",7);
define("TYPE_PLACE_IBLOCK_ID",9);
define("VID_IBLOCK_ID",10);
define("CATALOG_FILES_IBLOCK_ID",8);

if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') 
{
    define("IS_AJAX",true);
} else
{
    define("IS_AJAX",false);
}

require($_SERVER["DOCUMENT_ROOT"].'/local/php_interface/service/mobile_detect.php');
require($_SERVER["DOCUMENT_ROOT"].'/local/php_interface/service/tools.php');
require($_SERVER["DOCUMENT_ROOT"].'/local/php_interface/service/404.php');

//ЗАПРЕТ УДАЛЕНИЯ ЭЛЕМЕНТОВ из инфоблока настроек
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", "OnBeforeIBlockElementDeleteHandler");
function OnBeforeIBlockElementDeleteHandler($ID)
{
    $res = CIBlockElement::GetByID($ID);
    if($ar_res = $res->GetNext())
    {  
        global $USER;
        if ($ar_res['IBLOCK_ID']==SETTINGS_IBLOCK_ID && !$USER->IsAdmin())
        {
            global $APPLICATION;
            $APPLICATION->throwException("Элементы в данном инфоблоке нельзя удалить");
            return false;
        }  
    }         
}

AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "DoIBlockAfterSave");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "DoIBlockAfterSave");
function DoIBlockAfterSave($arFields) {
    $fotoPropsText=array();
    
    if (!isset($arFields["PROPERTY_VALUES"]["34"]))
    {
        $arPropsFotos=array();
        $db_props = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arFields["ID"], "sort", "asc", Array("CODE"=>"FOTOS"));
        while ($ob = $db_props->GetNext())
        {
            $arPropsFotos[]=array('DESCRIPTION' => $ob['DESCRIPTION']);
        }  
    } else
    {
        $arPropsFotos=$arFields["PROPERTY_VALUES"]["34"];
    }
    foreach ($arPropsFotos as $fotoProp)
    {
        $arCurDescr=explode("#",$fotoProp["DESCRIPTION"]);
        $fotoPropsText[]=$arCurDescr[0];
    }
    CIBlockElement::SetPropertyValues($arFields["ID"], CATALOG_IBLOCK_ID, join(", ",$fotoPropsText), "INFO_FROM_FOTOS");
    CIBlockElement::UpdateSearch($arFields["ID"],true);
    $GLOBALS['CACHE_MANAGER']->ClearByTag("iblock_id_".CATALOG_IBLOCK_ID);
}
if(!function_exists("pre")) {
    function pre($var, $die = false, $all = false)
    {
        global $USER;
        if ($USER->IsAdmin() || $all == true) {
            ?><?mb_internal_encoding('utf-8');?>

            <font style="text-align: left; font-size: 12px">
                <pre><? print_r($var) ?></pre>
            </font><br>
            <?
        }
        if ($die) {
            die;
        }
    }
}
require ($_SERVER["DOCUMENT_ROOT"].'/local/php_interface/sendmail.php');

function GetRateFromCBR(){
    global $APPLICATION;

    $arCurrencies = array("USD","EUR","CNY");
    //делаем запрос к www.cbr.ru с просьбой отдать курс на нынешнюю дату
    $strQueryText = QueryGetData("www.cbr.ru", 80, "/scripts/XML_daily.asp", $QUERY_STR, $errno, $errstr);

    //получаем XML и конвертируем в кодировку сайта
    $charset = "windows-1251";
    if (preg_match("/<"."\?XML[^>]{1,}encoding=[\"']([^>\"']{1,})[\"'][^>]{0,}\?".">/i", $strQueryText, $matches))
    {
        $charset = Trim($matches[1]);
    }
    $strQueryText = preg_replace ("<!DOCTYPE[^>]{1,}>", "", $strQueryText);
    $strQueryText = preg_replace ("<"."\?XML[^>]{1,}\?".">", "", $strQueryText);
    $strQueryText = $APPLICATION->ConvertCharset($strQueryText, $charset, SITE_CHARSET);

    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/xml.php");

    //парсим XML
    $objXML = new CDataXML();
    $res = $objXML->LoadString($strQueryText);
    if($res !== false)
        $arData = $objXML->GetArray();
    else
        $arData = false;

    $arRates = Array();

    //получаем курс нужной валюты
    if (is_array($arData) && count($arData["ValCurs"]["#"]["Valute"])>0)
    {
        for ($j1 = 0; $j1<count($arData["ValCurs"]["#"]["Valute"]); $j1++)
        {
            $CUR_CURRENCY = $arData["ValCurs"]["#"]["Valute"][$j1]["#"]["CharCode"][0]["#"];
            if (in_array($arData["ValCurs"]["#"]["Valute"][$j1]["#"]["CharCode"][0]["#"],$arCurrencies))
            {
                $arRates[$CUR_CURRENCY]['RATE_CNT'] = IntVal($arData["ValCurs"]["#"]["Valute"][$j1]["#"]["Nominal"][0]["#"]);
                $arRates[$CUR_CURRENCY]['RATE'] = DoubleVal(str_replace(",", ".", $arData["ValCurs"]["#"]["Valute"][$j1]["#"]["Value"][0]["#"]))/$arRates[$CUR_CURRENCY]['RATE_CNT'];
                //$NEW_RATE['DATE_RATE'] = $DATE_RATE;
            }
        }
    }
    if($arRates["USD"]["RATE"] > 0) COption::SetOptionString("main","CUR_USD", $arRates["USD"]["RATE"]);
    if($arRates["EUR"]["RATE"] > 0) COption::SetOptionString("main","CUR_EUR", $arRates["EUR"]["RATE"]);
    if($arRates["CNY"]["RATE"] > 0) COption::SetOptionString("main","CUR_CNY", $arRates["CNY"]["RATE"]);

    COption::SetOptionString("main","CUR_DATE", date("d.m.Y"));
    //pre($arRates);
    //возвращаем код вызова функции, чтобы агент не "убился"
    return 'GetRateFromCBR();';
}

?>