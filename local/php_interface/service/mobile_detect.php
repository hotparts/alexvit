<?
require($_SERVER["DOCUMENT_ROOT"].'/local/php_interface/lib/Mobile_Detect.php');
$detectDevice = new Mobile_Detect;
if ($detectDevice->isMobile()) 
{
    define('IS_GADGET', 1);
    if ($detectDevice->isTablet()) 
    {
        define('IS_TABLET', 1);
        define('IS_PHONE', 0);
    } else 
    {
        define('IS_TABLET', 0);
        define('IS_PHONE', 1);
    }
} else
{
    define('IS_GADGET', 0);
    define('IS_PHONE', 0);
    define('IS_TABLET', 0);
}
?>