<?
function sklonenie($n, $forms) {
    return $n%10==1&&$n%100!=11?$forms[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$forms[1]:$forms[2]);
}

function sortSectionsBySort($a, $b) { 
    if ($a['SORT'] === $b['SORT']) return 0; 
    return (int)$a['SORT'] > (int)$b['SORT'] ? 1 : -1; 
} 

function removeqsvar($url, $varname) {
    parse_str($url, $vars);
    unset($vars[$varname]);
    return http_build_query($vars);
}

CModule::IncludeModule("iblock");

//добавление ог-мета-тегов в <head>////////////////////////////////////////////////
AddEventHandler('main', 'OnEpilog', array('CMyEpilogHooks', 'OpenGraph'));
class CMyEpilogHooks
{
   function OpenGraph()
   {
      GLOBAL $APPLICATION;
      foreach (array('og_title', 'og_image', 'og_url', 'og_site_name', 'og_type', 'og_description') as $prop_name)
      {
         $value = $APPLICATION->GetDirProperty($prop_name);
         if ($prop_name == 'og_url' && empty($value))
            $value = $APPLICATION->GetCurPage(false);
         if (in_array($prop_name, array('og_image', 'og_url')) && !empty($value))
         {
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on")
            {
                $value = 'https://'.$_SERVER["SERVER_NAME"].$value;    
            } else
            {
                $value = 'http://'.$_SERVER["SERVER_NAME"].$value;
            }
         }
         $prop_code = str_replace('og_', 'og:', $prop_name);
         if (!empty($value))
         {
            if ("og:image"==$prop_code)
            {
                $APPLICATION->AddHeadString('<meta property="'.$prop_code.'" content="'.$value.'?'.date('d_m_Y_H_i_s').'" />');
            } else
            {
                $APPLICATION->AddHeadString('<meta property="'.$prop_code.'" content="'.$value.'" />');    
            }
            
         }
      }
   }
}

//получаем настройки
$arSelect = Array("ID", "CODE", "PROPERTY_VALUE", "PROPERTY_FILE");
$arFilter = Array("IBLOCK_ID"=>SETTINGS_IBLOCK_ID, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    if ($arFields["PROPERTY_FILE_VALUE"]>0)
    {
        $src=CFile::GetFileArray($arFields["PROPERTY_FILE_VALUE"]);
        $src=$src["SRC"];
        define(strtoupper($arFields["CODE"]."_FILE"),$src);
    }
    if (trim($arFields["PROPERTY_VALUE_VALUE"])!="")
    {
        $val=trim(str_replace("\n","<br />",$arFields["~PROPERTY_VALUE_VALUE"]));
        $val=str_replace("\r\n","",$val);
        $val=str_replace("\r","",$val);
        
        if ($arFields["CODE"]=="PHONE_1")
        {
            $phone=preg_replace("/[^\d]/i","",$val);
            $phoneValURL=$phone[0]."-".$phone[1].$phone[2].$phone[3]."-".$phone[4].$phone[5].$phone[6]."-".$phone[7].$phone[8].$phone[9].$phone[10];
            if ($phone[0]=="7")
            {
                $phoneValURL="+".$phoneValURL;
            }
            define(strtoupper($arFields["CODE"]."_TEXT"),$val);
            define("PHONE_1_URL_TEXT",$phoneValURL);
        } else
        if ($arFields["CODE"]=="KURS_DOLLARA" || $arFields["CODE"]=="KURS_EVRO")
        {
            define(strtoupper($arFields["CODE"]."_TEXT"),str_replace(",",".",$val));
        } else
        {
            define(strtoupper($arFields["CODE"]."_TEXT"),$val);
        }
    }
}
?>