<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

$dbRes = CFile::GetList(
    $arOrder = [],
    $arFilter = []
);

$arFiles = [];
while ($arFileItem = $dbRes->Fetch()){
    $arFiles [] = $arFileItem;
}

file_put_contents('photo_descr.json',json_encode($arFiles,JSON_UNESCAPED_UNICODE));

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin_after.php");