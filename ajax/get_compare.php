<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
 
if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) && 
	!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && 
	strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
    
    if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'del')
    {
        unset($_SESSION["CATALOG_COMPARE_LIST"]['21']["ITEMS"][intval($_REQUEST['id'])]);        
    }
    
	$APPLICATION->IncludeComponent(
		"bitrix:catalog.compare.list",
		"",
		Array(
			"AJAX_MODE" => "N",
			"IBLOCK_TYPE" => "catalog",
			"IBLOCK_ID" => "21",
			"DETAIL_URL" => "",
			"COMPARE_URL" => SITE_DIR."catalog/compare.php",
			"NAME" => "CATALOG_COMPARE_LIST",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N"
		),
	false
	);
}
?>