<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("О компании");
$APPLICATION->SetPageProperty("title", "О компании - ООО «Алексвит»");
?>
<script src="/local/templates/main-responsive/js/jquery.carouFredSel-6.0.4-packed.js"></script>
<script src="/local/templates/main-responsive/js/my/about-common.js"></script>


<div>

	ООО «Алексвит» является одним из крупнейших поставщиков на рынке напольных покрытий и потолков ведущих мировых
	компаний. <br>
	<br>
	В ассортименте реализуемых нами товаров продукция популярнейших зарубежных и хорошо зарекомендовавших себя
	российских производителей. <br>
	<br>
	Мы предлагаем вам напольные покрытия высокого качества как премиум, так и эконом класса по демократичным ценам. В
	работе с клиентами мы применяем индивидуальный подход. Сотрудничая с нашей компанией, вы в первую очередь
	приобретаете надежного поставщика. <br>
	<br>
	Помимо приобретения товара на выгодных для вас условиях, наша компания осуществляет бесплатную доставку на объект по
	Москве и Московской области. <br>
	<br>
	Наша компания готова выполнить работы по проектированию, 3д моделированию, интерьеров и фасадов.<br>
	<br>
	Сотрудничество с нами гарантирует соблюдение сроков поставки и целостности груза. Большой ассортимент товара и
	постоянное его обновление удовлетворит самый взыскательный вкус потребителя. Наличие складского хозяйства и
	высококвалифицированных логистических услуг позволит в кратчайшие сроки осуществить ваш заказ. <br>

</div>

<div class="container about-brands">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-3  logo-block">
			<img alt="ООО «АлексВит» поставка напольных покрытий и потолков" src="/upload/iblock/8d9/logo_alexvit.png">
		</div>
		<div class="col-12 col-sm-12 col-md-8">
			<div class="about-logos">
				<?$APPLICATION->IncludeComponent(
					"bitrix:news.list",
					"about-brands",
					Array(
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"ADD_SECTIONS_CHAIN" => "N",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "N",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "N",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"CHECK_DATES" => "Y",
						"DETAIL_URL" => "",
						"DISPLAY_BOTTOM_PAGER" => "N",
						"DISPLAY_DATE" => "Y",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"DISPLAY_TOP_PAGER" => "N",
						"FIELD_CODE" => array(0=>"NAME",1=>"DETAIL_PICTURE",2=>"DETAIL_TEXT",),
						"FILTER_NAME" => "arrFilterProjectsIndex",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"IBLOCK_ID" => "7",
						"IBLOCK_TYPE" => "catalog",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"INCLUDE_SUBSECTIONS" => "Y",
						"MESSAGE_404" => "",
						"NEWS_COUNT" => "9999",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => ".default",
						"PAGER_TITLE" => "Новости",
						"PARENT_SECTION" => "",
						"PARENT_SECTION_CODE" => "",
						"PREVIEW_TRUNCATE_LEN" => "",
						"PROPERTY_CODE" => array(0=>"",1=>"FOTOS",),
						"SET_BROWSER_TITLE" => "N",
						"SET_LAST_MODIFIED" => "N",
						"SET_META_DESCRIPTION" => "N",
						"SET_META_KEYWORDS" => "N",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "N",
						"SHOW_404" => "N",
						"SORT_BY1" => "SORT",
						"SORT_BY2" => "SORT",
						"SORT_ORDER1" => "ASC",
						"SORT_ORDER2" => "ASC"
					)
				);?>
			</div>
		</div>
	</div>
</div>

<div class="present-line">
	<div class="container">
	<div class="row">
		<div class="container">
			<div class="pr-line">
				<a class="pdf-icon" href="/upload/alexvit.pdf"> <img src="/local/templates/main-responsive/img/icon-pdf.png" alt=""></a> 
				<span class="hdr">Ознакомиться с презентацией компании</span> 
				<a class="btn-download" download="" href="/upload/alexvit.pdf">Скачать</a>
			</div>
		</div>
	</div>
	</div>
</div>

<div class="container about-sertificates">
	<div class="row">
		<h2>Сертификаты</h2>
		<div class="slider sert">
			<span class="btn-prev"></span> <span class="btn-next"></span>
			<div class="owl-carousel-col4">
				<?$APPLICATION->IncludeComponent(
					"bitrix:news.list",
					"sertificates",
					Array(
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"ADD_SECTIONS_CHAIN" => "Y",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "N",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "N",
						"CHECK_DATES" => "Y",
						"COMPONENT_TEMPLATE" => "sertificates",
						"DETAIL_URL" => "",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"DISPLAY_DATE" => "Y",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"DISPLAY_TOP_PAGER" => "N",
						"FIELD_CODE" => array(0=>"",1=>"",),
						"FILTER_NAME" => "",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"IBLOCK_ID" => "14",
						"IBLOCK_TYPE" => "okompanii",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
						"INCLUDE_SUBSECTIONS" => "Y",
						"MESSAGE_404" => "",
						"NEWS_COUNT" => "50",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => ".default",
						"PAGER_TITLE" => "Новости",
						"PARENT_SECTION" => "",
						"PARENT_SECTION_CODE" => "",
						"PREVIEW_TRUNCATE_LEN" => "",
						"PROPERTY_CODE" => array(0=>"",1=>"SFOTO",2=>"",),
						"SET_BROWSER_TITLE" => "Y",
						"SET_LAST_MODIFIED" => "N",
						"SET_META_DESCRIPTION" => "Y",
						"SET_META_KEYWORDS" => "Y",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "Y",
						"SHOW_404" => "N",
						"SORT_BY1" => "ACTIVE_FROM",
						"SORT_BY2" => "SORT",
						"SORT_ORDER1" => "DESC",
						"SORT_ORDER2" => "ASC",
						"STRICT_SECTION_CHECK" => "N"
					)
				);?>
			</div>
		</div>
	</div>
</div>
<div class="container about-thanks">
	<div class="row">
		<h2>Благодарности от наших клиентов</h2>
		<div class="slider thanks">
			<span class="btn-prev"></span> <span class="btn-next"></span>
			<div class="owl-carousel-col2">
				<?$APPLICATION->IncludeComponent(
					"bitrix:news.list",
					"thanks",
					Array(
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"ADD_SECTIONS_CHAIN" => "Y",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "N",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "N",
						"CHECK_DATES" => "Y",
						"COMPONENT_TEMPLATE" => "thanks",
						"DETAIL_URL" => "",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"DISPLAY_DATE" => "Y",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"DISPLAY_TOP_PAGER" => "N",
						"FIELD_CODE" => array(0=>"",1=>"",),
						"FILTER_NAME" => "",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"IBLOCK_ID" => "15",
						"IBLOCK_TYPE" => "okompanii",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
						"INCLUDE_SUBSECTIONS" => "Y",
						"MESSAGE_404" => "",
						"NEWS_COUNT" => "50",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => ".default",
						"PAGER_TITLE" => "Новости",
						"PARENT_SECTION" => "",
						"PARENT_SECTION_CODE" => "",
						"PREVIEW_TRUNCATE_LEN" => "",
						"PROPERTY_CODE" => array(0=>"",1=>"SFOTO",2=>"",),
						"SET_BROWSER_TITLE" => "Y",
						"SET_LAST_MODIFIED" => "N",
						"SET_META_DESCRIPTION" => "Y",
						"SET_META_KEYWORDS" => "Y",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "Y",
						"SHOW_404" => "N",
						"SORT_BY1" => "ACTIVE_FROM",
						"SORT_BY2" => "SORT",
						"SORT_ORDER1" => "DESC",
						"SORT_ORDER2" => "ASC",
						"STRICT_SECTION_CHECK" => "N"
					)
				);?>
			</div>
		</div>
	</div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>