<?require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?CModule::IncludeModule("iblock");?>
<?
$arSelect = Array("ID", "NAME", "DETAIL_PICTURE", "DETAIL_TEXT",
	"PROPERTY_HIM_TABLE_PRICE",
	"PROPERTY_COLOR", 
	"PROPERTY_HIMIYA_PROPS", 
	"PROPERTY_CATALOG_FILE",
	"PROPERTY_HIMIYA_FILE_INFO_DOWNLOAD"
);
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "ID"=>(int)$_GET["id"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
if($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
	$img = CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);
	
	$CATALOG_FILE="";
	$INFO_DOWNLOAD_FILE="";
	if ($arFields["PROPERTY_CATALOG_FILE_VALUE"]>0)
	{
		$arSelect = Array("ID", "NAME", "PROPERTY_FILE");
		$arFilter = Array("IBLOCK_ID"=>CATALOG_FILES_IBLOCK_ID, "ID"=>$arResult["PROPERTY_CATALOG_FILE_VALUE"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		if($ob = $res->GetNextElement())
		{
			$arFields2 = $ob->GetFields();
			$src=CFile::GetFileArray($arFields2["PROPERTY_FILE_VALUE"]);
			$CATALOG_FILE=$src["SRC"];
		}    
	}
	if ($arFields["PROPERTY_HIMIYA_FILE_INFO_DOWNLOAD_VALUE"]>0)
	{
		$src=CFile::GetFileArray($arFields["PROPERTY_HIMIYA_FILE_INFO_DOWNLOAD_VALUE"]);
		$INFO_DOWNLOAD_FILE=$src["SRC"];
	}
	?>
	<div class="white-popup white-popup-block zoom-anim-dialog">
		<article class="product-item-popup__wrap">
			<?
			if ($arFields["DETAIL_PICTURE"])
			{
				?>
				<div class="product-img-wrap">
					<img src="<?=$img["src"]?>" alt="">
				</div>
				<header class="product-item__header">
				<?    
			} else
			{
				?>
				<header class="product-item__header" style="width: 100%;">
				<?
			}
			?>
				<h1 class="product-item__title"><?=$arFields["NAME"]?></h1>
				<?
				$needShowPriceTable=false;
				$isPriceTableTH[1]=false;
				$isPriceTableTH[2]=false;
				$isPriceTableTH[3]=false;
				$isPriceTableTH[4]=false;
				$isPriceTableTH[5]=false;
				foreach ($arFields["PROPERTY_HIM_TABLE_PRICE_VALUE"] as $row)
				{
					$i=0;
					foreach ($row as $col)
					{
						$i++;
						$curVal=trim($col);
						if ($curVal!="")
						{
							$needShowPriceTable=true;
							$isPriceTableTH[$i]=true;
						}
					}
				}

				if ($needShowPriceTable)
				{
					?>
					<table class="table-char">
						<thead>
							<tr>
								<?
								if($isPriceTableTH[1])
								{
									?><th>Фасовка</th><?    
								}
								if($isPriceTableTH[2])
								{
									?><th>Ед.&nbsp;изм.</th><?    
								}
								if($isPriceTableTH[3])
								{
									?><th>Розничная</th><?    
								}
								if($isPriceTableTH[4])
								{
									?><th>Оптовая</th><?    
								}
								if($isPriceTableTH[5])
								{
									?><th>Специальная</th><?    
								}
								?>
							</tr>
						</thead>
						<tbody>
							<?
							foreach ($arFields["PROPERTY_HIM_TABLE_PRICE_VALUE"] as $row)
							{
								?><tr><?
								if($isPriceTableTH[1])
								{
									$curVal=trim($row["col1"]);
									if (is_numeric($curVal))
									{
										?><td style="white-space: nowrap;"><?=number_format($curVal, 0, ',', ' ' );?> <span class="detail-price__currency">₽</span></td><?
									} else
									{
										?><td style="white-space: nowrap;"><?=$curVal?></td><?
									}    
								}
								if($isPriceTableTH[2])
								{
									$curVal=trim($row["col2"]);
									if (is_numeric($curVal))
									{
										?><td style="white-space: nowrap;"><?=number_format($curVal, 0, ',', ' ' );?> <span class="detail-price__currency">₽</span></td><?
									} else
									{
										?><td style="white-space: nowrap;"><?=$curVal?></td><?
									}    
								}
								if($isPriceTableTH[3])
								{
									$curVal=trim($row["col3"]);
									if (is_numeric($curVal))
									{
										?><td style="white-space: nowrap;"><?=number_format($curVal, 0, ',', ' ' );?> <span class="detail-price__currency">₽</span></td><?
									} else
									{
										?><td style="white-space: nowrap;"><?=$curVal?></td><?
									}    
								}
								if($isPriceTableTH[4])
								{
									$curVal=trim($row["col4"]);
									if (is_numeric($curVal))
									{
										?><td style="white-space: nowrap;"><?=number_format($curVal, 0, ',', ' ' );?> <span class="detail-price__currency">₽</span></td><?
									} else
									{
										?><td style="white-space: nowrap;"><?=$curVal?></td><?
									}    
								}
								if($isPriceTableTH[5])
								{
									$curVal=trim($row["col5"]);
									if (is_numeric($curVal))
									{
										?><td style="white-space: nowrap;"><?=number_format($curVal, 0, ',', ' ' );?> <span class="detail-price__currency">₽</span></td><?
									} else
									{
										?><td style="white-space: nowrap;"><?=$curVal?></td><?
									}    
								}
								?></tr><?
							}
							?>                                
						</tbody>
					</table>
					<?
				}
				?>    	
			</header>
			<br /><br />	
			<div class="product-item__descr content">
				<?
				$detailText=trim($arFields["~DETAIL_TEXT"]);
				if ($detailText!="")
				{
					if ($arFields["DETAIL_TEXT_TYPE"]=="html")
					{
						echo $detailText;
					} else
					{
						?><p><?=str_replace("\n","<br />",$detailText)?></p><?
					}
				}
				
				if (count($arFields["PROPERTY_HIMIYA_PROPS_VALUE"])>0)
				{
					?>
					<div class="product-item__table-title">Характеристики</div>
					<table  class="product-item__table"><tbody>
						<?
						foreach ($arFields["PROPERTY_HIMIYA_PROPS_VALUE"] as $key=>$val)
						{
							?>
							<tr>
								<td><?=$val?></td>
								<td><?=$arFields["PROPERTY_HIMIYA_PROPS_DESCRIPTION"][$key]?></td>
							</tr>
							<?
						}
						?>
					</tbody></table>
					<?
				}
				?>
			</div>
			<div class="product-item__btn-wrap">
				<a href="#" class="customFormLink product-item__btn-order"
					data-params="YNYYN-NNYNN"
					data-form-code="himiya_zakaz"
					data-title="Заказать <?=$arFields["NAME"]?>"
					data-text=""
					data-btn-text="Отправить"
					data-service-form-name=""
					data-field-title-phone=""
					data-field-title-name=""
					data-field-placeholder-name="Представьтесь пожалуйста"
					data-field-placeholder-phone="Ваш контактный телефон"
					data-custom-textarea-title=""
					data-custom-textarea-placeholder="Комментарии"
					>Заказать</a>
				<?$prod = trim(htmlspecialchars($_GET["prod"]));?>
				<a class="product-item__btn-sert" href="/sertifikaty/?brand=<?=$prod?>">Сертификаты</a>
				<?
				if ($CATALOG_FILE!="")
				{
					?>
					<a target="_blank" style="margin-left: 10px;" class="product-item__btn-sert" href="<?=$CATALOG_FILE?>">Каталог</a>
					<?
				}
				if ($INFO_DOWNLOAD_FILE!="")
				{
					?>
					<a target="_blank" style="margin-left: 10px;width: 280px;" class="product-item__btn-sert" href="<?=$INFO_DOWNLOAD_FILE?>">Информация для скачивания</a>
					<?
				}
				?>
			</div>
		</article>
	</div>
	<?
}
?>