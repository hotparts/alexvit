<?require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?CModule::IncludeModule("iblock");?>
<?
$result=array();

$ip=$_SERVER['REMOTE_ADDR'];
$user_agent=trim(htmlspecialchars($_SERVER['HTTP_USER_AGENT'])); 

//СОБИРАЕМ И ОБРАБАТЫВАЕМ данные с формы////////////////////////////////////////////////////
foreach ($_POST as $key=>$val)
{
    $_POST[$key]=trim(htmlspecialchars($val));
}

$textarea="";
if ($_POST["textarea"]!="")
{
    $textarea=$_POST["textarea_title"].": ".$_POST["textarea"];
}


/*Описание формы для базы*/
$form_info="";
if ($_POST["serviceFormName"]!="")
{
    $form_info.="Название формы: ".$_POST["serviceFormName"]."\n";
}
if ($_POST["form_title"]!="")
{
    $form_info.="Заголовок формы: ".$_POST["form_title"]."\n";
}
if ($_POST["form_text"]!="")
{
    $form_info.="Текст на форме: ".$_POST["form_text"]."\n";
}
if ($_POST["form_text_bottom_before_btn"]!="")
{
    $form_info.="Текст перед кнопкой: ".$_POST["form_text_bottom_before_btn"]."\n";
}
if ($_POST["form_text_bottom_after_btn"]!="")
{
    $form_info.="Текст после кнопки: ".$_POST["form_text_bottom_after_btn"]."\n";
}
if ($textarea!="")
{
    $form_info.=$textarea."\n";
}
if ($_POST["hiddenAnyText"]!="")
{
    $form_info.=$_POST["hiddenAnyText"]."\n";
}

$user_name="не указано";
if ($_POST["name"]!="")
{
    $user_name=$_POST["name"];
}
$user_phone="не указан";
if ($_POST["phone"]!="")
{
    $user_phone=$_POST["phone"];
}
$user_mail="не указана";
$user_mail_from=COption::GetOptionString("main", "email_from");
if ($_POST["email"]!="")
{
    $user_mail_from=$_POST["email"];
    $user_mail=$_POST["email"];
}

$customField1='';
if ($_POST["customField1"]!="" && $_POST["customField1Name"]!="")
{
    $customField1=$_POST["customField1Name"].": ".$_POST["customField1"]."<br />";
}

$customField2='';
if ($_POST["customField2"]!="" && $_POST["customField2Name"]!="")
{
    $customField2=$_POST["customField2Name"].": ".$_POST["customField2"]."<br />";
}

//ВСТАВЛЯЕМ заявку в базу
$el = new CIBlockElement;
$PROP = array();

if ($_FILES["file"]["name"]!="")
{
    $curImg=array(
        "name" => $_FILES["file"]["name"],
        "type" => $_FILES["file"]["type"],
        "tmp_name" => $_FILES["file"]["tmp_name"],
        "error" => $_FILES["file"]["error"],
        "size" => $_FILES["file"]["size"]
    );
    $PROP[14]=$curImg;
}

$PROP[3] = $_POST["form_code"];
$PROP[13] = $form_info;
$PROP[5] = $ip;
$PROP[6] = $user_mail;
$PROP[7] = $_POST["phone"];
$PROP[8] = $textarea;
$PROP[10] = $user_agent;
$PROP[11] = $_POST["referrer"];
$PROP[12] = $_SERVER["HTTP_REFERER"];
$PROP[15] = $_POST["customField1Name"];
$PROP[16] = $_POST["customField1"];
$PROP[17] = $_POST["customField2Name"];
$PROP[18] = $_POST["customField2"];

$arLoadProductArray = Array(
  "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
  "IBLOCK_ID"      => REQUESTS_IBLOCK_ID,
  "PROPERTY_VALUES"=> $PROP,
  "NAME"           => $user_name,
  "ACTIVE"         => "N",            // активен
);


if($PRODUCT_ID = $el->Add($arLoadProductArray))
{
    $arSelectFile = Array("ID", "NAME", "PROPERTY_FILE");
    $arFilterFile = Array("IBLOCK_ID"=>REQUESTS_IBLOCK_ID, "ID" => $PRODUCT_ID);
    $resFile = CIBlockElement::GetList(Array(), $arFilterFile, false, false, $arSelectFile);
    if($obFile = $resFile->GetNextElement())
    {
        $arFieldsFile = $obFile->GetFields();
        $fileID=$arFieldsFile["PROPERTY_FILE_VALUE"];
    }
    
    $successDB="Y";
}
else
{
     $successDB="N";
     AddMessage2Log($el->LAST_ERROR);
}

//ОТПРАВЛЯЕМ НАМ заявку по почте
$arrFiles=array();
if ($fileID>0)
{
    $arrFiles=array($fileID);
    AddMessage2Log($fileID);
    
}
$arEventFields = array(
	"FIO" => $user_name,
    "PHONE" => $user_phone,
    "EMAIL_FROM" => $user_mail_from,
    "EMAIL" => $user_mail,
    "CUSTOM_FIELDS" => $customField1.$customField2,
    "FORM_INFO" => str_replace("\n","<br />",$form_info),
    "IP" => $ip,
    "REFERRER" => $_POST["referrer"],
    "URL" => $_SERVER["HTTP_REFERER"],
); 
CEvent::SendImmediate("NEW_REQUEST", "s1", $arEventFields,"",29,$arrFiles);
////////////////////////////////////////////////////////////////////////

    
$result["success"]="1";
echo json_encode($result);
?>