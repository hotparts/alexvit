<?php

if (file_exists(dirname(__FILE__) .'/anchor_links.php')) {
	$anc_links = unserialize(file_get_contents(dirname(__FILE__) .'/anchor_links.php'));
}
if (!isset($anc_links) ||!is_array($anc_links)) $anc_links = array();

$exit = false;
$page = $_SERVER['REQUEST_URI'];

if(strpos($page, 'favicon.ico') !== false) $exit = true;

if(preg_match('/\/index.php$/',$page)) $page = preg_replace('/^(.*\/)index.php/','$1',$page);
//$pageAr = explode('?',$page);
//$page = $pageAr[0];
//$page = trim($page,'/');

if(preg_match('/(\.png|\.jpg|\.gif|\.css|\.js)$/',$page)) $exit = true;

if(!$exit) {
	$used_anchors = array();
	$match = false;
	$txt = '';


	// Тут мы проверяем есть ли привязка текста для данной страницы

	if(isset($anc_links)) foreach($anc_links as $key => $page_info) {
		
		$used_anchors[] = $page_info['txt'];
		
		if(!$match) {
			if(is_array($page_info['uri'])) {
				foreach($page_info['uri'] as $uri) {
					if($uri == $page) {
						$txt = $page_info['txt'];
						$match = true;
					}
				}
			} else {
				if($page_info['uri'] == $page) {
					$txt = $page_info['txt'];
					$match = true;
				}
			}
		}
	}

	if(!$match) {
		
		$anchors_file = file(dirname(__FILE__) .'/anchors.txt');
		foreach($anchors_file as $k => $v) {
			$anchors_file[$k] = trim($v);
		}
		$anchors_count = count($anchors_file);
		$anchors = $anchors_file;
		
		foreach($anchors as $k => $v) {
			if(array_search($v,$used_anchors) !== false) unset($anchors[$k]);
		}
		$mask = '/href=(?:"|\')(?:http:\/\/)?(?:www\.)?.+\.[a-z]{2,4}\/(.*)(?:"|\')/si';

		if(count($anchors) != 0) { 	// Если есть неиспользованные анкоры
			$not_cycle_link = false;
			$j = 1;

			while($not_cycle_link == false && $j < $anchors_count) {
				$ankey = array_rand($anchors);
				$txt = $anchors[$ankey];
				$matches = array();

				preg_match($mask, $txt, $matches);

				if(empty($matches)) { 
					$not_cycle_link = true;
				} else {
					if($page != trim($matches[1],'/')) {
						$not_cycle_link = true;
					}
				}
			}
			
			$anc_links[] = array('uri' => $page, 'txt' => $txt);
			
		} else { // Если все анкоры уже расставлены, то мы записываем текущую страницу в массив со случайным анкором
			$not_cycle_link = false;
			$j = 1;

			while($not_cycle_link == false && $j < $anchors_count) {

				$key = rand(0, $anchors_count - 1);
				
				preg_match($mask, $anc_links[$key]['txt'], $matches);
				if(!empty($matches[1])) {
					if(is_array($anc_links[$key]['uri'])) {
						$cycle = false;
						foreach($anc_links[$key]['uri'] as $sub_uri) {
							if($sub_uri == trim($matches[1],'/')) {
								$cycle = true;
							}
						}
						if(!$cycle) $not_cycle_link = true;
					} else {
						if($anc_links[$key]['uri'] != trim($matches[1],'/')) {
							$not_cycle_link = false;
						}
					}
				} else {
					$not_cycle_link = true;
				}
				$j++;
			}
			
			$txt = $anc_links[$key]['txt'];
			
			if(is_array($anc_links[$key]['uri'])) {
				$anc_links[$key]['uri'][] = $page;
			} else {
				$anc_links[$key]['uri'] = array($anc_links[$key]['uri'], $page);
			}
		}
		
		file_put_contents(dirname(__FILE__) .'/anchor_links.php', serialize($anc_links));
	}

	echo $txt;
}
?>