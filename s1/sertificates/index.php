<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("sertificates");?>
<div class="row">
<div class="col-xs-3">
    <div class="left-catalog-menu">
        <p class="title">Каталог</p>
        <div class="catalog-section-list">

            <ul>
                <li><a href="/catalog/montazh-napolnykh-pokrytiy/">Монтаж напольных покрытий<i class="menu-arrow"></i></a>
                    <ul>
                        <li><a href="/catalog/montazh-napolnykh-pokrytiy/">Монтаж напольных покрытий</a></li>
                        <li><a href="/catalog/montazh-napolnykh-pokrytiy/">Монтаж напольных покрытий</a></li>
                        <li><a href="/catalog/montazh-napolnykh-pokrytiy/">Монтаж напольных покрытий</a></li>
                    </ul>
                </li>        
                <li><a href="/catalog/napolnye-pokrytiya/">Напольные покрытия<i class="menu-arrow"></i></a></li>
                <li><a href="/catalog/nastennye-pokrytiya/">Настенные покрытия<i class="menu-arrow"></i></a></li>
                <li><a href="/catalog/potolki/">Потолки<i class="menu-arrow"></i></a></li>
                <li><a href="/catalog/sertifikaty/">Сертификаты<i class="menu-arrow"></i></a></li>
                <li><a href="/catalog/soputstvuyushchie-tovary/">Сопутствующие товары<i class="menu-arrow"></i></a></li>
                <li><a href="/catalog/khimiya/">Химия<i class="menu-arrow"></i></a>
            <ul>
                        <li id="bx_4145281613_10"><a class="" href="/catalog/khimiya/forbo/">Forbo</a></li>
                        <li id="bx_4145281613_11"><a class="" href="/catalog/khimiya/homa/">Homa</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
   <div class="right-catalog col-xs-9">
        <h1>Сертификаты</h1>
        <div class="row">   
        <ul class="catalog-logos-tree sertificates">
            <li>
                <a class="catalog-section closed" href="javascript:void(0);">Forbo</a>
                    <ul class="catalog-logos-items">
                        <li><a href="javascript:void(0);"><i>Сертификат соответсвия ГОСТ 450-00</a></i></li>
                        <li><a href="javascript:void(0);"><i>Сертификат качества</a></i></li>
                        <li><a href="javascript:void(0);"><i>Сертификат противопоказаний</a></i></li>
                    </ul>

            </li>
            <li>
                <a class="catalog-section closed" href="javascript:void(0);">Desso</a>
                    <ul class="catalog-logos-level2">
                        <li>
                            <a class="catalog-section closed" href="javascript:void(0);">Напольные покрытия</a>
                            <ul class="catalog-logos-items">
                                <li><a href="javascript:void(0);"><i>Сертификат соответсвия ГОСТ 450-00</a></i></li>
                                <li><a href="javascript:void(0);"><i>Сертификат качества</a></i></li>
                                <li><a href="javascript:void(0);"><i>Сертификат противопоказаний</a></i></li>
                            </ul>
                        </li>
                        <li>
                            <a class="catalog-section closed" href="javascript:void(0);">Химия</a>
                            <ul class="catalog-logos-items">
                                <li><a href="javascript:void(0);"><i>Сертификат соответсвия ГОСТ 450-00</a></i></li>
                                <li><a href="javascript:void(0);"><i>Сертификат качества</a></i></li>
                                <li><a href="javascript:void(0);"><i>Сертификат противопоказаний</a></i></li>
                            </ul>
                        </li>
                    </ul>
            </li>
            <li>
                <a class="catalog-section closed" href="javascript:void(0);">Escom</a>
                <ul class="catalog-logos-items">
                    <li><a href="javascript:void(0);"><i>Сертификат соответсвия ГОСТ 450-00</a></i></li>
                    <li><a href="javascript:void(0);"><i>Сертификат качества</a></i></li>
                    <li><a href="javascript:void(0);"><i>Сертификат противопоказаний</a></i></li>
                </ul>

            </li>
        </ul>
        </div>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>