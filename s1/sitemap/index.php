<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Карта сайта");
$APPLICATION->SetPageProperty("title", "Карта сайта - ООО «АлексВит»");
?>
<ul class="map-level-0">
	<li><a href="/">Главная</a></li>
	<li><a href="/o-kompanii/">О компании</a></li>
	<li>Каталог	
		<ul>
		<? $arFilter = array("IBLOCK_ID" => 3, "ACTIVE" => "Y"); 
		$sect = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, false);

		while($sect_result = $sect->GetNext())
		{
			echo '<li><a href="'.$sect_result['SECTION_PAGE_URL'].'">'.$sect_result['NAME'].'</a></li>';
		}?>
		</ul>
	</li>
	<li><a href="/proekty/">Проекты</a></li>
	<li><a href="/novosti/">Новости</a></li>
	<li><a href="/kontakty/">Контакты</a></li>		
</ul>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>