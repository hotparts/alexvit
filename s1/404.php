<?
if (defined("ERROR_404"))
{
    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404","Y");    
    require($_SERVER["DOCUMENT_ROOT"]."/local/templates/main-responsive/header.php");
} else
{    
    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404","Y");    
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");    
}
$APPLICATION->SetTitle("404 - Страница не найдена - ООО «АлексВит»");
?>
<br /><br />
<div class="small-section">
  <div class="container">
    <div class="row t-a-c">
      <div class="col-md-12">
        <div class="mt-40 mb-40">
          <h1>Ошибка 404</h1>
          <div class="big-desc">
            <p>Запрашиваемая Вами страница не найдена.<br>Возможно она была удалена, либо Вы набрали неверный адрес.</p>
          </div>
        </div>
        <div class="big-btn"><a href="/">Перейти на главную</a></div><br/>
		 <div class="big-btn"><a href="/sitemap/">Карта сайта</a></div>
      </div>
    </div>
  </div>
</div>
<?
if (defined("ERROR_404"))
{
    require($_SERVER["DOCUMENT_ROOT"]."/local/templates/main-responsive/footer.php");
} else
{    
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");    
}
?>