<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
IncludeTemplateLangFile(__FILE__);
?>

<? if (CSite::InDir(SITE_DIR . 'news/') or CSite::InDir(SITE_DIR . 'contacts/') or CSite::InDir(SITE_DIR . 'catalog/') or CSite::InDir(SITE_DIR . 'personal/') or CSite::InDir(SITE_DIR . 'auth/') or CSite::InDir(SITE_DIR . 'search/')) : ?>

<? else: ?>
    </div>
<? endif; ?>

</main><!-- .content -->
<?/*div class="container-fluid">
    <? if (CSite::InDir(SITE_DIR . 'index.php')): ?>
        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/tree.php", "EDIT_TEMPLATE" => "")); ?>
    <? endif; ?>
</div*/?>
</div><!-- .wrapper -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-2 logo-block">
                <?if ($_SERVER['REQUEST_URI'] != '/'){?><a href="/" class="logo-link"><?}else{?><span class="logo-link"><?}?>
					<img src="<?=LOGOTIP_FILE?>" alt="Alexvit" />
                    <?if ($_SERVER['REQUEST_URI'] != '/'){?></a><?}else{?></span><?}?>
            </div>
            <div class="col-xs-12 col-sm-4 copyright-block">
                <small>
                    <?if($_SERVER['REQUEST_URI'] == '/'){?>
                        &copy; Copyright. 2014-<?=date('Y')?> г. ООО «АлексВит» — напольные и настенные покрытия в Москве
                    <?}else{?>
                        &copy; Copyright. 2014-<?=date('Y')?> г. ООО «АлексВит» — <?php include($_SERVER['DOCUMENT_ROOT'].'/copyrights/give_me_copyright.php'); ?>
                    <?}?>
                    <br>
                    Все права защищены. Копирование материалов<br>
                    с сайта без разрешения правооблодателя<br>
                    запрещено законом.</small>
            </div>
            <div class="col-xs-12 col-sm-2 col-xs-offset-1 adress-block">
                <p><?=FOOTER_ADDRESS_TEXT?></p>
                <div class="socialItems">
                    <a href="https://vk.com/alexvit_ru" target="_blank">
                        <img src="/include/vk.png" alt="">
                    </a>
                    <a href="https://www.instagram.com/alexvit.ru/" target="_blank">
                        <img src="/include/instagram.png" alt="">
                    </a>
                    <a href="https://business.facebook.com/alexvitalexvit/?ref=your_pages" target="_blank">
                        <img src="/include/facebook.png" alt="">
                    </a>
                    <a href="https://t.me/alexvit_ru" target="_blank">
                        <img src="/include/telegram.png" alt="">
                    </a>
                    <a href="https://www.youtube.com/channel/UC_dy1RBpFQa7gvV-aiGLwMA/" target="_blank">
                        <img src="/include/youtube.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-2 col-xs-offset-1 contacts-block">
                <a class="phone ya-phone" href="tel:<?=PHONE_1_URL_TEXT?>"><?=PHONE_1_TEXT?></a><br>
                <a class="mail" href="mailto:info@alexvit.ru">info@alexvit.ru</a>
                <?/*p class="created-by">Создание сайта <a href="https://ameton.ru/" target="_blank">Ameton</a></p*/?>
            </div>
        </div>
    </div>
    <a href="#" class="customFormLink footerQuestionLink"
       data-params="YNYYN-NNYNN"
       data-form-code="shapka_zvonok"
       data-title="Введите ваши данные, мы перезвоним"
       data-text="Заполните форму заявки и мы дадим Вам скидку и мы продадим вам по цене конкурентов."
       data-btn-text="Отправить"
       data-service-form-name=""
       data-field-title-phone=""
       data-field-title-name=""
       data-field-placeholder-name="Представьтесь пожалуйста"
       data-field-placeholder-phone="Ваш контактный телефон"
       data-custom-textarea-title=""
       data-custom-textarea-placeholder="Удобное время звонка"
       data-metrika-counter="43821664"
       data-metrika-target-success="RECALL"
       data-metrika-target-try="RECALL"
       data-ga-target-success="otpravit"
       data-ga-target-try="otpravit"
    >
        <span class="footerQuestionLink-icon"></span>
        <span class="footerQuestionLink-inner"></span>
    </a>
</footer>
<?/*footer>
    <div class="container-fluid">
        <div class="row">
            <div class="!copyright col-xs-12">
                <div class="company_name col-xs-12 col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="copyright">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/copyright.php", "EDIT_TEMPLATE" => "")); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div id="bx-composite-banner"></div>
                        </div>
                    </div>
                </div>
                <div class="righ_txt col-xs-12 col-md-6">
                    <div class="copyright-marketplace">
                        <a href="http://marketplace.1c-bitrix.ru/partners/detail.php?ID=712923.php" class="krayt"
                           target="_blank"><?= GetMessage("READY_SOLUTION") ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer*/?>
<aside class="left_panel">
    <div class="head">
        <a href="/">
            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/company_name.inc.php", "EDIT_TEMPLATE" => "")); ?>
        </a>
        <i class="mdi mdi-close"></i>
    </div>
    <div class="content">
        <? $APPLICATION->IncludeComponent("bitrix:menu", "catalog-menu", array(
            "ROOT_MENU_TYPE" => "catalog",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "MENU_CACHE_GET_VARS" => array(),
            "MAX_LEVEL" => "1",
            "CHILD_MENU_TYPE" => "catalog",
            "USE_EXT" => "Y",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "N"
        ),
            false
        ); ?>
        <? $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "top-menu",
            Array(
                "ROOT_MENU_TYPE" => "top",
                "MAX_LEVEL" => "1",
                "CHILD_MENU_TYPE" => "",
                "USE_EXT" => "N",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => array()
            ),
            false
        ); ?>
        <? $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form",
            "emarket_auth",
            array(
                "REGISTER_URL" => SITE_DIR . "auth/",
                "FORGOT_PASSWORD_URL" => "",
                "PROFILE_URL" => SITE_DIR . "personal/",
                "SHOW_ERRORS" => "N"
            ),
            false
        ); ?>
        <? $APPLICATION->IncludeComponent(
            "bitrix:search.title",
            ".default",
            array(
                "NUM_CATEGORIES" => "1",
                "TOP_COUNT" => "5",
                "ORDER" => "rank",
                "USE_LANGUAGE_GUESS" => "Y",
                "CHECK_DATES" => "N",
                "SHOW_OTHERS" => "N",
                "PAGE" => "#SITE_DIR#search/index.php",
                "CATEGORY_0_TITLE" => "'" . GetMessage("H_CATALOGSEARCH") . "'",
                "CATEGORY_0" => array(
                    0 => "iblock_catalog",
                ),
                "CATEGORY_0_iblock_catalog" => array(
                    0 => "5",
                ),
                "SHOW_INPUT" => "Y",
                "INPUT_ID" => "search_input",
                "CONTAINER_ID" => "search"
            ),
            false
        ); ?>
    </div>
</aside>
<div class="overflow"></div>
<? if (COption::GetOptionString("krayt.stroi2", "SHOW_MODAL") == "Y"): ?>
    <div id="cookies_modal">
        <div class="cookies_modal_text">
            <?= COption::GetOptionString("krayt.stroi2", "MODAL_TEXT"); ?>
        </div>
        <div class="cookies_modal_btn">
            <button id="cookies_modal_btn" class="btn btn_blue"><?= GetMessage('cookies_btn') ?></button>
        </div>
    </div>
    <script>
        BX.ready(function () {
            var show_cookies_modal = BX.getCookie('show_cookies_modal');

            if (!show_cookies_modal) {
                BX.adjust(BX('cookies_modal'), {style: {display: 'block'}});
            }
            ;
            BX.bind(BX('cookies_modal_btn'), 'click', function () {
                BX.setCookie('show_cookies_modal', "1", {path: "/"});
                BX.adjust(BX('cookies_modal'), {style: {display: 'none'}});
            });
        });
    </script>
<? endif; ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(54270616, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/54270616" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script src="//code.jivosite.com/widget.js" data-jv-id="beE7nxo9Si" async></script>
<div class="scroll-top">
</div>

<!-- calltouch -->
<script type="text/javascript">
(function(w,d,n,c){w.CalltouchDataObject=n;w[n]=function(){w[n]["callbacks"].push(arguments)};if(!w[n]["callbacks"]){w[n]["callbacks"]=[]}w[n]["loaded"]=false;if(typeof c!=="object"){c=[c]}w[n]["counters"]=c;for(var i=0;i<c.length;i+=1){p(c[i])}function p(cId){var a=d.getElementsByTagName("script")[0],s=d.createElement("script"),i=function(){a.parentNode.insertBefore(s,a)};s.type="text/javascript";s.async=true;s.src="https://mod.calltouch.ru/init.js?id="+cId;if(w.opera=="[object Opera]"){d.addEventListener("DOMContentLoaded",i,false)}else{i()}}})(window,document,"ct","bzoa35z2");
</script>

<script type="text/javascript" >
jQuery(document).on('click', 'form input[type="button"]', function() {
	var m = jQuery(this).closest('form');
    var fio = m.find('input[name="name"]').val();
    var phone = m.find('input[name="phone"]').val();
    var ct_site_id = '38775';
    var sub = 'Купить в 1 клик';
    var ct_data = {
        fio: fio,
        phoneNumber: phone,
        subject: sub,
		requestUrl: location.href,
        sessionId: window.call_value
    };
    if (!!fio && !!phone){
		console.log(ct_data);
        jQuery.ajax({
            url: 'https://api-node15.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
            dataType: 'json', type: 'POST', data: ct_data, async: false
        });
    }
});
</script>

<script type="text/javascript" >
jQuery('#bx-soa-order-form a[class="pull-right btn btn_blue"]').click(function() {
	var m = jQuery(this).closest('form');
    var fio = m.find('#soa-property-1').val();
    var phone = m.find('#soa-property-3').val();
    var mail = m.find('#soa-property-2').val();
    var adres = m.find('#soa-property-7').val();
    var comment = m.find('#orderDescription').val();
    var ct_site_id = '38775';
    var sub = 'Оформление заказа';
    var ct_data = {
        fio: fio,
        phoneNumber: phone,
        subject: sub,
        email: mail,
		comment: comment,
		requestUrl: location.href,
        sessionId: window.call_value
    };
    if (!!fio && !!mail && !!phone && !!adres){
		console.log(ct_data);
        jQuery.ajax({
            url: 'https://api-node15.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
            dataType: 'json', type: 'POST', data: ct_data, async: false
        });
    }
});
</script>
<script type="text/javascript" >
$('[href="/modal-forms/feedback/"]').click(function() {
setTimeout(function(){
jQuery('a[name="submit"]').click(function() {
    var m = jQuery(this).closest('section.form-window');
    var fio = m.find('input[name="fio"]').val();
    var phone = m.find('input[name="phone"]').val();
    var ct_site_id = '38775';
    var sub = 'Позвонить';
    var ct_data = {
        fio: fio,
        phoneNumber: phone,
        subject: sub,
        requestUrl: location.href,
        sessionId: window.call_value
    };
    if (!!fio && !!phone){
        console.log(ct_data);
        jQuery.ajax({
            url: 'https://api-node15.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
            dataType: 'json', type: 'POST', data: ct_data, async: false
        });
    }
});
}, 1000);
});
</script>
<!-- calltouch -->

</body>
</html>
