<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="left-catalog-menu">
    <p class="title">Каталог</p>
    <div class="catalog-section-list">
        <ul>
<?
$previousLevel = 0;
foreach($arResult as $key=>$arItem):?>
	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li class="parent<?if ($arItem["SELECTED"]):?> active<?endif?>">
                <a href="<?=$arItem["LINK"]?>" <?if ($arItem["SELECTED"]):?>class="active"<?endif?>><?=$arItem["TEXT"]?><i class="menu-arrow"></i></a>
				<ul<?if ($arItem["SELECTED"]):?> class="opened active"<?endif?>>
		<?else:?>
			<li class="parent<?if ($arItem["SELECTED"]):?> active<?endif?>">
                <a href="<?=$arItem["LINK"]?>" <?if ($arItem["SELECTED"]):?> class="active"<?endif?>><?=$arItem["TEXT"]?><i class="menu-arrow"></i></a>
				<ul<?if ($arItem["SELECTED"]):?> class="opened active"<?endif?>>
		<?endif?>

	<?else:?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li><a href="<?=$arItem["LINK"]?>"<?if ($arItem["SELECTED"]):?> class="active"<?endif?>><?=$arItem["TEXT"]?></a></li>
		<?else:?>
			<li><a href="<?=$arItem["LINK"]?>"<?if ($arItem["SELECTED"]):?> class="active"<?endif?>><?=$arItem["TEXT"]?></a></li>
		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
<?endforeach?>

</ul></li>

</ul></div></div>
<?endif?>