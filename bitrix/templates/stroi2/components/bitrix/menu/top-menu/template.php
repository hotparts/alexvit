<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if (!empty($arResult)):?>
<ul class="menu">
    <? foreach($arResult as $arItem): ?>
        <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
            <?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
        <?endif?>
        <?if ($arItem["IS_PARENT"]):?>
            <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                <li class="parent"><a href="<?if($arItem["TEXT"]=='Сотрудничество'):?>javascript:void(0)<?else:?><?=$arItem["LINK"]?><?endif;?>" class="black_blue<?if ($arItem["SELECTED"]):?> selected<?endif?>"><?=$arItem["TEXT"]?></a>
                <ul>
            <?else:?>
                <li><a href="<?=$arItem["LINK"]?>" class="black_blue<?if ($arItem["SELECTED"]):?> selected<?endif?>"><?=$arItem["TEXT"]?></a>
                <ul>
            <?endif?>
        <?else:?>
            <?if ($arItem["PERMISSION"] > "D"):?>
                <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                    <li><a href="<?=$arItem["LINK"]?>" class="black_blue<?if ($arItem["SELECTED"]):?> selected<?endif?>"><?=$arItem["TEXT"]?></a></li>
                <?else:?>
                    <li><a class="black_blue<?if ($arItem["SELECTED"]):?> selected<?endif?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
                <?endif?>
            <?else:?>
                <?if ($arItem["DEPTH_LEVEL"] == 1):?>
                    <li><a href="" class="black_blue<?if ($arItem["SELECTED"]):?> selected<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
                <?else:?>
                    <li><a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
                <?endif?>
            <?endif?>
        <?endif?>
        <?$previousLevel = $arItem["DEPTH_LEVEL"];?>
    <?endforeach?>
    <?if ($previousLevel > 1)://close last item tags?>
        <?=str_repeat("</ul></li>", ($previousLevel-1) );?>
    <?endif?>
    <div class="clear_both"></div>
</ul>
<?endif?>