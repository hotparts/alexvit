<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (count($arResult["ITEMS"])>0)
{
    ?>
    <div class="main-projects">
        <div class="container">
            <h2>Реализованные проекты</h2>
            <div class="islider project-slider-main">
                <?
                foreach($arResult["ITEMS"] as $key=>$arItem)
                {
                    $img = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width'=>263, 'height'=>283), BX_RESIZE_IMAGE_EXACT, true);
                    ?>
                    <div class="project-item">
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                            <i class="top-line"></i><i class="left-line"></i>
                            <img src="<?=$img["src"]?>" alt="<?=$arItem["NAME"]?>">
                            <p><?=$arItem["NAME"]?></p>
                            <div class="item-shadow"></div>
                        </a>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>    
    </div>
    <?
}
?>