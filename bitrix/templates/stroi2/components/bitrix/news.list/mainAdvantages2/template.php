<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="s-advantages">
    <div class="container-fluid">
        <div class="row">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="col-12 col-xm-6 col-sm-6 col-md-4 col-lg-4 col-xl-4" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="advantages__item marginBottom15">
                        <?if($arItem['PREVIEW_PICTURE']['SRC']):?>
                        <div class="advantages__thumb">
                            <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['~NAME']?>">
                        </div>
                        <?endif;?>
                        <div class="advantages__content">
                            <div class="advantages__title"><?=$arItem['~NAME']?></div>
                            <div class="advantages__description"><?=$arItem['~PREVIEW_TEXT']?></div>
                        </div>
                    </div>
                </div>
            <?endforeach;?>
        </div>
    </div>
</section>