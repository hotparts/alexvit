<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (count($arResult["ITEMS"])>0)
{
	foreach($arResult["ITEMS"] as $key=>$arItem)
    {
		$foto=$arItem["PROPERTIES"]["SFOTO"]["VALUE"];
        if (!empty($arItem["PROPERTIES"]["SFOTO"]["VALUE"]))
        {
			$img = CFile::ResizeImageGet($foto, array('width'=>9999, 'height'=>209), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$imgbig = CFile::ResizeImageGet($foto, array('width'=>2000, 'height'=>1024), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        ?>
			<div class="slide-item">
				<a href="<?=$imgbig["src"]?>" class="img fancybox" rel="gal1">
					<img src="<?=$img["src"]?>" alt="<?=$arItem["NAME"]?>"/>
				</a>
			</div><? 
		}	
	}
}
?>