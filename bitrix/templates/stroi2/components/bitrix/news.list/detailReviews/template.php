<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<a href="/modal-forms/review/" data-type="ajax" data-fancybox class="btn btn_blue feedbackBtn">
    <div class="review-add__title"><?=GetMessage('DETAIL_ADD_REVIEWS')?></div>
</a>
<div class="reviewBlock">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="review-detail<?if($arItem['ACTIVE'] == 'Y'):?> open<?endif;?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>"<?if($arItem['~DETAIL_TEXT']):?> style="margin-bottom: 0!important;"<?endif;?>>
            <div class="review__heading" id="<?=$arItem['ID']?>">
                <div class="review__title">
                    <?=$arItem['NAME']?>
                    <span class="review__date">
                        (<?=$arItem['DISPLAY_ACTIVE_FROM']?>)
                    </span>
                </div>
                <div class="review__toggle">
                    <div class="review-toggler"><i class="icon-angle-down"></i></div>
                </div>
            </div>
            <div class="review__body"<?if($arItem['ACTIVE'] == 'Y'):?> style="display:block;"<?endif;?>>
                <p><?=$arItem['~PREVIEW_TEXT']?></p>
            </div>
        </div>
        <?if($arItem['~DETAIL_TEXT']):?>
            <div class="review__admin">
                <p class="review__title">Администратор alexvit.ru</p>
                <p><?=$arItem['~DETAIL_TEXT']?></p>
            </div>
        <?endif;?>
    <?endforeach;?>
</div>
<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <br /><?=$arResult["NAV_STRING"]?>
    <?endif;?>
</div>