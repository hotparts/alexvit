<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if($arResult['ITEMS']){
    foreach ($arResult['ITEMS'] as $key=>$arItem){
        if($arItem['PROPERTIES']['DOCTOR']['VALUE']){
            $arResult['DOCTORS'][$arItem['PROPERTIES']['DOCTOR']['VALUE']] = $arItem['PROPERTIES']['DOCTOR']['VALUE'];
        }
        if($arParams['ACTIVE_REVIEW'] == $arItem['ID']){
            $arResult['ITEMS'][$key]['ACTIVE'] = 'Y';
        }
    }
    if($arResult['DOCTORS']){
        $arSelect = Array("ID", "NAME", "PROPERTY_POSITION", "PREVIEW_PICTURE");
        $arFilter = Array("IBLOCK_ID"=>PRmed::CIBlock_Id("prymery_content","prymery_doctors"), "ID" => $arResult['DOCTORS'], "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
        while($ob = $res->Fetch())
        {
            if($ob['PREVIEW_PICTURE']){
                $Resize = CFile::ResizeImageGet($ob['PREVIEW_PICTURE'], array('width'=>105, 'height'=>105), BX_RESIZE_IMAGE_EXACT, true);
                $ob['PICTURE'] = $Resize['src'];
            }
            $arResult['DOCTORS'][$ob['ID']] = $ob;
        }
    }
}