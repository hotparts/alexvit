<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (count($arResult["ITEMS"]) > 0) {
    ?>
    <div class="row projects">
        <div class="col-xs-4">
            <div class="projects-list">
                <h1>Проекты</h1>
                <ul>
                    <?
                    foreach ($arResult["ITEMS"] as $key => $arItem) {
                        ?>
                        <li><a<?
                        if ($key == 0) {
                            ?> class="active"<?
                        } ?> href="javascript:void(0)" data-id="<?= $arItem["ID"] ?>"><?= $arItem["NAME"] ?>
                            , <?= $arItem["PROPERTIES"]["ADDRESS"]["VALUE"] ?></a></li><?
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="col-xs-8">
            <?
            foreach ($arResult["ITEMS"] as $key => $arItem) {
                $fotos = array();
                $img = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width' => 2560, 'height' => 9999), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                $imgThumb = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width' => 120, 'height' => 120), BX_RESIZE_IMAGE_EXACT, true);
                $imgBig = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width' => 1920, 'height' => 1920), BX_RESIZE_IMAGE_PROPORTIONAL, true);

                $fotos[] = array(
                    "THUMB" => $imgThumb["src"],
                    "BIG" => $imgBig["src"],
                    "BIG_W" => $imgBig["width"],
                    "BIG_H" => $imgBig["height"],
                );

                if (!empty($arItem["PROPERTIES"]["FOTOS"]["VALUE"])) {
                    foreach ($arItem["PROPERTIES"]["FOTOS"]["VALUE"] as $foto) {
                        $imgThumb = CFile::ResizeImageGet($foto, array('width' => 120, 'height' => 120), BX_RESIZE_IMAGE_EXACT, true);
                        $imgBig = CFile::ResizeImageGet($foto, array('width' => 1920, 'height' => 1920), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                        $fotos[] = array(
                            "THUMB" => $imgThumb["src"],
                            "BIG" => $imgBig["src"],
                            "BIG_W" => $imgBig["width"],
                            "BIG_H" => $imgBig["height"],
                        );
                    }
                }
                ?>
                <div class="project_item<?
                if ($key == 0) {
                    ?> active<?
                } ?>" data-id="<?= $arItem["ID"] ?>">
                <div class="section main-photo">
                    <img src="<?= $img["src"] ?>" alt="<?= $arItem["NAME"] ?>">
                </div>
                <div class="section producer<?
                if (count($fotos) == 1) {
                    ?> hidden<?
                } ?>">
                    <div class="main-logos">
                        <div class="islider project-slider" itemscope itemtype="http://schema.org/ImageGallery">
                            <?
                            foreach ($fotos as $foto) {
                                ?>
                                <figure class="logo-item" itemprop="associatedMedia" itemscope
                                        itemtype="http://schema.org/ImageObject">
                                    <a rel="gal_<?= $arItem["ID"] ?>" class="fancybox_img" href="<?= $foto["BIG"] ?>" data-size="<?= $foto["BIG_W"] ?>x<?= $foto["BIG_H"] ?>"
                                       itemprop="contentUrl">
                                                <span>
                                                    <img src="<?= $foto["THUMB"]; ?>" alt=""/>
                                                </span>
                                    </a>
                                <figcaption itemprop="caption description"><?= $arItem["NAME"] ?></figcaption>
                                </figure><?
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?
                $detailText = trim($arItem["~DETAIL_TEXT"]);
                if ($detailText != "") {
                    ?>
                    <div class="catalog-descript">
                        <?
                        if ($arItem["DETAIL_TEXT_TYPE"] == "html") {
                            echo $detailText;
                        } else {
                            ?><p><?= str_replace("\n", "<br />", $detailText) ?></p><?
                        }
                        ?>
                    </div>
                    <?
                }
                ?>
                </div><?
            }
            ?>
        </div>
    </div>
    <?
}
?>