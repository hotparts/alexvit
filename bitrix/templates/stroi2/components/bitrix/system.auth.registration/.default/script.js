/**
 * Created by aleksander on 27.06.2017.
 */
function sendForm(form)
{
    if(!$("#pk_input_comment").is(':checked'))
    {
        $("#pk_wrp_comment").css({'border-bottom': "1px solid #F44336"});
        return false;
    }

}

$(document).ready(function() {

    $("#pk_input_comment").click(function () {

        if (!$(this).is(':checked')) {
            $(this).addClass('error');
            $('#pk_wrp_comment').addClass('error');
        } else {
            $(this).removeClass('error');
            $('#pk_wrp_comment').removeClass('error').css({'border-bottom': "none"});
        }
    });

});