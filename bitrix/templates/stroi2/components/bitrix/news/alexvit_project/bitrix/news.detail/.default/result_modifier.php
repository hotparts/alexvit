<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
if($arResult['DETAIL_PICTURE'])
    $arResult['DETAIL_PICTURE'] = CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width'=>460, 'height'=>320), BX_RESIZE_IMAGE_EXACT, true);

if(!empty($arResult["PROPERTIES"]["FOTOS"]["VALUE"]))
{
    $arFiles = array();
    foreach($arResult["PROPERTIES"]["FOTOS"]["VALUE"] as $value)
        $arFiles[] = $value;

    $rsFile = CFile::GetList(array(),array("@ID" => implode(",",$arFiles)),array());
    while($arFile = $rsFile->GetNext())
        $arResult["PHOTO"][] = $arFile;

    foreach($arResult["PHOTO"] as $i => $value)
    {
        $tmp = CFile::ResizeImageGet(
            $value,
            array('width' => 555, 'height' => 386),
            BX_RESIZE_IMAGE_EXACT,
            true
        );
        $arResult["PHOTO"][$i]["RESIZE"] = $tmp;
        if($arResult["PHOTO"][$i]["RESIZE"]["src"] == "")
            $arResult["PHOTO"][$i]["RESIZE"]["src"] = "/upload/".$value["SUBDIR"]."/".$value["FILE_NAME"];
    }
}
?>