<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
    <div class="news row">
        <div class="col-xs-12">
            <h2><?=$arResult['NAME']?></h2>
            <div>
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $renderImage = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], Array("width" => 170, "height" => 135), BX_RESIZE_IMAGE_EXACT, false);
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="col-xs-12 col-sm-4" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <a class="item_project" href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
                        <?
                        $renderImage = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], Array("width" => 400, "height" => 400), BX_RESIZE_IMAGE_EXACT, false);
                        echo '<img alt="'.$arItem["NAME"].'" src="'.$renderImage["src"].'" />';
                        ?>
                        <i class="top-line"></i><i class="left-line"></i>
                        <div class="news_content col-xs-8">
                            <h3><?echo $arItem["NAME"]?></h3>
                            <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
                                <p><?echo $arItem["PREVIEW_TEXT"];?></p>
                            <?endif;?>
                        </div>
                        <div class="item-shadow"></div>
                    </a>
                </div>
            <?endforeach;?>
            <div class="clear_both"></div>
            </div>
        </div>
    </div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
