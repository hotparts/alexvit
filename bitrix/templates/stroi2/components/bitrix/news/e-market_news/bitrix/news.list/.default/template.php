<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
    <div class="!news row">
        <div class="col-xs-12">
            <h2><?=$arResult['NAME']?></h2>
            <div class="row" >
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="col-xs-12 col-sm-6 col-md-6" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="advice">
                         <div class="adviceimg">
                             <div class="adviceimgtext"><?echo $arItem["NAME"]?></div>
                             <?
                             $renderImage = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], Array("width" => 495, "height" => 235), BX_RESIZE_IMAGE_EXACT, false);
                             echo '<img alt="'.$arItem["NAME"].'" src="'.$renderImage["src"].'" />';
                             ?>
                             <div class="clearfix"></div>
                         </div>
                        <div class="adviceinfo">
                            <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
                                <p><?=preg_replace("/&#?[a-z0-9]{2,8};/i","",$arItem["PREVIEW_TEXT"]);?></p>
                            <?endif;?>
                            <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
                                Читать
                            </a>
                        </div>
                    </div>
                </div>
            <?endforeach;?>
            <div class="clear_both"></div>
            </div>
        </div>
    </div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
