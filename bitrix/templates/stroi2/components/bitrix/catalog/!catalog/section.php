<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
$this->setFrameMode(true);

$vidName="";
$text=""; $h1="";
$meta_title="";
$meta_desc="";
$meta_key="";

if($_SERVER["REQUEST_URI"]=="/brand-armstrong/"){$h1="Напольные покрытия Armstrong";}
elseif($_SERVER["REQUEST_URI"]=="/brand-barlinek/"){$h1="Напольные покрытия Barlinek";}
elseif($_SERVER["REQUEST_URI"]=="/brand-grabo/"){$h1="Напольные покрытия Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/brand-modulyss/"){$h1="Напольные покрытия Modulyss";}
elseif($_SERVER["REQUEST_URI"]=="/brand-vertigo-trend/"){$h1="Напольные покрытия VERTIGO Trend";}
elseif($_SERVER["REQUEST_URI"]=="/khimiya/forbo/"){$h1="Грунтовки и клей Forbo Eurocol";}
elseif($_SERVER["REQUEST_URI"]=="/khimiya/homa/"){$h1="Грунтовки и клеи Homa";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/"){$h1="Напольные покрытия";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/dizayn-plitka-pvkh/brand-forbo/"){$h1="Дизайн плитка ПВХ Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/falshpol/brand-perfaten/"){$h1="Фальшполы Perfaten";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/"){$h1="Флокированное покрытие Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/igloprobivnoy-kovrolin/brand-forbo/"){$h1="Иглопробивной ковролин Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/"){$h1="Керамогранит ColiseumGres";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/"){$h1="Керамогранит Estima";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=derevo"){$h1="Керамогранит Estima под дерево";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=dizayn"){$h1="Керамогранит Estima серия Дизайн";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=kamen"){$h1="Керамогранит Estima под камень";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/"){$h1="Керамогранит Kerama Marazzi";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=derevo"){$h1="Керамогранит Kerama Marazzi под дерево";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=kamen"){$h1="Керамогранит Kerama Marazzi под камень";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-armstrong/"){$h1="Коммерческий линолеум Armstrong";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-armstrong/?vid=gomogennyy"){$h1="Гомогенный линолеум Armstrong";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/"){$h1="Коммерческий линолеум Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-grabo/"){$h1="Коммерческий линолеум Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-grabo/?vid=polukummercheskiy"){$h1="Полукоммерческий линолеум Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-polyflor/"){$h1="Коммерческий линолеум Polyflor";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-escom/"){$h1="Ковровая плитка Escom";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-forbo/"){$h1="Ковровая плитка Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-modulyss/"){$h1="Ковровая плитка Modulyss";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/"){$h1="Натуральный линолеум Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=marmoleum-marbled"){$h1="Натуральный линолеум Forbo Marmoleum Marbled";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/parketnaya-doska/brand-barlinek/"){$h1="Паркетная доска Barlinek";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/parketnaya-doska/brand-grabo/"){$h1="Паркетная доска — Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/sportivnye-pokrytiya/brand-forbo/"){$h1="Спортивные покрытия — Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/stsenicheskie-pokrytiya/brand-grabo/"){$h1="Сценические покрытия — Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/nastennye-pokrytiya/keramicheskaya-plitka/brand-kerama-marazzi/"){$h1="Керамическая плитка Kerama Marazzi";}
elseif($_SERVER["REQUEST_URI"]=="/nastennye-pokrytiya/keramicheskaya-plitka/brand-sokol/"){$h1="Керамическая плитка Сокол";}
elseif($_SERVER["REQUEST_URI"]=="/nastennye-pokrytiya/lakokrasochnye-materialy/kraski-lacos/"){$h1="Лакокрасочные материалы Lacos";}
elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/"){$h1="Подвесные и аккустические потолки Ecophon";}
elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/?group_collection=advantage"){$h1="Модульные потолки Ecophon Advantage";}
elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/?group_collection=focus"){$h1="Модульные потолки Ecophon Focus";}
elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/?group_collection=hygiene"){$h1="Модульные потолки Ecophon Hygiene";}
elseif($_SERVER["REQUEST_URI"]=="/potolki/svobodno-visyashchie-elementy-i-baffly/brand-ecophon/"){$h1="Баффлы Ecophon";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/dizayn-plitka-pvkh/brand-grabo/"){$h1="Дизайн плитка ПВХ Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/sportivnye-pokrytiya/brand-grabosport/"){$h1="Спортивные покрытия GraboSport";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/dizayn-plitka-pvkh/brand-vertigo-trend/"){$h1="Дизайн плитка ПВХ VERTIGO Trend";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-armstrong/?vid=protivoskolzyashchiy"){$h1="Противоскользящий коммерческий линолеум Armstrong";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/?vid=beton"){$h1="Керамогранит ColiseumGres Бетон";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/?vid=derevo"){$h1="Керамогранит ColiseumGres Дерево";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/?vid=kamen"){$h1="Керамогранит ColiseumGres Камень";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/?vid=mramor"){$h1="Керамогранит ColiseumGres Мрамор";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-escom/?vid=petlevoy-vors"){$h1="Ковровая плитка Escom с петлевым ворсом";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=beton"){$h1="Керамогранит Estima Бетон";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=mramor"){$h1="Керамогранит Estima Мрамор";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=tekhnika"){$h1="Керамогранит Estima Техника";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-grabo/?vid=gomogennyy"){$h1="Гомогенный коммерческий линолеум Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=beton"){$h1="Керамогранит Kerama Marazzi Бетон";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=dizayn"){$h1="Керамогранит Kerama Marazzi Дизайн";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=mramor"){$h1="Керамогранит Kerama Marazzi Мрамор";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-polyflor/?vid=gomogennyy"){$h1="Гомогенный коммерческий линолеум Polyflor";}

elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/brand-forbo/"){$h1="Напольные покрытия Forbo"; $meta_title="Напольные покрытия Forbo оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа напольных покрытий Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="напольные покрытия Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/?group_collection=flotex-colour"){$h1="Флокированное покрытие Forbo Flotex Colour"; $meta_title="Флокированное покрытие Forbo группы Flotex Colour оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа флокированных покрытий Forbo Flotex Colour по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="флокированное покрытие forbo flotex colour";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/?group_collection=flotex-linear"){$h1="Флокированное покрытие Forbo Flotex Linear"; $meta_title="Флокированное покрытие Forbo группы Flotex Linear оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа флокированных покрытий Forbo Flotex Linear по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="флокированное покрытие forbo flotex linear";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/?group_collection=flotex-sottsass"){$h1="Флокированное покрытие Forbo Flotex Sottsass"; $meta_title="Флокированное покрытие Forbo группы Flotex Sottsass оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа флокированных покрытий Forbo Flotex Sottsass по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="флокированное покрытие forbo flotex sottsass";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/?group_collection=flotex-tibor"){$h1="Флокированное покрытие Forbo Flotex Tibor"; $meta_title="Флокированное покрытие Forbo группы Flotex Tibor оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа флокированных покрытий Forbo Flotex Tibor по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="флокированное покрытие forbo flotex tibor";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/?group_collection=flotex-vision-"){$h1="Флокированное покрытие Forbo Flotex Vision"; $meta_title="Флокированное покрытие Forbo группы Flotex Vision оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа флокированных покрытий Forbo Flotex Vision по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="флокированное покрытие forbo flotex vision";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/?vid=gomogennyy"){$h1="Гомогенный коммерческий линолеум Forbo"; $meta_title="Гомогенный коммерческий линолеум Forbo оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа гомогенного коммерческого линолеума Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="гомогенный коммерческий линолеум Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/?vid=kommercheskiy"){$h1="Линолеум Forbo коммерческого типа"; $meta_title="Коммерческий линолеум Forbo коммерческого типа оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа линолеума Forbo коммерческого типа по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="линолеум Forbo коммерческого типа";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/?vid=polukummercheskiy"){$h1="Полукоммерческий линолеум Forbo"; $meta_title="Полукоммерческий линолеум Forbo оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа полукоммерческого линолеума Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="полукоммерческий линолеум Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/?vid=protivoskolzyashchiy"){$h1="Коммерческий линолеум Forbo противоскользящий"; $meta_title="Противоскользящий коммерческий линолеум Forbo оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа противоскользящего коммерческого линолеума Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="противоскользящий коммерческий линолеум Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/?vid=tokoprovodyashchie-poly-i-chistye-pomeshcheniya-"){$h1="Коммерческий линолеум Forbo: Токопроводящие полы и чистые помещения"; $meta_title="Коммерческий линолеум Forbo: токопроводящие полы и чистые помещения оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа токопроводящего коммерческого линолеума Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="токопроводящие полы Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-forbo/?vid=kombinirovannaya-petlya"){$h1="Ковровая плитка Forbo с комбинированной петлей"; $meta_title="Ковровая плитка Forbo с комбинированной петлёй оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа ковровой плитки Forbo с комбинированной петлей по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="ковровая плитка Forbo с комбинированной петлей";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-forbo/?vid=petlevoy-vors"){$h1="Ковровая плитка Forbo с петлевым ворсом"; $meta_title="Ковровая плитка Forbo с петлевым ворсом оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа ковровой плитки Forbo с петлевым ворсом по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="ковровая плитка Forbo с петлевым ворсом";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-forbo/?vid=razrezanaya-petlya"){$h1="Ковровая плитка Forbo с разрезаной петлей"; $meta_title="Ковровая плитка Forbo с разрезаной петлёй оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа ковровой плитки Forbo с разрезаной петлей по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="ковровая плитка Forbo с разрезаной петлей";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-forbo/?vid=svobodnyy-poryadok-ukladki"){$h1="Ковровая плитка Forbo со свободным порядком укладки"; $meta_title="Ковровая плитка Forbo со свободным порядком укладки оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа ковровой плитки Forbo со свободным порядком укладки по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="ковровая плитка Forbo со свободным порядком укладки";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=akusticheskiy-marmoleum"){$h1="Натуральный акустический линолеум Forbo Marmoleum"; $meta_title="Натуральный линолеум Forbo группы Акустический Marmoleum оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа акустического натурального линолеума Forbo Marmoleum по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="натуральный акустический линолеум forbo marmoleum";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=marmoleum-linear"){$h1="Натуральный линолеум Forbo Marmoleum Linear"; $meta_title="Натуральный линолеум Forbo группы Marmoleum Linear оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа натурального линолеума Forbo Marmoleum Linear по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="натуральный линолеум forbo marmoleum linear";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=marmoleum-patterned"){$h1="Натуральный линолеум Forbo Marmoleum Patterned"; $meta_title="Натуральный линолеум Forbo группы Marmoleum Patterned оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа натурального линолеума Forbo Marmoleum Patterned по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="натуральный линолеум forbo marmoleum patterned";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=marmoleum-solid"){$h1="Натуральный линолеум Forbo Marmoleum Solid"; $meta_title="Натуральный линолеум Forbo группы Marmoleum Solid оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа натурального линолеума Forbo Marmoleum Solid по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="натуральный линолеум forbo marmoleum solid";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=tokorasseivayushchiy-marmoleum"){$h1="Натуральный токорассеивающий линолеум Forbo"; $meta_title="Натуральный линолеум Forbo токорассеивающей группы оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа токорассеивающего натурального линолеума Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="натуральный токорассеивающий линолеум forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/sportivnye-pokrytiya/brand-forbo/?vid=gomogennyy"){$h1="Гомогенные спортивные покрытия Forbo"; $meta_title="Спортивные гомогенные покрытия Forbo оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа гомогенных покрытий Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="гомогенные спортивные покрытия Forbo";}

$arrUrlPath=explode("/",substr($APPLICATION->GetCurDir(),1,strlen($APPLICATION->GetCurDir())-2));
$filterCandidate=end($arrUrlPath);
$filterCandidateFirst=$arrUrlPath[0];

$isFilterCandidateFirst=false;
$isCollectionTpl=false;
$isTypePlaceTpl=false;
$isBrandTpl=false;

$isLaki=false;

$ADD_SECTIONS_CHAIN="Y";

if (strpos($filterCandidateFirst,"type-")===0)
{
    $filterCandidateFirst=explode("type-",$filterCandidateFirst);
    $filterCandidateFirst=$filterCandidateFirst[1];
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
    $arFilter = Array("IBLOCK_ID"=>TYPE_PLACE_IBLOCK_ID, "CODE"=>$filterCandidateFirst, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $curBrand=$arFields;
        $_GET["brand"]="type_".$arFields["ID"];
        
        $isTypePlaceTpl=true;
        
        if (count($arrUrlPath)==1)
        {
            $GLOBALS['sectionFilter'] = array("PROPERTY_TYPE_PLACE"=>$arFields["ID"]);
        } else
        {
            if (strpos($filterCandidate,"brand-")===0)
            {
                $filterCandidate=explode("brand-",$filterCandidate);
                $filterCandidate=$filterCandidate[1];
                $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
                $arFilter = Array("IBLOCK_ID"=>BRAND_IBLOCK_ID, "CODE"=>$filterCandidate, "ACTIVE"=>"Y");
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if($ob = $res->GetNextElement())
                {
                    $arFieldsBrand = $ob->GetFields();
                    $curBrand2=$arFieldsBrand;
                    $curSectCode=$arrUrlPath[count($arrUrlPath)-2];
                } else
                {
                    CHTTP::SetStatus("404 Not Found");
                    @define("ERROR_404","Y"); 
                }
            } else
            {
                $curSectCode=$arrUrlPath[count($arrUrlPath)-1];
            }
            
            $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'ACTIVE'=>'Y', 'CODE'=>$curSectCode);
            $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
            $isFilterCandidateFirst=true;
            if($ar_result = $db_list->GetNext())
            {
                $isCollectionTpl=true;
                $curSection=$ar_result;
                $arResult["VARIABLES"]["SECTION_ID"]=$ar_result['ID'];
                $GLOBALS['sectionFilter'] = array("PROPERTY_TYPE_PLACE"=>$arFields["ID"],"PROPERTY_BRAND"=>$arFieldsBrand["ID"]);
                $ADD_SECTIONS_CHAIN="N";
            } else
            {
                CHTTP::SetStatus("404 Not Found");
                @define("ERROR_404","Y");
            }
        }
    } else
    {
        CHTTP::SetStatus("404 Not Found");
        @define("ERROR_404","Y"); 
    }
} else
if (strpos($filterCandidate,"brand-")===0)
{
    $filterCandidate=explode("brand-",$filterCandidate);
    $filterCandidate=$filterCandidate[1];
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "DETAIL_TEXT");
    $arFilter = Array("IBLOCK_ID"=>BRAND_IBLOCK_ID, "CODE"=>$filterCandidate, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
		$text=$arFields["DETAIL_TEXT"];
        $curBrand=$arFields;
        
        if (count($arrUrlPath)==1)
        {
            $_GET["brand"]="brand_".$arFields["ID"];
            $isBrandTpl=true;
            $GLOBALS['sectionFilter'] = array("PROPERTY_BRAND"=>$arFields["ID"],'SECTION_ID' => array(1,2,3), "INCLUDE_SUBSECTIONS" => "Y");
        } else
        {
            $curSectCode=$arrUrlPath[count($arrUrlPath)-2];
            $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'ACTIVE'=>'Y', 'CODE'=>$curSectCode);
            $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
            
            if($ar_result = $db_list->GetNext())
            {
                $isCollectionTpl=true;
                $arResult["VARIABLES"]["SECTION_ID"]=$ar_result['ID'];
                $GLOBALS['sectionFilter'] = array("PROPERTY_BRAND"=>$arFields["ID"]);
            }
        }
    } else
    {
        CHTTP::SetStatus("404 Not Found");
        @define("ERROR_404","Y"); 
    }
} else
if (strpos($filterCandidate,"vid-")===0)
{
    $filterCandidate=explode("vid-",$filterCandidate);
    $filterCandidate=$filterCandidate[1];
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
    $arFilter = Array("IBLOCK_ID"=>VID_IBLOCK_ID, "CODE"=>$filterCandidate, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $curBrand=$arFields;
        $_GET["brand"]=$arFields["ID"];
        $curSectCode=$arrUrlPath[count($arrUrlPath)-2];
        $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'ACTIVE'=>'Y', 'CODE'=>$curSectCode);
        $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
        
        if($ar_result = $db_list->GetNext())
        {
            $isCollectionTpl=true;
            $arResult["VARIABLES"]["SECTION_ID"]=$ar_result['ID'];
            $GLOBALS['sectionFilter'] = array("PROPERTY_VID_TOVARA"=>$arFields["ID"]);
        } else
        {
            
        }
    } else
    {
        CHTTP::SetStatus("404 Not Found");
        @define("ERROR_404","Y"); 
    }
}

if (!$isTypePlaceTpl && !$isBrandTpl)
{
    $nav = CIBlockSection::GetNavChain(false, $arResult["VARIABLES"]["SECTION_ID"]);
    $nav_first_section = $nav->GetNext();
    $nav_second_section = $nav->GetNext();
    
    $res = CIBlockSection::GetByID($arResult["VARIABLES"]["SECTION_ID"]);
    if ($curSect = $res->GetNext())
    {
        
    } else
    {
        CHTTP::SetStatus("404 Not Found");
        @define("ERROR_404","Y"); 
    }
}

$tplSect="";
if ($isCollectionTpl)
{
    $tplSect='default_sub_collection';
} else
if ($isTypePlaceTpl)
{
    $tplSect='default_sub_place';
} else
if ($isBrandTpl)
{
    $tplSect='default_sub_brand';
} else
{
    switch ($nav_first_section["ID"])
    {
        //Монтаж напольных покрытий
        case "14":
            $tplSect='only_text';
            break;
        //сертификаты
        case "15":
        case "68":
            $tplSect='sertifikaty';
            break;
        //сопутствующие товары
        case "13":
            $tplSect='soputka';
            break;
        //химия
        case "4":
            //корневой раздел
            if ($curSect["DEPTH_LEVEL"]==1)
            {
                $tplSect='himiya_top';    
            } else
            //подраздел химии
            {
                $tplSect='himiya_sub';
            }
            break;
        default:
            //стандартный корневой раздел

            if ($nav_first_section["ID"]==2 && $nav_second_section["ID"]==33)
            {
                if ($arResult["VARIABLES"]["SECTION_ID"]==33)
                {
                    $tplSect='himiya_top';    
                } else
                //подраздел химии
                {
                    $isLaki=true;
                    $tplSect='himiya_sub';
                }
            } else
            if ($nav_first_section["ID"]==2 && $nav_second_section["ID"]==57)
            {
                if ($arResult["VARIABLES"]["SECTION_ID"]==57)
                {
                    $tplSect='himiya_top';    
                } else
                //подраздел химии
                {
                    $isLaki=true;
                    $tplSect='himiya_sub';
                }
            } else
            {
                if ($curSect["DEPTH_LEVEL"]==1)
                {
                    $tplSect='default_top';    
                } else
                //стандартный подраздел
                {
                    $tplSect='default_sub';
                }
            }
            break;
    }
}
if (is_array($curBrand) && $isFilterCandidateFirst)
{
    $APPLICATION->AddChainItem($curBrand["NAME"], "/type-".$filterCandidateFirst."/");
}
?>
<div class="row sertificates">
    <div class="col-xs-3">
        <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"catalog_left", 
	array(
		"ROOT_MENU_TYPE" => "catalog",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "4",
		"USE_EXT" => "Y",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "catalog_left",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N"
	),
	false
);?>
		
		<div class="prod-filter">
		<?if ($_GET['dev'] == 'y'){
		echo '123';
		$rs = CIBlockElement::GetList(
			array(), 
			array(
			"IBLOCK_ID" => 7, 
// 				array("ID" => CIBlockElement::SubQuery("ID", array("IBLOCK_ID" => 21, "PROPERTY_PKE" => 7405))),
// 				array("ID" => CIBlockElement::SubQuery("ID", array("IBLOCK_ID" => 21, "PROPERTY_PKE" => 7410))),
				array("ID" => CIBlockElement::SubQuery("ID", array("IBLOCK_ID" => 3, "PROPERTY_PKE" => "BRAND")))
			),
			false, 
			false,
			array("ID")
		);

		while($ar = $rs->GetNext()) {
			echo '<pre>';
			print_r($ar);
			echo '</pre>';
		}
		
		} ?>
		</div>
    </div>
    <div class="right-catalog col-xs-9">
        <h1>
			<? 
            if ($tplSect=='himiya_sub')
            {
                if ($isLaki)
                {
                    if(!$h1){?>Лакокрасочные материалы — <?}
                } else
                {
					if(!$h1){
					if(!strpos($_SERVER['REQUEST_URI'],'forbo')){ ?>Химия — <? }
				   }
                }
            }    
            if (!$isBrandTpl && !$isTypePlaceTpl)
            {
                if(!$h1) echo $APPLICATION->ShowTitle(false);                
            }        
            if (is_array($curBrand))
            {
                if (!$isBrandTpl && !$isTypePlaceTpl)
                {
                    if(!$h1) echo " — ";    
                }
                if(!$h1) echo $curBrand["NAME"];
                if ($isTypePlaceTpl && $arResult["VARIABLES"]["SECTION_ID"])
                {
                    if(!$h1) echo " — ".$curSection["NAME"];
                    if ($curBrand2)
                    {
                        if(!$h1) echo " ".$curBrand2["NAME"];
                    }
                }
                if ($_GET["vid"]!="")
                {
                    $arSelectVid = Array("ID", "NAME");
                    $arFilterVid = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "CODE" => $_GET["vid"]);
                    $resVid = CIBlockElement::GetList(Array(), $arFilterVid, false, false, $arSelectVid);
                    if($obVid = $resVid->GetNextElement())
                    {
                        $arFieldsVid = $obVid->GetFields();
                        $vidName=$arFieldsVid["NAME"];
                        $GLOBALS['sectionFilter']["PROPERTY_VID_TOVARA"]=$arFieldsVid["ID"];
                        if(!$h1){?> — <?=$vidName?><?}
                    }
                }
                if ($_GET["size"]!="")
                {
                    $arSelectSize = Array("ID", "NAME");
                    $arFilterSize = Array("IBLOCK_ID"=>11, "ACTIVE"=>"Y", "CODE" => $_GET["size"]);
                    $resSize = CIBlockElement::GetList(Array(), $arFilterSize, false, false, $arSelectSize);
                    if($obSize = $resSize->GetNextElement())
                    {
                        $arFieldsSize = $obSize->GetFields();
                        $sizeName=$arFieldsSize["NAME"];
                        $GLOBALS['sectionFilter']["PROPERTY_SIZES"]=$arFieldsSize["ID"];
                        if(!$h1){?> — Размер <?=$sizeName?><?}
                    }
                }
                if ($_GET["group_collection"]!="")
                {
                    $arSelectGC = Array("ID", "NAME");
                    $arFilterGC = Array("IBLOCK_ID"=>12, "ACTIVE"=>"Y", "CODE" => $_GET["group_collection"]);
                    $resGC = CIBlockElement::GetList(Array(), $arFilterGC, false, false, $arSelectGC);
                    if($obGC = $resGC->GetNextElement())
                    {
                        $arFieldsGC = $obGC->GetFields();
                        $GCName=$arFieldsGC["NAME"];
                        $GLOBALS['sectionFilter']["PROPERTY_GROUP"]=$arFieldsGC["ID"];
                        if(!$h1){?> — <?=$GCName?><?}
                    }
                }
            }
			if($h1){echo $h1;} 
            ?>
        </h1>
        <?$APPLICATION->IncludeComponent(
        	"bitrix:catalog.section",
        	$tplSect,
        	array(
                "SECTION_USER_FIELDS" => array("UF_ANONS"),
        		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
        		"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
        		"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
        		"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
        		"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
        		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
        		"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
        		"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
        		"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
        		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
        		"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
        		"BASKET_URL" => $arParams["BASKET_URL"],
        		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
        		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
        		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
        		"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
        		"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
        		"FILTER_NAME" => "sectionFilter",
        		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
        		"CACHE_TIME" => $arParams["CACHE_TIME"],
        		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
        		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        		"SET_TITLE" => $arParams["SET_TITLE"],
        		"MESSAGE_404" => $arParams["MESSAGE_404"],
        		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
        		"SHOW_404" => $arParams["SHOW_404"],
        		"FILE_404" => $arParams["FILE_404"],
        		"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
        		"PAGE_ELEMENT_COUNT" => 999,
        		"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
        		"PRICE_CODE" => $arParams["PRICE_CODE"],
        		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
        		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
        
        		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
        		"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
        		"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
        		"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
        		"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
        
        		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
        		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
        		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
        		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
        		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
        		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
        		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
        		"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
        		"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
        		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
        
        		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
        		"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
        		"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
        		"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
        		"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
        		"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
        		"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
        		"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
        
        		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
        		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
        		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        		"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
        		'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
        		'CURRENCY_ID' => $arParams['CURRENCY_ID'],
        		'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
        
        		'LABEL_PROP' => $arParams['LABEL_PROP'],
        		'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
        		'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
        
        		'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
        		'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
        		'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
        		'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
        		'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
        		'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
        		'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
        		'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
        		'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
        		'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
                'SHOW_ALL_WO_SECTION' => 'Y',
        		'TEMPLATE_THEME' => $_GET["brand"],
        		"ADD_SECTIONS_CHAIN" => $ADD_SECTIONS_CHAIN,
        		'ADD_TO_BASKET_ACTION' => $basketAction,
        		'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
        		'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
        		'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
        		'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
        	),
        	$component
        );?>
		
<?
if($_SERVER["REQUEST_URI"]=="/brand-armstrong/"||$_SERVER["REQUEST_URI"]=="/brand-barlinek/"
||$_SERVER["REQUEST_URI"]=="/brand-grabo/"||$_SERVER["REQUEST_URI"]=="/brand-modulyss/"
||$_SERVER["REQUEST_URI"]=="/brand-vertigo-trend/"){
echo $text;
}
if($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/dizayn-plitka-pvkh/brand-forbo/"){
echo '<p>&laquo;АлексВит&raquo; предлагает купить дизайн плитку ПВХ Forbo оптом с доставкой по России, а также по Москве и МО. Мы обеспечиваем низкие цены на высококачественные строительные товары, а также быструю доставку, стоимость которой зависит от удаленности пункта назначения.</p>
<p>Дизайн плитка ПВХ Forbo от известного европейского производителя является износостойким материалом, который прекрасно подходит даже для коммерческого использования в местах с высокой проходимостью.</p>
<p>Коллекции напольного покрытия имитируют следующие материалы:</p>
<ul>
	<li>дерево;</li>
	<li>камень.</li>
</ul>
<p>С нами вы получите высококачественные материалы по выгодной цене.</p>';
}
if($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=derevo"){
echo '<p>Создать элегантный интерьер, в который хотелось бы возвращаться снова и снова &ndash; это целое искусство. С оформлением пола вам поможет керамогранит Estima, имитирующий дерево. Применяется в местах, где полы подвергаются повышенным нагрузкам: в холлах, торговых залах, офисных помещениях, а также в жилых домах &ndash; в коридоре или на кухне. Отлично подходит для устройства системы &laquo;теплый пол&raquo;.</p>
<p>В зависимости от коллекции (Artwood, Brigantina Chalet, Taste Time, Sherwood, Vintage) керамогранит Estima под дерево выпускается в следующих размерах:</p>
<ul>
	<li>15х60;</li>
	<li>19,4х60;</li>
	<li>60x60.</li>
</ul>
<p>Чтобы купить понравившуюся коллекцию оптом, обратитесь в компанию Alexvit. Наш офис расположен в Москве. Доставка осуществляется по всей России.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=dizayn"){
echo '<p>Даже напольное покрытие способно стать важным штрихом в оформлении интерьера. Керамогранит Estima, относящийся к серии дизайн, отлично доказывает это утверждение.</p>
<p>В указанную линейку относятся следующие коллекции:</p>
<ul>
	<li>Altair;</li>
	<li>Comfort;</li>
	<li>Aglomerat;</li>
	<li>Venezia.</li>
</ul>
<p>Такое напольное покрытие заменит вам агломерированный камень, тканое полотно, декоративную штукатурку или мозаику. Идеально подойдет для интерьера, выполненного под старину.</p>
<p>В компании Alexvit вы купите нужную вам коллекцию оптом. Доставка товаров нашим клиентам осуществляется как по Москве, так и по всей России.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=kamen"){
echo '<p>Натуральные строительные материалы отличаются высокой стоимостью. Чтобы не тратить лишние средства, воспользуйтесь альтернативным решением: керамогранит Estima имитирует природный камень, обладает высоким качеством и умеренной стоимостью. Применяется в ванных комнатах, на стенах кухни, в интерьерах офисных зданий.</p>
<p>Коллекция Mixstone воспроизводит базальтовое покрытие. Коллекция Limestone имитирует природный известняк.</p>
<p>К сериям, воспроизводящим камень, также относятся:</p>
<ul>
	<li>Antica;</li>
	<li>Jazz;</li>
	<li>Energy;</li>
	<li>Olimpia;</li>
	<li>Pietra;</li>
	<li>Quarzite;</li>
	<li>Rust;</li>
	<li>Capri;</li>
</ul>
<p>Обратившись в компанию Alexvit, вы купите нужный вам вид напольного покрытия с доставкой по Москве и России. Продажи осуществляются исключительно оптом.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/falshpol/brand-perfaten/"){
echo '<p>Фальшполы Perfaten &ndash; новейшее слово в современном мире строительного дела. Представляют собой настил, который монтируется на определенном возвышении над полом в виде систем плит на основании.</p>
<p>Такой пол очень удобен, так как под ним возможно проложить необходимые коммуникации (канализацию, систему вентиляции или пожаротушения и т.д.), не повреждая основной структуры покрытия. Зачастую, фальшпол используется в тех случаях, когда необходимо быстро перестроить помещение в случае надобности клиента.</p>
<h2>Преимущество фальшпола Perfaten</h2>
<p>Приобретая Perfaten, вы получаете ряд неоспоримых преимуществ и положительных впечатлений от эксплуатации и монтажа такой системы плит. Наиболее весомыми выделяют:</p>
<ul>
	<li>многогранность цветовых решений &ndash; в зависимости от желания заказчика и дизайна помещения, вы можете выбрать нужный вам по цвету и фактуре фальшпол;</li>
	<li>способность выдерживать большие нагрузки без повреждения несущих конструкций и перегородок;</li>
	<li>отвечает всем стандартам качества благодаря контролю качества по всей длине технологического процесса производства;</li>
	<li>легкость установки и транспортировки к месту использования позволяет собрать такую систему за считанные минуты;</li>
	<li>широкий модельный ряд, который не оставит равнодушным заказчика.</li>
</ul>
<h2>Почему стоит покупать у нас?</h2>
<p>Приобретая у компании Alexvit, вы получаете высококачественный сервис и квалифицированных специалистов, знающих свое дело. Чтобы купить Perfaten, достаточно позвонить по телефону: +7 (499) 653 89 66. Мы доставляем товар не только по Москве, но и по всей территории России.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/"){
echo '<p>Если вы находитесь в поисках эластичного, не вызывающего аллергические реакции покрытия, способного показывать отменные показатели влагоустойчивости и звуконепроницаемости, тогда флокированное покрытие Forbo специально для вас.</p>
<p>Подобного рода напольное флокированное покрытие характеризуется отменными противоскользящими свойствами и длительным срок эксплуатации. Материал отвечает всем санитарно-гигиеническим нормам, поэтому применим как в обычных помещениях, так и в детских спальнях.</p>
<p>Приобретая флокированное покрытие Forbo в нашей компании, вы получаете ряд преимуществ:</p>
<ul>
	<li>широкий модельный ряд;</li>
	<li>оперативную обработку поступившего заказа;</li>
	<li>доставку по Москве и России;</li>
	<li>лояльную ценовую политику на оптовую покупку;</li>
	<li>качественную консультацию от наших специалистов и многое другое.</li>
</ul>
<p>Чтобы купить необходимое покрытие, достаточно позвонить по телефону: +7 (499) 653 89 66. Наши менеджеры ответят вам на все вопросы.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/igloprobivnoy-kovrolin/brand-forbo/"){
echo '<p>Если вам необходимо напольное покрытие высочайшего качества, при этом по разумной цене, тогда иглопробивной ковролин Forbo &ndash; отличное решение для вашего вопроса.</p>
<p>Это один из немногих представителей ковролинов, который умело сочетает в себе:</p>
<ul>
	<li>непревзойденное качество;</li>
	<li>большой выбор цветов;</li>
	<li>высокие показатели износостойкости.</li>
</ul>
<p>Чтобы купить любой понравившийся вам иглопробивной ковролин, достаточно позвонить по телефону: +7 (499) 653 89 66. Наши менеджеры ответят вам на все вопросы. Мы гарантируем качественное покрытие, демократичные цены, быстроту доставки к пункту назначения и сертифицированный товар.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/"){
echo '<p>Устали от серости напольного покрытия? Керамогранит Сoliseumgres исправит сложившуюся ситуацию и привнесут в вашу размеренную жизнь буйство красок и фактур.</p>
<p>Керамогранит Сoliseumgres представлен в плитах двух форматах: 30*30 и 45*45 см. Такая вариация достаточно удобна для применения в строениях с различной фактурой и размером первоначального покрытия.</p>
<p>Такое напольное покрытие помогает имитировать различные фактуры:</p>
<ul>
	<li>дерево;</li>
	<li>камень;</li>
	<li>бетон;</li>
	<li>мрамор.</li>
</ul>
<p>Кроме того, имеет множество коллекций, различных по фактуре и цветовой гамме. Вы сможете подобрать именно тот вариант напольного покрытия, который идеально подходит именно для вашего здания и дизайна.</p>
<p>Купить керамогранит оптом теперь очень просто &ndash; достаточно позвонить по телефону: +7 (499) 653 89 66. Наши менеджеры ответят вам на все вопросы. Мы гарантируем оперативную доставку по Москве и остальным городам России, лояльные цены и сервис на высшем уровне.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/"){
echo '<p>Керамогранит Estima &ndash; это удачное покрытие для пола, имеющее многогранность выбора цвета и фактуры. Спектр применения его также широк, сколь и ассортимент, поэтому каждый заказчик сможет найти именно ту фактуру, которая идеально подойдет к интерьеру помещения.</p>
<p>Данный керамогранит представлен в различных размерах:</p>
<ul>
	<li>15*60 см;</li>
	<li>19,4*120 см;</li>
	<li>30*30 см;</li>
	<li>30*120 см;</li>
	<li>40*40 см;</li>
	<li>&nbsp;60*60 см;</li>
	<li>60*120см.</li>
</ul>
<p>Благодаря высоким техническим характеристикам широко применяется для облицовки полов и стен в жилых, коммерческих и производственных помещениях.</p>
<p>Фактура многогранна и наша компания предлагает клиентам весь спектр имеющихся плиток, имитирующих различные материалы:</p>
<ul>
	<li>камень;</li>
	<li>мрамор;</li>
	<li>дерево;</li>
	<li>бетон;</li>
	<li>дизайнерское решение;</li>
	<li>техника и т.д.</li>
</ul>
<p>Стоит отметить, что керамогранит Estima представлен различными коллекциями, среди которых вы сможете найти ваш идеальный вариант напольного покрытия.</p>
<p>Чтобы получить консультацию о покупке керамогранита, свяжитесь с нами по телефону: +7 (499) 653 89 66. Наши менеджеры ответят вам на все вопросы. Мы осуществляем оптовые продажи, а также обеспечиваем быструю доставку, цена которой зависит от удаленности вашего населенного пункта.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=derevo"){
echo '<p>Керамогранит Kerama Marazzi (дерево) – оригинальная коллекция напольных покрытий из искусственного камня высокого качества с многообразными вариантами натуралистичных текстур элитных пород самой красивой древесины. Все модели плитки тщательно продуманы с точки зрения дизайна, позволяют быстро и комфортно обустроить жизненное пространство дома и создать уют в помещении.</p>
<p>Купить керамогранит Kerama Marazzi вы можете в Москве у компании Alexvit оптом. Возможна доставка заказа в любой город России.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=kamen"){
echo '<p>Керамогранит Kerama Marazzi (камень) – многообразный мир высококачественных керамических напольных покрытий, очень ярко и выразительно имитирующих бесконечные структуры, рисунки и цветовое разнообразие природного отделочного камня. Полы из искусственного камня не уступают натуральному аналогу по прочности, долговечности, красоте и эксплуатационной практичности, но существенно дешевле природного камня.</p>
<p>Приобрести оптом керамогранит Kerama Marazzi для отделки напольных покрытий, внутренних деталей интерьера и внешних фасадов зданий вы можете в компании Alexvit в Москве, с возможностью доставки заказа в любую точку России.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/"){
echo '<p>Коммерческий линолеум Forbo производится на голландском предприятии Forbo &ndash; одна из лидирующих фирм по выпуску натуральных напольных покрытий. Изделия этой компании отличаются экологичностью и изготавливаются с помощью современных технологий. Применяется в больницах, офисах, торговых центрах где к напольным покрытиям предъявляют повышенные требования.</p>
<p>Коммерческий линолеум Forbo отличается следующими характеристиками:</p>
<ul>
	<li>широкий выбор расцветок;</li>
	<li>износостойких слой и стекловолоконное основание;</li>
	<li>антистатическая и антибактериальная обработка поверхности;</li>
	<li>имитация под природные материалы;</li>
	<li>шумоизоляция;</li>
	<li>натуральные материалы в составе, безопасные для астматиков и аллергиков (джут, масло льна, природные красители и смолы, известняк);</li>
	<li>приемлемая цена;</li>
	<li>долгий срок службы (от 30 лет и дольше);</li>
</ul>
<p>Линолеум этой марки прекрасно подойдет для воплощения самых смелых дизайнерских решений. Купить коммерческий линолеум Forbo в компании Alexvit можно исключительно оптом. Цена будет зависеть от площади поверхности. Доставка из Москвы по России является платной.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-armstrong/"){
echo '<p>Коммерческий линолеум Armstrong производится только из натуральных материалов. Корпорация Armstrong &ndash; одна из ведущих фирм по производству напольных покрытий в мире. Её продукция отличается долговечностью и высочайшим уровнем качества.</p>
<p>Коммерческий линолеум Armstrong отличается следующими потребительскими характеристиками:</p>
<ul>
	<li>стойко переносит серьёзные механические нагрузки;</li>
	<li>влага остается на поверхности, не проникая внутрь структуры материала, это продлевает срок службы. Частую влажную уборку выдерживает спокойно, устойчив к воздействию кислот и щелочей.</li>
	<li>не поддается возгоранию;</li>
	<li>не скользит, поэтому нет никакого риска упасть и получить травму;</li>
	<li>достаточно увесистый и твердый, поэтому при монтаже на тщательно подготовленную поверхность ложится идеально и не требует обязательного приглашения профессионала.</li>
	<li>изготавливается с защитный слоем от 0,7 &ndash; 1.0 мм.</li>
</ul>
<p>Купить коммерческий линолеум Armstrong в компании Alexvit можно исключительно оптом. Цена будет зависеть от площади поверхности. Доставка из Москвы по России является платной.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/"){
echo '<p>Керамогранит Kerama Marazzi &ndash; популярный материал для отделки полов в помещениях. С успехом применяется не только в жилых домах, но и в промышленных, хозяйственных, помещениях общего пользования.</p>
<h2>Преимущества и недостатки керамогранита Kerama Marazzi</h2>
<p>Продукция KeramaMarazzi производится из высококачественной глины на фирменном итальянском оборудовании. За счет этого обеспечивается абсолютное соответствие международным стандартам ENISO. Качество товара потребителю гарантировано.</p>
<p>Керамогранит Kerama Marazzi обладает высокими показателями влагостойкости (отлично удерживает влагу на поверхности и не позволяет ей проникать внутрь структуры материала, тем самым разрушая её). Также устойчив к моющим химическим веществам, легко переносит достаточно низкие и высокие температуры окружающей среды. Эти полезные свойства нашли применение в отделке открытых помещений, бассейнов, саун.</p>
<p>Однако, покрытие имеет свои недостатки, которые являются продолжениями достоинств. Оно имеет большой вес, поэтому транспортировка его &ndash; дело нелегкое и затратное. Также покрытие сложно для монтажа, требует специального приспособления для резки, полимерного клея и навыков профессионала. Но это с лихвой окупиться большим сроком службы и ощущением комфорта от использования покрытия.</p>
<p>Купить керамогранит Kerama Marazzi в компании Alexvit можно исключительно оптом. Цена будет зависеть от площади поверхности. Доставка из Москвы по России является платной.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-grabo/"){
echo '<p>Коммерческий линолеум Grabo отличается от полукоммерческого большей прочностью и износостойкостью. Благодаря этим полезным свойствам его широко применяют в качестве напольного покрытия для помещений с большим потоком людей: школах, высших учебных заведениях, больницах, библиотеках, торговых комплексах, бизнес-центрах.</p>
<h2>Преимущества и недостатки коммерческого линолеума Grabo</h2>
<ol>
	<li>Гетерогенное напольное покрытие, в отличие от гомогенного изготавливается без мела и кварцевой крошки, что снижает риск появления (с возрастанием срока эксплуатации) белых полос на полу.</li>
	<li>Коммерческий линолеум Grabo может достигать 6 мм в толщину благодаря наличию трех слоев из винила, ПВХ и стекловолокна в составе. Такое внутренне устройство позволяет покрытию быть особенно надежным и стойко переносить высокие нагрузки. Этот самый прочный вид линолеума из всех.</li>
	<li>Гетерогенный линолеум обладает высоким показателем поглощения шума (от 10 Дб). Такая характеристика делает возможным использование его даже в библиотеке. В помещении с таким полом тишина и покой будут обеспечены.</li>
	<li>Простота работ по монтажу. Даже огромных размеров помещение может выстелить маленькая бригада рабочих. Есть лишь один нюанс, который стоит учитывать: поверхность должна быть предварительно вычищена и выровнена для лучшего сцепления материала и пола.</li>
	<li>Материал плохо пригоден для жилых помещений, некоторые виды нельзя укладывать в детских комнатах. К тому же стоимость его высока для столь маленького помещения как квартира, и использование коммерческого линолеума там просто невыгодно.</li>
</ol>
<p>Купить коммерческий линолеум Graboв компании Alexvit можно исключительно оптом. Доставка из Москвы по России является платной.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-grabo/?vid=polukummercheskiy"){
echo '<p>Полукоммерческий линолеум Grabo состоит из трех слоев:</p>
<ul>
	<li>Защитный. Толщина его составляет от 0,4 до 0,6 мм. Из-за этого повредить или испортить такой слой будет непросто.</li>
	<li>Декоративный. Благодаря этому слою покрытие получается вариативным в фактурах и расцветках и органично впишется в любой интерьер.</li>
	<li>подложка из поливинилхлорида. Она отвечает за прочность, надежную тепло- и звукоизоляцию. Благодаря ей, покрытие обеспечивает комфорт в помещении со средней проходимостью.</li>
</ul>
<p>Рекомендован для небольших офисов, гостиничных номеров.</p>
<p>Купить полукоммерческий линолеум Graboв компании Alexvit можно исключительно оптом. Доставка из Москвы по России является платной.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-polyflor/"){
echo '<p>Коммерческий линолеум Polyflor, в отличие от бытового линолеума, чаще всего используется не для отделки жилых помещений, а в качестве напольного покрытия для общественных мест: бизнес-центров, отелей, торговых помещений. Стоит он на порядок дороже бытового, поскольку способен выдерживать большую нагрузку и со временем верхний красочный слой не стирается. Поэтому он иногда представляет интерес и для владельцев загородных домов, которые хотят более долговечное покрытие.</p>
<h2>Преимущества и недостатки коммерческого линолеума Polyflor</h2>
<p>Материал от polyflor является гомогенным, то есть, обладает только одним жестким верхним слоем с идентичным узором по всей толщине. Поэтому разнообразием фактур и расцветок, увы, не порадует.</p>
<p>Продукция марки отличается долговечностью: защитный слой сверху предохраняет от воды, кислот и щелочей. Срок эксплуатации материала составляет более 25 лет.</p>
<p>Купить материал для пола в компании Alexvit можно исключительно оптом. Доставк аиз Москвы по России является платной.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-escom/"){
echo '<p>Ковровая плитка Escom &ndash; это напольное покрытие, по рисунку и структуре имитирующее ковер. За границей ковровую плитку широко используют в офисах, бизнес-центрах, гостиницах. В России этот материал появился совсем недавно, поэтому не стал таким же привычным вариантом напольного покрытия как линолеум. В 2013 году плитка фирмы Escom стала первой, появившейся на российском рынке. Ковровую плитку Escom в Москве сегодня можно купить у компании Alexvit.</p>
<h2>Разновидности ковровой плитки Escom</h2>
<p>Компания Alexvit предоставляет большой выбор рисунков на ковровой плитке. Коллекция объединяет в себе образцы одного рисунка в разном цвете.</p>
<p>Большими преимуществами материала являются светостойкость, износостойкость, отличная тепло- и звукоизоляция, наличие пропитки, предохраняющей от бактерий и загрязнения, а также экологичность и безопасность для аллергиков. Материал укладывается отдельными квадратами, которые стыкуются между собой образую целостное полотно, в случае деформации какого-либо участка его легко заменить без демонтажа всего покрытия.&nbsp; В изготовлении применяется технология антистатической и грязезащитной обработке. Приобрести покрытие в компании Alexvit можно только оптом. Доставка является платной.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-armstrong/?vid=gomogennyy"){
echo '<p>Гомогенный линолеум Armstrong имеет только один жесткий верхний слой с идентичным узором по всей толщине. Материал обладает следующими особенностями, которые рекомендуется учесть перед покупкой:</p>
<ul>
	<li>Выбор фактур и расцветок несколько скромнее, чем у гетерогенного линолеума. Стоит внимательно отнестись к выбору цвета, потому что покрытие отличается высокой прочностью и долгим сроком службы, поэтому должно радовать глаз.</li>
	<li>Простота монтажа: рисунок не требуется совмещать, поэтому стыков видно не будет.</li>
</ul>
<p>Купить материал для пола в компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-forbo/"){
echo '<p>Ковровая плитка Forbo обладает достойными эксплуатационными характеристиками и богатым выбором расцветок и фактур. Большими преимуществами материала является:</p>
<ul>
	<li>Износостойкость. Эта характеристика наиболее актуальна скорее для помещений с большим потоком людей.</li>
	<li>Натуральность и экологичность. Покрытие изготовлено из натуральных материалов, поэтому совершенно безопасно для аллергиков и астматиков.</li>
</ul>
<p>Купить материал для покрытия пола в компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-modulyss/"){
echo '<p>Ковровая плитка Modulyss соответствует европейским стандартам качества продукции. Основными преимуществами являются:</p>
<ul>
	<li>светостойкость;</li>
	<li>превосходная звуко- и теплоизоляция;</li>
	<li>наличие пропитки, предохраняющей от бактерий и загрязнения;</li>
	<li>компактная для перемещения и выгодная по условиям доставки;</li>
	<li>отдельные блоки плотно прилегают друг к другу, поэтому в процессе укладки не появляется швов между ними, куда может попадать грязь. Это довольно гигиеничный и не требовательный вид покрытия;</li>
	<li>если отдельные участки покрытия испортились и потеряли привлекательный внешний вид, то замену им будет легко приобрести. Также менять покрытие целиком не нужно, демонтируется только маленький участок;</li>
	<li>разнообразие и красивые оттенки расцветок создадут настроение и уют в помещении.</li>
</ul>
<p>Купить ковровую плитку бельгийского производителя Modulyss в компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/"){
echo '<p>Натуральный линолеум Forbo изготавливается исключительно из экологически чистых материалов.</p>
<p>Основные особенности:</p>
<ul>
	<li>стоимость этого покрытия, в сравнении с другими видами, выше, поскольку продукт изготовлен из натурального сырья. Но эта разница в цене совершенно оправдана;</li>
	<li>состав безопасен для астматиков и аллергиков (только натуральные компоненты: джут, масло льна, природные красители и смолы, известняк);</li>
	<li>поглощает шум, благодаря пробковому покрытию, пол не скрипит.</li>
</ul>
<p>Купить натуральный линолеум можно в компании Alexvit исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=marmoleum-marbled"){
echo '<p>Натуральный линолеум Forbo коллекции Mаrmoleum Mardled производиться исключительно из натуральных компонентов. Рекомендуем для медицинских учреждений и школ.</p>
<p>Основные преимущества коллекции:</p>
<ul>
	<li>подлинная мраморная структура &ndash; гордость голландской марки;</li>
	<li>возможность воплотить в жизнь самые смелые дизайнерские задумки благодаря многообразию цвета;</li>
	<li>покрытие легко укладывать.</li>
</ul>
<p>Купить натуральный линолеум компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/parketnaya-doska/brand-barlinek/"){
echo '<p>Паркетная доска Barlinek отличается высокой термической и акустической изоляцией, высоким сопротивлением деформации. Все компоненты, используемые в производстве, соответствуют строгим экологическим нормам.</p>
<p>Основные преимущества материала:</p>
<ul>
	<li>напольное покрытие изготовлено из натурального дерева, поэтому надолго сохранит тепло дома;</li>
	<li>качественный, после долго времени эксплуатации сохраняет первозданный внешний вид;</li>
	<li>процесс укладки простой и может не требовать вмешательства профессионалов;</li>
	<li>безопасность для здоровья, натуральный состав не вызывает аллергии и не провоцирует приступы астмы.</li>
</ul>
<p>Купить паркетную доску Barlinek можно в компании Alexvit оптом по привлекательной цене. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/parketnaya-doska/brand-grabo/"){
echo '<p>Паркетная доска &ndash; Grabo представлена в ассортименте компании Alexvit в составе трех коллекций из древесины ясеня, венге, дуба, мербау и бука. Отличительными особенностями материала являются:</p>
<ul>
	<li>натуральное дерево не выносит твердых предметов, на нем могут появляться царапины, поэтому материал требует деликатного обращения;</li>
	<li>стойко переносит влажность: не рассыхается, не коробится;</li>
	<li>легко моется, бесшумный.</li>
</ul>
<p>С таким материалом тепло останется в вашем доме надолго.</p>
<p>Купить материал для пола в компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/sportivnye-pokrytiya/brand-forbo/"){
echo '<p>Cпортивные покрытия &ndash; Forbo представлены в компании Alexvit двумя коллекциями Marmoleum Sport (14 цветовых образцов) и Sportline (12 цветовых образцов). Первая коллекция отличается от второй наличием резиновой подложки, во второй покрытие изготовлено из ПВХ. Голландский производитель изготовил эту линейку специально для тренировочных залов и спортивных площадок. Все необходимые гигиенические сертификаты и сертификаты безопасности есть в наличии. Основные преимущества материала:</p>
<ul>
	<li>цена (по сравнению со спортивным паркетом она гораздо ниже);</li>
	<li>противоскользящие свойства;</li>
	<li>малый риск получить травму;</li>
	<li>эластичность и гибкость;</li>
	<li>устойчивость к износу.</li>
</ul>
<p>Купить спортивное покрытие &ndash; Forbo в компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/stsenicheskie-pokrytiya/brand-grabo/"){
echo '<p>Cценическое покрытие &ndash; Grabo представлено в компании Alexvit в формате четырех коллекций. Покрытие найдет отличное применение в танцевальных залах (для любых разновидностей танцев, в том числе и балета), телевизионных студиях, танцевальных школах и фитнес-центрах. Основные преимущества материала:</p>
<ul>
	<li>Материал компактный, легко разворачивается и складывается в рулон, как коврик для йоги, благодаря чему становится незаменимым на гастролях, где не всегда могут оказаться подходящие условия для репетиций и выступлений. Такое покрытие &ndash; надежная защита от травм.</li>
	<li>Ударопрочность, не деформируется и не мнется при эксплуатации.</li>
	<li>Ассортимент компании Alexvit позволяет найти подходящее покрытие для любого вида площадок.</li>
</ul>
<p>Купить сценическое покрытие - Grabo в компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/nastennye-pokrytiya/keramicheskaya-plitka/brand-kerama-marazzi/"){
echo '<p>Керамическая плитка Kerama Marazzi в ассортименте компании Alexvit представлена двумя большими группами: &laquo;Неаполитанская&raquo; и &laquo;Палитра&raquo;. Эти коллекции отличаются яркой и разнообразной цветовой палитрой с рисунками и декором, выполненным вручную дизайнерами компании. Красивый интерьер, созданный с помощью этого материала, порадует самого взыскательного эстета.</p>
<p>Основные преимущества материала:</p>
<ul>
	<li>продукция компании делается в России, но используется фирменное итальянское оборудование. За счет этого обеспечивается абсолютное соответствие международным стандартам EN ISO. Качество товара потребителю гарантировано;</li>
	<li>размеры могут быть самыми разными. В компании Alexvit обязательно найдется подходящий.</li>
</ul>
<p>Купить такой доступный материал как керамическая плитка Kerama Marazzi&nbsp; в компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/nastennye-pokrytiya/keramicheskaya-plitka/brand-sokol/"){
echo '<p>Керамическая плитка Сокол производится в Московской области, поэтому цена её на порядок ниже, чем у итальянских аналогов. Существует стереотип, что от товара отечественного производителя не приходится ждать хорошего качества. Продукция марки, благодаря ряду преимуществ, полностью опровергает это утверждение.</p>
<p>Основные преимущества материала:</p>
<ul>
	<li>разнообразие цветов и фактур довольно большое, оно позволяет создать уникальный и уютный интерьер самыми минимальными средствами;</li>
	<li>материал прочный, выдерживает высокие нагрузки и не ломается. Трещин на нем не появляется, даже после падения на покрытие тяжелых предметов. Однако сам предмет (если он хрупкий) может повредиться; &nbsp;</li>
	<li>уложить можно без обращения к профессионалам, главное &ndash; подобрать подходящие размеры;</li>
	<li>при ограниченном бюджете &ndash; этот материал имеет достойное соотношение &laquo;цена качество&raquo;.</li>
	<li>привлекательный внешний вид &ndash; яркие цвета, глянцевый блеск;</li>
	<li>с него легко удалить даже самые стойкие пятна, покрытие переносит генеральные уборки с абразивными моющими средствами.</li>
</ul>
<p>Купить керамическую плитку отечественного производства Сокол в компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/"){
echo '<p>Подвесные и акустические потолки Ecophon &ndash; продукция шведского производства, отвечающая всем европейским стандартам качества (международный стандарт EN ISO 116 54). Материал незаменим в том помещении, где требуется создать комфортный уровень звука. Это может быть аудитория, бизнес-центр или развлекательный центр. Основные преимущества материала:</p>
<ul>
	<li>они обеспечивают поглощение отвлекающих, посторонних&nbsp; звуков и улучшают акустику в помещениях для переговоров и аудиториях. Коэффициент звукопоглощения составляет 0, 85 &ndash; 1, при максимальном значении 1.</li>
	<li>продукция ловко соблюдает баланс между внешним видом, качеством, техническими характеристиками и ценой. Скандинавская фирма-производитель &ndash; одна из лучших компаний, поставляющих на рынок подобное оборудование.</li>
	<li>материал, из которого изготовлены потолки, высокого качества и выдерживает большую нагрузку, поэтому они будут долго служить, не требуя замены или ремонта.</li>
	<li>воздействие влаги совершенно не страшно благодаря 95% влагостойкости.</li>
	<li>все системы отличаются простотой монтажа, поэтому установка не займет много времени. В редких случаях, когда будет необходимо заменить одну деталь, это можно будет сделать, не демонтируя всю систему полностью &ndash; деталь ремонтируется и устанавливается отдельно.</li>
	<li>также система предоставляет возможность установки любого из видов светильников и климат-контроля, что играет немалую роль в формировании комфортной атмосферы в помещении.</li>
	<li>материал негорюч, поэтому пожаробезопасность&nbsp; его высока, и помещение защищено надежно.</li>
	<li>материал не требует особенного ухода: его просто чистить и мыть.</li>
</ul>
<p>Купить подвесные и акустические потолки Ecophon в компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/?group_collection=advantage"){
echo '<p>Модульные потолки Ecophon Advantage различных конструкций предоставляют большое количество возможностей создавать разнообразные интерьеры. Основные преимущества материала:</p>
<ul>
	<li>высокая пожаробезопасность, негорючесть;</li>
	<li>простота ухода: еженедельной влажной уборки достаточно для поддержания чистоты;</li>
	<li>95% влагостойкости &ndash; не деформируется под действием жидкости, что в первую очередь необходимо в помещениях с влажной средой;</li>
	<li>цена и технические характеристики гармонично сочетаются между собой.</li>
</ul>
<p>Купить модульные потолки в компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/?group_collection=focus"){
echo '<p>Модульные потолки Ecophon Focus отличаются красотой внешнего вида и функциональностью. Основные преимущества:</p>
<ul>
	<li>Состав из стекловойлока и ПВХ обеспечивает надежную шумоизоляцию в любом помещении.</li>
	<li>Разнообразие видов систем, кромок и уровней предоставляет возможности сделать интерьер привлекательнее.</li>
	<li>Монтаж не слишком сложный и проходит быстро. Благодаря отличному качеству заменять материал потребуется не скоро.</li>
</ul>
<p>Купить модульные потолки в компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/?group_collection=hygiene"){
echo '<p>Модульные потолки Ecophon Hygiene необходимо устанавливать в тех помещениях, которые постоянно подвергаются загрязнению, а чистота там должна строго соблюдаться. Этими местами являются:</p>
<ul>
	<li>больничные палаты;</li>
	<li>цеха комбинатов пищевой промышленности;</li>
	<li>кухни заведений общественного питания;</li>
	<li>посты медсестер.</li>
</ul>
<p>Походящими приспособлениями, прекрасно функционирующими, неприхотливыми в уходе являются Foodtec A, Clinic E, Advance A C3 и другие. Они устойчивы к появлению коррозию даже от постоянного промывания благодаря специальному антикоррозионному покрытию.</p>
<p>Купить модульные потолки в компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
elseif($_SERVER["REQUEST_URI"]=="/potolki/svobodno-visyashchie-elementy-i-baffly/brand-ecophon/"){
echo '<p>Баффлы Ecophon &ndash; декоративные свободно висящие элементы, отлично поглощают звук, применяются в торговых центрах, офисах и в других помещениях с повещенной проходимостью. Шведы уделяют особое внимание качеству, поэтому товар отличают такие преимущества:</p>
<ul>
	<li>особое строение защищает от пожаров. Стекловолокно, входящее в состав, не горючий материал;</li>
	<li>соответствие стандарту ISO 4611: панели не деформируются и не расслаиваются под длительным действием влаги;</li>
	<li>простота ухода: влажную уборку рекомендуется проводить раз в неделю.</li>
</ul>
<p>Купить баффлы Ecophon в компании Alexvit можно исключительно оптом. Доставка из Москвы по России осуществляется платно.</p>';
}
?>		
    </div>
</div>
<?
if ($tplSect=='himiya_sub')
{
    if ($isLaki)
    {
        $curTitle='Лакокрасочные материалы — '.$APPLICATION->GetTitle(false);
    } else
    {
        //if(!strpos($_SERVER['REQUEST_URI'],'forbo')) $curTitle='Химия — '.$APPLICATION->GetTitle(false); 
    }
 
    //if(!strpos($_SERVER['REQUEST_URI'],'forbo')) $APPLICATION->SetPageProperty('title',$curTitle);
} else
if (is_array($curBrand))
{
    if ($isBrandTpl || $isTypePlaceTpl)
    {
        $curTitle=$curBrand["NAME"];
        if ($isTypePlaceTpl && $arResult["VARIABLES"]["SECTION_ID"])
        {
            $curTitle.=" — ".$curSection["NAME"];
            if ($curBrand2)
            {
                $curTitle.=" ".$curBrand2["NAME"];
            }
        }
    } else
    {
        $curTitle=$APPLICATION->GetTitle(false)." — ". $curBrand["NAME"];    
    }
    if ($vidName!="")
    {
        $curTitle.=" — ".$vidName;
    }
    if ($sizeName!="")
    {
        $curTitle.=" — Размер ".$sizeName;
    }
    if ($GCName!="")
    {
        $curTitle.=" — Размер ".$GCName;
    }
    
    $APPLICATION->SetPageProperty('title',$curTitle);
}
if (is_array($curBrand) && !$isFilterCandidateFirst)
{
    $APPLICATION->AddChainItem($curBrand["NAME"], $APPLICATION->GetCurDir());
    if ($vidName!="")
    {
        $APPLICATION->AddChainItem($vidName, $APPLICATION->GetCurDir()."?vid=".$_GET["vid"]);
    }
    if ($sizeName!="")
    {
        if ($vidName=="")
        {
            $APPLICATION->AddChainItem($sizeName, $APPLICATION->GetCurDir()."?size=".$_GET["size"]);
        } else
        {
            $APPLICATION->AddChainItem($sizeName, $APPLICATION->GetCurDir()."?size=".$_GET["size"]."&vid=".$_GET["vid"]);
        }
    }
    if ($GCName!="" && $sizeName!="" && $vidName!="")
    {
        $APPLICATION->AddChainItem($GCName, $APPLICATION->GetCurDir()."?size=".$_GET["size"]."&vid=".$_GET["vid"]."&group_collection=".$_GET["group_collection"]);
    } else
    if ($GCName!="" && $sizeName!="")
    {
        $APPLICATION->AddChainItem($GCName, $APPLICATION->GetCurDir()."?size=".$_GET["size"]."&group_collection=".$_GET["group_collection"]);        
    } else
    if ($GCName!="" && $vidName!="")
    {
        $APPLICATION->AddChainItem($GCName, $APPLICATION->GetCurDir()."?vid=".$_GET["vid"]."&group_collection=".$_GET["group_collection"]);
    } else
    if ($GCName!="")
    {
        $APPLICATION->AddChainItem($GCName, $APPLICATION->GetCurDir()."?group_collection=".$_GET["group_collection"]);
    }
    
}

if ($meta_title) {
	$APPLICATION->SetPageProperty('title', $meta_title);
}

if ($meta_desc) {
	$APPLICATION->SetPageProperty("description", $meta_desc);
}

if ($meta_key) {
	$APPLICATION->SetPageProperty('keywords', $meta_key);
}
?>