<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach ($arResult["BREADCRUMBS"] as $section)
{
   $APPLICATION->AddChainItem($section["NAME"], $section["URL"]);
}
if (CMain::IsHTTPS())
{
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://'.$_SERVER["SERVER_NAME"].$arResult["CANONICAL_URL"].'" />');
} else
{
    if(!strpos($_SERVER['REQUEST_URI'],'forbo')) $APPLICATION->AddHeadString('<link rel="canonical" href="http://'.$_SERVER["SERVER_NAME"].$arResult["CANONICAL_URL"].'" />');    
}
?>