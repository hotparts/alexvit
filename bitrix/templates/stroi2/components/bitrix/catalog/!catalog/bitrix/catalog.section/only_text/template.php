<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="producer">
    <?
    $detailText=trim($arResult["~DESCRIPTION"]);
    if ($detailText!="")
    {
    	?>
    	<div class="catalog-descript">
            <?
            if ($arResult["PICTURE"])
            {
                $img = CFile::ResizeImageGet($arResult["PICTURE"]["ID"], array('width'=>263, 'height'=>9999), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                $img=$img["src"];
            } else
            {
                $img=SITE_TEMPLATE_PATH."/img/no-photo.png";
            }
            ?>
            <div class="catalog-descript-pict">
                <img src="<?=$img?>" alt="<?=$arItem["NAME"]?>">
            </div>
    	<?
    	if ($arResult["DESCRIPTION_TYPE"]=="html")
    	{
     		echo $detailText;
    	} else
     	{
    		?><p><?=str_replace("\n","<br />",$detailText)?></p><?
      	}
    	?>
    	</div>
    <?
    }
    ?>
</div>