<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>
<?
$arResult['BREADCRUMBS']=array();

$arResult["BRAND_CODE"]="";

$arResult["CANONICAL_URL"]="";
$res = CIBlockSection::GetByID($arResult["IBLOCK_SECTION_ID"]);
if($ar_res = $res->GetNext())
{
    $arSelect = Array("ID", "NAME", "CODE");
    $arFilter = Array("IBLOCK_ID"=>BRAND_IBLOCK_ID, "ID"=>$arResult["PROPERTIES"]["BRAND"]["VALUE"], "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($ob = $res->GetNextElement())
    {
        $arFieldsBrand = $ob->GetFields();
        $arResult["BRAND_CODE"]=$arFieldsBrand["CODE"];
        $arResult["CANONICAL_URL"]=$ar_res["SECTION_PAGE_URL"].'brand-'.$arFieldsBrand["CODE"]."/".$arResult["CODE"].".html";
    }
}
  
$arResult["CATALOG_FILE"]="";
if ($arResult["PROPERTIES"]["CATALOG_FILE"]["VALUE"]>0)
{
    
    $arSelect = Array("ID", "NAME", "PROPERTY_FILE");
    $arFilter = Array("IBLOCK_ID"=>CATALOG_FILES_IBLOCK_ID, "ID"=>$arResult["PROPERTIES"]["CATALOG_FILE"]["VALUE"]);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $src=CFile::GetFileArray($arFields["PROPERTY_FILE_VALUE"]);
        $arResult["CATALOG_FILE"]=$src["SRC"];
    }    
}


$arResult["BRAND"]="";
$arrUrlPath=explode("/",substr($APPLICATION->GetCurDir(),1,strlen($APPLICATION->GetCurDir())-2));
$filterCandidate=end($arrUrlPath);
$filterCandidateFirst=$arrUrlPath[0];

if (strpos($filterCandidateFirst,"type-")===0)
{
    $filterCandidateFirst=explode("type-",$filterCandidateFirst);
    $filterCandidateFirst=$filterCandidateFirst[1];
    $arSelect = Array("ID", "NAME", "CODE");
    $arFilter = Array("IBLOCK_ID"=>TYPE_PLACE_IBLOCK_ID, "CODE"=>$filterCandidateFirst, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $arResult['BREADCRUMBS'][]=array(
            "NAME" => 'Каталог',
            #"NAME" => $arFields["NAME"],
            "URL" => '/type-'.$arFields["CODE"]."/"
        );
        
        $nav = CIBlockSection::GetNavChain(false, $arResult["IBLOCK_SECTION_ID"]);
        while ($nav_first_section = $nav->GetNext())
        {
            $sectionName[]=$nav_first_section["NAME"];
        }
        $arResult['BREADCRUMBS'][]=array(
            "NAME" => join(" - ",$sectionName),
            "URL" => '/type-'.$arFields["CODE"].$arResult["SECTION"]["SECTION_PAGE_URL"]
        );
        
        if (strpos($filterCandidate,"brand-")===0)
        {
            $filterCandidate=explode("brand-",$filterCandidate);
            $filterCandidate=$filterCandidate[1];
            $arSelect = Array("ID", "NAME", "CODE");
            $arFilter = Array("IBLOCK_ID"=>BRAND_IBLOCK_ID, "CODE"=>$filterCandidate, "ACTIVE"=>"Y");
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if($ob = $res->GetNextElement())
            {
                $arFieldsBrand = $ob->GetFields();
                $arResult['BREADCRUMBS'][]=array(
                    "NAME" => $arFieldsBrand["NAME"],
                    "URL" => '/type-'.$arFields["CODE"].$arResult["SECTION"]["SECTION_PAGE_URL"]."brand-".$arFieldsBrand["CODE"]."/"
                );
            }
        }
        
        $arResult['BREADCRUMBS'][]=array(
            "NAME" => $arResult["NAME"],
            "URL" => '/type-'.$arFields["CODE"].$arResult["SECTION"]["SECTION_PAGE_URL"].$arResult["CODE"].".html"
        );
    }
} else
{
    $filterCandidate=end($arrUrlPath);
    $isCollectionTpl=false;
    if (strpos($filterCandidate,"brand-")===0)
    {
        $filterCandidate=explode("brand-",$filterCandidate);
        $filterCandidate=$filterCandidate[1];
        
        $arSelect = Array("ID", "NAME", "CODE");
        $arFilter = Array("IBLOCK_ID"=>BRAND_IBLOCK_ID, "CODE"=>$filterCandidate, "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $arResult["BRAND"]=array(
                "NAME" => $arFields["NAME"],
                "CODE" => $arFields["CODE"],
                "PREFIX" => "brand"
            );
        }
    } else
    if (strpos($filterCandidate,"vid-")===0)
    {
        $filterCandidate=explode("vid-",$filterCandidate);
        $filterCandidate=$filterCandidate[1];
        
        $arSelect = Array("ID", "NAME", "CODE");
        $arFilter = Array("IBLOCK_ID"=>VID_IBLOCK_ID, "CODE"=>$filterCandidate, "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $arResult["BRAND"]=array(
                "NAME" => $arFields["NAME"],
                "CODE" => $arFields["CODE"],
                "PREFIX" => "vid"
            );
        }
    } 
    //КРОШКИ
    
    
    $nav = CIBlockSection::GetNavChain(false, $arResult["IBLOCK_SECTION_ID"]);
    while ($nav_first_section = $nav->GetNext())
    {
        $arResult['BREADCRUMBS'][]=array(
            "NAME" => $nav_first_section["NAME"],
            "URL" => $nav_first_section["SECTION_PAGE_URL"]
        );
    }
    if (is_array($arResult["BRAND"]))
    {
        $arResult['BREADCRUMBS'][]=array(
            "NAME" => $arResult["BRAND"]["NAME"],
            "URL" => $arResult["SECTION"]["SECTION_PAGE_URL"].$arResult["BRAND"]["PREFIX"]."-".$arResult["BRAND"]["CODE"]."/"
        );
        $arResult['BREADCRUMBS'][]=array(
            "NAME" => $arResult["NAME"],
            "URL" => $arResult["SECTION"]["SECTION_PAGE_URL"].$arResult["BRAND"]["PREFIX"]."-".$arResult["BRAND"]["CODE"]."/".$arResult["CODE"].".html"
        );
    } else
    {
        $arResult['BREADCRUMBS'][]=array(
            "NAME" => $arResult["NAME"],
            "URL" => $arResult["SECTION"]["SECTION_PAGE_URL"].$arResult["CODE"].".html"
        );
    }
}

//размеры
$sizes_all=array();
$arResult["SIZES"]=array();
$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID"=>11, "ACTIVE"=>"Y", "CODE" => $_GET["size"]);
$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $sizes_all[$arFields["ID"]]=$arFields["NAME"];
}

$arResult["SIZES"]=array();
foreach ($arResult["PROPERTIES"]["SIZES"]["VALUE"] as $curSizeTovara)
{
    if ($curSizeTovara>0 && isset($sizes_all[$curSizeTovara]))
    {
        $arResult["SIZES"][]=$sizes_all[$curSizeTovara];
    }
}

$cp = $this->__component; // объект компонента
if (is_object($cp))
{
   $cp->arResult['BREADCRUMBS'] = $arResult["BREADCRUMBS"];
   $cp->arResult['CANONICAL_URL'] = $arResult["CANONICAL_URL"];
   $cp->SetResultCacheKeys(array(
        'BREADCRUMBS',
        'CANONICAL_URL'
   ));
}
?>