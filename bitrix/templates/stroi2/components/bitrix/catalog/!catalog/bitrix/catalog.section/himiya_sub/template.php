<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<style type="text/css">
.him-category .col-md-4{width: 100% !important;}
.him-category .catalog-items-list{float: none !important;}
.him-category .img{margin-top:30px;}
.him-category .him-category-text {text-align: left;word-break: break-word;}
.him-category .catalog-grid__item-wrap .catalog-grid__item-tcell img{height: 200px;max-width: none;}
.him-category .catalog-grid__item-wrap .catalog-grid__item-tcell{overflow: hidden;}
.him-category table table td{border: 1px solid #000;padding:2px;}
.him-category table{width: 100%;}
.him-category a:hover{color: #000 !important;}
.him-category .price{margin-top:10px;}
.him-category .price b{color:#ffb400;font-size:20px;}
</style>
<div class="producer him-category">
    <?
		if (count($arResult["itemsBySection"])>0){
			if ($arResult["SKIP_FIRST_SECTION"]=="N"){
			?>
				<? if (($arResult['ID'] == 25) || ($arResult['ID'] == 26)){?>
					<ul class="row catalog-grid default_catalog_top khimiya">
						<?
						foreach ($arResult["itemsBySection"] as $sectionName=>$sectionItems)
						{
							if ($arResult["SKIP_FIRST_SECTION"]=="N")
							{
							?>
								<li class="catalog-grid__item col-xs-4 col-vtop">
									<p class="catalog-grid__item-title"><a href="<?=$sectionItems['LINK']?>"><?=$sectionName?></a></p>
									<div class="catalog-grid__item-wrap">
										<span class="catalog-grid__item-tcell">
											<a href="<?=$sectionItems['LINK']?>">
											<?$img = CFile::ResizeImageGet($sectionItems['PICTURE'], array('width'=>300, 'height'=>300), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
											<? if (!$img) $img['src'] = '/upload/iblock/8d9/logo_alexvit.png'; ?>
												<img src="<?=$img['src']?>" alt="<?=$sectionName?>"/>
											</a>
											<span></span>
										</span>
									</div>
								</li>                      
							<?
							}
						}
						?>
					</ul>
				<? }else{ ?>
					<div class="catalog-items-list">
					<?
						$res = CIBlockElement::GetList (
							Array("ID" => "ASC"),
							Array("SECTION_ID"=>$arResult['ID']),
							false,
							false,
							Array("ID", "NAME", 'DETAIL_PAGE_URL', "DETAIL_PICTURE", "DETAIL_TEXT", "PROPERTY_HIMIYA_PROPS", "PROPERTY_HIM_TABLE_PRICE")
// 							Array('ID', 'NAME', 'DETAIL_PAGE_URL')
						);
					
						while($ob = $res->GetNextElement())
						{
							$arFields = $ob->GetFields();
							$Text=$arFields["DETAIL_TEXT"];
							if ($Text!="")
							{
								$max_length_anons=150;
								$anons=strip_tags($Text);
								if (strlen($anons)>$max_length_anons)
								{
									$pos_probel=strpos($anons," ",$max_length_anons);
									if ($pos_probel>0)
									{
										$anons=substr($anons,0,$pos_probel);
									} else
									{
										$anons=substr($anons,0,$max_length_anons);
									}
									$anons.='...';
								}
							}
							$img = CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array('width'=>200, 'height'=>200), BX_RESIZE_IMAGE_PROPORTIONAL, true);
							$price='';
							foreach ($arFields["PROPERTY_HIM_TABLE_PRICE_VALUE"] as $key=>$val){
								if(!$price) $price=$val["col4"];
								if($price!='по запросу'){
									$price1=preg_replace("/[^0-9]/", '', $price);
									if($val["col4"]=='-') $val1=10000000;
									else $val1=preg_replace("/[^0-9]/", '', $val["col4"]);
									if($price1>$val1) $price=$val["col4"];
								}
							}
							if($price=='-') $price='';
							?>
							<div class="col-md-4">
								<a href="<?=urldecode($arFields['DETAIL_PAGE_URL'])?>"><div class="item">
								<table>
								<tr><td colspan="5"><b><?=$arFields["NAME"]?></b><br/></td></tr>
								<tr>
									<td class="him-category-text" width="200"><?=str_replace("\n","<br />",$anons)?><p class="price"><?if($price){ if($price!='по запросу') echo 'Цена от '; echo '<b>'.$price.'</b>';}?></p></td>
									<td width="30"></td>
									<td width="200">
										<div class="img"><img src="<?=$img['src']?>" alt="<?=$arFields["NAME"]?>" /></div>
									</td>
									<td width="30"></td>
									<td>
										<table  class="product-item__table"><tbody>
											<?
											$i=0;
											
											foreach ($arFields["PROPERTY_HIMIYA_PROPS_VALUE"] as $key=>$val)
											{if($i<4){
												?>
												<tr>
													<td><?=$val?></td>
													<td><?=$arFields["PROPERTY_HIMIYA_PROPS_DESCRIPTION"][$i]?></td>
												</tr>
												<?$i++;
											}}
											?>
										</tbody></table>
									</td>
								</tr></table>
								</div></a>
							</div>
							<?
						}
					?>
					</div>
				<? } ?>
			<?
			}
		}

    $detailText=trim($arResult["~DESCRIPTION"]);
    if ($detailText!="")
    {
    	?><br />
    	<div class="catalog-descript">
    	<?
    	if ($arResult["DESCRIPTION_TYPE"]=="html")
    	{
     		echo $detailText;
    	} else
     	{
    		?><p><?=str_replace("\n","<br />",$detailText)?></p><?
      	}
    	?>
    	</div>
    <?
    }
    ?>
</div>