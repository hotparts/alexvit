<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arResult["itemsByBrand"]=array();
foreach ($arResult["ITEMS"] as $item)
{
    $curBrand=strip_tags($item["DISPLAY_PROPERTIES"]["BRAND"]["DISPLAY_VALUE"]);
    if ($curBrand!="" && $item["PROPERTIES"]["SERT_FILE"]["VALUE"]!="")
    {
        /*$dopFotos=array();
        if ($item["PROPERTIES"]["FOTOS"]["VALUE"])
        {
            foreach ($item["PROPERTIES"]["FOTOS"]["VALUE"] as $value)
            {
                $src=CFile::GetFileArray($value);
                $dopFotos[]=array(
                    "IMG"  => $src["SRC"],
                    "IMG_W"  => $src["WIDTH"],
                    "IMG_H"  => $src["HEIGHT"],
                );
            }
        }
        $arResult["itemsByBrand"][$curBrand][]=array(
            "NAME"      => $item["NAME"],
            "IMG"       => $item["DETAIL_PICTURE"]["SRC"],
            "IMG_W"     => $item["DETAIL_PICTURE"]["WIDTH"],
            "IMG_H"     => $item["DETAIL_PICTURE"]["HEIGHT"],
            "DOP_FOTOS" => $dopFotos
        );*/
        $src=CFile::GetFileArray($item["PROPERTIES"]["SERT_FILE"]["VALUE"]);
        $src=$src["SRC"];
        $arResult["itemsByBrand"][$curBrand][]=array(
            "NAME"      => $item["NAME"],
            "FILE"      => $src
        );
    }
}
ksort($arResult["itemsByBrand"]);
?>