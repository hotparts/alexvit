<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arrUrlPath=explode("/",substr($APPLICATION->GetCurDir(),1,strlen($APPLICATION->GetCurDir())-2));
$filterCandidate=end($arrUrlPath);
if (strpos($filterCandidate,"type-")===0)
{
    $filterCandidate=explode("type-",$filterCandidate);
    $filterCandidate=$filterCandidate[1];
}
$arResult["SECTIONS"]=array();
foreach ($arResult["ITEMS"] as $item)
{
    $curSectionID=$item["~IBLOCK_SECTION_ID"];
    
    $nav = CIBlockSection::GetNavChain(false, $curSectionID);
    $nav_first_section = $nav->GetNext();
    $nav_second_section = $nav->GetNext();
    
    if ($nav_first_section["ID"]!=4 && $nav_second_section["ID"]!=33 && $nav_second_section["ID"]!=57)
    {
        $res = CIBlockSection::GetByID($curSectionID);
        if($ar_res = $res->GetNext())
        {
            if ($ar_res["PICTURE"])
            {
                $img = CFile::ResizeImageGet($ar_res["PICTURE"], array('width'=>261, 'height'=>275), BX_RESIZE_IMAGE_EXACT, true);
                $img=$img["src"];                
            } else
            {
                $img=SITE_TEMPLATE_PATH.'/img/no-photo.png';   
            }
            
            $resTopSection = CIBlockSection::GetByID($ar_res["IBLOCK_SECTION_ID"]);
            if($ar_res_top_section = $resTopSection->GetNext())
            {
                $curBrand=strip_tags($item["DISPLAY_PROPERTIES"]["BRAND"]["DISPLAY_VALUE"]);
                
                if ($curBrand!="" && !$arResult["SECTIONS"][$ar_res_top_section["ID"]]["SUB_SECTIONS"][$curSectionID]["BRANDS"][$curBrand])
                {
                    $curBrandCode=explode('href="',$item["DISPLAY_PROPERTIES"]["BRAND"]["DISPLAY_VALUE"]);
                    $curBrandCode=$curBrandCode[1];
                    $curBrandCode=explode('">',$curBrandCode);
                    $curBrandCode=substr($curBrandCode[0],1);
                    
                    
                    $arResult["SECTIONS"][$ar_res_top_section["ID"]]["SORT"]=$ar_res_top_section["SORT"];
                    $arResult["SECTIONS"][$ar_res_top_section["ID"]]["NAME"]=$ar_res_top_section["NAME"];
                
                    if (!isset($arResult["SECTIONS"][$ar_res_top_section["ID"]]["SUB_SECTIONS"][$curSectionID]))
                    {
                        $arResult["SECTIONS"][$ar_res_top_section["ID"]]["SUB_SECTIONS"][$curSectionID]=array(
                            "SORT" => $ar_res["SORT"],
                            "NAME" => $ar_res['NAME'],
                            "IMG" => $img,
                            "URL" => "/type-".$filterCandidate."/".$ar_res_top_section["CODE"]."/".$ar_res["CODE"]."/"
                        );
                    }
                    $arResult["SECTIONS"][$ar_res_top_section["ID"]]["SUB_SECTIONS"][$curSectionID]["BRANDS"][$curBrand]=array(
                        "NAME" => $curBrand,
                        "URL" => "/type-".$filterCandidate."/".$ar_res_top_section["CODE"]."/".$ar_res["CODE"]."/".$curBrandCode
                    );
                }
            }
        }
    }
}


//добавляем в массив ХИМИЮ
$curTopSectID=4;
$arResult["SECTIONS"][$curTopSectID]=array();
$arResult["SECTIONS"][$curTopSectID]["SORT"]=9999999;
$arResult["SECTIONS"][$curTopSectID]["SUB_SECTIONS"]=array();

$res = CIBlockSection::GetByID($curTopSectID);
if($ar_res = $res->GetNext())
{  
    $arResult["SECTIONS"][$curTopSectID]["NAME"]=$ar_res['NAME'];
    if ($ar_res["PICTURE"])
    {
        $img = CFile::ResizeImageGet($ar_res["PICTURE"], array('width'=>261, 'height'=>275), BX_RESIZE_IMAGE_EXACT, true);
        $img=$img["src"];                
    } else
    {
        $img=SITE_TEMPLATE_PATH.'/img/no-photo.png';   
    }
    $arResult["SECTIONS"][$curTopSectID]["SUB_SECTIONS"][$ar_res["ID"]]=array(
        "SORT" => $ar_res["SORT"],
        "NAME" => '',
        "IMG" => $img,
        "URL" => $ar_res["SECTION_PAGE_URL"]
    );
    
    $arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'ACTIVE'=>'Y', 'SECTION_ID'=>$curTopSectID);
    $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
    while($ar_result = $db_list->GetNext())
    {
        $arResult["SECTIONS"][$curTopSectID]["SUB_SECTIONS"][$ar_res["ID"]]["BRANDS"][$ar_result["ID"]]=array(
            "NAME" => $ar_result["NAME"],
            "URL" => $ar_result["SECTION_PAGE_URL"]
        );
    }
}

//добавляем в массив Настенные покрытия - Лакокрасочные материалы
$curTopSectID=2;
if (!isset($arResult["SECTIONS"][$curTopSectID]))
{
    $arResult["SECTIONS"][$curTopSectID]=array();
    
    $arResult["SECTIONS"][$curTopSectID]["SUB_SECTIONS"]=array();
    
    $res = CIBlockSection::GetByID($curTopSectID);
    if($ar_res = $res->GetNext())
    {
        $arResult["SECTIONS"][$curTopSectID]["SORT"]=$ar_res["SORT"];
        $arResult["SECTIONS"][$curTopSectID]["NAME"]=$ar_res['NAME'];
    }
}
$curTopSectID2=33;
$res = CIBlockSection::GetByID($curTopSectID2);
if($ar_res = $res->GetNext())
{  
    
    if ($ar_res["PICTURE"])
    {
        $img = CFile::ResizeImageGet($ar_res["PICTURE"], array('width'=>261, 'height'=>275), BX_RESIZE_IMAGE_EXACT, true);
        $img=$img["src"];                
    } else
    {
        $img=SITE_TEMPLATE_PATH.'/img/no-photo.png';   
    }
    
    $arResult["SECTIONS"][$curTopSectID]["SUB_SECTIONS"][$curTopSectID2]=array(
        "SORT" => $ar_res["SORT"],
        "NAME" => $ar_res['NAME'],
        "IMG" => $img,
        "URL" => $ar_res["SECTION_PAGE_URL"]
    );
    
    $arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'ACTIVE'=>'Y', 'SECTION_ID'=>$curTopSectID2);
    $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
    while($ar_result = $db_list->GetNext())
    {
        $arResult["SECTIONS"][$curTopSectID]["SUB_SECTIONS"][$curTopSectID2]["BRANDS"][$ar_result["ID"]]=array(
            "NAME" => $ar_result["NAME"],
            "URL" => $ar_result["SECTION_PAGE_URL"]
        );
    }
}


uasort($arResult["SECTIONS"], 'sortSectionsBySort');
foreach ($arResult["SECTIONS"] as $sectionName=>&$sectionItems)
{
    if (isset($sectionItems["SUB_SECTIONS"]))
    {
        uasort($sectionItems["SUB_SECTIONS"], 'sortSectionsBySort');
    }
}
?>