<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (count($arResult["ITEMS"])>0)
{
    ?>
    <ul class="row catalog-grid default_catalog_top">
        <?
        foreach ($arResult["SECTIONS"] as $item)
        {
            ?><li class="catalog-grid__item col-xs-4 col-vtop">
                <p class="catalog-grid__item-title"><a href="<?=$item["URL"]?>"><?=$item["NAME"]?></a></p>
                <div class="catalog-grid__item-wrap">
                    <span class="catalog-grid__item-tcell">
                        <a href="<?=$item["URL"]?>">
                            <img src="<?=$item["IMG"]?>" alt="">
                        </a>
                    <span>
                </span></span></div>
                <?
                if (!empty($item["BRANDS"]))
                {
                    ?>
                    <div class="catalog-grid__item-addit-select">
                        <ul>
                            <?
                            foreach ($item["BRANDS"] as $brand)
                            {
                                ?><li>    
                                    <?=$brand?>
                                </li><?
                            }
                            ?>                            
                        </ul>    
                    </div>
                    <?
                }
                ?>
            </li><?
        }
        ?>
    </ul>
    <?
}
echo $arResult["~DESCRIPTION"];
?>