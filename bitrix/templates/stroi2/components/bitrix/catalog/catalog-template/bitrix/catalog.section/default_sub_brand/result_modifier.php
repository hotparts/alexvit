<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//pre($arResult["ITEMS"]);
$arResult["SECTIONS"]=array();
foreach ($arResult["ITEMS"] as $item)
{
    $curSectionID=$item["~IBLOCK_SECTION_ID"];
    
    $nav = CIBlockSection::GetNavChain(false, $curSectionID);
    $nav_first_section = $nav->GetNext();
    $nav_second_section = $nav->GetNext();
    
    if ($nav_second_section["ID"]!=33)
    {
        $res = CIBlockSection::GetByID($curSectionID);
        if($ar_res = $res->GetNext())
        {
            if ($ar_res["PICTURE"])
            {
                $img = CFile::ResizeImageGet($ar_res["PICTURE"], array('width'=>261, 'height'=>275), BX_RESIZE_IMAGE_EXACT, true);
                $img=$img["src"];                
            } else
            {
                $img=SITE_TEMPLATE_PATH.'/img/no-photo.png';   
            }
            
            $resTopSection = CIBlockSection::GetByID($ar_res["IBLOCK_SECTION_ID"]);
            if($ar_res_top_section = $resTopSection->GetNext())
            {
                
                $curBrand=strip_tags($item["DISPLAY_PROPERTIES"]["EMARKET_BRAND"]["DISPLAY_VALUE"]);
                if ($curBrand!="" && !$arResult["SECTIONS"][$ar_res_top_section["ID"]]["SUB_SECTIONS"][$curSectionID]["BRANDS"][$curBrand])
                {
                    $curBrandCode=explode('href="',$item["DISPLAY_PROPERTIES"]["EMARKET_BRAND"]["DISPLAY_VALUE"]);
                    $curBrandCode=$curBrandCode[1];
                    $curBrandCode=explode('">',$curBrandCode);
                    $curBrandCode=substr($curBrandCode[0],1);
                    $arResult["SECTIONS"][$ar_res_top_section["ID"]]["SORT"]=$ar_res_top_section["SORT"];
                    $arResult["SECTIONS"][$ar_res_top_section["ID"]]["NAME"]=$ar_res_top_section["NAME"];
                    $arResult["SECTIONS"][$ar_res_top_section["ID"]]["ID"]=$ar_res_top_section["ID"];
                    $arResult["SECTIONS"][$ar_res_top_section["ID"]]["PICTURE"]=$ar_res_top_section["PICTURE"];

                    $arResult["SECTIONS"][$ar_res_top_section["ID"]]["SUB_SECTIONS"][$curSectionID]=array(
                        "SORT" => $ar_res["SORT"],
                        "NAME" => $ar_res['NAME'],
                        "ID" => $ar_res['ID'],
                        "IMG" => $img,
                        "URL" => "/".$ar_res_top_section["CODE"]."/".$ar_res["CODE"]."/".$curBrandCode
                    );
                }
            }
        }
    }
}
uasort($arResult["SECTIONS"], 'sortSectionsBySort');
foreach ($arResult["SECTIONS"] as $sectionName=>&$sectionItems)
{
    if (isset($sectionItems["SUB_SECTIONS"]))
    {
        uasort($sectionItems["SUB_SECTIONS"], 'sortSectionsBySort');
    }
}
?>