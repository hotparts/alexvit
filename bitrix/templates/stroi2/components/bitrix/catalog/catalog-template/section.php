<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

$vidName="";
$text=""; $h1="";
$meta_title="";
$meta_desc="";
$meta_key="";
$count_global = count($GLOBALS['PAGE']);
if(stristr($GLOBALS['PAGE'][1], 'type-') === FALSE) {
    $arFilter = Array('IBLOCK_ID'=>3, 'ACTIVE'=>'Y', 'CODE' => $GLOBALS['PAGE'][$count_global-2]);
    $db_list = CIBlockSection::GetList(Array('SORT'=>"ASC"), $arFilter, true);
    while($ar_result = $db_list->Fetch())
    {
        $SECTION_ERROR[] = $ar_result;
    }
    if(!$SECTION_ERROR && !in_array('filter', $GLOBALS['PAGE'])){
        if($GLOBALS['PAGE'][1] != 'catalog'){
            if (defined("ERROR_404"))
            {
                CHTTP::SetStatus("404 Not Found");
                @define("ERROR_404","Y");
                require($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/stroi2/header.php");
            } else
            {
                CHTTP::SetStatus("404 Not Found");
                @define("ERROR_404","Y");
                require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
            }
        }
    }
}elseif(stristr($GLOBALS['PAGE'][1], 'brand-') === FALSE) {
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
    $arFilter = Array('IBLOCK_ID'=>7, 'ACTIVE'=>'Y', 'CODE' => str_replace('brand-','',$GLOBALS['PAGE'][1]));
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
    while($ob = $res->Fetch())
    {
        $SECTION_ERROR[] = $ob;
    }
    if(!$SECTION_ERROR && !in_array('filter', $GLOBALS['PAGE'])){
        if($GLOBALS['PAGE'][1] != 'catalog'){
            if (defined("ERROR_404"))
            {
                CHTTP::SetStatus("404 Not Found");
                @define("ERROR_404","Y");
                require($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/stroi2/header.php");
            } else
            {
                CHTTP::SetStatus("404 Not Found");
                @define("ERROR_404","Y");
                require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
            }
        }
    }
}else{
    $explode = explode('-',$GLOBALS['PAGE'][1]);
    $arSelect = Array("ID", "NAME", 'CODE');
    $arFilter = Array("IBLOCK_ID"=>9, "CODE"=>$explode[1], "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
    while($ob = $res->Fetch())
    {
        $type_el = $ob;
    }
    if(!$type_el){
        if (defined("ERROR_404"))
        {
            CHTTP::SetStatus("404 Not Found");
            @define("ERROR_404","Y");
            require($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/stroi2/header.php");
        } else
        {
            CHTTP::SetStatus("404 Not Found");
            @define("ERROR_404","Y");
            require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
        }
    }
}


if($_SERVER["REQUEST_URI"]=="/brand-armstrong/"){$h1="Напольные покрытия Armstrong";}
elseif($_SERVER["REQUEST_URI"]=="/brand-barlinek/"){$h1="Напольные покрытия Barlinek";}
elseif($_SERVER["REQUEST_URI"]=="/brand-grabo/"){$h1="Напольные покрытия Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/brand-modulyss/"){$h1="Напольные покрытия Modulyss";}
elseif($_SERVER["REQUEST_URI"]=="/brand-vertigo-trend/"){$h1="Напольные покрытия VERTIGO Trend";}
elseif($_SERVER["REQUEST_URI"]=="/khimiya/forbo/"){$h1="Грунтовки и клей Forbo Eurocol";}
elseif($_SERVER["REQUEST_URI"]=="/khimiya/homa/"){$h1="Грунтовки и клеи Homa";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/"){$h1="Напольные покрытия";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/dizayn-plitka-pvkh/brand-forbo/"){$h1="Дизайн плитка ПВХ Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/falshpol/brand-perfaten/"){$h1="Фальшполы Perfaten";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/"){$h1="Флокированное покрытие Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/igloprobivnoy-kovrolin/brand-forbo/"){$h1="Иглопробивной ковролин Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/"){$h1="Керамогранит ColiseumGres";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/"){$h1="Керамогранит Estima";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=derevo"){$h1="Керамогранит Estima под дерево";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=dizayn"){$h1="Керамогранит Estima серия Дизайн";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=kamen"){$h1="Керамогранит Estima под камень";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/"){$h1="Керамогранит Kerama Marazzi";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=derevo"){$h1="Керамогранит Kerama Marazzi под дерево";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=kamen"){$h1="Керамогранит Kerama Marazzi под камень";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-armstrong/"){$h1="Коммерческий линолеум Armstrong";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-armstrong/?vid=gomogennyy"){$h1="Гомогенный линолеум Armstrong";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/"){$h1="Коммерческий линолеум Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-grabo/"){$h1="Коммерческий линолеум Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-grabo/?vid=polukummercheskiy"){$h1="Полукоммерческий линолеум Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-polyflor/"){$h1="Коммерческий линолеум Polyflor";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-escom/"){$h1="Ковровая плитка Escom";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-forbo/"){$h1="Ковровая плитка Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-modulyss/"){$h1="Ковровая плитка Modulyss";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/"){$h1="Натуральный линолеум Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=marmoleum-marbled"){$h1="Натуральный линолеум Forbo Marmoleum Marbled";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/parketnaya-doska/brand-barlinek/"){$h1="Паркетная доска Barlinek";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/parketnaya-doska/brand-grabo/"){$h1="Паркетная доска — Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/sportivnye-pokrytiya/brand-forbo/"){$h1="Спортивные покрытия — Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/stsenicheskie-pokrytiya/brand-grabo/"){$h1="Сценические покрытия — Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/nastennye-pokrytiya/keramicheskaya-plitka/brand-kerama-marazzi/"){$h1="Керамическая плитка Kerama Marazzi";}
elseif($_SERVER["REQUEST_URI"]=="/nastennye-pokrytiya/keramicheskaya-plitka/brand-sokol/"){$h1="Керамическая плитка Сокол";}
elseif($_SERVER["REQUEST_URI"]=="/nastennye-pokrytiya/lakokrasochnye-materialy/kraski-lacos/"){$h1="Лакокрасочные материалы Lacos";}
elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/"){$h1="Подвесные и аккустические потолки Ecophon";}
elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/?group_collection=advantage"){$h1="Модульные потолки Ecophon Advantage";}
elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/?group_collection=focus"){$h1="Модульные потолки Ecophon Focus";}
elseif($_SERVER["REQUEST_URI"]=="/potolki/kassetnye-potolki/brand-ecophon/?group_collection=hygiene"){$h1="Модульные потолки Ecophon Hygiene";}
elseif($_SERVER["REQUEST_URI"]=="/potolki/svobodno-visyashchie-elementy-i-baffly/brand-ecophon/"){$h1="Баффлы Ecophon";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/dizayn-plitka-pvkh/brand-grabo/"){$h1="Дизайн плитка ПВХ Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/sportivnye-pokrytiya/brand-grabosport/"){$h1="Спортивные покрытия GraboSport";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/dizayn-plitka-pvkh/brand-vertigo-trend/"){$h1="Дизайн плитка ПВХ VERTIGO Trend";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-armstrong/?vid=protivoskolzyashchiy"){$h1="Противоскользящий коммерческий линолеум Armstrong";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/?vid=beton"){$h1="Керамогранит ColiseumGres Бетон";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/?vid=derevo"){$h1="Керамогранит ColiseumGres Дерево";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/?vid=kamen"){$h1="Керамогранит ColiseumGres Камень";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-coliseumgres/?vid=mramor"){$h1="Керамогранит ColiseumGres Мрамор";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-escom/?vid=petlevoy-vors"){$h1="Ковровая плитка Escom с петлевым ворсом";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=beton"){$h1="Керамогранит Estima Бетон";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=mramor"){$h1="Керамогранит Estima Мрамор";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-estima/?vid=tekhnika"){$h1="Керамогранит Estima Техника";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-grabo/?vid=gomogennyy"){$h1="Гомогенный коммерческий линолеум Grabo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=beton"){$h1="Керамогранит Kerama Marazzi Бетон";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=dizayn"){$h1="Керамогранит Kerama Marazzi Дизайн";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/keramogranit/brand-kerama-marazzi/?vid=mramor"){$h1="Керамогранит Kerama Marazzi Мрамор";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-polyflor/?vid=gomogennyy"){$h1="Гомогенный коммерческий линолеум Polyflor";}

elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/brand-forbo/"){$h1="Напольные покрытия Forbo"; $meta_title="Напольные покрытия Forbo оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа напольных покрытий Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="напольные покрытия Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/?group_collection=flotex-colour"){$h1="Флокированное покрытие Forbo Flotex Colour"; $meta_title="Флокированное покрытие Forbo группы Flotex Colour оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа флокированных покрытий Forbo Flotex Colour по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="флокированное покрытие forbo flotex colour";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/?group_collection=flotex-linear"){$h1="Флокированное покрытие Forbo Flotex Linear"; $meta_title="Флокированное покрытие Forbo группы Flotex Linear оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа флокированных покрытий Forbo Flotex Linear по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="флокированное покрытие forbo flotex linear";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/?group_collection=flotex-sottsass"){$h1="Флокированное покрытие Forbo Flotex Sottsass"; $meta_title="Флокированное покрытие Forbo группы Flotex Sottsass оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа флокированных покрытий Forbo Flotex Sottsass по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="флокированное покрытие forbo flotex sottsass";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/?group_collection=flotex-tibor"){$h1="Флокированное покрытие Forbo Flotex Tibor"; $meta_title="Флокированное покрытие Forbo группы Flotex Tibor оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа флокированных покрытий Forbo Flotex Tibor по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="флокированное покрытие forbo flotex tibor";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/flokirovannoe-pokrytie/brand-forbo/?group_collection=flotex-vision-"){$h1="Флокированное покрытие Forbo Flotex Vision"; $meta_title="Флокированное покрытие Forbo группы Flotex Vision оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа флокированных покрытий Forbo Flotex Vision по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="флокированное покрытие forbo flotex vision";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/?vid=gomogennyy"){$h1="Гомогенный коммерческий линолеум Forbo"; $meta_title="Гомогенный коммерческий линолеум Forbo оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа гомогенного коммерческого линолеума Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="гомогенный коммерческий линолеум Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/?vid=kommercheskiy"){$h1="Линолеум Forbo коммерческого типа"; $meta_title="Коммерческий линолеум Forbo коммерческого типа оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа линолеума Forbo коммерческого типа по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="линолеум Forbo коммерческого типа";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/?vid=polukummercheskiy"){$h1="Полукоммерческий линолеум Forbo"; $meta_title="Полукоммерческий линолеум Forbo оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа полукоммерческого линолеума Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="полукоммерческий линолеум Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/?vid=protivoskolzyashchiy"){$h1="Коммерческий линолеум Forbo противоскользящий"; $meta_title="Противоскользящий коммерческий линолеум Forbo оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа противоскользящего коммерческого линолеума Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="противоскользящий коммерческий линолеум Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kommercheskiy-linoleum/brand-forbo/?vid=tokoprovodyashchie-poly-i-chistye-pomeshcheniya-"){$h1="Коммерческий линолеум Forbo: Токопроводящие полы и чистые помещения"; $meta_title="Коммерческий линолеум Forbo: токопроводящие полы и чистые помещения оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа токопроводящего коммерческого линолеума Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="токопроводящие полы Forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-forbo/?vid=kombinirovannaya-petlya"){$h1="Ковровая плитка Forbo с комбинированной петлей"; $meta_title="Ковровая плитка Forbo с комбинированной петлёй оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа ковровой плитки Forbo с комбинированной петлей по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="ковровая плитка Forbo с комбинированной петлей";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-forbo/?vid=petlevoy-vors"){$h1="Ковровая плитка Forbo с петлевым ворсом"; $meta_title="Ковровая плитка Forbo с петлевым ворсом оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа ковровой плитки Forbo с петлевым ворсом по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="ковровая плитка Forbo с петлевым ворсом";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-forbo/?vid=razrezanaya-petlya"){$h1="Ковровая плитка Forbo с разрезаной петлей"; $meta_title="Ковровая плитка Forbo с разрезаной петлёй оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа ковровой плитки Forbo с разрезаной петлей по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="ковровая плитка Forbo с разрезаной петлей";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/kovrovaya-plitka/brand-forbo/?vid=svobodnyy-poryadok-ukladki"){$h1="Ковровая плитка Forbo со свободным порядком укладки"; $meta_title="Ковровая плитка Forbo со свободным порядком укладки оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа ковровой плитки Forbo со свободным порядком укладки по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="ковровая плитка Forbo со свободным порядком укладки";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=akusticheskiy-marmoleum"){$h1="Натуральный акустический линолеум Forbo Marmoleum"; $meta_title="Натуральный линолеум Forbo группы Акустический Marmoleum оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа акустического натурального линолеума Forbo Marmoleum по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="натуральный акустический линолеум forbo marmoleum";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=marmoleum-linear"){$h1="Натуральный линолеум Forbo Marmoleum Linear"; $meta_title="Натуральный линолеум Forbo группы Marmoleum Linear оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа натурального линолеума Forbo Marmoleum Linear по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="натуральный линолеум forbo marmoleum linear";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=marmoleum-patterned"){$h1="Натуральный линолеум Forbo Marmoleum Patterned"; $meta_title="Натуральный линолеум Forbo группы Marmoleum Patterned оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа натурального линолеума Forbo Marmoleum Patterned по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="натуральный линолеум forbo marmoleum patterned";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=marmoleum-solid"){$h1="Натуральный линолеум Forbo Marmoleum Solid"; $meta_title="Натуральный линолеум Forbo группы Marmoleum Solid оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа натурального линолеума Forbo Marmoleum Solid по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="натуральный линолеум forbo marmoleum solid";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/naturalnyy-linoleum/brand-forbo/?group_collection=tokorasseivayushchiy-marmoleum"){$h1="Натуральный токорассеивающий линолеум Forbo"; $meta_title="Натуральный линолеум Forbo токорассеивающей группы оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа токорассеивающего натурального линолеума Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="натуральный токорассеивающий линолеум forbo";}
elseif($_SERVER["REQUEST_URI"]=="/napolnye-pokrytiya/sportivnye-pokrytiya/brand-forbo/?vid=gomogennyy"){$h1="Гомогенные спортивные покрытия Forbo"; $meta_title="Спортивные гомогенные покрытия Forbo оптом в Москве - ООО «АлексВит»"; $meta_desc="Оптовая продажа гомогенных покрытий Forbo по доступным ценам. Доставка заказов на объекты по Москве и Московской области."; $meta_key="гомогенные спортивные покрытия Forbo";}

if (!stristr($GLOBALS['PAGE'][1], 'type-') === FALSE || !stristr($GLOBALS['PAGE'][1], 'brand-') === FALSE) {
    $arrUrlPath = explode("/", substr($APPLICATION->GetCurDir(), 1, strlen($APPLICATION->GetCurDir()) - 2));
    $filterCandidate = end($arrUrlPath);
    $filterCandidateFirst = $arrUrlPath[0];

    $isFilterCandidateFirst = false;
    $isCollectionTpl = false;
    $isTypePlaceTpl = false;
    $isBrandTpl = false;

    $isLaki = false;

    $ADD_SECTIONS_CHAIN = "Y";

    if (strpos($filterCandidateFirst, "type-") === 0) {
        $filterCandidateFirst = explode("type-", $filterCandidateFirst);
        $filterCandidateFirst = $filterCandidateFirst[1];
        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
        $arFilter = Array("IBLOCK_ID" => TYPE_PLACE_IBLOCK_ID, "CODE" => $filterCandidateFirst, "ACTIVE" => "Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $curBrand = $arFields;
            $_GET["brand"] = "type_" . $arFields["ID"];

            $isTypePlaceTpl = true;

            if (count($arrUrlPath) == 1) {
                $GLOBALS['sectionFilter'] = array("PROPERTY_TYPE_PLACE" => $arFields["ID"]);
            } else {
                if (strpos($filterCandidate, "brand-") === 0) {
                    $filterCandidate = explode("brand-", $filterCandidate);
                    $filterCandidate = $filterCandidate[1];
                    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
                    $arFilter = Array("IBLOCK_ID" => BRAND_IBLOCK_ID, "CODE" => $filterCandidate, "ACTIVE" => "Y");
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                    if ($ob = $res->GetNextElement()) {
                        $arFieldsBrand = $ob->GetFields();
                        $curBrand2 = $arFieldsBrand;
                        $curSectCode = $arrUrlPath[count($arrUrlPath) - 2];
                    } else {
                        CHTTP::SetStatus("404 Not Found");
                        @define("ERROR_404", "Y");
                    }
                } else {
                    $curSectCode = $arrUrlPath[count($arrUrlPath) - 1];
                }

                $arFilter = Array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'CODE' => $curSectCode);
                $db_list = CIBlockSection::GetList(Array($by => $order), $arFilter, true);
                $isFilterCandidateFirst = true;
                if ($ar_result = $db_list->GetNext()) {
                    $isCollectionTpl = true;
                    $curSection = $ar_result;
                    $arResult["VARIABLES"]["SECTION_ID"] = $ar_result['ID'];
                    $GLOBALS['sectionFilter'] = array("PROPERTY_TYPE_PLACE" => $arFields["ID"], "PROPERTY_EMARKET_BRAND" => $arFieldsBrand["ID"]);
                    $ADD_SECTIONS_CHAIN = "N";
                } else {
                    CHTTP::SetStatus("404 Not Found");
                    @define("ERROR_404", "Y");
                }
            }
        } else {
            CHTTP::SetStatus("404 Not Found");
            @define("ERROR_404", "Y");
        }
    } else
        if (strpos($filterCandidate, "brand-") === 0) {
            $filterCandidate = explode("brand-", $filterCandidate);
            $filterCandidate = $filterCandidate[1];
            $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "DETAIL_TEXT");
            $arFilter = Array("IBLOCK_ID" => BRAND_IBLOCK_ID, "CODE" => $filterCandidate, "ACTIVE" => "Y");
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $text = $arFields["DETAIL_TEXT"];
                $curBrand = $arFields;

                if (count($arrUrlPath) == 1) {
                    $_GET["brand"] = "brand_" . $arFields["ID"];
                    $isBrandTpl = true;
                    $GLOBALS['sectionFilter'] = array("PROPERTY_EMARKET_BRAND" => $arFields["ID"], 'SECTION_ID' => array(1, 2, 3), "INCLUDE_SUBSECTIONS" => "Y");
                } else {
                    $curSectCode = $arrUrlPath[count($arrUrlPath) - 2];
                    $arFilter = Array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'CODE' => $curSectCode);
                    $db_list = CIBlockSection::GetList(Array($by => $order), $arFilter, true);

                    if ($ar_result = $db_list->GetNext()) {
                        $isCollectionTpl = true;
                        $arResult["VARIABLES"]["SECTION_ID"] = $ar_result['ID'];
                        $GLOBALS['sectionFilter'] = array("PROPERTY_EMARKET_BRAND" => $arFields["ID"]);
                    }
                }
            } else {
                CHTTP::SetStatus("404 Not Found");
                @define("ERROR_404", "Y");
            }
        } else
            if (strpos($filterCandidate, "vid-") === 0) {
                $filterCandidate = explode("vid-", $filterCandidate);
                $filterCandidate = $filterCandidate[1];
                $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
                $arFilter = Array("IBLOCK_ID" => VID_IBLOCK_ID, "CODE" => $filterCandidate, "ACTIVE" => "Y");
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if ($ob = $res->GetNextElement()) {
                    $arFields = $ob->GetFields();
                    $curBrand = $arFields;
                    $_GET["brand"] = $arFields["ID"];
                    $curSectCode = $arrUrlPath[count($arrUrlPath) - 2];
                    $arFilter = Array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'CODE' => $curSectCode);
                    $db_list = CIBlockSection::GetList(Array($by => $order), $arFilter, true);

                    if ($ar_result = $db_list->GetNext()) {
                        $isCollectionTpl = true;
                        $arResult["VARIABLES"]["SECTION_ID"] = $ar_result['ID'];
                        $GLOBALS['sectionFilter'] = array("PROPERTY_VID_TOVARA" => $arFields["ID"]);
                    } else {

                    }
                } else {
                    CHTTP::SetStatus("404 Not Found");
                    @define("ERROR_404", "Y");
                }
            }

    if (!$isTypePlaceTpl && !$isBrandTpl) {
        $nav = CIBlockSection::GetNavChain(false, $arResult["VARIABLES"]["SECTION_ID"]);
        $nav_first_section = $nav->GetNext();
        $nav_second_section = $nav->GetNext();

        $res = CIBlockSection::GetByID($arResult["VARIABLES"]["SECTION_ID"]);
        if ($curSect = $res->GetNext()) {

        } else {
            CHTTP::SetStatus("404 Not Found");
            @define("ERROR_404", "Y");
        }
    }
    $tplSect = "";
    if ($isCollectionTpl) {
        $tplSect = 'default_sub_collection';
    } else
        if ($isTypePlaceTpl) {
            $tplSect = 'default_sub_place';
        } else
            if ($isBrandTpl) {
                $tplSect = 'default_sub_brand';
            } else {
                switch ($nav_first_section["ID"]) {
                    //Монтаж напольных покрытий
                    case "14":
                        $tplSect = 'only_text';
                        break;
                    //сертификаты
                    case "15":
                    case "68":
                        $tplSect = 'sertifikaty';
                        break;
                    //сопутствующие товары
                    case "13":
                        $tplSect = 'soputka';
                        break;
                    //химия
                    case "4":
                        //корневой раздел
                        if ($curSect["DEPTH_LEVEL"] == 1) {
                            $tplSect = 'himiya_top';
                        } else //подраздел химии
                        {
                            $tplSect = 'himiya_sub';
                        }
                        break;
                    default:
                        //стандартный корневой раздел

                        if ($nav_first_section["ID"] == 2 && $nav_second_section["ID"] == 33) {
                            if ($arResult["VARIABLES"]["SECTION_ID"] == 33) {
                                $tplSect = 'himiya_top';
                            } else //подраздел химии
                            {
                                $isLaki = true;
                                $tplSect = 'himiya_sub';
                            }
                        } else
                            if ($nav_first_section["ID"] == 2 && $nav_second_section["ID"] == 57) {
                                if ($arResult["VARIABLES"]["SECTION_ID"] == 57) {
                                    $tplSect = 'himiya_top';
                                } else //подраздел химии
                                {
                                    $isLaki = true;
                                    $tplSect = 'himiya_sub';
                                }
                            } else {
                                if ($curSect["DEPTH_LEVEL"] == 1) {
                                    $tplSect = 'default_top';
                                } else //стандартный подраздел
                                {
                                    $tplSect = 'default_sub';
                                }
                            }
                        break;
                }
            }
    if (is_array($curBrand) && $isFilterCandidateFirst) {
        $APPLICATION->AddChainItem($curBrand["NAME"], "/type-" . $filterCandidateFirst . "/");
    } ?>

    <div class="workarea grid2x1">
        <h1>
            <?
            if ($tplSect == 'himiya_sub') {
                if ($isLaki) {
                    if (!$h1) {
                        ?>Лакокрасочные материалы — <?
                    }
                } else {
                    if (!$h1) {
                        if (!strpos($_SERVER['REQUEST_URI'], 'forbo')) { ?>Химия — <? }
                    }
                }
            }
            if (!$isBrandTpl && !$isTypePlaceTpl) {
                if (!$h1) echo $APPLICATION->ShowTitle(false);
            }
            if (is_array($curBrand)) {
                if (!$isBrandTpl && !$isTypePlaceTpl) {
                    if (!$h1) echo " — ";
                }
                if (!$h1) echo $curBrand["NAME"];
                if ($isTypePlaceTpl && $arResult["VARIABLES"]["SECTION_ID"]) {
                    if (!$h1) echo " — " . $curSection["NAME"];
                    if ($curBrand2) {
                        if (!$h1) echo " " . $curBrand2["NAME"];
                    }
                }
                if ($_GET["vid"] != "") {
                    $arSelectVid = Array("ID", "NAME");
                    $arFilterVid = Array("IBLOCK_ID" => 10, "ACTIVE" => "Y", "CODE" => $_GET["vid"]);
                    $resVid = CIBlockElement::GetList(Array(), $arFilterVid, false, false, $arSelectVid);
                    if ($obVid = $resVid->GetNextElement()) {
                        $arFieldsVid = $obVid->GetFields();
                        $vidName = $arFieldsVid["NAME"];
                        $GLOBALS['sectionFilter']["PROPERTY_VID_TOVARA"] = $arFieldsVid["ID"];
                        if (!$h1) {
                            ?> — <?= $vidName ?><?
                        }
                    }
                }
                if ($_GET["size"] != "") {
                    $arSelectSize = Array("ID", "NAME");
                    $arFilterSize = Array("IBLOCK_ID" => 11, "ACTIVE" => "Y", "CODE" => $_GET["size"]);
                    $resSize = CIBlockElement::GetList(Array(), $arFilterSize, false, false, $arSelectSize);
                    if ($obSize = $resSize->GetNextElement()) {
                        $arFieldsSize = $obSize->GetFields();
                        $sizeName = $arFieldsSize["NAME"];
                        $GLOBALS['sectionFilter']["PROPERTY_SIZES"] = $arFieldsSize["ID"];
                        if (!$h1) {
                            ?> — Размер <?= $sizeName ?><?
                        }
                    }
                }
                if ($_GET["group_collection"] != "") {
                    $arSelectGC = Array("ID", "NAME");
                    $arFilterGC = Array("IBLOCK_ID" => 12, "ACTIVE" => "Y", "CODE" => $_GET["group_collection"]);
                    $resGC = CIBlockElement::GetList(Array(), $arFilterGC, false, false, $arSelectGC);
                    if ($obGC = $resGC->GetNextElement()) {
                        $arFieldsGC = $obGC->GetFields();
                        $GCName = $arFieldsGC["NAME"];
                        $GLOBALS['sectionFilter']["PROPERTY_GROUP"] = $arFieldsGC["ID"];
                        if (!$h1) {
                            ?> — <?= $GCName ?><?
                        }
                    }
                }
            }
            if ($h1) {
                echo $h1;
            }
            ?>
        </h1>
        <?
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            $tplSect,
            array(
                "SECTION_USER_FIELDS" => array("UF_ANONS"),
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
                "BASKET_URL" => $arParams["BASKET_URL"],
                "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                "FILTER_NAME" => "sectionFilter",
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "SET_TITLE" => $arParams["SET_TITLE"],
                "MESSAGE_404" => $arParams["MESSAGE_404"],
                "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                "SHOW_404" => $arParams["SHOW_404"],
                "FILE_404" => $arParams["FILE_404"],
                "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                "PAGE_ELEMENT_COUNT" => 999,
                "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                "PRICE_CODE" => $arParams["PRICE_CODE"],
                "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

                "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

                "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],

                "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

                "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

                'LABEL_PROP' => $arParams['LABEL_PROP'],
                'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

                'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
                'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
                'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
                'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
                'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
                'SHOW_ALL_WO_SECTION' => 'Y',
                'TEMPLATE_THEME' => $_GET["brand"],
                "ADD_SECTIONS_CHAIN" => $ADD_SECTIONS_CHAIN,
                'ADD_TO_BASKET_ACTION' => $basketAction,
                'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
                'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
                'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
            ),
            $component
        ); ?>

    </div>


<?
} else {
    $arFilter = Array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
        "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
    );
    $countElements = CIBlockElement::GetList(Array(), $arFilter, Array());

    if (!$arParams['FILTER_VIEW_MODE'])
        $arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
    $arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');
    $verticalGrid = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");

    if ($verticalGrid) {
        ?>
        <div class="workarea grid2x1"><?
    }
    if ($arParams['USE_FILTER'] == 'Y') {

        $arFilter = array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "ACTIVE" => "Y",
            "GLOBAL_ACTIVE" => "Y",
        );
        if (0 < intval($arResult["VARIABLES"]["SECTION_ID"])) {
            $arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
        } elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"]) {
            $arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
        }

        $obCache = new CPHPCache();
        if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog")) {
            $arCurSection = $obCache->GetVars();
        } elseif ($obCache->StartDataCache()) {
            $arCurSection = array();
            if (\Bitrix\Main\Loader::includeModule("iblock")) {
                $dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

                if (defined("BX_COMP_MANAGED_CACHE")) {
                    global $CACHE_MANAGER;
                    $CACHE_MANAGER->StartTagCache("/iblock/catalog");

                    if ($arCurSection = $dbRes->Fetch()) {
                        $CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_ID"]);
                    }
                    $CACHE_MANAGER->EndTagCache();
                } else {
                    if (!$arCurSection = $dbRes->Fetch())
                        $arCurSection = array();
                }
            }
            $obCache->EndDataCache($arCurSection);
        }
        if (!isset($arCurSection)) {
            $arCurSection = array();
        }

        ?>

        <?
        if ($countElements > 0) { ?>


        <?
        } ?>

        <?

    }
if ($verticalGrid)
{
    ?>
    <div class="catalog_section">
    <?

}
    ?>


    <?
//pre(stristr($GLOBALS['PAGE'][1], 'type-')===FALSE);
//    if (stristr($GLOBALS['PAGE'][3], 'type-') === FALSE || stristr($GLOBALS['PAGE'][3], 'brand-') === FALSE) {
    if (stristr($GLOBALS['PAGE'][3], 'brand-') === FALSE) {


        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "",
            array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
                "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
                "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
                "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
                "ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
                "PARAMS_PARENT" => $arParams
            ),
            $component
        );
    }
    ?>

    <?
    $intSectionID = 0; ?>

    <?
if ($countElements > 0)
{
    $dbPriceType = CCatalogGroup::GetList(
        array("SORT" => "ASC"),
        array("NAME" => "BASE")
    )->Fetch();
    $ID_PRICE = $dbPriceType['ID'];
    //default
    global $APPLICATION;
    $section_dir = $APPLICATION->GetCurDir();

    $section_view = (int)htmlspecialchars($APPLICATION->get_cookie('CATALOG_SECTION_VIEW'));
    if (!$section_view) $section_view = 1;
    $section_sort = (int)htmlspecialchars($APPLICATION->get_cookie('CATALOG_SECTION_SORT'));
    if (!$section_sort) $section_sort = 1;
    $section_page = (int)htmlspecialchars($APPLICATION->get_cookie('CATALOG_SECTION_PAGE'));
    if (!$section_page) $section_page = 1;


    //set filter
    if (!empty($_REQUEST['view'])) {
        $section_view = intval($_REQUEST['view']);
        $APPLICATION->set_cookie("CATALOG_SECTION_VIEW", $section_view, time() + 60 * 60 * 24 * 7);
    }
    if (!empty($_REQUEST['sort'])) {
        $section_sort = intval($_REQUEST['sort']);
        $APPLICATION->set_cookie("CATALOG_SECTION_SORT", $section_sort, time() + 60 * 60 * 24 * 7);
    }
    if (!empty($_REQUEST['page'])) {
        $section_page = intval($_REQUEST['page']);
        $APPLICATION->set_cookie("CATALOG_SECTION_PAGE", $section_page, time() + 60 * 60 * 24 * 7);
    }

    switch ($section_view) {
        case '1':
            $template = '.default';
            break;
        case '2':
            $template = 'list_1';
            break;
    }

    switch ($section_sort) {
        case '1':
            $arParams["ELEMENT_SORT_FIELD2"] = 'NAME';
            $arParams["ELEMENT_SORT_ORDER2"] = 'asc';
            break;
        case '2':
            $arParams["ELEMENT_SORT_FIELD2"] = 'NAME';
            $arParams["ELEMENT_SORT_ORDER2"] = 'desc';
            break;
        case '3':
            $arParams["ELEMENT_SORT_FIELD2"] = 'CATALOG_PRICE_' . $ID_PRICE;
            $arParams["ELEMENT_SORT_ORDER2"] = 'asc';
            break;
        case '4':
            $arParams["ELEMENT_SORT_FIELD2"] = 'CATALOG_PRICE_' . $ID_PRICE;
            $arParams["ELEMENT_SORT_ORDER2"] = 'desc';
            break;
        case '5':
            $arParams["ELEMENT_SORT_FIELD2"] = 'PROPERTY_EMARKET_RATING';
            $arParams["ELEMENT_SORT_ORDER2"] = 'asc';
            break;
        case '6':
            $arParams["ELEMENT_SORT_FIELD2"] = 'PROPERTY_EMARKET_RATING';
            $arParams["ELEMENT_SORT_ORDER2"] = 'desc';
            break;
        case '7':
            $arParams["ELEMENT_SORT_FIELD2"] = 'PROPERTY_EMARKET_COMMENTS_COUNT';
            $arParams["ELEMENT_SORT_ORDER2"] = 'asc';
            break;
        case '8':
            $arParams["ELEMENT_SORT_FIELD2"] = 'PROPERTY_EMARKET_COMMENTS_COUNT';
            $arParams["ELEMENT_SORT_ORDER2"] = 'desc';
            break;
    }

    switch ($section_page) {
        case '1':
            $arParams["PAGE_ELEMENT_COUNT"] = 40;
            break;
        case '2':
            $arParams["PAGE_ELEMENT_COUNT"] = 80;
            break;
        case '3':
            $arParams["PAGE_ELEMENT_COUNT"] = 120;
            break;
        case '4':
            $arParams["PAGE_ELEMENT_COUNT"] = 200;
            break;
        case '5':
            $arParams["PAGE_ELEMENT_COUNT"] = 1000;
            break;
    }

    ?>
    <div class="bg_section">
    <div class="container-fluid">

    <div class="filter_block">
        <script>
            $('.ajax_basket').append('<span class="btn btn_blue filter-btn visible-xs">Показать</span>');
        </script>>
        <?/*span class="btn btn_blue filter-btn visible-xs"><?= GetMessage('CSFILTER_TITL_FILTER') ?></span*/?>
        <?
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.smart.filter",
            "visual_" . ($arParams["FILTER_VIEW_MODE"] == "HORIZONTAL" ? "horizontal" : "vertical"),
            Array(
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID" => $arCurSection['ID'],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "PRICE_CODE" => $arParams["PRICE_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "SAVE_IN_SESSION" => "N",
                "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                "XML_EXPORT" => "Y",
                "SECTION_TITLE" => "NAME",
                "SECTION_DESCRIPTION" => "DESCRIPTION",
                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                "SEF_MODE" => $arParams["SEF_MODE"],
                "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
                "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
            ),
            $component,
            array('HIDE_ICONS' => 'Y')
        ); ?>
        <img src="/upload/free_example.jpg" class="free_example">

        <div class="expert_form">
            <img src="/upload/expert_choose.jpeg" class="free_example">
            <a data-fancybox data-type="ajax" href="/modal-forms/order/" class="order_img">Заказать расчет</a>
        </div>
    </div>

    <div class=" catalog_section_block">
    <div class="catalog-filter">
        <div class="option">
            <span class="view"><?= GetMessage('CSFILTER_VIEW'); ?>:</span>
            <a class="view mdi mdi-view-grid <?
            if ($section_view == 1) echo 'active'; ?>"
               href="<?= $APPLICATION->GetCurPageParam("view=1", array("view")) ?>"></a>
            <a class="view mdi mdi-reorder-horizontal <?
            if ($section_view == 2) echo 'active'; ?>"
               href="<?= $APPLICATION->GetCurPageParam("view=2", array("view")) ?>"></a>
        </div>
        <div class="option sort">
            <span><?= GetMessage('CSFILTER_SORTBY'); ?>:</span>
            <select onchange="window.location=this.value">
                <option <?
                        if (($section_sort == 1) || ($section_sort == 2)): ?>selected='selected'<?
                endif;
                ?>
                        value="<?
                        $tempSort = 1;
                        echo $APPLICATION->GetCurPageParam("sort=" . $tempSort, array("sort"));
                        ?>"
                ><?= GetMessage('CSFILTER_TITLE'); ?>
                </option>
                <option <?
                        if (($section_sort == 3)): ?>selected='selected'<?
                endif;
                ?>
                        value="<?
                        $tempSort = 3;
                        echo $APPLICATION->GetCurPageParam("sort=" . $tempSort, array("sort"));
                        ?>"
                ><?= GetMessage('CSFILTER_PRICE'); ?>
                </option>

                <option <?
                        if (($section_sort == 4)): ?>selected='selected'<?
                endif;
                ?>
                        value="<?
                        $tempSort = 4;
                        echo $APPLICATION->GetCurPageParam("sort=" . $tempSort, array("sort"));
                        ?>">
                    <?= GetMessage('CSFILTER_PRICE_DOWN'); ?>
                </option>

                <option <?
                        if (($section_sort == 5) || ($section_sort == 6)): ?>selected='selected'<?
                endif;
                ?>
                        value="<?
                        $tempSort = 6;
                        echo $APPLICATION->GetCurPageParam("sort=" . $tempSort, array("sort"));
                        ?>"
                ><?= GetMessage('CSFILTER_RATING'); ?>
                </option>
                <option <?
                        if (($section_sort == 7) || ($section_sort == 8)): ?>selected='selected'<?
                endif;
                ?>
                        value="<?
                        $tempSort = 8;
                        echo $APPLICATION->GetCurPageParam("sort=" . $tempSort, array("sort"));
                        ?>"
                ><?= GetMessage('CSFILTER_NUMBER'); ?>
                </option>
            </select>
        </div>
        <div class="option amount">
            <span><?= GetMessage('CSFILTER_PAGE'); ?>:</span>
            <select onchange="window.location=this.value">
                <option <?
                        if ($section_page == 1): ?>selected='selected'<?
                endif;
                ?> value="<?= $APPLICATION->GetCurPageParam("page=1", array("page")) ?>">40
                </option>
                <option <?
                        if ($section_page == 2): ?>selected='selected'<?
                endif;
                ?> value="<?= $APPLICATION->GetCurPageParam("page=2", array("page")) ?>">80
                </option>
                <option <?
                        if ($section_page == 3): ?>selected='selected'<?
                endif;
                ?> value="<?= $APPLICATION->GetCurPageParam("page=3", array("page")) ?>">120
                </option>
                <option <?
                        if ($section_page == 4): ?>selected='selected'<?
                endif;
                ?> value="<?= $APPLICATION->GetCurPageParam("page=4", array("page")) ?>">200
                </option>
                <option <?
                        if ($section_page == 5): ?>selected='selected'<?
                endif;
                ?>
                        value="<?= $APPLICATION->GetCurPageParam("page=5", array("page")) ?>"><?= GetMessage('CSFILTER_ALL'); ?></option>
            </select>
        </div>
    </div><?
} ?>

    <?
//    pre($arResult["VARIABLES"]["SECTION_CODE"]);
//    pre($arResult["VARIABLES"]["SECTION_ID"]);
    $intSectionID = $APPLICATION->IncludeComponent(
        "bitrix:catalog.section",
        $template,
        array(
            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
            "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
            "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
            "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
            "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
            "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
            "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
            "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
            "BASKET_URL" => $arParams["BASKET_URL"],
            "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
            "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
            "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
            "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
            "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
            "FILTER_NAME" => $arParams["FILTER_NAME"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
            "SET_TITLE" => $arParams["SET_TITLE"],
            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
            "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
            "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
            "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
            "PRICE_CODE" => $arParams["PRICE_CODE"],
            "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
            "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
            "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
            "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
            "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

            "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
            "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
            "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
            "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

            "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
            "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
            "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
            "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
            'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

            'LABEL_PROP' => $arParams['LABEL_PROP'],
            'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
            'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

            'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
            'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
            'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
            'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
            'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
            'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
            'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
            'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

            'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
            "ADD_SECTIONS_CHAIN" => "N",
            "COMPARE_NAME" => $arParams['COMPARE_NAME']
        ),
        $component
    );
    ?><?
    if ($verticalGrid) {
        ?></div>
        </div>


        </div>
        <? $APPLICATION->ShowViewContent('section_description'); ?>
        <div style="clear: both;"></div>
        </div>
        </div>
        <?
    }
}
?>
<?
if ($tplSect=='himiya_sub')
{
    if ($isLaki)
    {
        $curTitle='Лакокрасочные материалы — '.$APPLICATION->GetTitle(false);
    } else
    {
        //if(!strpos($_SERVER['REQUEST_URI'],'forbo')) $curTitle='Химия — '.$APPLICATION->GetTitle(false);
    }

    //if(!strpos($_SERVER['REQUEST_URI'],'forbo')) $APPLICATION->SetPageProperty('title',$curTitle);
} else
    if (is_array($curBrand))
    {
        if ($isBrandTpl || $isTypePlaceTpl)
        {
            $curTitle=$curBrand["NAME"];
            if ($isTypePlaceTpl && $arResult["VARIABLES"]["SECTION_ID"])
            {
                $curTitle.=" — ".$curSection["NAME"];
                if ($curBrand2)
                {
                    $curTitle.=" ".$curBrand2["NAME"];
                }
            }
        } else
        {
            $curTitle=$APPLICATION->GetTitle(false)." — ". $curBrand["NAME"];
        }
        if ($vidName!="")
        {
            $curTitle.=" — ".$vidName;
        }
        if ($sizeName!="")
        {
            $curTitle.=" — Размер ".$sizeName;
        }
        if ($GCName!="")
        {
            $curTitle.=" — Размер ".$GCName;
        }

        $APPLICATION->SetPageProperty('title',$curTitle);
    }
if (is_array($curBrand) && !$isFilterCandidateFirst)
{
    $APPLICATION->AddChainItem($curBrand["NAME"], $APPLICATION->GetCurDir());
    if ($vidName!="")
    {
        $APPLICATION->AddChainItem($vidName, $APPLICATION->GetCurDir()."?vid=".$_GET["vid"]);
    }
    if ($sizeName!="")
    {
        if ($vidName=="")
        {
            $APPLICATION->AddChainItem($sizeName, $APPLICATION->GetCurDir()."?size=".$_GET["size"]);
        } else
        {
            $APPLICATION->AddChainItem($sizeName, $APPLICATION->GetCurDir()."?size=".$_GET["size"]."&vid=".$_GET["vid"]);
        }
    }
    if ($GCName!="" && $sizeName!="" && $vidName!="")
    {
        $APPLICATION->AddChainItem($GCName, $APPLICATION->GetCurDir()."?size=".$_GET["size"]."&vid=".$_GET["vid"]."&group_collection=".$_GET["group_collection"]);
    } else
        if ($GCName!="" && $sizeName!="")
        {
            $APPLICATION->AddChainItem($GCName, $APPLICATION->GetCurDir()."?size=".$_GET["size"]."&group_collection=".$_GET["group_collection"]);
        } else
            if ($GCName!="" && $vidName!="")
            {
                $APPLICATION->AddChainItem($GCName, $APPLICATION->GetCurDir()."?vid=".$_GET["vid"]."&group_collection=".$_GET["group_collection"]);
            } else
                if ($GCName!="")
                {
                    $APPLICATION->AddChainItem($GCName, $APPLICATION->GetCurDir()."?group_collection=".$_GET["group_collection"]);
                }

}
$arSelect = Array("ID",'NAME','IBLOCK_ID','PROPERTY_PAGE');
$arFilter = Array("IBLOCK_ID"=>23, "PROPERTY_PAGE"=>$GLOBALS['requestUriWithoutParams'], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
while($ob = $res->Fetch())
{
    $seo_el = $ob;
}
if($seo_el):?>
    <div>
        <?$APPLICATION->IncludeComponent("bitrix:news.detail","seo",Array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "USE_SHARE" => "Y",
                "SHARE_HIDE" => "N",
                "SHARE_TEMPLATE" => "",
                "SHARE_HANDLERS" => array(""),
                "SHARE_SHORTEN_URL_LOGIN" => "",
                "SHARE_SHORTEN_URL_KEY" => "",
                "AJAX_MODE" => "Y",
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => "23",
                "ELEMENT_ID" => $seo_el['ID'],
                "ELEMENT_CODE" => "",
                "CHECK_DATES" => "Y",
                "FIELD_CODE" => Array("ID"),
                "PROPERTY_CODE" => Array(""),
                "IBLOCK_URL" => "news.php?ID=#IBLOCK_ID#\"",
                "DETAIL_URL" => "",
                "SET_TITLE" => "Y",
                "SET_CANONICAL_URL" => "Y",
                "SET_BROWSER_TITLE" => "Y",
                "BROWSER_TITLE" => "-",
                "SET_META_KEYWORDS" => "Y",
                "META_KEYWORDS" => "-",
                "SET_META_DESCRIPTION" => "Y",
                "META_DESCRIPTION" => "-",
                "SET_STATUS_404" => "Y",
                "SET_LAST_MODIFIED" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "ADD_ELEMENT_CHAIN" => "N",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "USE_PERMISSIONS" => "Y",
                "GROUP_PERMISSIONS" => Array("1"),
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "CACHE_GROUPS" => "Y",
                "DISPLAY_TOP_PAGER" => "Y",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Страница",
                "PAGER_TEMPLATE" => "",
                "PAGER_SHOW_ALL" => "Y",
                "PAGER_BASE_LINK_ENABLE" => "Y",
                "SHOW_404" => "Y",
                "MESSAGE_404" => "",
                "STRICT_SECTION_CHECK" => "Y",
                "PAGER_BASE_LINK" => "",
                "PAGER_PARAMS_NAME" => "arrPager",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N"
            )
        );?>
    </div>
<?endif;



if ($meta_title) {
    $APPLICATION->SetPageProperty('title', $meta_title);
}

if ($meta_desc) {
    $APPLICATION->SetPageProperty("description", $meta_desc);
}

if ($meta_key) {
    $APPLICATION->SetPageProperty('keywords', $meta_key);
}
?>