<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (count($arResult["SIZES"])>0 && !isset($_GET["q"]) && !isset($_GET["size"]))
{
    ?>
    <div class="group-prod">
        <div class="section texture">
            <div class="section-header size_item_header">Доступные размеры:</div>
            <div class="row">
                <?
                foreach ($arResult["SIZES"] as $size)
                {
                    ?><div class="col-vtop size_item">
                        <a href="<?=$size["URL"]?>"><?=$size["NAME"]?></a>
                    </div><?
                }
                ?>
            </div>
        </div> 
    </div>
    <?
}
if (count($arResult["VID_TOVARA"])>0 && !isset($_GET["q"]) && !isset($_GET["vid"]))
{
    foreach ($arResult["VID_TOVARA"] as $groupVid)
    {
        if (count($groupVid["ITEMS"])>0)
        {
            ?>
            <div class="group-prod">
                <div class="section-header size_item_header"><?=$groupVid["NAME"]?>:</div>
                <div class="section texture">
                    <div class="row">
                        <?
                        foreach ($groupVid["ITEMS"] as $vid)
                        {
                            ?><div class="col-vtop col-xs-2 section__item">
                                <div class="img-wrap">
                                    <a href="<?=$vid["URL"]?>"><img src="<?=$vid["IMG"]?>" alt="<?=$vid["NAME"]?>"></a>
                                </div>
                                <div class="title"><a href="<?=$vid["URL"]?>"><?=$vid["NAME"]?></a></div>
                            </div><?
                        }
                        ?>
                    </div>
                </div> 
            </div>
            <?
        }
    }
}
$classGroups="";
if (count($arResult["COLLECTIONS"])>0 && !isset($_GET["q"]) && !isset($_GET["group_collection"]))
{
    $classGroups=" isCollectionGroup";
    ?>
    <div class="group-prod">
        <div class="section texture">
            <ul class="row catalog-grid default_catalog_top">
                <?
                foreach ($arResult["COLLECTIONS"] as $group)
                {
                    ?><li class="catalog-grid__item col-xs-4 col-vtop">
                        <p class="catalog-grid__item-title"><a href="<?=$group["URL"]?>"><?=$group["NAME"]?></a></p>
                        <div class="catalog-grid__item-wrap">
                            <span class="catalog-grid__item-tcell">
                                <a href="<?=$group["URL"]?>">
                                    <img src="<?=$group["IMG"]?>" alt="">
                                </a>
                            <span>
                        </span></span></div>
                    </li><?
                }
                ?>
            </ul>
        </div> 
    </div>
    <?
}
?>
<div class="group-prod">
    <?
    foreach ($arResult["ITEMS"] as $arItem)
    {
        $cntColors=count($arItem["PROPERTIES"]["FOTOS"]["VALUE"]);
        $moreColors=$cntColors-7;
        ?>
        <div class="section collections<?=$classGroups?>" data-group="<?=$arItem["PROPERTIES"]["GROUP"]["VALUE"]?>">
            <div class="section-header<?if (count($arItem["SIZES"])>0){?> section-header-with-sizes<?}?>">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <?=$arItem["NAME"]?>
                </a>
            </div> 
            <?
            if (count($arItem["SIZES"])>0)
            {
                ?>
                <div class="sizes_list">Доступные размеры: <?=join(", ",$arItem["SIZES"])?></div>
                <?
            }
            ?>
            <div class="row">      
                <div class="section-img col-xs-4">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                        <?
                        if ($arItem["DETAIL_PICTURE"])
                        {
                            $img = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width'=>263, 'height'=>262), BX_RESIZE_IMAGE_EXACT, true);
                            $img=$img["src"];
                        } else
                        {
                            $img=SITE_TEMPLATE_PATH."/img/no-photo.png";
                        }
                        ?>
                        <img src="<?=$img?>" alt="<?=$arItem["NAME"]?>">
                    </a>
                </div>
                <ul class="section-tile col-xs-8">
                    <?
                    $i=0;
                    foreach ($arItem["PROPERTIES"]["FOTOS"]["VALUE"] as $key=>$color)
                    {
                        $i++;
                        $img = CFile::ResizeImageGet($color, array('width'=>77, 'height'=>77), BX_RESIZE_IMAGE_EXACT, true);
                        $img=$img["src"];
                        $articul=$arItem["PROPERTIES"]["FOTOS"]["DESCRIPTION"][$key];
                        if (stristr($articul,'#'))
                        {
                            $articul=explode('#',$articul);
                            $title=$articul[0];
                            $articul=$articul[0];
                        } else
                        {
                            $title=$articul;
                        }
                        ?><li class="col-vtop col-xs-2 img-wrap" title="<?=$title?>">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                <img src="<?=$img?>" alt="<?=$arItem["NAME"]?>">
                            </a>
                            <?
                            if ($articul!="")
                            {
                                ?><span class="articul"><?=$articul?></span><?
                            }
                            ?>
                        </li><?
                        if ($i==7) break;
                    }
                    if ($moreColors>0)
                    {
                        ?><li class="col-vmid col-xs-6 img-wrap wrap-btn-more">
                            <div class="wrap-btn-more__inner">
                                <a class="btn-more" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Смотреть еще <?=$moreColors?> <?=sklonenie($moreColors,array('цвет','цвета','цветов'))?></a>
                            </div>
                        </li><?
                    }
                    ?>
                </ul>    
            </div>
        </div> 
        <?
    }
    ?>
</div>
<?
if (isset($_GET["q"]) && count($arResult["ITEMS"])==0)
{
    ?><p>Сожалеем, но ничего не найдено.</p><?
}
?>