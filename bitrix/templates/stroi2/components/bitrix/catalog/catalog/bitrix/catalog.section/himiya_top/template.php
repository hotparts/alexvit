<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="producer">
    <?

	if (!empty($arResult["BRANDS"]))
    {
        ?>
        <div class="producer__select-prod">
            <?
            foreach ($arResult["BRANDS"] as $brand)
            {
                ?><div class="col-xs-2 col-vmid producer__prod">
                    <a href="<?=$brand["URL"]?>"><img src="<?=$brand["IMG"]?>" alt="<?=$brand["NAME"]?>"></a>
                </div><?
            }
            ?>        
        </div>
        <?
    }
    $detailText=trim($arResult["~DESCRIPTION"]);
    if ($arResult["PICTURE"]!="" || $detailText!="")
    {
        ?>
        <div class="producer__descr row">
            <?
            if ($detailText!="" && !$arResult["PICTURE"])
            {
                ?>
                <div class="section-img col-xs-4">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png" alt="">
                </div>
                <?
            } else
            if ($arResult["PICTURE"]!="")
            {
                $img = CFile::ResizeImageGet($arResult["PICTURE"]["ID"], array('width'=>262, 'height'=>900), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                ?>
                <div class="section-img col-xs-4">
                    <img src="<?=$img["src"]?>" alt="<?=$arResult["NAME"]?>">
                </div>
                <?
            }
            $detailText=trim($arResult["~DESCRIPTION"]);
            if ($detailText!="")
            {
            	?>
            	<div class="section-descr col-xs-8">
            	<?
            	if ($arResult["DESCRIPTION_TYPE"]=="html")
            	{
             		echo $detailText;
            	} else
             	{
            		?><p><?=str_replace("\n","<br />",$detailText)?></p><?
              	}
            	?>
            	</div>
            <?
            }
            ?>
        </div>
        <?    
    }

    ?>
</div>