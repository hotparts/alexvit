<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (count($arResult["ITEMS"])>0)
{
    ?>
    <ul class="row catalog-grid">
        <?
        foreach ($arResult["ITEMS"] as $item)
        {
            if ($item["DETAIL_PICTURE"])
            {
                $img = CFile::ResizeImageGet($item["DETAIL_PICTURE"]["ID"], array('width'=>261, 'height'=>275), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                $img=$img["src"];                
            } else
            {
                $img=SITE_TEMPLATE_PATH.'/img/no-photo.png';   
            }
            ?><li class="catalog-grid__item col-xs-4">
                <p class="catalog-grid__item-title"><a href="<?=$item["DETAIL_PAGE_URL"]?>"><?=$item["NAME"]?></a></p>
                <div class="catalog-grid__item-wrap">
                    <span class="catalog-grid__item-tcell">
                        <a href="<?=$item["DETAIL_PAGE_URL"]?>">
                            <img src="<?=$img?>" alt="">
                        </a>
                    <span>
                </span></span></div>
            </li><?
        }
        ?>
    </ul>
    <?
} else
{
    
}
?>