<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach ($arResult["ITEMS"] as $key=>&$arItem)
{
    $curSectionID=$arItem["~IBLOCK_SECTION_ID"];
    $nav = CIBlockSection::GetNavChain(false, $curSectionID);
    $nav_first_section = $nav->GetNext();
    $nav_second_section = $nav->GetNext();
    if ($nav_first_section["ID"]==4)
    {
        $arItem["DETAIL_PAGE_URL"]=$nav_second_section["SECTION_PAGE_URL"];
    } else
    if ($nav_second_section["ID"]==33 || $nav_second_section["ID"]==57)
    {
        $nav_third_section = $nav->GetNext();
        $arItem["DETAIL_PAGE_URL"]=$nav_third_section["SECTION_PAGE_URL"];
    }  
}
?>