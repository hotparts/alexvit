<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arResult["SECTIONS"]=array();
foreach ($arResult["ITEMS"] as $item)
{
    $curSectionID=$item["~IBLOCK_SECTION_ID"];
    
    if ($curSectionID>0 && !isset($arResult["SECTIONS"][$curSectionID]))
    {
        $res = CIBlockSection::GetByID($curSectionID);
        if($ar_res = $res->GetNext())
        {
            if ($ar_res["DEPTH_LEVEL"]>2)
            {
                $nav = CIBlockSection::GetNavChain(false, $ar_res["ID"]);
                $nav_first_section = $nav->GetNext();
                $ar_res = $nav->GetNext();
                $curSectionID=$ar_res["ID"];
           }
                if ($ar_res["PICTURE"])
                {
                    $img = CFile::ResizeImageGet($ar_res["PICTURE"], array('width'=>261, 'height'=>275), BX_RESIZE_IMAGE_EXACT, true);
                    $img=$img["src"];                
                } else
                {
                    $img=SITE_TEMPLATE_PATH.'/img/no-photo.png';   
                }
                
                $arResult["SECTIONS"][$curSectionID]=array(
                    "NAME" => $ar_res['NAME'],
                    "SORT" => $ar_res['SORT'],
                    "IMG" => $img,
                    "URL" => "/".$arResult["CODE"]."/".$ar_res["CODE"]."/",
                );
            
        }
    }
    if (isset($arResult["SECTIONS"][$curSectionID]))
    {
        if (!isset($arResult["SECTIONS"][$curSectionID]["BRANDS"]))
        {
            $arResult["SECTIONS"][$curSectionID]["BRANDS"]=array();
        }
        if ($curSectionID==33 || $curSectionID==57)
        {
            $arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'ACTIVE'=>'Y', 'SECTION_ID' => $curSectionID);
            $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
            while($ar_result = $db_list->GetNext())
            {
                $arResult["SECTIONS"][$curSectionID]["BRANDS"][$ar_result['NAME']]='<a href="'.$ar_result['SECTION_PAGE_URL'].'">'.$ar_result['NAME'].'</a>';
            }
        } else
        {
            $curBrand=strip_tags($item["DISPLAY_PROPERTIES"]["BRAND"]["DISPLAY_VALUE"]);
            if ($curBrand!="" && !isset($arResult["SECTIONS"][$curSectionID]["BRANDS"][$curBrand]))
            {
                $curBrandCode=explode('href="',$item["DISPLAY_PROPERTIES"]["BRAND"]["DISPLAY_VALUE"]);
                $curBrandCode=$curBrandCode[1];
                $curBrandCode=explode('">',$curBrandCode);
                $curBrandCode=substr($curBrandCode[0],1);
                $arResult["SECTIONS"][$curSectionID]["BRANDS"][$curBrand]='<a href="'.$arResult["SECTIONS"][$curSectionID]["URL"]."".$curBrandCode.'">'.$curBrand.'</a>';
            }
        }
    }
}
uasort($arResult["SECTIONS"],'sortSectionsBySort');
?>