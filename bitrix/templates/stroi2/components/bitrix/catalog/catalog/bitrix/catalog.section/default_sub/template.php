<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$showFullDescr=false;?>
<div class="group-prod">
	<?
    //bss 2018-01-29 ++
    if (!empty($arResult["BRANDS"]))
    {
        ?>
        <div class="section producer">
            <div class="section-header">Производители</div>
            <div class="main-logos">
                <div class="islider logo-slider<?if (count($arResult["BRANDS"])>5){?> with_padding<?}?>">
                    <?
                    foreach ($arResult["BRANDS"] as $brand)
                    {
                        ?>
                        <div class="logo-item">
                        <a href="<?=$brand["URL"]?>">
                            <img src="<?=$brand["IMG"]?>" alt="<?=$brand["NAME"]?>">
                        </a>
                        </div><?
                    }
                    ?>
                </div>
            </div>
        </div>
        <?
    }
	//bss 2018-01-29 --
	?>

    <?
    $detailText=trim($arResult["~DESCRIPTION"]);
    if ($arResult["PICTURE"]!="" || $detailText!="")
    {
        ?>
        <div class="section detailed-descr row">
            <?
            if ($detailText!="" && !$arResult["PICTURE"])
            {
                ?>
                <div class="section-img col-xs-6">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png" alt="">
                </div>
                <?
            } else
            if ($arResult["PICTURE"]!="")
            {
                $img = CFile::ResizeImageGet($arResult["PICTURE"]["ID"], array('width'=>408, 'height'=>900), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                ?>
                <div class="section-img col-xs-6">
                    <img src="<?=$img["src"]?>" alt="<?=$arResult["NAME"]?>">
                </div>
                <?
            }
            if ($detailText!="")
            {
            	?>
            	<div class="section-descr col-xs-6">
                    <?
                    $max_length_anons=400;
                    $anons=strip_tags($detailText);
                    if (strlen($anons)>$max_length_anons)
                    {
                        $showFullDescr=true;
                        $pos_probel=strpos($anons," ",$max_length_anons);
                        if ($pos_probel>0)
                        {
                            $anons=substr($anons,0,$pos_probel);
                        } else
                        {
                            $anons=substr($anons,0,$max_length_anons);
                        }
                        $anons.='...<a href="javascript:void(0)" class="read-more read-more-section">читать далее</a>';
                    }
                    ?>
					<noindex><p><?=str_replace("\n","<br />",$anons)?></p></noindex>
            	</div>
                <?
            }
            ?>
        </div>
        <?    
    }
	//bss 2018-02-09
	/*if (!empty($arResult["BRANDS"]))
    {
        ?>
        <div class="section producer">
            <div class="section-header">Производители</div>
            <div class="main-logos">
                <div class="islider logo-slider<?if (count($arResult["BRANDS"])>5){?> with_padding<?}?>">
                    <?
                    foreach ($arResult["BRANDS"] as $brand)
                    {
                        ?>
                        <div class="logo-item">
                            <a href="<?=$brand["URL"]?>">
                                <img src="<?=$brand["IMG"]?>" alt="<?=$brand["NAME"]?>">
                            </a>
                        </div><?
                    }
                    ?>
                </div>
            </div>    
        </div>
        <?
	}*/

    foreach ($arResult["VID_TOVARA"] as $groupVid)
    {
        if (count($groupVid["ITEMS"])>0)
        {
            ?>
            <div class="section texture">
                <div class="section-header"><?=$groupVid["NAME"]?></div>
                <div class="row">
					<div class="well2">
                    <?
                    foreach ($groupVid["ITEMS"] as $vid)
                    {
                        ?><div class="col-vtop col-xs-2 section__item">
                            <div class="img-wrap">
                                <a href="<?=$vid["URL"]?>"><img src="<?=$vid["IMG"]?>" alt="<?=$vid["NAME"]?>"><span>Выбрать</span></a>
                            </div>
                            <div class="title"><a href="<?=$vid["URL"]?>"><?=$vid["NAME"]?></a></div>
                        </div><?
                    }
                    ?>
                    </div>
                </div>
            </div> 
            <?
        }
    }
    
    if ($detailText!="" && $showFullDescr)
    {
    	?>
    	<div class="catalog-descript">
    	<?
    	if ($arResult["DESCRIPTION_TYPE"]=="html")
    	{
     		echo $detailText;
    	} else
     	{
    		?><p><?=str_replace("\n","<br />",$detailText)?></p><?
      	}
    	?>
    	</div>
        <?
    }
    ?>
</div>