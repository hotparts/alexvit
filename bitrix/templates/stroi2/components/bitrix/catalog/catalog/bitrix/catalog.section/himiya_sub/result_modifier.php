<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arResult["itemsBySection"]=array();
foreach ($arResult["ITEMS"] as $item)
{
    $curSection=$item["~IBLOCK_SECTION_ID"];
    
    $item_sections_res = CIBlockElement::GetElementGroups($item["ID"], true);
    $item_sections=array();
    while($ar_group = $item_sections_res->Fetch())
    {
        $item_sections[] = $ar_group["ID"];
    }
    foreach ($item_sections as $curSection)
    {
        if ($curSection>0)
        {
            $res = CIBlockSection::GetByID($curSection);
            if($ar_res = $res->GetNext())
            {
                if ($ar_res["DEPTH_LEVEL"]==4)
                {
                    $res2 = CIBlockSection::GetByID($ar_res["IBLOCK_SECTION_ID"]);
                    if($ar_res2 = $res2->GetNext())
                    {
                        $arResult["itemsBySection"][$ar_res2['NAME']]["SORT"]=$ar_res2["SORT"];
                        $arResult["itemsBySection"][$ar_res2['NAME']]["ITEMS2"][$ar_res['NAME']]["SORT"]=$ar_res["SORT"];
                        $arResult["itemsBySection"][$ar_res2['NAME']]["ITEMS2"][$ar_res['NAME']]["ITEMS"][$item["ID"]]=array(
                            "NAME" => $item["NAME"],
                            "ID"  => $item["ID"]
                        );
                    }
                } else
                {
                    $arResult["itemsBySection"][$ar_res['NAME']]["SORT"]=$ar_res["SORT"];
                    $arResult["itemsBySection"][$ar_res['NAME']]["ITEMS"][$item["ID"]]=array(
                        "NAME" => $item["NAME"],
                        "ID"  => $item["ID"],
                        "LINK"  => $item["DETAIL_PAGE_URL"]
                    );
                    $arResult["itemsBySection"][$ar_res['NAME']]["LINK"] = $ar_res['SECTION_PAGE_URL'];
                    $arResult["itemsBySection"][$ar_res['NAME']]["PICTURE"] = $ar_res["PICTURE"];
                }
            }
        }
    }
}

$nav = CIBlockSection::GetNavChain(false, $curSection);
$nav_first_section = $nav->GetNext();
$nav_second_section = $nav->GetNext();

if ($nav_second_section["ID"]==33 || $nav_second_section["ID"]==57)
{
    $arResult["SKIP_FIRST_SECTION"]="Y";
} else
{
    $arResult["SKIP_FIRST_SECTION"]="N";
}

uasort($arResult["itemsBySection"], 'sortSectionsBySort');
foreach ($arResult["itemsBySection"] as $sectionName=>&$sectionItems)
{
    if (isset($sectionItems["ITEMS2"]))
    {
        uasort($sectionItems["ITEMS2"], 'sortSectionsBySort');
    }
}
?>