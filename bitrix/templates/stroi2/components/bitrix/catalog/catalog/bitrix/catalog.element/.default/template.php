<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$showFullDescr=false;?>
<div class="right-catalog product-card">
    <?
    if (count($arResult["SIZES"])>0)
    {
        ?>
        <div class="sizes_list">Доступные размеры: <?=join(", ",$arResult["SIZES"])?></div>
        <?
    }
    ?>
    <div class="section detailed-descr">
        <div class="row">      
            <div class="section-img col-xs-7">
                <?
                if ($arResult["DETAIL_PICTURE"])
                {
                    $img = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"]["ID"], array('width'=>653, 'height'=>9999), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                    $img=$img["src"];
                    if (empty($arResult["PROPERTIES"]["SLAIDER_FOTOS"]["VALUE"])){
					?><img src="<?=$img?>" alt="<?=$arItem["NAME"]?>" /><?	/* height="490" */
					}
                } else
                {
                    $img=SITE_TEMPLATE_PATH."/img/no-photo.png";
                    ?><img src="<?=$img?>" alt="<?=$arItem["NAME"]?>"><?
                }
				
				if (!empty($arResult["PROPERTIES"]["SLAIDER_FOTOS"]["VALUE"]))
				{
					$i=0;
					?>
						<div class="islider my-slider product-slider" id="index_slider">
						<? if ($arResult["DETAIL_PICTURE"])
						{
							?><div><img src="<?=$img?>" alt="<?=$arItem["NAME"]?>" /></div><?	/* height="490" */
						}

							foreach ($arResult["PROPERTIES"]["SLAIDER_FOTOS"]["VALUE"] as $key=>$color)
							{
								$i++;
								$img = CFile::ResizeImageGet($color, array('width'=>653, 'height'=>9999), BX_RESIZE_IMAGE_PROPORTIONAL, true);
								$img=$img["src"];
								?>
								<div>
									<img src="<?=$img?>" alt="" />	<?/*  height="490" */?>
								</div>
								<?  
							}
							?>
						</div>
				<?
				}
				
                ?>                
            </div>
            <div class="section-descr col-xs-5">
                <?
                $detailText=trim($arResult["~DETAIL_TEXT"]);
                if ($detailText!="")
                {
                    $max_length_anons=400;
                    $anons=strip_tags($detailText);
                    if (strlen($anons)>$max_length_anons)
                    {
                        $showFullDescr=true;
                        $pos_probel=strpos($anons," ",$max_length_anons);
                        if ($pos_probel>0)
                        {
                            $anons=substr($anons,0,$pos_probel);
                        } else
                        {
                            $anons=substr($anons,0,$max_length_anons);
                        }
                        $anons.='...<a href="javascript:void(0)" class="read-more read-more-section">читать далее</a>';
                    }
                    ?>
				<noindex><p><?=str_replace("\n","<br />",$anons)?></p></noindex>
                    <?
                }
                
                if ($arResult["PROPERTIES"]["BTN_AFTER_TEXT"]["VALUE"]!="" && $arResult["PROPERTIES"]["BTN_AFTER_TEXT"]["DESCRIPTION"]!="")
                {
                    ?>
                    <div class="btn_after_descr">
                        <a class="btn-link" href="<?=$arResult["PROPERTIES"]["BTN_AFTER_TEXT"]["VALUE"]?>"><?=$arResult["PROPERTIES"]["BTN_AFTER_TEXT"]["DESCRIPTION"]?></a>
                    </div>
                    <?    
                }
                
                if ($arResult["PROPERTIES"]["PRICE"]["VALUE"]!="")
                {
                    ?>
                    <div class="price-block">
                        <div class="price-block__title-price">Цена за м<sup>2</sup>:</div>
                        <div class="price-block__number-price">
                            <div class="price-block__number-price-actual"><?=number_format($arResult["PROPERTIES"]["PRICE"]["VALUE"], 0, ',', ' ' );?> <span class="detail-price__currency">₽</span></div>
                        </div>
                    </div>
                    <?
                }
                
                $needShowPriceTable=false;
                $isPriceTableTH[1]=false;
                $isPriceTableTH[2]=false;
                $isPriceTableTH[3]=false;
                $isPriceTableTH[4]=false;
                $isPriceTableTH[5]=false;
                foreach ($arResult["PROPERTIES"]["KOL_TABLE_PRICE"]["VALUE"] as $row)
                {
                    $i=0;
                    foreach ($row as $col)
                    {
                        $i++;
                        $curVal=trim($col);
                        if ($curVal!="")
                        {
                            $needShowPriceTable=true;
                            $isPriceTableTH[$i]=true;
                        }
                    }
                }

                if ($needShowPriceTable)
                {
                    ?>
                    <table class="table-char tc_small">
                        <thead>
                            <tr>
                                <?
                                if($isPriceTableTH[1])
                                {
                                    ?><th>Фасовка</th><?    
                                }
                                if($isPriceTableTH[2])
                                {
                                    ?><th>Ед.&nbsp;изм.</th><?    
                                }
                                if($isPriceTableTH[3])
                                {
                                    ?><th>Розничная</th><?    
                                }
                                if($isPriceTableTH[4])
                                {
                                    ?><th>Оптовая</th><?    
                                }
                                if($isPriceTableTH[5])
                                {
                                    ?><th>Специальная</th><?    
                                }
                                ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?
                            foreach ($arResult["PROPERTIES"]["KOL_TABLE_PRICE"]["VALUE"] as $row)
                            {
                                ?><tr><?
                                if($isPriceTableTH[1])
                                {
                                    $curVal=trim($row["col1"]);
                                    if (is_numeric($curVal))
                                    {
                                        ?><td style="white-space: nowrap;"><?=number_format($curVal, 0, ',', ' ' );?> <span class="detail-price__currency">₽</span></td><?
                                    } else
                                    {
                                        ?><td style="white-space: nowrap;"><?=$curVal?></td><?
                                    }    
                                }
                                if($isPriceTableTH[2])
                                {
                                    $curVal=trim($row["col2"]);
                                    if (is_numeric($curVal))
                                    {
                                        ?><td style="white-space: nowrap;"><?=number_format($curVal, 0, ',', ' ' );?> <span class="detail-price__currency">₽</span></td><?
                                    } else
                                    {
                                        ?><td style="white-space: nowrap;"><?=$curVal?></td><?
                                    }    
                                }
                                if($isPriceTableTH[3])
                                {
                                    $curVal=trim($row["col3"]);
                                    if (is_numeric($curVal))
                                    {
                                        ?><td style="white-space: nowrap;"><?=number_format($curVal, 0, ',', ' ' );?> <span class="detail-price__currency">₽</span></td><?
                                    } else
                                    {
                                        ?><td style="white-space: nowrap;"><?=$curVal?></td><?
                                    }    
                                }
                                if($isPriceTableTH[4])
                                {
                                    $curVal=trim($row["col4"]);
                                    if (is_numeric($curVal))
                                    {
                                        ?><td style="white-space: nowrap;"><?=number_format($curVal, 0, ',', ' ' );?> <span class="detail-price__currency">₽</span></td><?
                                    } else
                                    {
                                        ?><td style="white-space: nowrap;"><?=$curVal?></td><?
                                    }    
                                }
                                if($isPriceTableTH[5])
                                {
                                    $curVal=trim($row["col5"]);
                                    if (is_numeric($curVal))
                                    {
                                        ?><td style="white-space: nowrap;"><?=number_format($curVal, 0, ',', ' ' );?> <span class="detail-price__currency">₽</span></td><?
                                    } else
                                    {
                                        ?><td style="white-space: nowrap;"><?=$curVal?></td><?
                                    }    
                                }
                                ?></tr><?
                            }
                            ?>                                
                        </tbody>
                    </table>
                    <br /><br />
                    <?
                }
                
                ?>
            </div>    
        </div>
    </div>
    <?
    $cntColors=count($arResult["PROPERTIES"]["FOTOS"]["VALUE"]);
    $moreColors=$cntColors-32;
    if (!empty($arResult["PROPERTIES"]["FOTOS"]["VALUE"]))
    {
        $i=0;
        ?>
        <div class="section collections">
            <div class="section-header">Цвета</div> 
            <div class="section-tile">
                <?
                foreach ($arResult["PROPERTIES"]["FOTOS"]["VALUE"] as $key=>$color)
                {
                    $i++;
                     
                    $img = CFile::ResizeImageGet($color, array('width'=>83, 'height'=>83), BX_RESIZE_IMAGE_EXACT, true);
                    $imgBig = CFile::ResizeImageGet($color, array('width'=>940, 'height'=>9999), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                    $img=$img["src"];
                    $imgBigWidth=$imgBig["width"];
                    $imgBig=$imgBig["src"];
                    
                    $articul=$arResult["PROPERTIES"]["FOTOS"]["DESCRIPTION"][$key];
                    if (stristr($articul,'#'))
                    {
                        $articul=explode('#',$articul);
                        $title=$articul[0];
                        $articul=$articul[0];
                    } else
                    {
                        $title=$articul;
                    }
                    
                    ?><div class="col-vtop col-xs-1 img-wrap ajax" <?if ($i>33){?>style="display:none"<?}?>>
                        <a href="javascript:void(0)">
                            <img src="<?=$img?>" title="<?=$title?>" data-src="<?=$imgBig?>" data-width="<?=$imgBigWidth?>" alt="">
                        </a>
                        <?
                        if ($articul!="")
                        {
                            ?><span class="articul"><?=$articul?></span><?
                        }
                        ?>
                     </div><?
                    
                }
                if ($moreColors>0)
                {
                    ?><div class="col-vmid col-xs-3 img-wrap wrap-btn-more">
                        <div class="wrap-btn-more__inner">
                            <a class="btn-more btn_more_colors_detail" href="javascript:void(0)">Смотреть еще 9 цветов</a>
                        </div>
                    </div><?
                }
                ?>
            </div>
        </div>
        <?
    }
    
    
    
    if (!empty($arResult["PROPERTIES"]["TABLE1"]["VALUE"]))
    {
        ?><div class="section collections"><?
        if ($arResult["PROPERTIES"]["TABLE1_TITLE"]["VALUE"]!="")
        {
            ?><div class="section-header"><?=$arResult["PROPERTIES"]["TABLE1_TITLE"]["VALUE"]?></div> <?
        }
        ?>
        <table class="table-char"><tbody>
        <?
        foreach ($arResult["PROPERTIES"]["TABLE1"]["VALUE"] as $key=>$row)
        {
            ?><tr>
                <?
                foreach ($row as $td)
                {
                    if ($key==0)
                    {
                        $col='th';
                    } else
                    {
                        $col='td';
                    }
                    if (trim($td)!="")
                    {
                        ?><<?=$col?>><?=trim($td)?></<?=$col?>><?    
                    }                    
                }
                ?>
            </tr><?
        }
        ?></tbody></table></div><?
    }
    if (!empty($arResult["PROPERTIES"]["TABLE2"]["VALUE"]))
    {
        ?><div class="section collections"><?
        if ($arResult["PROPERTIES"]["TABLE2_TITLE"]["VALUE"]!="")
        {
            ?><div class="section-header"><?=$arResult["PROPERTIES"]["TABLE2_TITLE"]["VALUE"]?></div> <?
        }
        ?>
        <table class="table-char"><tbody>
        <?
        foreach ($arResult["PROPERTIES"]["TABLE2"]["VALUE"] as $key=>$row)
        {
            ?><tr>
                <?
                foreach ($row as $td)
                {
                    if ($key==0)
                    {
                        $col='th';
                    } else
                    {
                        $col='td';
                    }
                    if (trim($td)!="")
                    {
                        ?><<?=$col?>><?=trim($td)?></<?=$col?>><?    
                    }                    
                }
                ?>
            </tr><?
        }
        ?></tbody></table></div><?
    }
    if (!empty($arResult["PROPERTIES"]["TABLE3"]["VALUE"]))
    {
        ?><div class="section collections"><?
        if ($arResult["PROPERTIES"]["TABLE3_TITLE"]["VALUE"]!="")
        {
            ?><div class="section-header"><?=$arResult["PROPERTIES"]["TABLE3_TITLE"]["VALUE"]?></div> <?
        }
        ?>
        <table class="table-char"><tbody>
        <?
        foreach ($arResult["PROPERTIES"]["TABLE3"]["VALUE"] as $key=>$row)
        {
            ?><tr>
                <?
                foreach ($row as $td)
                {
                    if ($key==0)
                    {
                        $col='th';
                    } else
                    {
                        $col='td';
                    }
                    if (trim($td)!="")
                    {
                        ?><<?=$col?>><?=trim($td)?></<?=$col?>><?    
                    }                    
                }
                ?>
            </tr><?
        }
        ?></tbody></table></div><?
    }
    if (!empty($arResult["PROPERTIES"]["TABLE4"]["VALUE"]))
    {
        ?><div class="section collections"><?
        if ($arResult["PROPERTIES"]["TABLE4_TITLE"]["VALUE"]!="")
        {
            ?><div class="section-header"><?=$arResult["PROPERTIES"]["TABLE4_TITLE"]["VALUE"]?></div> <?
        }
        ?>
        <table class="table-char"><tbody>
        <?
        foreach ($arResult["PROPERTIES"]["TABLE4"]["VALUE"] as $key=>$row)
        {
            ?><tr>
                <?
                foreach ($row as $td)
                {
                    if ($key==0)
                    {
                        $col='th';
                    } else
                    {
                        $col='td';
                    }
                    if (trim($td)!="")
                    {
                        ?><<?=$col?>><?=trim($td)?></<?=$col?>><?    
                    }                    
                }
                ?>
            </tr><?
        }
        ?></tbody></table></div><?
    }
    
    if ($arResult["BRAND_CODE"]!="" || $arResult["CATALOG_FILE"]!="")
    {
        ?>
        <div class="section collections btns_sert_catalog">
			<?/*<a href="#" class="customFormLink product-item__btn-order"
				data-params="YNYYN-NNYNN"
				data-form-code="himiya_zakaz"
				data-title="Заказать <?=$arResult["NAME"]?>"
				data-text=""
				data-btn-text="Отправить"
				data-service-form-name=""
				data-field-title-phone=""
				data-field-title-name=""
				data-field-placeholder-name="Представьтесь пожалуйста"
				data-field-placeholder-phone="Ваш контактный телефон"
				data-custom-textarea-title=""
				data-custom-textarea-placeholder="Комментарии"
				>Заказать</a>*/?>
            <?
            if ($arResult["BRAND_CODE"]!="")
            {
                ?>
                <a class="btn-link" href="/sertifikaty/?brand=<?=$arResult["BRAND_CODE"]?>">Сертификаты</a>
                <?
            }
            /*if ($arResult["CATALOG_FILE"]!="")
            {
                ?>
                <a class="btn-link" href="<?=$arResult["CATALOG_FILE"]?>" target="_blank">Каталог</a>
                <?
            }*/
            ?>
            <a class="btn-link" href="/informatsiya-dlya-skachivaniya/?brand=<?=$arResult["BRAND_CODE"]?>">Информация для скачивания</a>
        </div>    
        <?
    }
	?>
    
	<?
	$res = CIBlockElement::GetProperty($arResult['IBLOCK_ID'], $arResult['ID'], array("sort" => "asc"), array("CODE" => "HIM_TABLE_PRICE"));
	$i = 0;
	?><table class="table-char"><?
	while ($arFields = $res->GetNext()){
		?>
			<?if ($i == 0) { ?>
			<thead>
				<tr>
				<?
				foreach ($arFields["USER_TYPE_SETTINGS"]['COLUMNS'] as $row){
					echo '<th>'.$row['LANG']['ru']['NAME'].'</th>';
				}
				?>
				</tr>
			</thead>
			<tbody>
			<? } ?>
			
				<tr>
				<?
				foreach ($arFields["VALUE"] as $val){
					$curVal = trim($val);
					
					if (is_numeric($curVal)){
						?><td style="white-space: nowrap;"><?=number_format($curVal, 0, ',', ' ' );?> <span class="detail-price__currency">₽</span></td><?
					} else {
						?><td style="white-space: nowrap;"><?=$curVal?></td><?
					}
				}
				?>
				</tr>
		<?
		$i++;	
	}
	?>
		</tbody>
	</table>

	<div class="product-item__descr content">
		<?	
		$res = CIBlockElement::GetProperty($arResult['IBLOCK_ID'], $arResult['ID'], array("sort" => "asc"), array("CODE" => "HIMIYA_PROPS"));
// 		if (count($arFields["PROPERTY_HIMIYA_PROPS_VALUE"])>0)
		{
			?>
			<div class="product-item__table-title">Характеристики</div>
			<table  class="product-item__table"><tbody>
				<?
// 				foreach ($arFields["PROPERTY_HIMIYA_PROPS_VALUE"] as $key=>$val)
				while ($arFields = $res->GetNext())
				{
					?>
					<tr>
						<td><?=$arFields['VALUE']?></td>
						<td><?=$arFields['DESCRIPTION']?></td>
					</tr>
					<?
				}
				?>
			</tbody></table>
			<?
		}
		?>
	</div>
    
    <?
    if ($detailText!="" && $showFullDescr)
    {
    	?>
    	<div class="catalog-descript">
    	<?
    	if ($arResult["DESCRIPTION_TYPE"]=="html")
    	{
     		echo $detailText;
    	} else
     	{
    		?><p><?=str_replace("\n","<br />",$detailText)?></p><?
      	}
    	?>
    	</div>
        <?
    }
    ?>
</div>