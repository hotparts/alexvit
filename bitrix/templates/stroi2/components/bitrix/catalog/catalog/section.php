<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
$this->setFrameMode(true);

$vidName="";
$arrUrlPath=explode("/",substr($APPLICATION->GetCurDir(),1,strlen($APPLICATION->GetCurDir())-2));
$filterCandidate=end($arrUrlPath);
$filterCandidateFirst=$arrUrlPath[0];

$isFilterCandidateFirst=false;
$isCollectionTpl=false;
$isTypePlaceTpl=false;
$isBrandTpl=false;

$isLaki=false;

$ADD_SECTIONS_CHAIN="Y";

if (strpos($filterCandidateFirst,"type-")===0)
{
    $filterCandidateFirst=explode("type-",$filterCandidateFirst);
    $filterCandidateFirst=$filterCandidateFirst[1];
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
    $arFilter = Array("IBLOCK_ID"=>TYPE_PLACE_IBLOCK_ID, "CODE"=>$filterCandidateFirst, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $curBrand=$arFields;
        $_GET["brand"]="type_".$arFields["ID"];
        
        $isTypePlaceTpl=true;
        
        if (count($arrUrlPath)==1)
        {
            $GLOBALS['sectionFilter'] = array("PROPERTY_TYPE_PLACE"=>$arFields["ID"]);
        } else
        {
            if (strpos($filterCandidate,"brand-")===0)
            {
                $filterCandidate=explode("brand-",$filterCandidate);
                $filterCandidate=$filterCandidate[1];
                $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
                $arFilter = Array("IBLOCK_ID"=>BRAND_IBLOCK_ID, "CODE"=>$filterCandidate, "ACTIVE"=>"Y");
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if($ob = $res->GetNextElement())
                {
                    $arFieldsBrand = $ob->GetFields();
                    $curBrand2=$arFieldsBrand;
                    $curSectCode=$arrUrlPath[count($arrUrlPath)-2];
                } else
                {
                    CHTTP::SetStatus("404 Not Found");
                    @define("ERROR_404","Y"); 
                }
            } else
            {
                $curSectCode=$arrUrlPath[count($arrUrlPath)-1];
            }
            
            $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'ACTIVE'=>'Y', 'CODE'=>$curSectCode);
            $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
            $isFilterCandidateFirst=true;
            if($ar_result = $db_list->GetNext())
            {
                $isCollectionTpl=true;
                $curSection=$ar_result;
                $arResult["VARIABLES"]["SECTION_ID"]=$ar_result['ID'];
                $GLOBALS['sectionFilter'] = array("PROPERTY_TYPE_PLACE"=>$arFields["ID"],"PROPERTY_BRAND"=>$arFieldsBrand["ID"]);
                $ADD_SECTIONS_CHAIN="N";
            } else
            {
                CHTTP::SetStatus("404 Not Found");
                @define("ERROR_404","Y");
            }
        }
    } else
    {
        CHTTP::SetStatus("404 Not Found");
        @define("ERROR_404","Y"); 
    }
} else
if (strpos($filterCandidate,"brand-")===0)
{
    $filterCandidate=explode("brand-",$filterCandidate);
    $filterCandidate=$filterCandidate[1];
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "DETAIL_TEXT");
    $arFilter = Array("IBLOCK_ID"=>BRAND_IBLOCK_ID, "CODE"=>$filterCandidate, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
		$text=$arFields["DETAIL_TEXT"];
        $curBrand=$arFields;
        
        if (count($arrUrlPath)==1)
        {
            $_GET["brand"]="brand_".$arFields["ID"];
            $isBrandTpl=true;
            $GLOBALS['sectionFilter'] = array("PROPERTY_BRAND"=>$arFields["ID"],'SECTION_ID' => array(1,2,3), "INCLUDE_SUBSECTIONS" => "Y");
        } else
        {
            $curSectCode=$arrUrlPath[count($arrUrlPath)-2];
            $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'ACTIVE'=>'Y', 'CODE'=>$curSectCode);
            $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
            
            if($ar_result = $db_list->GetNext())
            {
                $isCollectionTpl=true;
                $arResult["VARIABLES"]["SECTION_ID"]=$ar_result['ID'];
                $GLOBALS['sectionFilter'] = array("PROPERTY_BRAND"=>$arFields["ID"]);
            }
        }
    } else
    {
        CHTTP::SetStatus("404 Not Found");
        @define("ERROR_404","Y"); 
    }
} else
if (strpos($filterCandidate,"vid-")===0)
{
    $filterCandidate=explode("vid-",$filterCandidate);
    $filterCandidate=$filterCandidate[1];
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
    $arFilter = Array("IBLOCK_ID"=>VID_IBLOCK_ID, "CODE"=>$filterCandidate, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $curBrand=$arFields;
        $_GET["brand"]=$arFields["ID"];
        $curSectCode=$arrUrlPath[count($arrUrlPath)-2];
        $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'ACTIVE'=>'Y', 'CODE'=>$curSectCode);
        $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
        
        if($ar_result = $db_list->GetNext())
        {
            $isCollectionTpl=true;
            $arResult["VARIABLES"]["SECTION_ID"]=$ar_result['ID'];
            $GLOBALS['sectionFilter'] = array("PROPERTY_VID_TOVARA"=>$arFields["ID"]);
        } else
        {
            
        }
    } else
    {
        CHTTP::SetStatus("404 Not Found");
        @define("ERROR_404","Y"); 
    }
}

if (!$isTypePlaceTpl && !$isBrandTpl)
{
    $nav = CIBlockSection::GetNavChain(false, $arResult["VARIABLES"]["SECTION_ID"]);
    $nav_first_section = $nav->GetNext();
    $nav_second_section = $nav->GetNext();
    
    $res = CIBlockSection::GetByID($arResult["VARIABLES"]["SECTION_ID"]);
    if ($curSect = $res->GetNext())
    {
        
    } else
    {
        CHTTP::SetStatus("404 Not Found");
        @define("ERROR_404","Y"); 
    }
}

$tplSect="";
if ($isCollectionTpl)
{
    $tplSect='default_sub_collection';
} else
if ($isTypePlaceTpl)
{
    $tplSect='default_sub_place';
} else
if ($isBrandTpl)
{
    $tplSect='default_sub_brand';
} else
{
    switch ($nav_first_section["ID"])
    {
        //Монтаж напольных покрытий
        case "14":
            $tplSect='only_text';
            break;
        //сертификаты
        case "15":
        case "68":
            $tplSect='sertifikaty';
            break;
        //сопутствующие товары
        case "13":
            $tplSect='soputka';
            break;
        //химия
        case "4":
            //корневой раздел
            if ($curSect["DEPTH_LEVEL"]==1)
            {
                $tplSect='himiya_top';    
            } else
            //подраздел химии
            {
                $tplSect='himiya_sub';
            }
            break;
        default:
            //стандартный корневой раздел

            if ($nav_first_section["ID"]==2 && $nav_second_section["ID"]==33)
            {
                if ($arResult["VARIABLES"]["SECTION_ID"]==33)
                {
                    $tplSect='himiya_top';    
                } else
                //подраздел химии
                {
                    $isLaki=true;
                    $tplSect='himiya_sub';
                }
            } else
            if ($nav_first_section["ID"]==2 && $nav_second_section["ID"]==57)
            {
                if ($arResult["VARIABLES"]["SECTION_ID"]==57)
                {
                    $tplSect='himiya_top';    
                } else
                //подраздел химии
                {
                    $isLaki=true;
                    $tplSect='himiya_sub';
                }
            } else
            {
                if ($curSect["DEPTH_LEVEL"]==1)
                {
                    $tplSect='default_top';    
                } else
                //стандартный подраздел
                {
                    $tplSect='default_sub';
                }
            }
            break;
    }
}
if (is_array($curBrand) && $isFilterCandidateFirst)
{
    $APPLICATION->AddChainItem($curBrand["NAME"], "/type-".$filterCandidateFirst."/");
}
?>
<div class="row sertificates">
    <div class="col-xs-3">
        <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"catalog_left", 
	array(
		"ROOT_MENU_TYPE" => "catalog",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "4",
		"USE_EXT" => "Y",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "catalog_left",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N"
	),
	false
);?>
		
		<div class="prod-filter">
		<?if ($_GET['dev'] == 'y'){
		echo '123';
		$rs = CIBlockElement::GetList(
			array(), 
			array(
			"IBLOCK_ID" => 7, 
// 				array("ID" => CIBlockElement::SubQuery("ID", array("IBLOCK_ID" => 21, "PROPERTY_PKE" => 7405))),
// 				array("ID" => CIBlockElement::SubQuery("ID", array("IBLOCK_ID" => 21, "PROPERTY_PKE" => 7410))),
				array("ID" => CIBlockElement::SubQuery("ID", array("IBLOCK_ID" => 3, "PROPERTY_PKE" => "BRAND")))
			),
			false, 
			false,
			array("ID")
		);

		while($ar = $rs->GetNext()) {
			echo '<pre>';
			print_r($ar);
			echo '</pre>';
		}
		
		} ?>
		</div>
    </div>
    <div class="right-catalog col-xs-9">
        <h1>
			<? 
            if ($tplSect=='himiya_sub')
            {
                if ($isLaki)
                {
                    if(!$h1){?>Лакокрасочные материалы — <?}
                } else
                {
					if(!$h1){
					if(!strpos($_SERVER['REQUEST_URI'],'forbo')){ ?>Химия — <? }
				   }
                }
            }    
            if (!$isBrandTpl && !$isTypePlaceTpl)
            {
                if(!$h1) echo $APPLICATION->ShowTitle(false);                
            }        
            if (is_array($curBrand))
            {
                if (!$isBrandTpl && !$isTypePlaceTpl)
                {
                    if(!$h1) echo " — ";    
                }
                if(!$h1) echo $curBrand["NAME"];
                if ($isTypePlaceTpl && $arResult["VARIABLES"]["SECTION_ID"])
                {
                    if(!$h1) echo " — ".$curSection["NAME"];
                    if ($curBrand2)
                    {
                        if(!$h1) echo " ".$curBrand2["NAME"];
                    }
                }
                if ($_GET["vid"]!="")
                {
                    $arSelectVid = Array("ID", "NAME");
                    $arFilterVid = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "CODE" => $_GET["vid"]);
                    $resVid = CIBlockElement::GetList(Array(), $arFilterVid, false, false, $arSelectVid);
                    if($obVid = $resVid->GetNextElement())
                    {
                        $arFieldsVid = $obVid->GetFields();
                        $vidName=$arFieldsVid["NAME"];
                        $GLOBALS['sectionFilter']["PROPERTY_VID_TOVARA"]=$arFieldsVid["ID"];
                        if(!$h1){?> — <?=$vidName?><?}
                    }
                }
                if ($_GET["size"]!="")
                {
                    $arSelectSize = Array("ID", "NAME");
                    $arFilterSize = Array("IBLOCK_ID"=>11, "ACTIVE"=>"Y", "CODE" => $_GET["size"]);
                    $resSize = CIBlockElement::GetList(Array(), $arFilterSize, false, false, $arSelectSize);
                    if($obSize = $resSize->GetNextElement())
                    {
                        $arFieldsSize = $obSize->GetFields();
                        $sizeName=$arFieldsSize["NAME"];
                        $GLOBALS['sectionFilter']["PROPERTY_SIZES"]=$arFieldsSize["ID"];
                        if(!$h1){?> — Размер <?=$sizeName?><?}
                    }
                }
                if ($_GET["group_collection"]!="")
                {
                    $arSelectGC = Array("ID", "NAME");
                    $arFilterGC = Array("IBLOCK_ID"=>12, "ACTIVE"=>"Y", "CODE" => $_GET["group_collection"]);
                    $resGC = CIBlockElement::GetList(Array(), $arFilterGC, false, false, $arSelectGC);
                    if($obGC = $resGC->GetNextElement())
                    {
                        $arFieldsGC = $obGC->GetFields();
                        $GCName=$arFieldsGC["NAME"];
                        $GLOBALS['sectionFilter']["PROPERTY_GROUP"]=$arFieldsGC["ID"];
                        if(!$h1){?> — <?=$GCName?><?}
                    }
                }
            }
			if($h1){echo $h1;} 
            ?>
        </h1>
        <?$APPLICATION->IncludeComponent(
        	"bitrix:catalog.section",
        	$tplSect,
        	array(
                "SECTION_USER_FIELDS" => array("UF_ANONS"),
        		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
        		"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
        		"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
        		"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
        		"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
        		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
        		"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
        		"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
        		"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
        		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
        		"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
        		"BASKET_URL" => $arParams["BASKET_URL"],
        		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
        		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
        		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
        		"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
        		"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
        		"FILTER_NAME" => "sectionFilter",
        		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
        		"CACHE_TIME" => $arParams["CACHE_TIME"],
        		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
        		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        		"SET_TITLE" => $arParams["SET_TITLE"],
        		"MESSAGE_404" => $arParams["MESSAGE_404"],
        		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
        		"SHOW_404" => $arParams["SHOW_404"],
        		"FILE_404" => $arParams["FILE_404"],
        		"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
        		"PAGE_ELEMENT_COUNT" => 999,
        		"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
        		"PRICE_CODE" => $arParams["PRICE_CODE"],
        		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
        		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
        
        		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
        		"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
        		"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
        		"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
        		"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
        
        		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
        		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
        		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
        		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
        		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
        		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
        		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
        		"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
        		"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
        		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
        
        		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
        		"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
        		"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
        		"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
        		"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
        		"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
        		"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
        		"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
        
        		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
        		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
        		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        		"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
        		'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
        		'CURRENCY_ID' => $arParams['CURRENCY_ID'],
        		'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
        
        		'LABEL_PROP' => $arParams['LABEL_PROP'],
        		'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
        		'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
        
        		'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
        		'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
        		'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
        		'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
        		'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
        		'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
        		'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
        		'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
        		'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
        		'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
                'SHOW_ALL_WO_SECTION' => 'Y',
        		'TEMPLATE_THEME' => $_GET["brand"],
        		"ADD_SECTIONS_CHAIN" => $ADD_SECTIONS_CHAIN,
        		'ADD_TO_BASKET_ACTION' => $basketAction,
        		'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
        		'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
        		'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
        		'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
        	),
        	$component
        );?>

    </div>
</div>