<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="group-prod">
    <?
    foreach ($arResult["ITEMS"] as $arItem)
    {
        ?>
        <p><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></p>
        <?
    }
    ?>
</div>
<?
if (isset($_GET["q"]) && count($arResult["ITEMS"])==0)
{
    ?><p>Сожалеем, но ничего не найдено.</p><?
}
?>