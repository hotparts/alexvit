<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (count($arResult["ITEMS"]) > 0) {
    foreach ($arResult["SECTIONS"] as $section) {
        if (!empty($section["SUB_SECTIONS"])) {
            ?>
            <div class="bx_catalog_tile">
                <h2 class="bx_catalog_tile_category_title"><?= $section["NAME"] ?></h2>

            </div>
            <div class="bx_catalog_tile">
                <ul class="row catalog-grid default_catalog_top section_line bx_catalog_tile_ul">
                    <?
                    foreach ($section["SUB_SECTIONS"] as $subsection) {
                        ?>
                        <li>
                        <div class="container-fluid">
                            <div class="section_element row">
                                <div class="section_info_block col-xs-4">
                                    <a href="<?= $subsection["URL"] ?>">
                                        <div class="section_img"
                                             style="background-image: url(<?= $subsection["IMG"] ?>);"></div>
                                        <?
                                        if ($subsection["NAME"] != "") {
                                            ?><h2><?= $subsection["NAME"] ?></h2><?
                                        }
                                        ?>
                                        <span class="link mdi mdi-chevron-right">Показать все</span>
                                    </a>
                                </div>
                                <?if (!empty($subsection["BRANDS"])) {?>
                                    <?  global $arFilBrands;
                                    $arFilBrands = false;
                                    $arFilBrands['PROPERTY_EMARKET_BRAND'] = $subsection['BRANDS_FILTER'];
                                    $APPLICATION->IncludeComponent(
                                        "bitrix:catalog.section",
                                        "catalog_inner",
                                        array(
                                            "TEMPLATE_THEME" => "blue",
                                            "PRODUCT_DISPLAY_MODE" => "N",
                                            "ADD_PICT_PROP" => "-",
                                            "LABEL_PROP" => "-",
                                            "PRODUCT_SUBSCRIPTION" => "N",
                                            "SHOW_DISCOUNT_PERCENT" => "N",
                                            "SHOW_OLD_PRICE" => "N",
                                            "MESS_BTN_BUY" => "",
                                            "MESS_BTN_ADD_TO_BASKET" => "",
                                            "MESS_BTN_SUBSCRIBE" => "",
                                            "MESS_BTN_DETAIL" => "",
                                            "MESS_NOT_AVAILABLE" => "",
                                            "AJAX_MODE" => "N",
                                            "IBLOCK_TYPE" => "catalog",
                                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                            "SECTION_ID" => $subsection['ID'],
//                                            "SECTION_CODE" => $arParams["SECTION_CODE"],
                                            "SECTION_USER_FIELDS" => array(
                                                0 => "",
                                                1 => "",
                                            ),
                                            "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                                            "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                                            "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                                            "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                                            "FILTER_NAME" => $arFilBrands,
                                            "INCLUDE_SUBSECTIONS" => "Y",
                                            "SHOW_ALL_WO_SECTION" => "N",
                                            "SECTION_URL" => "",
                                            "DETAIL_URL" => "",
                                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                                            "SET_META_KEYWORDS" => "N",
                                            "META_KEYWORDS" => "-",
                                            "SET_META_DESCRIPTION" => "N",
                                            "META_DESCRIPTION" => "-",
                                            "BROWSER_TITLE" => "-",
                                            "ADD_SECTIONS_CHAIN" => "N",
                                            "DISPLAY_COMPARE" => "N",
                                            "SET_TITLE" => "N",
                                            "PAGE_ELEMENT_COUNT" => "2",
                                            "LINE_ELEMENT_COUNT" => "2",
                                            "PROPERTY_CODE" => array(
                                                0 => "",
                                                1 => $arParams["PROPERTY_CODE"],
                                                2 => "",
                                            ),
                                            "OFFERS_CART_PROPERTIES" => "",
                                            "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                                            "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                                            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                                            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                                            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                                            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                                            "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
                                            "PRICE_CODE" => array(
                                            ),
                                            "USE_PRICE_COUNT" => "N",
                                            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
                                            "PRICE_VAT_INCLUDE" => "N",
                                            "USE_PRODUCT_QUANTITY" => "N",
                                            "CONVERT_CURRENCY" => "N",
                                            "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                                            "BASKET_URL" => $arParams["BASKET_URL"],
                                            "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                                            "PRODUCT_ID_VARIABLE" => "id",
                                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                                            "PRODUCT_PROPS_VARIABLE" => "prop",
                                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                            "PRODUCT_PROPERTIES" => array(
                                            ),
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                                            "CACHE_FILTER" => "N",
                                            "CACHE_GROUPS" => "N",
                                            "SET_STATUS_404" => "N",
                                            "PAGER_TEMPLATE" => ".default",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "DISPLAY_BOTTOM_PAGER" => "N",
                                            "PAGER_TITLE" => "",
                                            "PAGER_SHOW_ALWAYS" => "N",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "N",
                                            "HIDE_NOT_AVAILABLE" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N",
                                            "AJAX_OPTION_ADDITIONAL" => "",
                                            "COMPONENT_TEMPLATE" => "catalog_inner",
                                            "BACKGROUND_IMAGE" => "-",
                                            "SEF_MODE" => "N",
                                            "SET_BROWSER_TITLE" => "Y",
                                            "SET_LAST_MODIFIED" => "N",
                                            "USE_MAIN_ELEMENT_SECTION" => "N",
                                            "PAGER_BASE_LINK_ENABLE" => "N",
                                            "SHOW_404" => "N",
                                            "MESSAGE_404" => "",
                                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                            "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                                            "COMPATIBLE_MODE" => "Y"
                                        ),
                                        false
                                    );
                                    ?>
                                    <?
                                    /*foreach ($subsection["BRANDS"] as $brand) {
                                        ?>
                                        <div class="item col-xs-3">
                                            <div>
                                                <a href="<?= $brand["URL"] ?>">
                                                    <div class="product_img" style="background-image: url(<? echo $brand['PREVIEW_PICTURE']['SRC']; ?>)"></div>
                                                    <h3 title="Коллекция Energy"><?= $brand["NAME"] ?></h3>
                                                </a>
                                            </div>
                                        </div>
                                        <?
                                    }*/
                                    ?>
                                <?} ?>
                            </div>
                        </div>
                    </li><?
                    }
                    ?>
                </ul>
            </div>
            <?
        }
    } ?>
<? }else{ ?>
    Товары не найдены
<?}?>
