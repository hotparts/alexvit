<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arResult["VID_TOVARA"]=array();
if (stristr($APPLICATION->GetCurDir(),'/brand-'))
{
    $arFilter = Array('IBLOCK_ID'=>10, 'ACTIVE'=>'Y');
    $db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter, true);
    while($ar_result = $db_list->GetNext())
    {
        
            $arResult["VID_TOVARA"][$ar_result['ID']]["ITEMS"]=array();
            $arResult["VID_TOVARA"][$ar_result['ID']]["NAME"]=$ar_result['NAME'];
      
            $vseVidyTovarov=array();
            $arSelect = Array("ID", "NAME", "DETAIL_PICTURE", "IBLOCK_SECTION_ID", "CODE");
            $arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "IBLOCK_SECTION_ID" => $ar_result['ID'], "CODE" => $_GET["vid"]);
            $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();

                if ($arFields["DETAIL_PICTURE"])
                {
                    $img = CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array('width'=>118, 'height'=>118), BX_RESIZE_IMAGE_EXACT, true);
                    if (isset($arFields["ID"]) && $arFields["ID"]>0)
                    {
                        $url=array();
    
                        $url[]="vid=".$arFields["CODE"];
                        if ($_GET["size"]!="")
                        {
                            $url[]="size=".$_GET["size"];
                        } 
                        if ($_GET["group_collection"]!="")
                        {
                            $url[]="group_collection=".$_GET["group_collection"];
                        }
                        $vseVidyTovarov[$arFields["ID"]]=array(
                            "SECTION_ID" => $arFields['IBLOCK_SECTION_ID'],
                            "NAME" => $arFields['NAME'],
                            "IMG" => $img["src"],
                            "URL" => $APPLICATION->GetCurDir()."?".join("&",$url)
                        );
                    }
                }
            }
        
    }
}

//размеры
$sizes_all=array();
$arResult["SIZES"]=array();
$arSelect = Array("ID", "NAME", "CODE", "SORT");
$arFilter = Array("IBLOCK_ID"=>11, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    
    $url=array();
    
    if ($_GET["vid"]!="")
    {
        $url[]="vid=".$_GET["vid"];
    } 
    $url[]="size=".$arFields["CODE"];
    if ($_GET["group_collection"]!="")
    {
        $url[]="group_collection=".$_GET["group_collection"];
    }
    
    $sizes_all[$arFields["ID"]]=array(
        "NAME" => $arFields["NAME"],
        "URL" => $APPLICATION->GetCurDir()."?".join("&",$url),
        "SORT" => $arFields["SORT"]
    );
}

//коллекции
$collections_all=array();
$arResult["COLLECTIONS"]=array();
$arSelect = Array("ID", "NAME", "DETAIL_PICTURE", "SORT", "CODE");
$arFilter = Array("IBLOCK_ID"=>12, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    if ($arFields["DETAIL_PICTURE"])
    {
        $img = CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array('width'=>261, 'height'=>275), BX_RESIZE_IMAGE_EXACT, true);
        $img=$img["src"];                
    } else
    {
        $img=SITE_TEMPLATE_PATH.'/img/no-photo.png';   
    }
    
    $url=array();
    if ($_GET["vid"]!="")
    {
        $url[]="vid=".$_GET["vid"];
    }
    if ($_GET["size"]!="")
    {
        $url[]="size=".$_GET["size"];
    } 
    $url[]="group_collection=".$arFields["CODE"];

    $collections_all[$arFields["ID"]]=array(
        "NAME" => $arFields["NAME"],
        "ID" => $arFields["ID"],
        "SORT" => $arFields["SORT"],
        "IMG" => $img,
        "URL" => $APPLICATION->GetCurDir()."?".join("&",$url),    
    );
}

//КРОШКИ
$arResult['BREADCRUMBS']=array();
$arrUrlPath=explode("/",substr($APPLICATION->GetCurDir(),1,strlen($APPLICATION->GetCurDir())-2));
$filterCandidate=end($arrUrlPath);
$filterCandidateFirst=$arrUrlPath[0];
if (strpos($filterCandidateFirst,"type-")===0)
{
    $nav = CIBlockSection::GetNavChain(false, $arResult["ID"]);
    $sectionName=array();
    
    while ($nav_first_section = $nav->GetNext())
    {
        $sectionName[]=$nav_first_section["NAME"];
    }
    $arResult['BREADCRUMBS'][]=array(
        "NAME" => join(" - ",$sectionName),
        "URL" => "/".$filterCandidateFirst.$arResult["SECTION_PAGE_URL"]
    );

    if (strpos($filterCandidate,"brand-")===0)
    {
        $filterCandidate=explode("brand-",$filterCandidate);
        $filterCandidate=$filterCandidate[1];
        $arSelect = Array("ID", "NAME", "CODE");
        $arFilter = Array("IBLOCK_ID"=>BRAND_IBLOCK_ID, "CODE"=>$filterCandidate, "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if($ob = $res->GetNextElement())
        {
            $arFieldsBrand = $ob->GetFields();
            $arResult['BREADCRUMBS'][]=array(
                "NAME" => $arFieldsBrand["NAME"],
                "URL" => "/".$filterCandidateFirst.$arResult["SECTION_PAGE_URL"]."brand-".$arFieldsBrand["CODE"]."/"
            );
        }
    }
}
$cp = $this->__component; // объект компонента
if (is_object($cp))
{
   $cp->arResult['BREADCRUMBS'] = $arResult["BREADCRUMBS"];
   $cp->SetResultCacheKeys(array(
        'BREADCRUMBS'
   ));
}

foreach ($arResult["ITEMS"] as $key=>&$arItem)
{
    $skip=false;
    if (isset($_GET["q"]))
    {
        $curSectionID=$arItem["~IBLOCK_SECTION_ID"];
        $nav = CIBlockSection::GetNavChain(false, $curSectionID);
        $nav_first_section = $nav->GetNext();
        $nav_second_section = $nav->GetNext();
        if ($nav_first_section["ID"]==4 || $nav_second_section["ID"]==33 || $nav_second_section["ID"]==57)
        {
            $skip=true;
        }    
    }
    
    if (!$skip)
    {
        if ($APPLICATION->GetCurDir()!="/")
        {
//            $arItem["DETAIL_PAGE_URL"]=$APPLICATION->GetCurDir().$arItem["CODE"].".html";
        } else
        {
            $res = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
            if($ar_res = $res->GetNext())
            {
                $arSelect = Array("ID", "NAME", "CODE");
                $arFilter = Array("IBLOCK_ID"=>BRAND_IBLOCK_ID, "ID"=>$arItem["PROPERTIES"]["BRAND"]["VALUE"], "ACTIVE"=>"Y");
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if($ob = $res->GetNextElement())
                {
                    $arFieldsBrand = $ob->GetFields();
//                    $arItem["DETAIL_PAGE_URL"]=$ar_res["SECTION_PAGE_URL"].'brand-'.$arFieldsBrand["CODE"]."/".$arItem["CODE"].".html";
                }
            }
        }
        foreach ($arItem["PROPERTIES"]["VID_TOVARA"]["VALUE"] as $curVidTovara)
        {
            if ($curVidTovara>0 && isset($arResult["VID_TOVARA"][$vseVidyTovarov[$curVidTovara]["SECTION_ID"]]))
            {
                $arResult["VID_TOVARA"][$vseVidyTovarov[$curVidTovara]["SECTION_ID"]]["ITEMS"][$vseVidyTovarov[$curVidTovara]["NAME"]]=$vseVidyTovarov[$curVidTovara];
            }
        }
        $arItem["SIZES"]=array();
        foreach ($arItem["PROPERTIES"]["SIZES"]["VALUE"] as $curSizeTovara)
        {
            if ($curSizeTovara>0 && isset($sizes_all[$curSizeTovara]))
            {
                $arItem["SIZES"][]=$sizes_all[$curSizeTovara]["NAME"];
                $arResult["SIZES"][$curSizeTovara]=$sizes_all[$curSizeTovara];
            }
        }
        
        
        if ($arItem["PROPERTIES"]["GROUP"]["VALUE"]>0 && isset($collections_all[$arItem["PROPERTIES"]["GROUP"]["VALUE"]]))
        {
            $arResult["COLLECTIONS"][$arItem["PROPERTIES"]["GROUP"]["VALUE"]]=$collections_all[$arItem["PROPERTIES"]["GROUP"]["VALUE"]];
        }
        
    } else
    {
        unset($arResult["ITEMS"][$key]);
    }
}
uasort($arResult["SIZES"],'sortSectionsBySort');
uasort($arResult["COLLECTIONS"],'sortSectionsBySort');