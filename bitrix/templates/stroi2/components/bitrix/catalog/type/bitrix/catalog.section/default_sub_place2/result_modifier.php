<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arrUrlPath=explode("/",substr($APPLICATION->GetCurDir(),1,strlen($APPLICATION->GetCurDir())-2));
$filterCandidate=end($arrUrlPath);
if (strpos($filterCandidate,"type-")===0)
{
    $filterCandidate=explode("type-",$filterCandidate);
    $filterCandidate=$filterCandidate[1];
}
$arResult["SECTIONS"]=array();
foreach ($arResult["ITEMS"] as $item) {
    $curSectionID=$item["~IBLOCK_SECTION_ID"];

        $res = CIBlockSection::GetByID($curSectionID);
        if($ar_res = $res->GetNext())
        {
            if ($ar_res["PICTURE"])
            {
                $img = CFile::ResizeImageGet($ar_res["PICTURE"], array('width'=>261, 'height'=>275), BX_RESIZE_IMAGE_EXACT, true);
                $img=$img["src"];                
            } else
            {
                $img=SITE_TEMPLATE_PATH.'/img/no-photo.png';   
            }
            
            $resTopSection = CIBlockSection::GetByID($ar_res["IBLOCK_SECTION_ID"]);
            if($ar_res_top_section = $resTopSection->GetNext()){
                $curBrandCode=explode('href="',$item["DISPLAY_PROPERTIES"]["BRAND"]["DISPLAY_VALUE"]);
                $curBrandCode=$curBrandCode[1];
                $curBrandCode=explode('">',$curBrandCode);
                $curBrandCode=substr($curBrandCode[0],1);
                $brand_id = $item["DISPLAY_PROPERTIES"]["EMARKET_BRAND"]["VALUE"];

                $arResult["SECTIONS"][$ar_res_top_section["ID"]]["SORT"]=$ar_res_top_section["SORT"];
                $arResult["SECTIONS"][$ar_res_top_section["ID"]]["NAME"]=$ar_res_top_section["NAME"];

                if (!isset($arResult["SECTIONS"][$ar_res_top_section["ID"]]["SUB_SECTIONS"][$curSectionID]))
                {
                    $arResult["SECTIONS"][$ar_res_top_section["ID"]]["SUB_SECTIONS"][$curSectionID]=array(
                        "ID" => $ar_res["ID"],
                        "SORT" => $ar_res["SORT"],
                        "NAME" => $ar_res['NAME'],
                        "IMG" => $img,
//                        "URL" => "/type-".$filterCandidate."/".$ar_res_top_section["CODE"]."/".$ar_res["CODE"]."/"
                        "URL" => $ar_res['SECTION_PAGE_URL']
                    );
                }
                $arResult["SECTIONS"][$ar_res_top_section["ID"]]["SUB_SECTIONS"][$curSectionID]["BRANDS"][$curBrand]=array(
                    "NAME" => $curBrand,
                    "URL" => "/type-".$filterCandidate."/".$ar_res_top_section["CODE"]."/".$ar_res["CODE"]."/".$curBrandCode
                );
                $arResult["SECTIONS"][$ar_res_top_section["ID"]]["SUB_SECTIONS"][$curSectionID]["BRANDS_FILTER"][]=$brand_id;
            }
        }
}

//uasort($arResult["SECTIONS"], 'sortSectionsBySort');
//foreach ($arResult["SECTIONS"] as $sectionName=>&$sectionItems)
//{
//    if (isset($sectionItems["SUB_SECTIONS"]))
//    {
//        uasort($sectionItems["SUB_SECTIONS"], 'sortSectionsBySort');
//    }
//}
?>