<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

$arrUrlPath=explode("/",substr($APPLICATION->GetCurDir(),1,strlen($APPLICATION->GetCurDir())-2));
$filterCandidate=end($arrUrlPath);
$filterCandidateFirst=$arrUrlPath[0];

$isFilterCandidateFirst=false;
$isCollectionTpl=false;
$isTypePlaceTpl=false;
$isBrandTpl=false;

$isLaki=false;

$ADD_SECTIONS_CHAIN="Y";

if (strpos($filterCandidateFirst,"type-")===0)
{
    $filterCandidateFirst=explode("type-",$filterCandidateFirst);
    $filterCandidateFirst=$filterCandidateFirst[1];
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
    $arFilter = Array("IBLOCK_ID"=>TYPE_PLACE_IBLOCK_ID, "CODE"=>$filterCandidateFirst, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    if($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $curBrand=$arFields;
        $_GET["brand"]="type_".$arFields["ID"];

        $isTypePlaceTpl=true;

        if (count($arrUrlPath)==1)
        {
            $GLOBALS['sectionFilter'] = array("PROPERTY_TYPE_PLACE"=>$arFields["ID"]);
        } else
        {
            if (strpos($filterCandidate,"brand-")===0)
            {
                $filterCandidate=explode("brand-",$filterCandidate);
                $filterCandidate=$filterCandidate[1];
                $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
                $arFilter = Array("IBLOCK_ID"=>BRAND_IBLOCK_ID, "CODE"=>$filterCandidate, "ACTIVE"=>"Y");
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if($ob = $res->GetNextElement())
                {
                    $arFieldsBrand = $ob->GetFields();
                    $curBrand2=$arFieldsBrand;
                    $curSectCode=$arrUrlPath[count($arrUrlPath)-2];
                } else
                {
                    CHTTP::SetStatus("404 Not Found");
                    @define("ERROR_404","Y");
                }
            } else
            {
                $curSectCode=$arrUrlPath[count($arrUrlPath)-1];
            }

            $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'ACTIVE'=>'Y', 'CODE'=>$curSectCode);
            $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
            $isFilterCandidateFirst=true;
            if($ar_result = $db_list->GetNext())
            {
                $isCollectionTpl=true;
                $curSection=$ar_result;
                $arResult["VARIABLES"]["SECTION_ID"]=$ar_result['ID'];
                $GLOBALS['sectionFilter'] = array("PROPERTY_TYPE_PLACE"=>$arFields["ID"],"PROPERTY_BRAND"=>$arFieldsBrand["ID"]);
                $ADD_SECTIONS_CHAIN="N";
            } else
            {
                CHTTP::SetStatus("404 Not Found");
                @define("ERROR_404","Y");
            }
        }
    } else
    {
        CHTTP::SetStatus("404 Not Found");
        @define("ERROR_404","Y");
    }
} else
    if (strpos($filterCandidate,"brand-")===0)
    {
        $filterCandidate=explode("brand-",$filterCandidate);
        $filterCandidate=$filterCandidate[1];
        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "DETAIL_TEXT");
        $arFilter = Array("IBLOCK_ID"=>BRAND_IBLOCK_ID, "CODE"=>$filterCandidate, "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $text=$arFields["DETAIL_TEXT"];
            $curBrand=$arFields;

            if (count($arrUrlPath)==1)
            {
                $_GET["brand"]="brand_".$arFields["ID"];
                $isBrandTpl=true;
                $GLOBALS['sectionFilter'] = array("PROPERTY_BRAND"=>$arFields["ID"],'SECTION_ID' => array(1,2,3), "INCLUDE_SUBSECTIONS" => "Y");
            } else
            {
                $curSectCode=$arrUrlPath[count($arrUrlPath)-2];
                $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'ACTIVE'=>'Y', 'CODE'=>$curSectCode);
                $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);

                if($ar_result = $db_list->GetNext())
                {
                    $isCollectionTpl=true;
                    $arResult["VARIABLES"]["SECTION_ID"]=$ar_result['ID'];
                    $GLOBALS['sectionFilter'] = array("PROPERTY_BRAND"=>$arFields["ID"]);
                }
            }
        } else
        {
            CHTTP::SetStatus("404 Not Found");
            @define("ERROR_404","Y");
        }
    } else
        if (strpos($filterCandidate,"vid-")===0)
        {
            $filterCandidate=explode("vid-",$filterCandidate);
            $filterCandidate=$filterCandidate[1];
            $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
            $arFilter = Array("IBLOCK_ID"=>VID_IBLOCK_ID, "CODE"=>$filterCandidate, "ACTIVE"=>"Y");
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();
                $curBrand=$arFields;
                $_GET["brand"]=$arFields["ID"];
                $curSectCode=$arrUrlPath[count($arrUrlPath)-2];
                $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'ACTIVE'=>'Y', 'CODE'=>$curSectCode);
                $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);

                if($ar_result = $db_list->GetNext())
                {
                    $isCollectionTpl=true;
                    $arResult["VARIABLES"]["SECTION_ID"]=$ar_result['ID'];
                    $GLOBALS['sectionFilter'] = array("PROPERTY_VID_TOVARA"=>$arFields["ID"]);
                } else
                {

                }
            } else
            {
                CHTTP::SetStatus("404 Not Found");
                @define("ERROR_404","Y");
            }
        }

if (!$isTypePlaceTpl && !$isBrandTpl)
{
    $nav = CIBlockSection::GetNavChain(false, $arResult["VARIABLES"]["SECTION_ID"]);
    $nav_first_section = $nav->GetNext();
    $nav_second_section = $nav->GetNext();

    $res = CIBlockSection::GetByID($arResult["VARIABLES"]["SECTION_ID"]);
    if ($curSect = $res->GetNext())
    {

    } else
    {
        CHTTP::SetStatus("404 Not Found");
        @define("ERROR_404","Y");
    }
}
if (is_array($curBrand) && $isFilterCandidateFirst)
{
    $APPLICATION->AddChainItem($curBrand["NAME"], "/type-".$filterCandidateFirst."/");
}






$arFilter = Array(
	"IBLOCK_ID"=>$arParams["IBLOCK_ID"], 
	"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
	"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
);
$countElements = CIBlockElement::GetList(Array(), $arFilter, Array());

if (!$arParams['FILTER_VIEW_MODE'])
	$arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');
$verticalGrid = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");

if ($verticalGrid)
{
	?>
<div class="workarea grid2x1"><?
}
if ($arParams['USE_FILTER'] == 'Y')
{

	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
	);
	if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
	{
		$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
	}
	elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
	{
		$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
	}

	$obCache = new CPHPCache();
	if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
	{
		$arCurSection = $obCache->GetVars();
	}
	elseif ($obCache->StartDataCache())
	{
		$arCurSection = array();
		if (\Bitrix\Main\Loader::includeModule("iblock"))
		{
			$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

			if(defined("BX_COMP_MANAGED_CACHE"))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");

				if ($arCurSection = $dbRes->Fetch())
				{
					$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
				}
				$CACHE_MANAGER->EndTagCache();
			}
			else
			{
				if(!$arCurSection = $dbRes->Fetch())
					$arCurSection = array();
			}
		}
		$obCache->EndDataCache($arCurSection);
	}
	if (!isset($arCurSection))
	{
		$arCurSection = array();
	}
	
	?>
	
	<?if($countElements > 0) {?>
            

		
	<?}?>
	
	<?

}
if ($verticalGrid)
{
	?>          
            <div class="catalog_section">
        <?
        
}
?><?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"",
	array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
		"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
		"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
		"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
		"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
        "PARAMS_PARENT" => $arParams
	),
	$component
);?>

<?$intSectionID = 0;?>

<?
if($countElements > 0) 
{
	$dbPriceType = CCatalogGroup::GetList(
        array("SORT" => "ASC"),
        array("NAME" => "BASE")
    )->Fetch();
    $ID_PRICE = $dbPriceType['ID'];
	//default
	global $APPLICATION;
	$section_dir = $APPLICATION->GetCurDir();
	
	$section_view = (int)htmlspecialchars($APPLICATION->get_cookie('CATALOG_SECTION_VIEW'));
	if(!$section_view) $section_view = 1;
	$section_sort = (int)htmlspecialchars($APPLICATION->get_cookie('CATALOG_SECTION_SORT'));
	if(!$section_sort) $section_sort = 1;
	$section_page = (int)htmlspecialchars($APPLICATION->get_cookie('CATALOG_SECTION_PAGE'));
	if(!$section_page) $section_page = 1;


	//set filter
	if(!empty($_REQUEST['view'])) 
	{
		$section_view = intval($_REQUEST['view']);
		$APPLICATION->set_cookie("CATALOG_SECTION_VIEW", $section_view, time()+60*60*24*7);
	}
	if(!empty($_REQUEST['sort'])) 
	{
		$section_sort = intval($_REQUEST['sort']);
		$APPLICATION->set_cookie("CATALOG_SECTION_SORT", $section_sort, time()+60*60*24*7);	
	}
	if(!empty($_REQUEST['page'])) 
	{
		$section_page = intval($_REQUEST['page']);
		$APPLICATION->set_cookie("CATALOG_SECTION_PAGE", $section_page, time()+60*60*24*7);	
	}
	
	switch($section_view)
	{
		case '1': $template = '.default'; break;
		case '2': $template = 'list_1'; break;
	}
	
	switch($section_sort)
	{
		case '1':
			$arParams["ELEMENT_SORT_FIELD2"] = 'NAME';
			$arParams["ELEMENT_SORT_ORDER2"] = 'asc';
		break;
		case '2': 
			$arParams["ELEMENT_SORT_FIELD2"] = 'NAME';
			$arParams["ELEMENT_SORT_ORDER2"] = 'desc';
		break;
		case '3':
			$arParams["ELEMENT_SORT_FIELD2"] = 'CATALOG_PRICE_'.$ID_PRICE;
			$arParams["ELEMENT_SORT_ORDER2"] = 'asc';
		break;
		case '4':
			$arParams["ELEMENT_SORT_FIELD2"] = 'CATALOG_PRICE_'.$ID_PRICE;
			$arParams["ELEMENT_SORT_ORDER2"] = 'desc';
		break;
		case '5':
			$arParams["ELEMENT_SORT_FIELD2"] = 'PROPERTY_EMARKET_RATING';
			$arParams["ELEMENT_SORT_ORDER2"] = 'asc';
		break;
		case '6':
			$arParams["ELEMENT_SORT_FIELD2"] = 'PROPERTY_EMARKET_RATING';
			$arParams["ELEMENT_SORT_ORDER2"] = 'desc';
		break;
		case '7':
			$arParams["ELEMENT_SORT_FIELD2"] = 'PROPERTY_EMARKET_COMMENTS_COUNT';
			$arParams["ELEMENT_SORT_ORDER2"] = 'asc';
		break;
		case '8':
			$arParams["ELEMENT_SORT_FIELD2"] = 'PROPERTY_EMARKET_COMMENTS_COUNT';
			$arParams["ELEMENT_SORT_ORDER2"] = 'desc';
		break;
	}
	
	switch($section_page)
	{
		case '1': $arParams["PAGE_ELEMENT_COUNT"] = 40; break;
		case '2': $arParams["PAGE_ELEMENT_COUNT"] = 80; break;
		case '3': $arParams["PAGE_ELEMENT_COUNT"] = 120; break;
		case '4': $arParams["PAGE_ELEMENT_COUNT"] = 200; break;
		case '5': $arParams["PAGE_ELEMENT_COUNT"] = 1000; break;
	}
	
	?>
    <div class="bg_section">
    <div class="container-fluid">
            <div class="filter_block">
                <span class="btn btn_blue filter-btn visible-xs"><?=GetMessage('CSFILTER_TITL_FILTER')?></span>
		<?$APPLICATION->IncludeComponent(
			"bitrix:catalog.smart.filter",
			"visual_".($arParams["FILTER_VIEW_MODE"] == "HORIZONTAL" ? "horizontal" : "vertical"),
			Array(
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID" => $arCurSection['ID'],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "PRICE_CODE" => $arParams["PRICE_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "SAVE_IN_SESSION" => "N",
                "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                "XML_EXPORT" => "Y",
                "SECTION_TITLE" => "NAME",
                "SECTION_DESCRIPTION" => "DESCRIPTION",
                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                "SEF_MODE" => $arParams["SEF_MODE"],
                "SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
                "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
			),
			$component,
			array('HIDE_ICONS' => 'Y')
		);?>
                </div>
                
                <div class=" catalog_section_block">
                <div class="catalog-filter">
		<div class="option">
			<span class="view"><?=GetMessage('CSFILTER_VIEW');?>:</span>
			<a class="view mdi mdi-view-grid <?if($section_view == 1) echo 'active';?>" href="<?=$APPLICATION->GetCurPageParam("view=1", array("view"))?>" ></a>
			<a class="view mdi mdi-reorder-horizontal <?if($section_view == 2) echo 'active';?>" href="<?=$APPLICATION->GetCurPageParam("view=2", array("view"))?>" ></a>
		</div>
		<div class="option sort">
			<span><?=GetMessage('CSFILTER_SORTBY');?>:</span>
                        <select onchange="window.location=this.value">
                            <option <?if(($section_sort == 1) || ($section_sort == 2)):?>selected='selected'<?endif;?>
                                	value="<?
					$tempSort = 1;
					echo $APPLICATION->GetCurPageParam("sort=".$tempSort, array("sort"));
                                        ?>"                                                         
                            ><?=GetMessage('CSFILTER_TITLE');?>
                            </option>
                            <option <?if(($section_sort == 3)):?>selected='selected'<?endif;?>
                                	value="<?					
					$tempSort = 3;
					echo $APPLICATION->GetCurPageParam("sort=".$tempSort, array("sort"));
                                        ?>"                                                         
                            ><?=GetMessage('CSFILTER_PRICE');?>
                            </option>                            
                            
                            <option  <?if(($section_sort == 4)):?>selected='selected'<?endif;?>
				value="<?					
					$tempSort = 4;
					echo $APPLICATION->GetCurPageParam("sort=".$tempSort, array("sort"));
				?>">
				<?=GetMessage('CSFILTER_PRICE_DOWN');?>
                            </option> 
                            
                            <option <?if(($section_sort == 5) || ($section_sort == 6)):?>selected='selected'<?endif;?>
                                	value="<?
					$tempSort = 6;
					echo $APPLICATION->GetCurPageParam("sort=".$tempSort, array("sort"));
                                        ?>"                                                         
                            ><?=GetMessage('CSFILTER_RATING');?>
                            </option>
                            <option <?if(($section_sort == 7) || ($section_sort == 8)):?>selected='selected'<?endif;?>
                                	value="<?
					$tempSort = 8;
					echo $APPLICATION->GetCurPageParam("sort=".$tempSort, array("sort"));					
                                    ?>"                                                         
                            ><?=GetMessage('CSFILTER_NUMBER');?>
                            </option>
                        </select>   
		</div>
		<div class="option amount">
			<span><?=GetMessage('CSFILTER_PAGE');?>:</span>
                        <select onchange="window.location=this.value">
			<option <?if($section_page == 1):?>selected='selected'<?endif;?> value="<?=$APPLICATION->GetCurPageParam("page=1", array("page"))?>">40</option>	
			<option <?if($section_page == 2):?>selected='selected'<?endif;?> value="<?=$APPLICATION->GetCurPageParam("page=2", array("page"))?>">80</option>
			<option <?if($section_page == 3):?>selected='selected'<?endif;?> value="<?=$APPLICATION->GetCurPageParam("page=3", array("page"))?>">120</option>	
			<option <?if($section_page == 4):?>selected='selected'<?endif;?> value="<?=$APPLICATION->GetCurPageParam("page=4", array("page"))?>">200</option>	
			<option <?if($section_page == 5):?>selected='selected'<?endif;?> value="<?=$APPLICATION->GetCurPageParam("page=5", array("page"))?>"><?=GetMessage('CSFILTER_ALL');?></option>	
                        </select>
		</div>
                </div><?
}?>

                <?$intSectionID = $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        $template,
                        array(
                                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                                "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                                "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                                "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                                "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                                "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                                "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                                "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                                "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
                                "BASKET_URL" => $arParams["BASKET_URL"],
                                "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                                "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                                "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                                "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                                "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                                "FILTER_NAME" => $arParams["FILTER_NAME"],
                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                                "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                                "SET_TITLE" => $arParams["SET_TITLE"],
                                "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                                "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                                "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
                                "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                                "PRICE_CODE" => $arParams["PRICE_CODE"],
                                "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                                "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

                                "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                                "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                                "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                                "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                                "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

                                "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                                "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                                "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                                "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                                "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                                "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                                "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

                                "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                                "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                                "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                                "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                                "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                                "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                                "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                                "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

                                "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                                "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                                "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                                "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
                                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

                                'LABEL_PROP' => $arParams['LABEL_PROP'],
                                'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                                'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

                                'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                                'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                                'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                                'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                                'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                                'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
                                'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
                                'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
                                'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
                                'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

                                'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                                "ADD_SECTIONS_CHAIN" => "N",
                                "COMPARE_NAME" => $arParams['COMPARE_NAME']
                        ),
                        $component
                );
                ?><?
if ($verticalGrid)
{
	?></div>
           </div>



            </div>
                <? $APPLICATION->ShowViewContent('section_description'); ?>
	<div style="clear: both;"></div>
        </div>
</div>
<?
}
?>
