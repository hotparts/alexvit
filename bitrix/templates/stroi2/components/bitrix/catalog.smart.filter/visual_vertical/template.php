<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

CJSCore::Init(array("fx"));

if (file_exists($_SERVER["DOCUMENT_ROOT"].$this->GetFolder().'/themes/'.$arParams["TEMPLATE_THEME"].'/colors.css'))
	$APPLICATION->SetAdditionalCSS($this->GetFolder().'/themes/'.$arParams["TEMPLATE_THEME"].'/colors.css');
?>
<div class="bx_sidebar">
<div class="bx_filter_vertical bx_<?=$arParams["TEMPLATE_THEME"]?>">
	<h3><?echo GetMessage("CT_BCSF_FILTER_TITLE")?></h3>
	<div class="bx_filter_section m4">
		
		<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
			<?foreach($arResult["ITEMS"] as $key=>$arItem):
				$key = md5($key);
				?>
				<?if(isset($arItem["PRICE"])):?>
					<?
					if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
						continue;
					?>
					<div class="bx_filter_container price">
                        <span class="bx_filter_container_modef"></span>
						<span class="bx_filter_container_title active"><?=GetMessage('CT_BCSF_FILTER_COST')?><i class="fa fa-angle-down"></i></span>
                        <div class="bx_filter_hide">
                            <div class="bx_filter_param_area">

                                        <input
                                            class="min-price"
                                            type="text"
                                            name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                            id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                            value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
                                            placeholder="<?echo round($arItem["VALUES"]["MIN"]["VALUE"], 2)?>"
                                            size="5"
                                            onkeyup="smartFilter.keyup(this)"
                                        />
                                <span>-</span>

                                        <input
                                            class="max-price"
                                            type="text"
                                            name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                            id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                            value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
                                            placeholder="<?echo round($arItem["VALUES"]["MAX"]["VALUE"], 2)?>"
                                            size="5"
                                            onkeyup="smartFilter.keyup(this)"
                                        />

                                                        <span class="mdi mdi-currency-rub"></span>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="bx_ui_slider_track" id="drag_track_<?=$key?>">
                                <div class="bx_ui_slider_range" style="left: 0; right: 0%;"  id="drag_tracker_<?=$key?>"></div>
                                <a class="bx_ui_slider_handle left"  href="javascript:void(0)" style="left:0;" id="left_slider_<?=$key?>"></a>
                                <a class="bx_ui_slider_handle right" href="javascript:void(0)" style="right:0%;" id="right_slider_<?=$key?>"></a>
                            </div>
                        </div>
					</div>

					<script type="text/javascript">
						var DoubleTrackBar<?=$key?> = new cDoubleTrackBar('drag_track_<?=$key?>', 'drag_tracker_<?=$key?>', 'left_slider_<?=$key?>', 'right_slider_<?=$key?>', {
							OnUpdate: function(){
								BX("<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>").value = this.MinPos;
								BX("<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>").value = this.MaxPos;
							},
							Min: parseFloat(<?=$arItem["VALUES"]["MIN"]["VALUE"]?>),
							Max: parseFloat(<?=$arItem["VALUES"]["MAX"]["VALUE"]?>),
							MinInputId : BX('<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>'),
							MaxInputId : BX('<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>'),
							FingerOffset: 10,
							MinSpace: 1,
							RoundTo: 1,
						});
					</script>
				<?endif?>
			<?endforeach?>

			<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
				<?if($arItem["PROPERTY_TYPE"] == "N" ):?>
					<?
					if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
						continue;
					?>
					<div class="bx_filter_container price">
						<span class="bx_filter_container_title active"><?=$arItem["NAME"]?><i class="fa fa-angle-down"></i></span>
						<div class="bx_filter_param_area">
							
								<input
									class="min-price"
									type="text"
									name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
									id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
									value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>" 
									placeholder="<?echo round($arItem["VALUES"]["MIN"]["VALUE"], 2)?>"
									size="5"
									onkeyup="smartFilter.keyup(this)"
								/>
								
                                                                <span>-</span>
                                                                
								<input
									class="max-price"
									type="text"
									name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
									id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
									value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>" 
									placeholder="<?echo round($arItem["VALUES"]["MAX"]["VALUE"], 2)?>" 
									size="5"
									onkeyup="smartFilter.keyup(this)"
								/>
							
							<div style="clear: both;"></div>
						</div>
						<div class="bx_ui_slider_track" id="drag_track_<?=$key?>">
							<div class="bx_ui_slider_range" style="left: 0; right: 0%;"  id="drag_tracker_<?=$key?>"></div>
							<a class="bx_ui_slider_handle left"  href="javascript:void(0)" style="left:0;" id="left_slider_<?=$key?>"></a>
							<a class="bx_ui_slider_handle right" href="javascript:void(0)" style="right:0%;" id="right_slider_<?=$key?>"></a>
						</div>
					</div>
					
					<?
					$number_param = array();
					$number_param['MIN'] = explode('.',(string)$arItem["VALUES"]["MIN"]["VALUE"]);
					$number_param['MAX'] = explode('.', (string)$arItem["VALUES"]["MAX"]["VALUE"]);
					
					if(strlen($number_param['MIN'][1]) > 1)
					{
						$number_param['RoundTo'] = 0.1;
						$number_param['Precision'] = 2;
					}
					else
					{
						$number_param['RoundTo'] = 1;
						$number_param['Precision'] = 1;
					}
					
					
					?>
					
					<script type="text/javascript">
						var DoubleTrackBar<?=$key?> = new cDoubleTrackBar('drag_track_<?=$key?>', 'drag_tracker_<?=$key?>', 'left_slider_<?=$key?>', 'right_slider_<?=$key?>', {
							OnUpdate: function(){
								BX("<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>").value = this.MinPos;
								BX("<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>").value = this.MaxPos;
							},
							Min: parseFloat(<?=$arItem["VALUES"]["MIN"]["VALUE"]?>),
							Max: parseFloat(<?=$arItem["VALUES"]["MAX"]["VALUE"]?>),
							MinInputId : BX('<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>'),
							MaxInputId : BX('<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>'),
							FingerOffset: 10,
							MinSpace: 1,
							RoundTo: <?=$number_param['RoundTo']?>,
							Precision: <?=$number_param['Precision']?>
						});
					</script>
				<?elseif(!empty($arItem["VALUES"]) && !isset($arItem["PRICE"])):?>
                    <?if($arItem["NAME"]=='Цвет' || $arItem["NAME"]=='Группа цвета'):?>
                    <div class="bx_filter_container">
                        <span class="bx_filter_container_modef"></span>
                        <span class="bx_filter_container_title"><?=$arItem["NAME"]?><i class="fa fa-angle-down"></i></span>
                        <div class="bx_filter_block bx_filter_hide" style="display: none">
                            <div class="bx-filter-param-btn-inline">
                                <?foreach ($arItem["VALUES"] as $val => $ar):?>
                                    <input
                                            style="display: none"
                                            type="checkbox"
                                            name="<?=$ar["CONTROL_NAME"]?>"
                                            id="<?=$ar["CONTROL_ID"]?>"
                                            value="<?=$ar["HTML_VALUE"]?>"
                                        <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                    />
                                    <?
                                    $class = "";
                                    if ($ar["CHECKED"])
                                        $class.= " bx-active";
                                    if ($ar["DISABLED"])
                                        $class.= " disabled";
                                    ?>
                                    <label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'bx-active');">
												<span class="bx-filter-param-btn bx-color-sl">
													<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
                                                        <span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
                                                    <?endif?>
												</span>
                                    </label>
                                <?endforeach?>
                            </div>
                            <div class="clear_both"></div>
                        </div>
                    </div>
                    <?else:?>
                    <div class="bx_filter_container">
                        <span class="bx_filter_container_modef"></span>
                        <span class="bx_filter_container_title"><?=$arItem["NAME"]?><i class="fa fa-angle-down"></i></span>
                        <div class="bx_filter_block bx_filter_hide<?/*if($arItem["NAME"]=='Производитель'):*/?> oneSelectBrand<?/*endif?><?if($arItem["NAME"]=='Тип помещения'):?> oneSelectType<?endif;*/?>" style="display:none;">
                            <?foreach($arItem["VALUES"] as $val => $ar):?>
                                <span class="<?echo $ar["DISABLED"] ? 'disabled': ''?>">
							<input
                                    type="checkbox"
                                    class="checkbox"
                                    value="<?echo $ar["HTML_VALUE"]?>"
                                    name="<?echo $ar["CONTROL_NAME"]?>"
                                    id="<?echo $ar["CONTROL_ID"]?>"
								<?echo $ar["CHECKED"]? 'checked="checked"': ''?>
								onclick="smartFilter.click(this)"
                                    <?if ($ar["DISABLED"]):?>disabled<?endif?>
							/>
							<label class="mdi mdi-check" for="<?echo $ar["CONTROL_ID"]?>"><?echo $ar["VALUE"];?></label>
						</span>
                            <?endforeach;?>
                            <div class="clear_both"></div>
                        </div>
                    </div>
                    <?endif;?>
				<?endif;?>
			<?endforeach;?>
			
			<?foreach($arResult["HIDDEN"] as $arItem):?>
				<input
					type="hidden"
					name="<?echo $arItem["CONTROL_NAME"]?>"
					id="<?echo $arItem["CONTROL_ID"]?>"
					value="<?echo $arItem["HTML_VALUE"]?>"
				/>
			<?endforeach;?>
			<div style="clear: both;"></div>
			<div class="bx_filter_control_section">
                <span class="icon"></span>
                <?/*input class="btn btn_blue bx_filter_search_button" type="submit" id="set_filter" name="set_filter" value="<?=GetMessage("CT_BCSF_SET_FILTER")?>" /*/?>
                <div id="modef2">
                    <a class="btn btn_blue bx_filter_search_button" style="padding: 6px 0;" type="submit" href="<?echo $arResult["FILTER_URL"]?>" id="set_filter" name="set_filter">Показать</a>
                    <input class="bx_filter_search_button link" type="submit" id="del_filter" name="del_filter" value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>" />
                </div>

				<div class="bx_filter_popup_result right" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;">
					<?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
					<span class="arrow"></span>
					<a href="<?echo $arResult["FILTER_URL"]?>"><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
                    <div class="triangle"></div>
				</div>
			</div>
		</form>
		<div style="clear: both;"></div>
	</div>
</div>
</div>
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>');
</script>