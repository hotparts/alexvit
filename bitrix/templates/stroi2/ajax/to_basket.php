<? if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') die();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!CModule::IncludeModule("iblock")) {
	$this->AbortResultCache();
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
}
if (!CModule::IncludeModule("catalog")) {
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
}

if (CModule::IncludeModule("sale"))
{
	$arFilter = array("IBLOCK_ID" => 3, "ID"=>$_REQUEST['ID']);
	$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false, Array("ID", "IBLOCK_ID","DETAIL_PAGE_URL"));
	while($ar_fields = $res->GetNext()) {
		$Product['DETAIL_PAGE_URL'] = $ar_fields['DETAIL_PAGE_URL'];
	}
	//добавляю товар в корзину со свойствами
	if (CModule::IncludeModule("sale"))
	{
		$arFields = array(
//			"PRODUCT_ID" => $_REQUEST['ID'],
			"PRODUCT_ID" => rand(99999,9999999999999999),
			"PRICE" => trim($_REQUEST["PRICE"]),
			"CURRENCY" => "RUB",
			"QUANTITY" => $_REQUEST['QUANTITY'],
			"LID" => LANG,
			"DELAY" => "N",
			"CAN_BUY" => "Y",
			"NAME" => $_REQUEST["NAME"],
			"MODULE" => "my_module",
			"NOTES" => $_REQUEST['PICTURE'],
			"DETAIL_PAGE_URL" => $Product['DETAIL_PAGE_URL']
		);
//		$arProps[] = array(
//			"NAME" => "Картинка",
//			"CODE" => "PICTURE",
//			"VALUE" => $_REQUEST['PICTURE']
//		);
//
//		$arFields["PROPS"] = $arProps;

		$result = CSaleBasket::Add($arFields);
	}
	/*$arProps = array(
		"NAME" => "Артикул",
		"CODE" => "ARTICLE",
		"VALUE" => $_REQUEST["ARTICLE"]
	);
	$arFields["PROPS"] = $arProps;*/
}

//загоняю в буфер содержимое корзины
ob_start(); ?>

<? $APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line",
	"header_line",
	array(
		"HIDE_ON_BASKET_PAGES" => "N",
		"PATH_TO_BASKET" => SITE_DIR . "personal/basket/",
		"PATH_TO_ORDER" => SITE_DIR . "personal/order/",
		"PATH_TO_PERSONAL" => SITE_DIR . "personal/",
		"PATH_TO_PROFILE" => SITE_DIR . "personal/",
		"PATH_TO_REGISTER" => SITE_DIR . "login/",
		"POSITION_FIXED" => "N",
		"SHOW_AUTHOR" => "N",
		"SHOW_DELAY" => "N",
		"SHOW_EMPTY_VALUES" => "Y",
		"SHOW_IMAGE" => "Y",
		"SHOW_NOTAVAIL" => "N",
		"SHOW_NUM_PRODUCTS" => "Y",
		"SHOW_PERSONAL_LINK" => "N",
		"SHOW_PRICE" => "Y",
		"SHOW_PRODUCTS" => "Y",
		"SHOW_SUBSCRIBE" => "N",
		"SHOW_SUMMARY" => "N",
		"SHOW_TOTAL_PRICE" => "Y",
		"COMPONENT_TEMPLATE" => "header_line"
	),
	false
); ?>

<? $arJSON["BASKET_HTML"] = ob_get_contents(); ?>
<? ob_end_clean();

echo json_encode($arJSON);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php"); ?>