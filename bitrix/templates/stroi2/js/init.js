$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
  	items:1,
  	nav: false,
  	dots: true,
  	loop:true,
  	autoplay: true,
    autoplayTimeout:7000
  });

    $(".owl-carousel_1").owlCarousel({
  	items:4,
  	nav: false,
  	dots: false,
        margin: 30,
        responsiveClass:true,
        responsive:{
            0:{
                items:3,
            },
            992:{
                items:4,
                nav:false
            }
        }
  });

    $(".owl-carousel_similar").owlCarousel({
  	items:4,
  	nav: true,
  	dots: false,
  	loop:false,
  	autoplay:true,
    navText: [],
    navRewind: false,
        responsive:{
            0:{
                items:1
            },
            500:{
                items:2
            },
            740:{
                items:3
            },
            992:{
                items:4
            }
        }
  });

    $("#owl-carousel_recomend").owlCarousel({
  	items:4,
  	nav: true,
  	dots: false,
  	loop:false,
  	autoplay:true,
    margin: 30,
    navText: [],
        responsive:{
            0:{
                items:1
            },
            500:{
                items:2
            },
            740:{
                items:3
            },
            992:{
                items:4
            }
        }
  });

      $(".owl-carousel-compare").owlCarousel({
            items:4,
            nav: true,
            dots: false,
            loop:false,
            autoplay:false,
                margin: 0,
                navText: [],
                navRewind: false
        });

        $(".owl-carousel-col2").owlCarousel({
            items:2,
            nav: true,
            dots: false,
            loop:false,
            autoplay:false,
            margin: 0,
            navText: [],
            navRewind: false,
            responsive:{
                0:{
                    items:1
                },
                740:{
                    items:2
                },
                 
            }
        });

        $(".owl-carousel-col4").owlCarousel({
            items:4,
            nav: true,
            dots: false,
            loop:false,
            autoplay:false,
            margin: 30,
            navText: [],
            navRewind: false,
            responsive:{
                0:{
                    items:1
                },
                500:{
                    items:2
                },
                740:{
                    items:3
                },
                992:{
                    items:4
                }
            }
        });

        $('.sert .btn-prev').on('click', function(){
            $(".owl-carousel-col4").trigger('prev.owl.carousel');
        })
        $('.sert .btn-next').on('click', function(){
            $(".owl-carousel-col4").trigger('next.owl.carousel');
        })


        $('.thanks .btn-prev').on('click', function(){
            $(".owl-carousel-col2").trigger('prev.owl.carousel');
        })
        $('.thanks .btn-next').on('click', function(){
            $(".owl-carousel-col2").trigger('next.owl.carousel');
        })

    $(".owl-carousel_similar .owl-controls .owl-nav .owl-prev").addClass("mdi mdi-chevron-left");
    $(".owl-carousel_similar .owl-controls .owl-nav .owl-next").addClass("mdi mdi-chevron-right");

    $('#btn_forgot').click(function() {
    $('#forgotpass').slideToggle('500');
  });

  $('select').selectBox({
      mobile: true
  });

    $('.filter-btn').click(function() {
        $("html,body").animate({scrollTop: 270}, 1000);
        $('.bx_sidebar').slideToggle('500');
    });

  $(".selectBox-arrow").addClass("mdi mdi-menu-down");

   
   $(".btn_basket").hover(
        function() {
            $(".bx-basket-item-list").stop().fadeIn("300").find('.bx-basket-item-list-container').jScrollPane();
        }, function() {
            $(".bx-basket-item-list").stop().fadeOut("fast");
         }
    );

    

    $(".btn_compare").hover(
        function() {
            $(".bx_catalog_compare_form").stop().fadeIn("300").jScrollPane();
        }, function() {
            $(".bx_catalog_compare_form").stop().fadeOut("fast");
        }
    );



    $('.catalog-compare-result .head .mSlider-window li .close').hover(
        function() {
            $(this).closest('.mdi-close').addClass('active');
        }, function() {
            $(this).closest('.mdi-close').removeClass('active');
            $(this).closest('.mdi-close').removeClass('active_a');
        }
  );

    $('.catalog-compare-result .head .mSlider-window li .close').mousedown(function(eventObject){
        $(this).closest('.mdi-close').addClass('active_a');
    });


    $('.additional_info .item h3').click(function() {
    $(this).closest('.item').find('.item_content').slideToggle('400');
    $(this).find('.mdi').toggleClass('open');
  });

  

    $(".left_panel .content .catalog-menu .lvl-2").append("<div class='back'><i class='mdi mdi-chevron-left'>Назад</i></div>");

    $('.left_panel .content .catalog-menu i').click(function() {
        $(this).closest('li').find('.lvl-2').toggleClass('open');
    });

    $('.mobile_menu, .left_panel .head .mdi-close').click(function() {
        $('.left_panel').toggleClass("open");
        $('.overflow').toggleClass("open");
        $('.left_panel .lvl-2').removeClass("open");
    });


   $("#enter__forgotpsw-form").on("submit", function() {

        var $this = $(this);

        $.ajax({
            type: 'post',
            url: '/ajax/forgotpsw.php',
            data: $this.serialize(),
            dataType: 'json',
            success: function(data) {
                if(data.TYPE === "ERROR") {
                    $('.enter__forgotpsw-error', $this).html('<p><font class="errortext">'+data.MESSAGE+'</font></p>');
                }
                else if(data.TYPE === "OK") {
                    $this.html('<p><font class="notetext">'+data.MESSAGE+'</font></p>');
                }
            }
        }, "json");
        return false;
    });

    var lineElem = document.getElementById('line');

    var lineSourceBottom = lineElem.getBoundingClientRect().top + window.pageYOffset;

    window.onscroll = function() {
        if (lineElem.classList.contains('fixed') && window.pageYOffset < lineSourceBottom) {
            lineElem.classList.remove('fixed');
        }
        else if (window.pageYOffset > lineSourceBottom) {
            lineElem.classList.add('fixed');
        };

    }

    $(".mobile_menu").on("click", function() {
        $('body').bind('mousewheel touchmove', function (e) { e.preventDefault(); });
    });

    $(".left_panel .head .mdi-close").on("click", function() {
        $('body').unbind('mousewheel touchmove');
    });

    
    $('#emarket-compare-list').hover(
        function() {
            $('body').bind('mousewheel touchmove', function (e) { e.preventDefault(); });
    }, function() {
            $('body').unbind('mousewheel touchmove');
    }
    );
});