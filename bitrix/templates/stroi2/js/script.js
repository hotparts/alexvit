$(document).ready(function () {
    $('.bx_filter_container_title').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
            $(this).next('.bx_filter_hide').stop().slideUp();
        } else {
            // $('.bx_filter_container_title').removeClass('active');
            // $('.bx_filter_container_title').find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
            $(this).find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
            // $('.bx_filter_container_title').next('.bx_filter_hide').stop().slideUp();
            $(this).next('.bx_filter_hide').stop().slideDown();
            $(this).addClass('active');
        }
    })
    $('.scroll-top').on('click', function () {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    });

    //запретить выбрать несколько
    $('.oneSelectBlock span label').on('click', function () {
        if($(this).parent().find('input').prop("checked")){
        }else{
            $(this).parent().each(function () {
                if($(this).find('input').prop("checked")){
                    $(this).find('input').prop("checked",false);
                }
            })
            $(this).find('input').prop( "checked" , true);
        }
    })


    $('.oneSelectBrand span label').on('click', function () {
        $('.oneSelectBrand span input').prop("checked", false);
        $('.oneSelectBrand span input').attr('checked', false);
    })
    $('.oneSelectType span label').on('click', function () {
        $('.oneSelectType span input').prop("checked", false);
        $('.oneSelectType span input').attr('checked', false);
    })


    $('.bx_filter_container').each(function(){
        if($(this).find('input').is(':checked')){
            $(this).find('.bx_filter_container_title').addClass('active');
            $(this).find('.bx_filter_container_title i').removeClass().addClass('fa fa-angle-up');
            $(this).find('.bx_filter_block').show();
        }else{
            console.log('eeee')
        }
    })

    $('.fancybox').fancybox({});
    if ($('.logo-slider').length) {
        $('.logo-slider').slick({
            fade: false,
            arrows: true,
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 5,
            touchThreshold: 100,
            autoplay: false,
            autoplaySpeed: 5000,
            draggable: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    }
    $('.fancybox_img').fancybox({});
    /*проекты*/
    $('.project_item .project-slider').on('init', function (event, slick, currentSlide, nextSlide) {
        initPhotoSwipeFromDOM('.project-slider .slick-track', true);
    });
    $('.projects-list a').click(function () {
        var curID = $(this).data('id');
        location.hash = curID;
        $('.projects-list a').removeClass('active');
        $(this).addClass('active');
        $('.project_item').removeClass('active');
        $('.project_item[data-id="' + curID + '"]').addClass('active');

        if (!$('.project_item[data-id="' + curID + '"] .project-slider').hasClass('slick-initialized')) {
            $('.project_item[data-id="' + curID + '"] .project-slider').slick({
                arrows: true,
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 4,
                autoplay: false,
                touchThreshold: 100,
                autoplaySpeed: 5000,
                adaptiveHeight: true,
                nextArrow: '<button type="button" class="slick-next slider__next"><i class="icon-next-big"></i></button>',
                prevArrow: '<button type="button" class="slick-prev slider__prev"><i class="icon-prev-big"></i></button>',
                draggable: false
            });

        }
    });
    $('.projects .main-photo img').click(function () {
        $(this).parents('.project_item').find('.project-slider figure').first().click();
    });
    if (location.pathname == "/proekty/") {
        if (location.hash != "") {
            var curProjectID = location.hash.substr(1);
        } else {
            var curProjectID = $('.projects-list a.active').data('id');
        }
        $('.projects-list a[data-id="' + curProjectID + '"]').click();
    }

    /*проекты --- конец*/

    if ($('.project-slider-main').length) {
        $(document).on('click', '.project-slider-main .slick-dots .arr_left', function () {
            $('.project-slider-main .slider__prev').click();
        });
        $(document).on('click', '.project-slider-main .slick-dots .arr_right', function () {
            $('.project-slider-main .slider__next').click();
        });
        $('.project-slider-main').on('init', function (event, slick, currentSlide, nextSlide) {
            $('.project-slider-main .slick-dots li').wrapAll('<span>');
            $('.project-slider-main .slick-dots').prepend('<span class="arrow arr_left"></span>');
            $('.project-slider-main .slick-dots').append('<span class="arrow arr_right"></span>');
        });
        $('.project-slider-main').slick({
            arrows: true,
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            autoplay: false,
            autoplaySpeed: 5000,
            touchThreshold: 100,
            adaptiveHeight: true,
            nextArrow: '<button type="button" class="slick-next slider__next"><i class="icon-next-big"></i></button>',
            prevArrow: '<button type="button" class="slick-prev slider__prev"><i class="icon-prev-big"></i></button>',
            draggable: false,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
    $('.inline-list a').on('click', function () {
        $('.inline-list a').removeClass('active');
        $(this).addClass('active');
        var filter = $(this).data('filter');
        if (filter == 'all') {
            $('#product-tiles .item').show();
        } else {
            $('#product-tiles .item').hide();
            $('#product-tiles .filter_' + filter).show();
        }
    })
    $('#product-tiles .item a').on('click', function () {
        $('#product-tiles .item').removeClass('active');
        $(this).parent().addClass('active');
        $('.item-info').hide();
        $('#product-tiles .button-plus').removeClass('active');

        var url = $(this).data('url');
        var article = $(this).data('article');
        var name = $(this).data('name');
        var xmlid = $(this).data('xmlid');

        $('.header_catalog-detail h1').html('<span>' + article + '</span> ' + name);

        $('.slider_collection').hide();
        $('.slider_' + xmlid).show();
        // $('.wrp_big_img .bx_bigimages:first').attr('href',url);
        // $('.wrp_big_img .bx_bigimages:first img').attr('src',url);
        $('html').animate({
            scrollTop: $('.bx_item_slider').offset().top
        }, 1000);
    })
    $('.resize_color').on('click', function () {
        var url = $(this).data('url');
        $('.wrp_big_img .bx_bigimages:first').attr('href', url);
        $('.wrp_big_img .bx_bigimages:first img').attr('src', url);
        $('html').animate({
            scrollTop: $('.bx_item_slider').offset().top
        }, 1000);
    })
    $('#product-tiles .button-plus').on('click', function () {
        $('#product-tiles .button-plus').removeClass('active');
        $(this).addClass('active');
        if ($(this).parents().eq(1).hasClass('active')) {
            $('.item-info').hide();
            $(this).parents().eq(1).removeClass('active');
        } else {
            $('#product-tiles .item').removeClass('active');
            $(this).parents().eq(1).addClass('active');
            var id = $(this).data('id');
            $('.item-info').hide();
            $('.item-info_' + id).show();
        }
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 30,
        slidesPerView: 3,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            1280: {
                spaceBetween: 10,
            }
          }
    });
    var galleryTop = new Swiper('.gallery-top', {
        loop: true,
        spaceBetween: 15,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs
        },
        
    });


    //close cookie

    $('#cookies_modal_btn').on('click', function (event) {
        $('#cookies_modal').hide()
    })

    //
})