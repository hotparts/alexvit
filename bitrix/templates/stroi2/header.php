<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

IncludeTemplateLangFile(__FILE__);
CJSCore::RegisterExt('lang_js', array(
    'lang' => '/bitrix/templates/emarket2/lang/' . LANGUAGE_ID . '/js/script.php'
));
CJSCore::Init(array('lang_js'));
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="cmsmagazine" content="df80fa5f980c721d845aad3420f1d0dd"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/favicon/manifest.json">
    <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/favicon/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="Alexvit">
    <meta name="application-name" content="Alexvit">
    <meta name="msapplication-config" content="/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <meta name="yandex-verification" content="0795b8c416e1aa9b" />
    <meta name="google-site-verification" content="5PWskfPm6sS0ZY_ASsOMC91jjPlnEtk5obIFMWD3Dv4" />
    <!--[if lt IE 9]>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/html5shiv.js"></script><![endif]-->
    <?
    //include jquery-1.11.0 (http://jquery.com/)
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery-1.11.0.min.js");

    //include bootstrap script
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/bootstrap/js/bootstrap.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.form.js");

    //include owl_carousel script
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/owl_carousel/owl.carousel.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/owl_carousel/owl.carousel.min.js");

    //include selectBox script
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/selectbox/jquery.selectBox.js");

    //include FANCYBOX js
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.fancybox.min.js");

    //include my js
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/init.js");
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/swiper.min.css");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/swiper.min.js');
    //include bootstrap css
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/js/bootstrap/css/bootstrap.min.css");

    //include material_icon css
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/materialdesignicons.min.css");

    //include owl-carousel css
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/owl.carousel.css");
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/js/slick_slider/slick-theme.css");
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/js/slick_slider/slick.css");

    //include owl-carousel css
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/js/selectbox/jquery.selectBox.css");

    //include fancybox css
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/js/jquery.fancybox.min.css");


    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.maskedinput.min.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/is.mobile.js");
    //include masonry (http://masonry.desandro.com/)
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/masonry.pkgd.min.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/slick_slider/slick.min.js");
    //include jscrollpane (https://github.com/vitch/jScrollPane)
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jscrollpane/jquery.mousewheel.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jscrollpane/jquery.jscrollpane.min.js');
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/js/jscrollpane/jquery.jscrollpane.css');
    //include main script
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/script.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.elevatezoom.js");
    $APPLICATION->ShowHead();
    ?>
    <? $GLOBALS["PAGE"] = explode("/", $APPLICATION->GetCurPage()); ?>
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <script>
        var EmarketSite = {SITE_DIR: '<?=SITE_DIR?>'};
    </script>

</head>
<?global $USER;?>
<body>
<div id="panel">
    <? $APPLICATION->ShowPanel(); ?>
</div>
<header>
    <div id="top-panel">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <? $APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top-menu", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"COMPONENT_TEMPLATE" => "top-menu"
	),
	false
); ?>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:system.auth.form",
                        "emarket_auth",
                        array(
                            "REGISTER_URL" => SITE_DIR . "auth/",
                            "FORGOT_PASSWORD_URL" => "",
                            "PROFILE_URL" => SITE_DIR . "personal/",
                            "SHOW_ERRORS" => "N"
                        ),
                        false
                    ); ?>
                    <div class="clear_both"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="header row" itemscope itemtype="http://schema.org/LocalBusiness">
            <div class="col-xs-4 col-md-3">
                <a class="<? if ($cur_page != SITE_DIR . "index.php") echo 'site-logo'; ?>" href="<?= SITE_DIR ?>"
                   itemprop="name">
                    <img src="/upload/images/logo_alexvit.png" alt="Alexvit">
                    <? //$APPLICATION->IncludeComponent("bitrix:main.include","", Array("AREA_FILE_SHOW" => "file","PATH" => SITE_DIR."include/company_name.inc.php","EDIT_TEMPLATE" => ""));?>
                </a>
            </div>
            <div class="hidden-xs hidden-sm col-md-3">

            </div>
            <div class="hidden-xs hidden-sm col-md-2">
                <p class="workInfo">
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/title.php"), false); ?>
                </p>
            </div>
            <div class="hidden-xs hidden-sm col-md-2">
                <div class="deliveryRussia">
                    Доставка по всей России
                </div>
            </div>
            <div class="col-xs-8 col-md-2 header_phone">
                <a data-fancybox="" data-type="ajax" class="btn btn_blue feedbackBtn" href="/modal-forms/feedback/">Позвонить</a>
                <?// $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/telephone_inc.php"), false); ?>
            </div>
        </div>
    </div>
    <div class="container-fluid panels" id="line">
        <div class="row">
            <div class="col-xs-12">
                <div class="header_panel">
                    <div class="row">
                        <div class="col-sm-6 col-md-8 col-lg-8 col-xs-3">
                            <div class="mobile_menu">
                                <i class="mdi mdi-menu"></i>
                            </div>
                            <div class="btn_catalog">
                                <a class="btn btn_blue" href="javascript:void(0)"><i class="mdi mdi-menu"></i><span
                                            class="txt"><?= GetMessage("H_CATALOG") ?></span></a></span>
                                <? if (!CSite::InDir(SITE_DIR . 'index.php')): ?>
                                    <? $APPLICATION->IncludeComponent("bitrix:menu", "catalog-menu", array(
                                        "ROOT_MENU_TYPE" => "catalog",
                                        "MENU_CACHE_TYPE" => "A",
                                        "MENU_CACHE_TIME" => "3600",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "MENU_CACHE_GET_VARS" => array(),
                                        "MAX_LEVEL" => "1",
                                        "CHILD_MENU_TYPE" => "catalog",
                                        "USE_EXT" => "Y",
                                        "DELAY" => "N",
                                        "ALLOW_MULTI_SELECT" => "N"
                                    ),
                                        false
                                    ); ?>
                                <? endif; ?>
                            </div>

                            <? $APPLICATION->IncludeComponent(
	"bitrix:search.title", 
	".default", 
	array(
		"NUM_CATEGORIES" => "1",
		"TOP_COUNT" => "5",
		"ORDER" => "rank",
		"USE_LANGUAGE_GUESS" => "Y",
		"CHECK_DATES" => "N",
		"SHOW_OTHERS" => "N",
		"PAGE" => "#SITE_DIR#catalog/index.php",
		"CATEGORY_0_TITLE" => "'".GetMessage("H_CATALOGSEARCH")."'",
		"SHOW_INPUT" => "Y",
		"INPUT_ID" => "search_input",
		"CONTAINER_ID" => "search",
		"COMPONENT_TEMPLATE" => ".default",
		"CATEGORY_0" => array(
		)
	),
	false
); ?>
                        </div>
                        <div class="!btns_header col-sm-6 col-md-4 col-lg-4 col-xs-9">
                            <?/*div id="emarket-compare-list" class="btn_compare">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/compare.php"), false); ?>
                            </div*/?>
                            <div class="btn_compare">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/telephone_inc.php"), false); ?>
                            </div>
                            <div class="btn_basket ajax_basket">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:sale.basket.basket.line",
                                    "header_line",
                                    array(
                                        "HIDE_ON_BASKET_PAGES" => "N",
                                        "PATH_TO_BASKET" => SITE_DIR . "personal/basket/",
                                        "PATH_TO_ORDER" => SITE_DIR . "personal/order/",
                                        "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                                        "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                                        "PATH_TO_REGISTER" => SITE_DIR . "login/",
                                        "POSITION_FIXED" => "N",
                                        "SHOW_AUTHOR" => "N",
                                        "SHOW_DELAY" => "N",
                                        "SHOW_EMPTY_VALUES" => "Y",
                                        "SHOW_IMAGE" => "Y",
                                        "SHOW_NOTAVAIL" => "N",
                                        "SHOW_NUM_PRODUCTS" => "Y",
                                        "SHOW_PERSONAL_LINK" => "N",
                                        "SHOW_PRICE" => "Y",
                                        "SHOW_PRODUCTS" => "Y",
                                        "SHOW_SUBSCRIBE" => "N",
                                        "SHOW_SUMMARY" => "N",
                                        "SHOW_TOTAL_PRICE" => "Y",
                                        "COMPONENT_TEMPLATE" => "header_line"
                                    ),
                                    false
                                ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="clear_both">
                    </div>
                </div>
            </div>
        </div>
    </div>
</header><!-- .header-->
<div class="container-fluid slaider">
    <? if (CSite::InDir(SITE_DIR . 'index.php')): ?>
        <div class="index_cat_menu">
            <? $APPLICATION->IncludeComponent("bitrix:menu", "catalog-menu-main", array(
                "ROOT_MENU_TYPE" => "catalog",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => array(),
                "MAX_LEVEL" => "1",
                "CHILD_MENU_TYPE" => "catalog",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N"
            ),
                false
            ); ?>
        </div>
        <div class="slaider_block">
            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/slider.php"), false); ?>
        </div>
    <? else: ?>
</div>
<main>
    <div class="container-fluid">
        <? $APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "e-market",
            Array(
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0"
            )
        ); ?>

        <? if (CSite::InDir(SITE_DIR . 'news/') or CSite::InDir(SITE_DIR . 'contacts/') or CSite::InDir(SITE_DIR . 'catalog/') or CSite::InDir(SITE_DIR . 'personal/') or CSite::InDir(SITE_DIR . 'auth/') or CSite::InDir(SITE_DIR . 'search/')) : ?>

        <? else: ?>
        <div class="static_page">
            <? endif; ?>
            <? if (CSite::InDir(SITE_DIR . 'catalog/')): ?>
        </div>
    <? endif; ?>
        <? endif; ?>



