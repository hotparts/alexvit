<?$APPLICATION->IncludeComponent(
										"bitrix:catalog.compare.list", 
										".default", 
										array(
											"ACTION_VARIABLE" => "action",
											"AJAX_MODE" => "N",
											"AJAX_OPTION_ADDITIONAL" => "",
											"AJAX_OPTION_HISTORY" => "N",
											"AJAX_OPTION_JUMP" => "N",
											"AJAX_OPTION_STYLE" => "N",
											"COMPARE_URL" => SITE_DIR."catalog/compare.php",
											"DETAIL_URL" => "",
											"IBLOCK_ID" => "21",
											"IBLOCK_TYPE" => "catalog",
											"NAME" => "CATALOG_COMPARE_LIST",
											"POSITION" => "top left",
											"POSITION_FIXED" => "Y",
											"PRODUCT_ID_VARIABLE" => "id",
											"COMPONENT_TEMPLATE" => ".default"
										),
										false
									);?>