<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/json');
$data = array(
    "status" => 1,
    "data" => array()
);
if($_REQUEST['code'])
{
    $code = $_REQUEST['code']; 
    if (!CModule::IncludeModule("highloadblock"))
	return;

    $hl['NAME'] = "SiteKrayt";
    
    $dbHblock = HL\HighloadBlockTable::getList(
    	array(
    		"filter" => array("NAME" => $hl['NAME'])
    	))->Fetch();
        
         if ($dbHblock)
        {        
            $hldata = HL\HighloadBlockTable::getById($dbHblock["ID"])->fetch();
            $hlentity = HL\HighloadBlockTable::compileEntity($hldata);
    
    	    $entity_data_class = $hlentity->getDataClass();      
            
            $dataSite = $entity_data_class::getList(array(
                "select" => array("*"),
                "filter" => array("UF_CODE"=>$code)
            ))->fetch();
            
            if($dataSite)
            {
                $data['data']  = $dataSite;
            }else
            {
                 $data['status'] = 0;
                 $data['error'] = "No site  by code".$code;
            }
        }

}else
{
    $data['status'] = 0;
    $data['error'] = "No code market";
}


echo json_encode($data);