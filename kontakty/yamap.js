// https://api.yandex.ru/maps/doc/jsapi/2.x/update/concepts/update.xml
// <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
// <script type="text/javascript" src="/js/yamap.js"></script>

 ymaps.ready(function () {
	var coords = [55.754659, 37.756650];
	var companyName = 'Офис ООО «АлексВит»';
 	
	var myMap = new ymaps.Map("YMapsID_1", {
 		center: coords,
		zoom: 14,
 	});
	
	myMap.controls.add("mapTools")
 	.add("zoomControl")
	.add("typeSelector")
	//.add("miniMap")
 	.add("scaleLine");
 	
	var myPlacemark = new ymaps.Placemark(
 		coords, {
			iconContent: companyName,
			balloonContentHeader: companyName,
			balloonContentBody: '<br />Адрес: 111123 г. Москва, ул. Плеханова, дом 4А <br />+7 (499) 653-89-66 <br />info@alexvit.ru'
		}, {
 			draggable: false,
			preset: 'twirl#blueStretchyIcon',
 			hideIconOnBallon: false
 		}
	);
 	
 	myMap.geoObjects.add(myPlacemark);
	
	coords = [55.785129, 37.871302];
	companyName = 'Склад ООО «АлексВит»';
 	
	myMap = new ymaps.Map("YMapsID_2", {
 		center: coords,
		zoom: 14,
 	});
	
	myMap.controls.add("mapTools")
 	.add("zoomControl")
	.add("typeSelector")
	//.add("miniMap")
 	.add("scaleLine");
 	
	myPlacemark = new ymaps.Placemark(
 		coords, {
			iconContent: companyName,
			balloonContentHeader: companyName,
			balloonContentBody: '<br />Адрес: Московская область, г. Балашиха, <br />Западная промзона, шоссе Энтузиастов, дом 2 <br />+7 (499) 653-89-66 <br />info@alexvit.ru'
		}, {
 			draggable: false,
			preset: 'twirl#blueStretchyIcon',
 			hideIconOnBallon: false
 		}
	);
 	
 	myMap.geoObjects.add(myPlacemark);
 });