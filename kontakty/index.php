<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
$APPLICATION->SetPageProperty("title", "Контакты - ООО «АлексВит»");
$APPLICATION->SetPageProperty("description", "Адрес офиса компании ООО «АлексВит» в Москве. Контактные данные офиса и склада, время работы.");
$APPLICATION->SetPageProperty("keywords", "контакты, адрес офиса, адрес склада, схема проезда, телефон"); ?><input
        id="prbutton" onclick="window.print();" type="button" value="Печать">
<? /*
<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript" src="/kontakty/yamap.js"></script>
*/ ?>
    <div class="vcard">
        <div class="row">
            <div class="col-md-6">
                <h2>Адрес офиса <span class="fn org">ООО «Алексвит»</span></h2>
                <p>
                    <span class="adr" style="font-size: 14pt;"> <span class="postal-code">111123</span> <span
                                class="locality">г. Москва</span>, <span
                                class="street-address">ул. Плеханова, дом 4А</span></span><br>
                    <span style="font-size: 14pt;"> </span><br>
                    <span class="tel" style="font-size: 14pt;">8 (499) 653-89-66&nbsp;</span><br>
                    <span style="font-size: 14pt;">
				&nbsp;&nbsp;</span><br>
                    <span style="font-size: 14pt;"> </span>
                    <!--<a href="http://alexvit6262.ameton.ru/kontakty/?clear_cache=Y&back_url_admin=%2Fbitrix%2Fadmin%2Ffileman_medialib_admin.php%3Flang%3Dru"><span style="font-size: 14pt;">info@alexvit.ru</span></a>-->
                    <a class="email" href="mailto:info@alexvit.ru" style="font-size: 14pt;">info@alexvit.ru</a>
                </p>
            </div>
            <div class="col-md-6">
                <script type="text/javascript" charset="utf-8" async data-skip-moving="true"
                        src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A8e6e6d91736a0b110030ec9ee3b7dce203cf3889ea536f9d4a0b8cd4ef67c9a2&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <h2>Адрес склада</h2>
                <span style="font-size: 14pt;"> </span>
                <p>
 <span style="font-size: 14pt;">
				Московская область, г. Балашиха,Западная промзона, Объездное ш., д. 2</span><br>
                    <span style="font-size: 14pt;"> </span><br>
                    <span style="font-size: 14pt;">
				8 (499) 653-89-66 &nbsp;</span><br>
                    <span style="font-size: 14pt;">
				&nbsp;&nbsp;</span><br>
                    <span style="font-size: 14pt;"> </span>
                    <!--<a href="http://alexvit6262.ameton.ru/kontakty/?clear_cache=Y&back_url_admin=%2Fbitrix%2Fadmin%2Ffileman_medialib_admin.php%3Flang%3Dru"><span style="font-size: 14pt;">info@alexvit.ru</span></a>-->
                    <a href="mailto:info@alexvit.ru" style="font-size: 14pt;">info@alexvit.ru</a>
                </p>
            </div>
            <div class="col-md-6">
                <? /* <!-- <img width="524" src="/upload/medialibrary/84b/whatsapp-image-2016_11_01-at-13.03.43.jpeg" height="298"> -->
<!-- 			<div id="YMapsID_2" style="width:100%; height:300px;"></div> --> */ ?>
                <script type="text/javascript" charset="utf-8" async async data-skip-moving="true"
                        src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A5a0400343f6a55b58ae10ca66f058ab112ac6843e2b3d2b013e30d340a45cb40&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
        </div>
        <br>
        <?/*div class="row">
            <div class="col-md-6">
                <h2>Адрес офиса</h2>
                <span style="font-size: 14pt;"> </span>
                <p>
                    <span style="font-size: 14pt;">
				г. Санкт-Петербург, пр. Авиаконструкторов 35, корпус 4а ,офис 304</span><br>
                    <span style="font-size: 14pt;"> </span><br>
                    <span style="font-size: 14pt;">8 (499) 653-89-66</span>&nbsp;<br>
                    <span style="font-size: 14pt;"> </span><br>
                    <span style="font-size: 14pt;">
				Региональный представитель : Владислав Лежнин &nbsp;</span><br>
                    <span style="font-size: 14pt;">
				&nbsp;&nbsp;</span><br>
                    <span style="font-size: 14pt;"> </span>
                </p>
            </div>
            <div class="col-md-6">
                <script type="text/javascript" charset="utf-8" async data-skip-moving="true"
                        src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aaaa43fecd54f2d9bc76b2c1c8ed02d35e9df0ed8056b9f60ee206cfeba569c2b&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
        </div>
        <br*/?>
        <div class="row">
            <div class="col-md-6">
                <h2>Адрес офиса</h2>
                <span style="font-size: 14pt;"> </span>
                <p>
 <span style="font-size: 14pt;">
				г. Краснодар, ул Московская 59/1</span><br>
                    <span style="font-size: 14pt;"> </span><br>
                    <span style="font-size: 14pt;">
				+7&nbsp;(925)&nbsp;124-40-24 &nbsp;</span><br>
                    <span style="font-size: 14pt;"> </span><br>
                    <span style="font-size: 14pt;">
				Региональный представитель : Аванесян Михаил &nbsp;</span><br>
                    <span style="font-size: 14pt;">
				&nbsp;&nbsp;</span><br>
                    <span style="font-size: 14pt;"> </span>
                </p>
            </div>
            <div class="col-md-6">
                <? /* <!-- <img width="524" src="/upload/medialibrary/84b/whatsapp-image-2016_11_01-at-13.03.43.jpeg" height="298"> -->
<!-- 			<div id="YMapsID_2" style="width:100%; height:300px;"></div> --> */ ?>
                <script type="text/javascript" charset="utf-8" async data-skip-moving="true"
                        src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Adacc3d031e52cd0dd8efa59e9bad8fb01cd91b31908fe91fe7eb028103331855&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
        </div>
        <?/*div class="row">
            <div class="col-md-6">
                <h2>Адрес офиса</h2>
                <span style="font-size: 14pt;"> </span>
                <p>
 <span style="font-size: 14pt;">
				г. Новосибирск, ул Кропоткина д 271, офис 615</span><br>
                    <span style="font-size: 14pt;"> </span><br>
                    <span style="font-size: 14pt;">
				+7 923 176-19-07 &nbsp;</span><br>
                    <span style="font-size: 14pt;"> </span><br>
                    <span style="font-size: 14pt;">
				Региональный представитель : Овсянникова Елена Ивановна &nbsp;</span><br>
                    <span style="font-size: 14pt;">
				&nbsp;&nbsp;</span><br>
                    <span style="font-size: 14pt;"> </span>
                </p>
            </div>
            <div class="col-md-6">
                <script type="text/javascript" charset="utf-8" async data-skip-moving="true"
                        src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A3106210eed05ddf624ff62e66e33eaf9f56174ffcc74796a8be2ca4bbb46bf64&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
        </div*/?>
    </div>
    <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>